/* MEX FUNCTION 
 *
 *  Save results
 *
 */


#include <string.h>
#include <ctype.h>
#include "mex.h"
#include "listdouble.h"

// -------------------------------------------------------------------------

const char *strwhat2do[] = {"ADD", "SAVE", "FREE"};

typedef enum {
	WHAT2DO_ADD = 0,
	WHAT2DO_SAVE,
	WHAT2DO_FREE
};

// -------------------------------------------------------------------------
// convert a string to upper case letters
void str2upper(char *str)
{
  while (*str != '\0')
  {
	  *str = toupper(*str);
	  str++;
  }
}

// -------------------------------------------------------------------------
// find index of a sting in an array of stings
int findinputstr(const mxArray *mxstr, const char **names, int numnames)
{
	int i, buflen = mxGetNumberOfElements(mxstr) + 1;
	char *buf = (char *) mxCalloc(buflen, sizeof(char));

	if (mxGetString(mxstr, buf, buflen) != 0)
		mexErrMsgTxt("Could not convert string data.");

	str2upper(buf);
	
	for(i = 0; i < numnames; i++)
	{
		if (strcmp(buf, names[i]) == 0)
			break;
	}

	mxFree(buf);

	if(i < numnames)
		return i;
	
	return -1;
}

// -------------------------------------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	static ListDouble<float *> *buf = NULL;
	static int len = 0;
	int l, opt;
	int i, j, num;
	float *data, *ptr;

	if (nrhs < 1)
		mexErrMsgTxt("Not enough input arguments.");

	if (mxIsEmpty(prhs[0]) && !mxIsChar(prhs[0]))
		mexErrMsgTxt("First argument has to be a string.");

	// what to do
	opt = findinputstr(prhs[0], strwhat2do, 3);

	switch (opt)
	{
		case WHAT2DO_ADD:  // [out,num] = bkupdate('add', [1xn] single);

			if ((nrhs > 2) || (nlhs > 1))
				mexErrMsgTxt("Wrong number of I/O arguments");

			if (mxIsEmpty(prhs[1]) || !mxIsSingle(prhs[1]))
				mexErrMsgTxt("Wrong input.");

			// initialize buffer
			if (!buf)
			{
				buf = (ListDouble<float *> *) new ListDouble<float *>;
				len = mxGetNumberOfElements(prhs[1]);
			}
			
			// test number of input items
			if ((l = mxGetNumberOfElements(prhs[1])) != len)
				mexErrMsgTxt("Input has wrong size.");

			// ptr to source data
			data = (float *) mxGetData(prhs[1]);
			// ptr to destination data
			ptr = (float *) malloc(len * sizeof(float));
			// copy
			memcpy(ptr, data, len * sizeof(float));
			// add to the list
			buf->AddHead(ptr);		

			if (nlhs)
				plhs[0] = mxCreateDoubleScalar(buf->Num());

			break;

		case WHAT2DO_SAVE:

			if ((nrhs > 1) || (nlhs > 1))
				mexErrMsgTxt("Wrong number of I/O arguments");

			if (!buf)
				mexErrMsgTxt("Nothing to save");

			plhs[0] = mxCreateNumericMatrix(num = buf->Num(), len, mxSINGLE_CLASS, mxREAL);
			data = (float *) mxGetData(plhs[0]);

			for (j = 0; j < num; j++)
			{
				ptr = buf->DeleteTail();
				for (i = 0; i < len; i++)
					data[j+i*num] = ptr[i];
				free(ptr);
			}

			delete buf;
			buf = NULL;
			len = 0;

			break;

		case WHAT2DO_FREE: // resupdate('free');
			
			if ((nrhs != 1) || (nlhs != 0))
				mexErrMsgTxt("Wrong number of I/O arguments");

			if (buf)
			{
				while(!buf->IsEmpty())
				{
					ptr = buf->DeleteTail();
					free(ptr);
				}

				delete buf;
				buf = NULL;
				len = 0;
			}

			break;


		default:

			mexErrMsgTxt("Unknown option.");
	}



}