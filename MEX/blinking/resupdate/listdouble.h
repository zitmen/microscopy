#ifndef __LISTDOUBLE_H__
#define __LISTDOUBLE_H__

// ---------------------------------------------------------------------------
// double-linked list
// ---------------------------------------------------------------------------

template <typename T>
class ListDouble {

private:
	struct item {
		T    val;
		item *prev;
		item *next;
			
		item(item *prev, item *next, const T & val)
		: val(val), prev(prev), next(next)
		{}
	};
	
	item *first, *last;   // start/end of the list
	int num;              // number of items

public:

	// default constructor
	ListDouble()
	: num(0), first(NULL), last(NULL)
	{}

	// cleanup
	~ListDouble()
	{
		item *p;
		while( (p = first) != NULL )
		{
			first = p->next;
			//delete p->val; ???
			delete p;
		}
		num = 0;
	}

	// add item at the beginning
	ListDouble& AddHead(const T & val)
	{
		item *p = new item(NULL,first,val);
		if (first)
			first->prev = p;
		else
			last = p;
		first = p;
		num++;
		return *this;
	}

	// add item at the end
	ListDouble& AddTail(const T & val)
	{
		item *p = new item(last,NULL,val);
		if (last)
			last->next = p;
		else
			first = p;
		last = p;
		num++;
		return *this;
	}

	T DeleteHead()
	{
		if (!first)
			throw "LINKLIST: List is empty";
		item *p = first;
		first = p->next;
		if (first)
			first->prev = NULL;
		else
			last = NULL;
		T val = p->val;
		num--;
		delete p;
		return val;
	}

	T DeleteTail()
	{
		if (!last)
			throw "LINKLIST: List is empty";
		item *p = last;
		last = p->prev;
		if (last)
			last->next= NULL;
		else
			first = NULL;
		T val = p->val;
		num--;
		delete p;
		return val;
	}

	// get number of items
	int Num()
	const
	{
		return num;
	}

	// is list empty?
	bool IsEmpty()
	const
	{
		return (first == NULL);
	}


	// cancel copy constructor (Virius 1, p.140)
	ListDouble(const ListDouble&);
	ListDouble& operator=(const ListDouble&);
};

#endif
