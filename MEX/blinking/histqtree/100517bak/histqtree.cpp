/*
   MEX FUNCTION
 
   Generates a Quad-tree based histogram

   Syntax:
 
   histqtree(points, roi)
   histqtree(points, roi, maxevents)
   histqtree(points, roi, maxevents, renderdepth)

   [H, maxdepth, numpts] = histqtree(...)

   where

   points      ... [m x 2] single matrix of points
   roi         ... [x, y, w, h] is origin and width and height of the window
   maxevents   ... maximum number of events in leaf nodes (default 255)
   renderdepth ... rendering depth of the tree (default 5); set to 0 for "maxdepth"
   H           ... [p x q] single matrix representing quad-tree histogram
   maxdepth    ... maxim tree depth
   numpts      ... number of points in the histogram
   

*/

#include "mex.h"
#include "quadtree.h"

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	QuadTree *qt;
	float *ptx, *pty;
	int maxevents = QT_MAX_EVENTS_DEFAULT;
	int renderdepth = QT_MAX_RENDER_DEPTH_DEFAULT;
	double *roi, roisize;
	int i, numpts, imsize = 0;
	

	// check number of I/O arguments
	if ( (nrhs < 2) || (nrhs > 4) )
		mexErrMsgTxt("Wrong number of input arguments.");
	if ( (nlhs < 1) || (nlhs > 3) )
		mexErrMsgTxt("Wrong number of output arguments.");

	// check points
	if ( mxIsEmpty(prhs[0]) || !mxIsSingle(prhs[0]) )
		mexErrMsgTxt("Points must be stored in a single matrix.");
	if ( mxGetN(prhs[0]) != 2 )
		mexErrMsgTxt("Points must be 2D data.");

	// get ROI
	if ( mxIsEmpty(prhs[1]) || !mxIsDouble(prhs[1]) || (mxGetNumberOfElements(prhs[1]) != 4) )
		mexErrMsgTxt("ROI must be a vector [x, y, w, h].");

	roi = mxGetPr(prhs[1]);
	
	if ( (roi[2] <= 0) || (roi[3] <=0 ) )
		mexErrMsgTxt("Not a valid ROI.");

	roisize = Max(roi[2],roi[3]);

	// get maximum number of events
	if (nrhs > 2) {
		if ( mxIsEmpty(prhs[2]) || !mxIsNumeric(prhs[2]) || (mxGetNumberOfElements(prhs[2]) != 1) )
			mexErrMsgTxt("Maximum number of events in leaf nodes must be a scalar.");
		if ( (maxevents = (int) mxGetScalar(prhs[2])) < 1 )
			mexErrMsgTxt("Maximal number of events must be a positive number.");
	}

	// get rendering depth
	if (nrhs > 3) {
		if ( mxIsEmpty(prhs[3]) || !mxIsNumeric(prhs[3]) || (mxGetNumberOfElements(prhs[3]) != 1) )
			mexErrMsgTxt("Rendering depth must be a scalar.");
		if ( (renderdepth = (int) mxGetScalar(prhs[3])) < 0 )
			mexErrMsgTxt("Rendering depth must be a positive number.");
	}

	// get pointers to data
	numpts = mxGetM(prhs[0]);
	ptx = (float *) mxGetData(prhs[0]);
	pty = ptx + numpts;

	// create a quad-tree and fill it with events
	qt = new QuadTree(point((float) roi[0], (float) roi[1]), point( (float) (roi[0]+roisize), (float) (roi[1]+roisize)), maxevents);
	for (i = 0; i < numpts; i++)
		qt->Add(point(*(ptx++),*(pty++)));

	// corect depth to maximum three depth and compute the final image size
	qt->RenderSize(&renderdepth, &imsize);

	// render the final result
	plhs[0] = mxCreateNumericMatrix(imsize, imsize, mxSINGLE_CLASS, mxREAL);
	qt->Render(matrix((float *) mxGetData(plhs[0]), imsize, imsize, imsize), renderdepth);

	if (nlhs > 1)
		plhs[1] = mxCreateDoubleScalar(qt->Depth());

	if (nlhs > 2)
		plhs[2] = mxCreateDoubleScalar((double) qt->Num());

	delete qt;
}