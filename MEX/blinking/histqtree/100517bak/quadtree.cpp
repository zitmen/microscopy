#include "quadtree.h"

static const int A = 0;
static const int B = 2;
static const int C = 1;
static const int D = 3;

static const int ABCD[4][2] = {{0,0}, {1,1}, {0,1}, {1,0}};  // ABCD[i] {row,col}

// ---------------------------------------------------------------------------
QuadTreeNode::QuadTreeNode()
: children(NULL), parent(NULL), events(NULL), depth(0), num(0)//, linktolist(NULL)
{}

// ---------------------------------------------------------------------------
QuadTreeNode::QuadTreeNode(const point& a, const point& c, QuadTreeNode * const parent)
: children(NULL), parent(parent), num(0)//, linktolist(NULL)
{
	corners[A] = a;
	corners[C] = c;
	size = Abs(a - c);

	if (parent)
		depth = parent->depth + 1;
	else
		depth = 0;
	
	events = new listofevents;
}

// ---------------------------------------------------------------------------
QuadTreeNode::~QuadTreeNode()
{
	if (events)
		delete events;

	if (children)
	{
		for(int i = 0; i < 4; i++)
			delete children[i];
		free(children);
	}
}

// ---------------------------------------------------------------------------
inline bool QuadTreeNode::isIn(const point& pt)
const
{
	return ((pt.x >= corners[A].x) && (pt.x < corners[C].x) &&
			(pt.y >= corners[A].y) && (pt.y < corners[C].y));
}

// ---------------------------------------------------------------------------
qterror QuadTreeNode::FindLeafNode(const point &pt, QuadTreeNode** ptr)
{
	int i;
	
	*ptr = this;

	// point is out of magins of the given node
	if (!(*ptr)->isIn(pt))
		return qtOutOfMargins;

	// find leaf node for the given point
	while (!(*ptr)->isLeaf())
	{
		for (i = 0; i < 4; i++)
		{
			if ((*ptr)->children[i]->isIn(pt))
				break;
		}
		*ptr = (*ptr)->children[i];
	}

	return qtOK;
}

// ---------------------------------------------------------------------------
qterror QuadTreeNode::Add(const point& pt)
{
	QuadTreeNode *ptr = this;

	// find leaf if this node is not a leaf 
	if (!isLeaf())
	{	
		qterror err = FindLeafNode(pt, &ptr);
		if (err != qtOK)
			return err;
	}

	// add events
	ptr->events->AddHead(pt);

	// add number of events to all patent nodes;
	while (ptr)
	{
		ptr->num++;
		ptr = ptr->parent;
	}

	return qtOK;
}

// ---------------------------------------------------------------------------
//     a - e - b
//     | A | B |
//     f - g - h
//     | D | C |
//     d - i - c
qterror QuadTreeNode::Split()
{
	int i;
	point pt, a = corners[A], c = corners[C];
	point center = Avg(a, c);

	if (!isLeaf())
		return qtNotALeafNode;

	// create leaf nodes
	children = (QuadTreeNode **) malloc(4 * sizeof(QuadTreeNode *));
	children[A] = new QuadTreeNode(a, center, this);
	children[B] = new QuadTreeNode(point(center.x,corners[A].y), point(c.x,center.y), this);
	children[C] = new QuadTreeNode(center, c, this);
	children[D] = new QuadTreeNode(point(a.x,center.y), point(center.x,c.y), this);

	// move events from the current node to the new leaves
	while (!events->isEmpty())
	{
		pt = events->DeleteHead();
		
		for (i = 0; i < 4; i++)
		{
			if (children[i]->isIn(pt))
				break;
		}
		
		children[i]->events->AddHead(pt);
	}
	// and remove the event list from the current node
	delete events;
	events = NULL;
	
	return qtOK;
}

// ---------------------------------------------------------------------------
// default
QuadTree::QuadTree()
: maxevents(QT_MAX_EVENTS_DEFAULT) , root(NULL), depth(0) //, list(NULL)
{
}

// ---------------------------------------------------------------------------
// make a root
QuadTree::QuadTree(const point& a, const point& c, const int maxevents)
: maxevents(maxevents), depth(0)
{
	root = new QuadTreeNode(a, c, NULL);
//	list = new listofnodes;
//	root->linktolist = list->AddHead(root);
}

// ---------------------------------------------------------------------------
// destructor
QuadTree::~QuadTree()
{
//	if (list)
//		delete list;

	if (root)
		delete root; 
}

// ---------------------------------------------------------------------------
qterror QuadTree::Add(const point& pt)
{
	qterror err;
	QuadTreeNode *ptr = NULL;

	// quit if the point is out of margins
	if ((err = root->FindLeafNode(pt, &ptr)) != qtOK)
		return err;

	// split the current node if it is full
	if (ptr->events->Num() >= (size_t) maxevents)
	{
		// maximal depth of the three leaves
		depth = Max(depth, ptr->depth);

		// create children
		if ((err = ptr->Split()) != qtOK)
			return err;
/*
		// delete current node from the list of nodes
		list->Delete(ptr->linktolist);
		ptr->linktolist = listofnodes::Node();

		// add childrens to list of nodes
		for(int i = 0; i < 4; i++)
			ptr->children[i]->linktolist = list->AddHead(ptr->children[i]);
*/
	}

	// add point
	return ptr->Add(pt);
}

// ---------------------------------------------------------------------------

qterror QuadTree::RenderNode(const QuadTreeNode *node, matrix& subimage, const int rendertodepth = 0)
{
	static int depth = 0;

	// set maximum rendering depth
	if (rendertodepth)
		depth = rendertodepth;

	if ((node->depth < depth) && !node->isLeaf())
	{
		// recursion
		int row, col;
		int size = subimage.W() >> 1; // size = previous/2
		
		for(int i = 0; i < 4; i++)
		{
			row = size*ABCD[i][0];
			col = size*ABCD[i][1];
			RenderNode(node->children[i], matrix(subimage, row, col, size, size));
		}
	}
	else
		// fill leaves with intensity proportional to the number of events divided by the leaf area
		subimage.Set((float) node->num / (node->size.x * node->size.y));

	return qtOK;
}

// ---------------------------------------------------------------------------
// compute size of the rendered image based on the given rendering depth
qterror QuadTree::RenderSize(int *rendertodepth, int *imsize)
{
	if (*rendertodepth < 0)
		return qtRenderError;
	
	// rendering depth
	if (*rendertodepth == 0)
		// 0 is for maximum tree depth
		*rendertodepth = depth;
	else
		// cannot be bigger than the maximum depth of the tree
		*rendertodepth = Min(*rendertodepth, depth);

	// imsize = 2^rendertodepth
	*imsize = 1 << *rendertodepth;

	return qtOK;
}

// ---------------------------------------------------------------------------

qterror QuadTree::Render(matrix& image, int rendertodepth = 0)
{
	int imsize = 0;
	
	if (RenderSize(&rendertodepth, &imsize) != qtOK)
		return qtRenderError;

	if (image.Mem() == NULL)
		return qtRenderError;

	if (imsize > image.W())
		return qtRenderError;

	return RenderNode(root, image, rendertodepth);
}
