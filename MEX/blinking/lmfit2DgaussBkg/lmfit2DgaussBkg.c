/*
 Levenberg-Marquardt Least Squares Fitting of 2D Gauss 
 

 Usage: 

    [par, stdbkg, exitflag, numiter, message] = lmfit2Dgauss(par0, XY, VAL)

	  par,par0 .... [x,y,A,z,sigma]
	  XY       .... [npts x 2] double matrix
	  VAL      .... [npts x 1] double matrix
	  stdbkg   .... standard deviation of background: std(VALraw - VALfit) at XY coordinates
	  exitflag
 */

#include <math.h>
#include <float.h>
#include "mex.h"
#include "lmmin.h"

//#define SQR(x) ((x)*(x))

__inline double SQR(double x)	
{
	return x*x;
}

typedef struct {
    double *varvec;		// variables        x,y, ...
    double *funvec;		// function values  fun(x,y,...)	
    double (*f) (double **var, double *params);
} my_data_type;


// ------------------------------------------------------------------------
// function value at given variable
// ------------------------------------------------------------------------

// x,y,A,z,s
#define DIM 2			// 2 dimensions
#define NUMPAR 5        // # of parameters
double my_fit_function(double **var, double *par)
{
    double cntvar[DIM];

	cntvar[0] = *var[0] - par[0];
	cntvar[1] = *var[1] - par[1];

	return par[2] * exp(-0.5 * (SQR(cntvar[0]) + SQR(cntvar[1])) / SQR(par[4])) + par[3];
}

// ------------------------------------------------------------------------
// Compute residua as chi2 = sum((y-f(x,p))^2)
// ------------------------------------------------------------------------

//double sumchi2(double *params, int m_dat, void *data)
//{
//    my_data_type *mydata = (my_data_type *) data;
//	double	sum = 0, *pfun, *pvar[DIM];
//	int		i;
//
//	// initialize pointers to data and variables
//	pfun = mydata->funvec;
//	pvar[0] = mydata->varvec;
//	pvar[1] = mydata->varvec + m_dat;
//    
//	// go through all data
//    for (i = 0; i < m_dat; i++)
//	{
//		sum += SQR(*(pfun++) - mydata->f(pvar, params));
//		pvar[0]++; pvar[1]++;
//	}
//
//	return sum;
//}

double stdbkg(double *params, int m_dat, void *data)
{
    my_data_type *mydata = (my_data_type *) data;
	double	*pfun, *pvar[DIM], *diff;
	double mu = 0, sum = 0;
	int		i;

	diff = (double *) malloc(m_dat  * sizeof(double));

	// compute difference and mean
	pfun = mydata->funvec;
	pvar[0] = mydata->varvec;
	pvar[1] = mydata->varvec + m_dat;
    for (i = 0; i < m_dat; i++)
	{
		mu += diff[i] = *(pfun++) - mydata->f(pvar, params);
		pvar[0]++; pvar[1]++;
	}
	mu /= m_dat;

	// compute std
    for (i = 0; i < m_dat; i++)
	{
		sum += SQR(diff[i] - mu);
	}
	
	free(diff);
	return sqrt(sum / (m_dat - 1));
}


// ------------------------------------------------------------------------
// Compute residua for all data points
// ------------------------------------------------------------------------
void my_evaluate(double *params, int m_dat, double *fvec, void *data, int *info)
{
    my_data_type *mydata = (my_data_type *) data;
	double	*pfun, *pvar[DIM];//, maxval = maxvec(m_dat,mydata->funvec);
	int		i;

	// initialize pointers to data and variables
	pfun = mydata->funvec;
	pvar[0] = mydata->varvec;
	pvar[1] = mydata->varvec + m_dat;
    
	// go through all data
    for (i = 0; i < m_dat; i++)
	{
		*(fvec++) = *(pfun++) - mydata->f(pvar, params);
		pvar[0]++; pvar[1]++;
	}    

	if ((fabs(params[0]) > 4) || (fabs(params[1]) > 4) || (params[2] < 0) || (params[2] > 1) || (params[3] < 0) || (params[3] > 1) || (params[4] < 0))
		*info = -1;
	else
		*info = *info;		/* to prevent a 'unused variable' warning */
}

// ------------------------------------------------------------------------
// Control parameters
// ------------------------------------------------------------------------
#define LM_MACHEP     DBL_EPSILON   /* resolution of arithmetic */
#define LM_USERTOL    30*LM_MACHEP  /* users are recommened to require this */

void my_control_init( lm_control_type * control )
{
    control->maxcall = 1000;
    control->epsilon = LM_USERTOL;
    control->stepbound = 100.;
    control->ftol = LM_USERTOL;
    control->xtol = LM_USERTOL;
    control->gtol = LM_USERTOL;
}

// ------------------------------------------------------------------------
// Print convergence info
// ------------------------------------------------------------------------
void my_print(int n_par, double *par, int m_dat, double *fvec,
		      void *data, int iflag, int iter, int nfev)
{
   // double f, y, t;
    int i;
    my_data_type *mydata = (my_data_type *) data;

    if (iflag == 2) {
	mexPrintf("trying step in gradient direction\n");
    } else if (iflag == 1) {
	mexPrintf("determining gradient (iteration %d)\n", iter);
    } else if (iflag == 0) {
	mexPrintf("starting minimization\n");
    } else if (iflag == -1) {
	mexPrintf("terminated after %d evaluations\n", nfev);
    }

    mexPrintf("  par: ");
    for (i = 0; i < n_par; ++i)
	mexPrintf(" %12g", par[i]);
    mexPrintf(" => norm: %12g\n", lm_enorm(m_dat, fvec));
}

// ------------------------------------------------------------------------
// M A I N
// ------------------------------------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  // data and pameter arrays:

    lm_control_type		control;
    my_data_type		data;
    int					m_dat, n_p = NUMPAR;    // number of data points and fitting parameters
    double				*p;

	if (nrhs != 3)
		mexErrMsgTxt("Usage: [[x,y,A,b,s], stdbkg, exitflag, numiter, message] = lmfit2Dgauss([x0,y0,A0,b0,s0], XY, VAL)");

	if (!mxIsDouble(prhs[0]))
		mexErrMsgTxt("First input argument must be a double matrix.");

	// initialize I/O parameters
	plhs[0] = mxDuplicateArray(prhs[0]);
	p = (double *) mxGetPr(plhs[0]);

	// initialize input data
    data.f = my_fit_function;
    data.varvec = (double *) mxGetPr(prhs[1]);
    data.funvec = (double *) mxGetPr(prhs[2]);
	m_dat = mxGetNumberOfElements(prhs[2]);	
	
	// do the fitting
	my_control_init( &control );
	//lm_minimize( m_dat, n_p, p, my_evaluate, my_print, &data, &control );
	lm_minimize( m_dat, n_p, p, my_evaluate, NULL, &data, &control );

    //mexPrintf( "status: %s after %d evaluations\n",
    //        lm_shortmsg[control.info], control.nfev );

	// std of background:  std(VALraw - VALfit) at XY coordinates
	if (nlhs > 1)
		plhs[1] = mxCreateDoubleScalar(stdbkg(p, m_dat, &data));

	// exit flag
	if (nlhs > 2)
		plhs[2] = mxCreateDoubleScalar(control.info);

	// # iterations
	if (nlhs > 3)
		plhs[3] = mxCreateDoubleScalar(control.nfev);

	// message
	if (nlhs > 4)
		plhs[4] = mxCreateString(lm_infmsg[control.info]);
}
