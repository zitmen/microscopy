/*
    Filter [x,y] points so they are not closer than a minimum distance

    valid = circfilter([u,v], mindist);
*/


#include "mex.h"

inline double SQR(double x)
{
	return x*x;
}

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double *x, *y, dst2;
	unsigned int i,j, numpts;
	mxLogical *valid;

	// check input arguments
	if (nrhs != 2)
		mexErrMsgTxt("2 input arguments are necessary: valid = circfilter([x,y], mindist);");
	
	if (!mxIsDouble(prhs[0]) || (mxGetNumberOfDimensions(prhs[0]) != 2) || (mxGetN(prhs[0]) != 2))
		mexErrMsgTxt("First input argument must be double vector with two columns.");

	if (!mxIsDouble(prhs[1]) || (mxGetNumberOfElements(prhs[1]) != 1))
		mexErrMsgTxt("Minimal distance must be double scalar.");

	// read values
	numpts = mxGetM(prhs[0]);
	x = mxGetPr(prhs[0]);
	y = x + numpts;	
	dst2 = SQR(mxGetScalar(prhs[1]));

	// allocate output values
	plhs[0] = mxCreateLogicalMatrix(numpts, 1);
	if (numpts == 0)
		return;
	
	valid = mxGetLogicals(plhs[0]);
	for(i=0;i<numpts;i++)
		valid[i] = true;

	// check distances
	for(i=0;i<numpts-1;i++)
	{
		if (!valid[i])
			continue;

		for(j=i+1;j<numpts;j++)
		{
			if (!valid[j])
				continue;

			if ((SQR(x[i]-x[j]) + SQR(y[i]-y[j])) < dst2)
				valid[j] = false;
		}
	}
}
