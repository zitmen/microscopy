/*
 * Project:  Levenberg-Marquardt Least Squares Fitting of 2D Gauss 
 *
 * File:     fitlm.c
 *
 * Contents: MEX function for least-squares fit.
 *           optimize p to fit some data y(t) by my_fit_function(t;p).
 */

#include <math.h>
#include <float.h>
#include "mex.h"
#include "lmmin.h"


//#define SQR(x) ((x)*(x))

__inline double SQR(double x)	
{
	return x*x;
}

typedef struct {
    double *varvec;		// variables        x,y, ...
    double *funvec;		// function values  fun(x,y,...)	
    double (*f) (double **var, double *params);
} my_data_type;

// ------------------------------------------------------------------------
// Fit G(x,y,N), sigma is fixed, b = 0
// ------------------------------------------------------------------------
double sigma2;
#define DIM 2			// 2 dimensions
#define NUMPAR 3        // # of parameters
double my_fit_function(double **var, double *par)
{
    double cntvar[DIM];
	cntvar[0] = *var[0] - par[0];
	cntvar[1] = *var[1] - par[1];

	return par[2] * exp(-0.5 * (SQR(cntvar[0]) + SQR(cntvar[1])) / sigma2 );
}

/* ------------------------------------------------------------------------ */
/* Compute residua as chi2 = sum((y-f(x,p))^2)                              */
/* ------------------------------------------------------------------------ */

double sumchi2(double *params, int m_dat, void *data)
{
    my_data_type *mydata = (my_data_type *) data;
	double	sum = 0, *pfun, *pvar[DIM];
	int		i;

	// initialize pointers to data and variables
	pfun = mydata->funvec;
	pvar[0] = mydata->varvec;
	pvar[1] = mydata->varvec + m_dat;
    
	// go through all data
    for (i = 0; i < m_dat; i++)
	{
		sum += SQR(*(pfun++) - mydata->f(pvar, params));
		pvar[0]++; pvar[1]++;
	}

	return sum;
}

/* ------------------------------------------------------------------------ */
/* Compute residua for all data points                                      */
/* ------------------------------------------------------------------------ */
void my_evaluate(double *params, int m_dat, double *fvec, void *data, int *info)
{
    my_data_type *mydata = (my_data_type *) data;
	double	*pfun, *pvar[DIM];//, maxval = maxvec(m_dat,mydata->funvec);
	int		i;

	// initialize pointers to data and variables
	pfun = mydata->funvec;
	pvar[0] = mydata->varvec;
	pvar[1] = mydata->varvec + m_dat;
    
	// go through all data
    for (i = 0; i < m_dat; i++)
	{
		*(fvec++) = *(pfun++) - mydata->f(pvar, params);
		pvar[0]++; pvar[1]++;
	}    

	if ((params[2] < 0) || (params[2] > 1))
	    *info = -1;
	else
		*info = *info;		/* to prevent a 'unused variable' warning */
}

/* ------------------------------------------------------------------------ */
/* Control parameters                                                       */
/* ------------------------------------------------------------------------ */
#define LM_MACHEP     DBL_EPSILON   /* resolution of arithmetic */
#define LM_USERTOL    30*LM_MACHEP  /* users are recommened to require this */

void my_control_init( lm_control_type * control )
{
    control->maxcall = 1000;
    control->epsilon = LM_USERTOL;
    control->stepbound = 100.;
    control->ftol = LM_USERTOL;
    control->xtol = LM_USERTOL;
    control->gtol = LM_USERTOL;
}

/* ------------------------------------------------------------------------ */
/* Print convergence info                                                   */
/* ------------------------------------------------------------------------ */
void my_print(int n_par, double *par, int m_dat, double *fvec,
		      void *data, int iflag, int iter, int nfev)
{
   // double f, y, t;
    int i;
    my_data_type *mydata = (my_data_type *) data;

    if (iflag == 2) {
	mexPrintf("trying step in gradient direction\n");
    } else if (iflag == 1) {
	mexPrintf("determining gradient (iteration %d)\n", iter);
    } else if (iflag == 0) {
	mexPrintf("starting minimization\n");
    } else if (iflag == -1) {
	mexPrintf("terminated after %d evaluations\n", nfev);
    }

    mexPrintf("  par: ");
    for (i = 0; i < n_par; ++i)
	mexPrintf(" %12g", par[i]);
    mexPrintf(" => norm: %12g\n", lm_enorm(m_dat, fvec));
}

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  // data and pameter arrays:

    lm_control_type		control;
    my_data_type		data;
    int					m_dat, n_p = NUMPAR;    // number of data points and fitting parameters
    double				*p;

	if (nrhs != 4) {
		mexPrintf("Usage: [[x,y,A], sumerr2,exitflag] = lmfit2Dgauss([x0,y0,A0], XY, VAL, sigma)");
		return;
	}

	if (!mxIsDouble(prhs[0]))
		mexErrMsgTxt("First input argument must be a double matrix.");

	// initialize I/O parameters
	plhs[0] = mxDuplicateArray(prhs[0]);
	p = (double *) mxGetPr(plhs[0]);

	// initialize input data
    data.f = my_fit_function;
    data.varvec = (double *) mxGetPr(prhs[1]);
    data.funvec = (double *) mxGetPr(prhs[2]);
	m_dat = mxGetNumberOfElements(prhs[2]);	
	sigma2 = mxGetScalar(prhs[3]); sigma2 *= sigma2;
	
	// do the fitting
	my_control_init( &control );
	//lm_minimize( m_dat, n_p, p, my_evaluate, my_print, &data, &control );
	lm_minimize( m_dat, n_p, p, my_evaluate, NULL, &data, &control );

    //mexPrintf( "status: %s after %d evaluations\n",
    //        lm_shortmsg[control.info], control.nfev );

	// residuum sum(chi2) 
	if (nlhs > 1)
		plhs[1] = mxCreateDoubleScalar(sumchi2(p, m_dat, &data));

	// exit flag
	if (nlhs > 2)
		plhs[2] = mxCreateDoubleScalar(control.info);

	// # iterations
	if (nlhs > 3)
		plhs[3] = mxCreateDoubleScalar(control.nfev);

	// message
	if (nlhs > 4)
		plhs[4] = mxCreateString(lm_infmsg[control.info]);
}
