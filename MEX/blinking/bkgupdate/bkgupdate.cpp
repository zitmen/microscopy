/* MEX FUNCTION 
 *
 *  Circular buffer for 16bit images
 *
 */


//#include "matrix.h"

#include <string.h>
#include <ctype.h>
//#include <memory.h>
#include "mex.h"

typedef unsigned short uint16;
typedef unsigned int uint32;

// -------------------------------------------------------------------------

const char *what2do[3] = {"INIT", "ADD", "FREE"};

enum {
	WHAT2DO_INIT = 0,
	WHAT2DO_ADD  = 1,
	WHAT2DO_FREE = 2
};

// -------------------------------------------------------------------------
// Circular buffer
class CircBuf{
private:
	uint16 *data;
	int num, size;			// #items, buffer size
	int m, n, numel;		// #elements in one item, dimensions
	int idx;				// index curent item

public:

	// CircBuf(int numdims, int * dims, int size) - would be better
	// template <class inType, class outType, storageType> - to combine 16bit storage and float inputs/outputs ???
	CircBuf(int m, int n, int size)
	: data(NULL), size(size), m(m), n(n), idx(0), num(0)
	{				
		numel = m*n;
		// allocate memory for buffer
		data = (uint16 *) operator new(numel * size * sizeof(uint16));
	}

	~CircBuf()
	{
		if (data)
			delete (data);
		data = NULL;
	}

	mxArray *Add(const mxArray *mxin)
	{
		mxArray *mxout;
		double *out;
		uint16 *ptr;
		int i;

		if ((mxGetM(mxin) != m) || (mxGetN(mxin) != n))
			mexErrMsgTxt("Input data has wrong size.");

		// get old image
		if (num < size)
		{
			mxout = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
			num++;
		}
		else
		{
			mxout = mxCreateNumericMatrix(m, n, mxDOUBLE_CLASS, mxREAL);
			//out = (uint16 *) mxGetData(mxout);
			out = mxGetPr(mxout);
			for(i = 0, ptr = data+idx*numel; i < numel; i++, ptr++, out++)
				*out = (double) *ptr;
		}

		// save new image
		switch (mxGetClassID(mxin))
		{
			case mxDOUBLE_CLASS:
				{
					double *in = mxGetPr(mxin);
					for(i = 0, ptr = data+idx*numel; i < numel; i++, ptr++, in++)
						*ptr = (uint16) *in;	
				}
				break;

			case mxSINGLE_CLASS:
				{
					float *in = (float *) mxGetData(mxin);
					for(i = 0, ptr = data+idx*numel; i < numel; i++, ptr++, in++)
						*ptr = (uint16) *in;
				}
				break;

			case mxUINT16_CLASS:
				{
					uint16 *in = (uint16 *) mxGetData(mxin);
					for(i = 0, ptr = data+idx*numel; i < numel; i++, ptr++, in++)
						*ptr = *in;	
				}
				break;

			default:
				mexErrMsgTxt("Input data has wrong type (allowed is double/single/uint16).");
		}

		// increment index
		if (++idx >= size)
			idx = 0;
	
		return mxout;
	}

	int Num()
	const
	{
		return num;
	}
};

// -------------------------------------------------------------------------
// convert a string to upper case letters
void str2upper(char *str)
{
  while (*str != '\0')
  {
	  *str = toupper(*str);
	  str++;
  }
}

// -------------------------------------------------------------------------
// find index of a sting in an array of stings
int findinputstr(const mxArray *mxstr, const char **names, int numnames)
{
	int i, buflen = mxGetNumberOfElements(mxstr) + 1;
	char *buf = (char *) mxCalloc(buflen, sizeof(char));

	if (mxGetString(mxstr, buf, buflen) != 0)
		mexErrMsgTxt("Could not convert string data.");

	str2upper(buf);
	
	for(i = 0; i < numnames; i++)
	{
		if (strcmp(buf, names[i]) == 0)
			break;
	}

	mxFree(buf);

	if(i < numnames)
		return i;
	
	return -1;
}

// -------------------------------------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	static CircBuf *buf = NULL;
	int opt;

	if (nrhs < 1)
		mexErrMsgTxt("Not enough input arguments.");

	if (mxIsEmpty(prhs[0]) && !mxIsChar(prhs[0]))
		mexErrMsgTxt("First argument has to be a string.");

	// what to do
	opt = findinputstr(prhs[0], what2do, 3);

	switch (opt)
	{
		case WHAT2DO_INIT:  // bkupdate('init', m, n, buflen);
			
			if ((nrhs != 4) && (nlhs != 0))
				mexErrMsgTxt("Wrong number of I/O arguments");

			if (buf)
				delete buf;

			buf = (CircBuf *) new CircBuf((int) mxGetScalar(prhs[1]), (int) mxGetScalar(prhs[2]), (int) mxGetScalar(prhs[3]));
			
			break;
			
		case WHAT2DO_ADD:  // [out,num] = bkupdate('add', in);

			if ((nrhs != 2) && (nlhs > 2))
				mexErrMsgTxt("Wrong number of I/O arguments");

			if (mxIsEmpty(prhs[1]))
				mexErrMsgTxt("Wrong input.");

			if (!buf)
				mexErrMsgTxt("Buffer is not initialized.");;

			plhs[0] = buf->Add(prhs[1]);

			if (nlhs > 1)
				plhs[1] = mxCreateDoubleScalar(buf->Num());

			break;

		case WHAT2DO_FREE: // bkupdate('free');
			
			if ((nrhs != 1) && (nlhs != 0))
				mexErrMsgTxt("Wrong number of I/O arguments");

			if (buf)
				delete buf;
				buf = NULL;
			break;

		default:

			mexErrMsgTxt("Unknown option.");
	}
}