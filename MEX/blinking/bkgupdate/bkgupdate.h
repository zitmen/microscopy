#ifndef __CIRCBUF_H__
#define __CIRCBUF_H__

#include "mex.h"

typedef unsigned short uint16;

class CircBuf{
private:
	uint16 *data;
	int size;				// buffer size
	int m, n, numel;		// #elements in one item, dimensions
	int idx;				// index curent item

public:

	CircBuf()
	: data(NULL), size(0), numels(0), m(0), n(0), idx(0)
	{}

	~CircBuf()
	{
		Free();
	}

	void Init(int m, int n, int size)
	{
		if (data)
			Free();

		this->m = m;
		this->n = n;
		numels = m*n;
		this->size = size;
		// allocate memory for buffer
		data = (uint16 *) mxMalloc(numel * size * sizeof(T));
	}

	void Free()
	{
		if (data)
			mxFree(data);
		data = NULL;
		m = n = numels = size = idx = 0;
	}

	mxArray *Add(const mxArray *mxin)
	{
		mxArray *mxout;
		uint16 *in, *out;
		int numel = m*n;

		// get old image
		mxout = mxCreateNumericArray(m, n, mxUINT16_CLASS, mxREAL);
		out = (uint16 *) mxGetData(mxout);
		memcpy(out, data+idx*numel, numel*sizeof(uint16));

		// save new image
		in = (uint16 *) mxGetData(mxin);
		memcpy(data+idx*numel, in, numel*sizeof(uint16));

		// increment index
		if (++idx >= size)
			idx = 0;
	
		return mxout;
	}

};

#endif