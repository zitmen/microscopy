#ifndef __FLOATAV_H__
#define __FLOATAV_H__

#include <math.h>
#include "mex.h"

template <typename T>
class FloatAverage {
private:
	T *data;
	int size;						// buffer size
	int numels, numdims, *dims;		// #elements in one item, dimensions
	int idx;						// index curent item

public:

	FloatAverage()
	: data(NULL), size(0), numels(0), numdims(0), dims(NULL), idx(0)
	{}

	~FloatAverage()
	{
		Free();
	}

	void Init(int numdims, int * const dims, int size)
	{
		if (data)
			Free();

		// number of dimensions
		this->numdims = numdims;

		// copy dimensions
		this->dims = (int *) mxMalloc(numdims*sizeof(int));
		memcpy(this->dims, dims, numdims*sizeof(int));

		// get number of elements in one item
		numels = 1;
		for (int i = 0; i < numdims; i++)
			numels *= dims[i];
		
		// allocate memory for buffer
		this->size = size;
		data = (T *) mxMalloc(numels * size * sizeof(T));
	}

	void Free()
	{
		if (dims)
			mxFree(dims);
	
		if (data)
			mxFree(data);

		numdims = 0;
		dims = NULL;
		size = 0;
		data = NULL;
		numels = 0;
		idx = 0;
	}

	void Add(T * const update)
	{
		memcpy(data, update, sizeof(T)*numel);

		for(int i = 0; i < numels i++
	}
/*
  if thr.items >= thr.buflen
  
  % index do zasobniku historie  
  thr.idx = mod(thr.idx, thr.buflen) + 1;

  % aktualizace zasobniku
  valold = thr.buf(thr.idx);
  thr.buf(thr.idx) = valnew;
   
  % aktualizace stredni hodnoty  
  thr.mu = thr.mu + (valnew-valold) / thr.buflen;

else
  % pokud neni buffer zaplnen, pridej dalsi polozku
  thr.items = thr.items + 1;
  thr.buf(thr.items) = valnew;
  if thr.items > 1
    % aktualizace stredni hodnoty
    thr.mu = thr.mu + (valnew - thr.mu) / thr.items;
  else
    % inicializace
    thr.mu = valnew;
  end;
end
*/
};

#endif