#ifndef __MEXDBL2INT_H__
#define __MEXDBL2INT_H__

#include <memory.h>
#include "mex.h"

class dbl2int
{
private:
	int *p;
	int num;

public:
	// default constructor
	dbl2int(const mxArray *mxptr)
	{	
		double *ptr;
		if (!mxIsEmpty(mxptr) && !mxIsDouble(mxptr))
			mexErrMsgTxt("Input value must be double.");

		ptr = mxGetPr(mxptr);
		num = mxGetNumberOfElements(mxptr);
		
		p = (int *) mxMalloc(num * sizeof(int));
		for(int i = 0; i < num; i++)
			p[i] = (int) ptr[i];
	}
/*	
	// copy constructor
	dbl2int(const dbl2int& ptr)
	{
		num = ptr.num;
		p = (int *) mxMalloc(num * sizeof(int));
		memcpy(p, ptr.p, num*sizeof(int));
	}
*/
	~dbl2int()
	{
		if (p)
			mxFree(p);	
	}

	int * const Ptr()
	const
	{
		return p;
	}

	int Num()
	const
	{
		return num;
	}

/*	
	// insert operator
	dbl2int& operator=(const mxArray *mxptr)
	{
		double *ptr = mxGetPr(mxptr);
		num = mxGetNumberOfElements(mxptr);
		for(int i = 0; i < num; i++)
			p[i] = (int) *(ptr++);
		return *this;
	}

	// copy operator
	dbl2int& operator=(const dbl2int& ptr)
	{
		num = ptr.num;
		memcpy(p, ptr.p, num*sizeof(int));
		return *this;
	}
*/
};

#endif