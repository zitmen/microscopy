
#include "mex.h"
#include "dct.h"

/* ------------------------------------------------------------------------ */
// dct
/* ------------------------------------------------------------------------ */

void read_points(cardinal np, float *pts)
{
  index i;
  for (i = 0; i < np; i++)
    p_array[i].x = *pts++;
  for (i = 0; i < np; i++)
	p_array[i].y = *pts++;
} 


/*void write_points(cardinal np, float *dst,  point *src)
{
  index i;
  for (i = 0; i < np; i++)
    *dst++ = src[i].x;
  for (i = 0; i < np; i++)
	*dst++ = src[i].y;
}*/

//  Print the ring of edges about each vertex.
void print_edges(cardinal n, unsigned int *dst)
{
  edge *e_start, *e;
  point *u, *v;
  index i;

  for (i = 0; i < n; i++) {
    u = &p_array[i];
    e_start = e = u->entry_pt;
    do
    {
      v = Other_point(e, u);
      if (u < v)
	  {
		  //mexPrintf("%d %d\n", u - p_array, v - p_array);
		  *dst++ = u - p_array + 1;  // MATLAB + 1
		  *dst++ = v - p_array + 1;  // MATLAB + 1
	  }
      e = Next(e, u);
    } while (!Identical_refs(e, e_start));
  }
} 

// Print the ring of edges about each vertex.
void print_triangles(cardinal n, unsigned int *dst)
{
  edge *e_start, *e, *next;
  point *u, *v, *w;
  index i;
  point *t;

  for (i = 0; i < n; i++) {
    u = &p_array[i];
    e_start = e = u->entry_pt;
    do
    {
      v = Other_point(e, u);
      if (u < v) {
		  next = Next(e, u);
		  w = Other_point(next, u);
		  if (u < w)
			  if (Identical_refs(Next(next, w), Prev(e, v))) {  
				  /* Triangle. */
				  if (v > w) { t = v; v = w; w = t; }
				  mexPrintf("%d %d %d\n", u - p_array, v - p_array, w - p_array);
				  *dst++ = u - p_array + 1;  // MATLAB + 1
				  *dst++ = v - p_array + 1;  // MATLAB + 1
				  *dst++ = w - p_array + 1;  // MATLAB + 1
			  }
	  }

      /* Next edge around u. */
      e = Next(e, u);
    } while (!Identical_refs(e, e_start));
  }
} 

/* ------------------------------------------------------------------------ */
// M A I N
/* ------------------------------------------------------------------------ */

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	
    int		dim;    // dimension, number of data points
	float	*data;
	edge *l_cw, *r_ccw;
	index i, n;
	point **p_sorted, **p_temp;


	// initialize input parameters
	if ((dim = mxGetN(prhs[0])) != 2)
		panic("Wrong number of dimensions.");
	if ((n = mxGetM(prhs[0])) <= 3)
		panic("Number of points has to be greater than 3.");
	if (!mxIsSingle(prhs[0]))
		panic("Data has to be single precision.");

    data = (float *) mxGetData(prhs[0]);


	alloc_memory(n);
	read_points(n, data);


	/* Initialise entry edge pointers. */
	for (i = 0; i < n; i++)
		p_array[i].entry_pt = NULL;

	/* Sort. */		
	p_sorted = (point **)mxMalloc((unsigned)n*sizeof(point *));
	if (p_sorted == NULL)
		panic("triangulate: not enough memory\n");
	p_temp = (point **)mxMalloc((unsigned)n*sizeof(point *));
	if (p_temp == NULL)
		panic("triangulate: not enough memory\n");
	for (i = 0; i < n; i++)
		p_sorted[i] = p_array + i;
	merge_sort(p_sorted, p_temp, 0, n-1);
	mxFree((char *)p_temp);


	/* Triangulate. */
	divide(p_sorted, 0, n-1, &l_cw, &r_ccw);

	

    if (nlhs > 0)
	{
		int ntriu = 2*n - 2; // - b (# vertices of a convex hull)  max. number of triangles
		plhs[0] = mxCreateNumericMatrix(3, ntriu, mxUINT32_CLASS, mxREAL);
		print_triangles(n, (unsigned int *) mxGetData(plhs[0]));
	}


	if (nlhs > 1)
	{
		int nedges = 4*n - 4; // number of edges
		plhs[1] = mxCreateNumericMatrix(2, nedges, mxUINT32_CLASS, mxREAL);
		print_edges(n, (unsigned int *) mxGetData(plhs[1]));
	}

	mxFree((char *)p_sorted);
	
	free_memory();
}