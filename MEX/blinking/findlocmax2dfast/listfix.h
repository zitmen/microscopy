#ifndef __LISTFIX_H__
#define __LISTFIX_H__

// ---------------------------------------------------------------------------
// fixed size list
// ---------------------------------------------------------------------------

template <class T>
class listfix {

private:
	T *list;
	int num;    // number of saved items
	int size;    // buffer size

public:

	// create an empty buffer of a given size
	listfix(int size)
	: size(size), num(0)	
	{
		list = (T*) operator new(size * sizeof(T));
		//if (( ) == NULL)
		//	throw "LISTFIX: Not enough memory.";
	}

		// free memory
	~listfix()
	{
		if( list != NULL )
			operator delete(list);
	}


	// add item to the end
	void Append(T item)
	{
		if (num >= size)
			throw "LISTFIX: Buffer is full.";
		*(list+num) = item;
		num ++;
	}

	// remove last item
	T Remove()
	{
		if (num == 0)
			throw "LISTFIX: Buffer is empty.";

		num--;
		return *(list+num);
	}

	// get number of saved num
	int Num()
	const
	{
		return num;
	}

	// get length of the buffer
	int Size()
	const
	{
		return size;
	}

	// is buffer empty
	bool IsEmpty()
	const
	{
		return (num == 0);
	}

	// is buffer full
	bool IsFull()
	const
	{
		return (num >= len);
	}

	// cancel copy constructor (Virius 1, p.140)
	listfix(const listfix&);
	listfix& operator=(const listfix&);
};

#endif
