/*  MATLAB MEX FUNCTION
 *
 *  Find local maxima in 4 neighbourhood of a 2D matrix.
 *
 *  Note: This method is suitable for batch processing of matrices
 *  of the same size as it uses static memmory allocation.
 *
 *  Usage:
 *
 *   idx = findlocmax2dfast(X)   ... where X is double matrix [m x n]
 *
 *   findlocmax2dfast()          ... free up memmory
 *
 *   Pavel Krizek, April 2010
 */

#include "mex.h"
#include "listfix.h"

typedef unsigned int uint32;
typedef listfix<uint32> list;

// ---------------------------------------------------------------------------	
void findlocmax4neighbour(double * const data, const int m, const int n, list *ll)
{
	int i,j,k;
	double *p = data, *q[4];	

	// init
	p += m + 1;
	q[0] = p-1;
	q[1] = p+1;
	q[2] = p-m;
	q[3] = p+m;

	// find all local maxima in 4 neighbourhood
	for(j = 1; j < n-1; j++) {
		
		// process one colon
		for(i = 1; i < m-1; i++) {

			// find local maxima
			if ((*p >= *q[0]) && (*p >= *q[1]) && (*p >= *q[2]) && (*p >= *q[3]))
				ll->Append(p-data+1); // MATLAB+1

			// next row
			p++;
			for(k=0; k<4; k++)
				q[k]++;
		}
		
		// next colon
		p+=2;
		for(k=0; k<4; k++)
			q[k]+=2;
	}	
}

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	static int m = 0, n = 0;	// remember matrix size
	static list *ll = NULL;		// remember allocated memmory
	int mm, nn;
	uint32 *out;

	// reset if no input
	if (nrhs == 0)
	{
		if (ll)
		{
			delete ll;
			ll = NULL;
			m = n = 0;
		}
		return;
	}

	// matrix must be double
	if (!mxIsDouble(prhs[0]))
		mexErrMsgTxt("Input matrix must be double.");

	mm = mxGetM(prhs[0]);
	nn = mxGetN(prhs[0]);

	// if matrix size changed
	if (((mm != m) || (nn != n)) && ll)
	{
		delete ll;
		ll = NULL;
		m = n = 0;
	}

	// allocate memmory
	if (!ll)
	{
		m = mm;
		n = nn;
		ll = (list *) new list(m*n);
	}

	// find local maxima
	findlocmax4neighbour(mxGetPr(prhs[0]), m, n, ll);

	// save output
	plhs[0] = mxCreateNumericMatrix(ll->Num(), 1, mxUINT32_CLASS,mxREAL);
	out = (unsigned int*) mxGetData(plhs[0]);
	while (!ll->IsEmpty())
		*(out++) = ll->Remove();
}
