//	Templated 2D array (matrix)

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "defs.h"

// forward declarations
template <class Type> class Matrix;
//template <class Type, class Base> class MemMatrix;


template <class Type>
class Matrix

{
	// replace matrix rows with those from m
//	void ReplaceRows(const Matrix<Type> & m);

protected:

	Type	*		start;		// first matrix element
	int				w;			// width  of matrix
	int				h;			// height of matrix
	int				memw;		// memory width of matrix (skip to next/prev)

public:

	// construct on given memory
	Matrix(Type * mem, int w, int h, int memw)
	: start(mem), w(w), h(h), memw(memw)
	{
		ASSERT(w >= 0 && h >= 0 && memw >= w);
	}
	
	// construct as a submatrix of a given matrix
	Matrix(Matrix & m, int row, int col, int w, int h)
	: start(m.Item(row,col)), w(w), h(h), memw(m.MemW())
	{
		ASSERT(w >= 0 && col+w <= m.W());
		ASSERT(h >= 0 && row+h <= m.H());
	}

	// assignment operator from the same sized matrix
	Matrix & operator =(const Matrix & m)
	{
		ASSERT(w == m.W() && h == m.H());

		// prevent self assignment

		if(&m != this)
			ReplaceRows(m);

		return *this;
	}

	// set all matrix items to a given value, PK, May 2010
	void Set(Type val)
	{
		int i,j;
		Type *ptr;

		for(j = 0; j < h; j++)
			for(i = 0, ptr = start + j*memw; i < w; i++, ptr++)
				*ptr = val;
	}

	// get width of matrix
	int W()
	const
	{
		return w;
	}

	// get height of matrix
	int H()
	const
	{
		return h;
	}

	// get memory width of matrix
	int MemW()
	const
	{
		return memw;
	}

	// get number of elements in matrix
	int Num()
	const
	{
		return w*h;
	}

	// get modifiable start of vector 
	Type * Mem()
	{
		return start;
	}

	// get const start of vector
	const Type * Mem()
	const
	{
		return start;
	}

	// get modifiable end of vector
	Type * End()
	{
		return start+memw*h;
	}

	// get const end of vector
	const Type * End()
	const
	{
		return start+memw*h;
	}

	// get modifiable row of matrix
	Type * Row(int row)
	{
		ASSERT(row >= 0 && row < h);

		return start+row*memw;
	}

	// get const row of matrix
	const Type * Row(int row)
	const
	{
		ASSERT(row >= 0 && row < h);

		return start+row*memw;
	}

	// get modifiable column of matrix
	Type * Col(int col)
	{
		ASSERT(col >= 0 && col < w);

		return start+col;
	}

	// get const column of matrix
	const Type * Col(int col)
	const
	{
		ASSERT(col >= 0 && col < w);

		return start+col;
	}

	// get modifiable pointer to matrix item
	Type * Item(int row, int col)
	{
		ASSERT(row >= 0 && row < h);
		ASSERT(col >= 0 && col < w);

		return start+row*memw+col;
	}

	// get const pointer to matrix item
	const Type * Item(int row, int col)
	const
	{
		ASSERT(row >= 0 && row < h);
		ASSERT(col >= 0 && col < w);

		return start+row*memw+col;
	}

	// get modifiable matrix row 
	Type * operator [](int row)
	{
		ASSERT(row >= 0 && row < h);

		return start+row*memw;
	}

	// get const matrix row
	const Type * operator [](int row)
	const
	{
		ASSERT(row >= 0 && row < h);

		return start+row*memw;
	}
/*
	// get modifiable matrix row as array
	Array<Type> operator ()(int row)
	{
		ASSERT(row >= 0 && row < h);

		return Array<Type>(start+row*memw,w);
	}

	// get const matrix row as array
	const Array<Type> operator ()(int row)
	const
	{
		ASSERT(row >= 0 && row < h);

		return Array<Type>(start+row*memw,w);
	}
*/
	// range-safe access to matrix item referrence
	Type & operator ()(int row, int col)
	{
		ASSERT(row >= 0 && row < h);
		ASSERT(col >= 0 && col < w);

		return start[row*memw+col];
	}

	// range-safe access to matrix item value 
	const Type & operator ()(int row, int col)
	const
	{
		ASSERT(row >= 0 && row < h);
		ASSERT(col >= 0 && col < w);

		return start[row*memw+col];
	}
};
/*
template <class Type>
void Matrix<Type>::ReplaceRows(const Matrix<Type> & m)

{
	Type * dest = start;

	for(const Type * ptr = m.Mem(); ptr != m.End(); ptr += m.MemW())
	{
		DataTraits<Type>::Replace(ptr,ptr+w,dest);
		dest += memw;
	}
}
*/

/* -------------------------------------------------------------------------

template <class Type, class Base = Matrix<Type> >
class MemMatrix : public Base

{
	// use standard allocator
	USE_STD_ALLOCATOR(Type,Alloc);
	
	// copy matrix rows from m
	void CopyRows(const Matrix<Type> & m);

	// replace matrix rows with those from m
	void ReplaceRows(const Matrix<Type> & m);

protected:

	// resolve base members
	using Base::start;
	using Base::w;
	using Base::h;
	using Base::memw;

public:

	// prepare empty matrix
	MemMatrix()
	: Base(NULL,0,0,0)
	{}

	// construct new uninitialized matrix with specified size
	MemMatrix(int w, int h)
	: Base(Alloc::Allocate(w*h),w,h,w)
	{
		DataTraits<Type>::Init(start,start+w*h);
	}

	// construct new matrix with specified size
	MemMatrix(int w, int h, const Type & val)
	: Base(Alloc::Allocate(w*h),w,h,w)
	{
		DataTraits<Type>::Fill(start,start+w*h,val);
	}

	// construct from another matrix
	MemMatrix(const Matrix<Type> & m)
	: Base(Alloc::Allocate(m.Num()),m.W(),m.H(),m.W())
	{
		CopyRows(m);
	}

	// copy constructor
	MemMatrix(const MemMatrix<Type,Base> & m)
	: Base(Alloc::Allocate(m.Num()),m.W(),m.H(),m.W())
	{
		DataTraits<Type>::Copy(m.Mem(),m.End(),start);
	}

	// cleanup
	~MemMatrix()
	{
		DataTraits<Type>::Destroy(start,start+w*h);
		Alloc::Deallocate(start,w*h);
	}

	// assignment operator from another matrix
	MemMatrix & operator =(const Matrix<Type> & m);

	// assignment operator from another memory matrix
	MemMatrix & operator =(const MemMatrix<Type,Base> & m);

	// swap itself with the other matrix
	void Swap(MemMatrix<Type,Base> & m)
	{
		::Swap(start,m.start);
		::Swap(w,m.w);
		::Swap(h,m.h);
		::Swap(memw,m.memw);
	}

	// reallocate to new size
	void Reallocate(int newW, int newH)
	{
		int sz    = w*h;
		int newSz = newW*newH;

		ASSERT(newSz >= 0);

		// destroy all data

		DataTraits<Type>::Destroy(start,start+sz);

		// reallocate only if needed

		if(newSz != sz)
		{
			Alloc::Deallocate(start,sz);
			start = Alloc::Allocate(newSz);
			memw = w = newW;
			h = newH;
		}

		// initialize data

		DataTraits<Type>::Init(start,start+newSz);
	}

	// read from stream
	friend InStream & operator >> <>(InStream &, MemMatrix<Type,Base> &);
};

// ------------------------------------------------------------------------- 

// ------------------------------------------------------------------------- 

template <class Type, class Base>
void MemMatrix<Type,Base>::CopyRows(const Matrix<Type> & m)
	
{
	Type * dest = start;

	for(const Type * ptr = m.Mem(); ptr != m.End(); ptr += m.MemW())
		dest = DataTraits<Type>::Copy(ptr,ptr+w,dest);
}

// -------------------------------------------------------------------------

template <class Type, class Base>
void MemMatrix<Type,Base>::ReplaceRows(const Matrix<Type> & m)
{
	Type * dest = start;

	for(const Type * ptr = m.Mem(); ptr != m.End(); ptr += m.MemW())
		dest = DataTraits<Type>::Replace(ptr,ptr+w,dest);
}

// ------------------------------------------------------------------------- 

template <class Type, class Base>
MemMatrix<Type,Base> & MemMatrix<Type,Base>::operator =(const Matrix<Type> & m)
	
{
	// prevent self-assignment

	if(&m != this)
	{
		int size = m.Num();
		int here = Base::Num();

		// store new size

		w = memw = m.W();
		h = m.H();

		// check size

		if(size == here)
		{
			// simply replace values

			ReplaceRows(m);
		}
		else
		{
			// reallocation needed

			DataTraits<Type>::Destroy(start,start+here);
			Alloc::Deallocate(start,here);

			start = Alloc::Allocate(size);
			CopyRows(m);
		}
	}

	return *this;
}

// -------------------------------------------------------------------------

template <class Type, class Base>
MemMatrix<Type,Base> & 
MemMatrix<Type,Base>::operator =(const MemMatrix<Type,Base> & m)

{
	// prevent self-assignment

	if(&m != this)
	{
		int size = m.Num();
		int here = Base::Num();

		// store new size

		w = memw = m.W();
		h = m.H();

		// check size

		if(size == here)
		{
			// simply replace values

			DataTraits<Type>::Replace(m.Mem(),m.End(),start);
		}
		else
		{
			// reallocation needed

			Destroy(start,start+here);
			Alloc::Deallocate(start,here);

			start = Alloc::Allocate(size);
			DataTraits<Type>::Copy(m.Mem(),m.Mem()+size,start);
		}
	}

	return *this;
}
*/

#endif
