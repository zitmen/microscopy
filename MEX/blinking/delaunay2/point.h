//	2D point

#ifndef __POINT_H
#define __POINT_H

#include <math.h>
#include "func.h"

/* ------------------------------------------------------------------------- */

// 2D point (origin based vector) in standard coordinate system
template <class Type>
struct Point

{
	Type	x;			// x (in image row : top ->down ) coordinate
	Type	y;			// y (in image col : left->right) coordinate

	// default constructor
	Point()
	: x(0.0), y(0.0)
	{}

	// construct new point
	Point(Type x, Type y)
	: x(x), y(y)
	{}

	// add point to this one
	Point & operator +=(const Point & p)
	{
		x += p.x;
		y += p.y;

		return *this;
	}

	// subtract point from this one
	Point & operator -=(const Point & p)
	{
		x -= p.x;
		y -= p.y;

		return *this;
	}

	// multiply point by scalar
	Point & operator *=(Type a)
	{
		x *= a;
		y *= a;

		return *this;
	}

	// divide point by scalar
	Point & operator /=(Type a)
	{
		x /= a;
		y /= a;

		return *this;
	}

	// return norm of point
	Type operator ~()
	const
	{
		return Sqr(x)+Sqr(y);
	}

	// return norm of point 
	Type Norm()
	const
	{
		return Sqr(x)+Sqr(y);
	}

	// normalize point
	Point & Normalize()
	{
		Type norm = Dist();

		x /= norm;
		y /= norm;

		return *this;
	}

	// return size of point (distance from origin)
	Type Dist()
	const
	{
		return sqrt(Norm());
	}

	// return angle of point (in the range -PI .. PI)
	Type Angle()
	const
	{
		return atan2(y,x);
	}

	// convert from polar coordinates
	static Point FromPolar(Type r, Type a)
	{
		return Point(cos(a)*r,sin(a)*r);
	}
};

/* ------------------------------------------------------------------------- */

// inverse point
template <class Type>
inline Point<Type> operator -(const Point<Type> & p)

{
	return Point<Type>(-p.x,-p.y);
}

/* ------------------------------------------------------------------------- */

// add two points
template <class Type>
inline Point<Type> operator +(const Point<Type> & a, const Point<Type> & b)
{
	return Point<Type>(a) += b;
}

/* ------------------------------------------------------------------------- */

// subtract two points 
template <class Type>
inline Point<Type> operator -(const Point<Type> & a, const Point<Type> & b)
{
	return Point<Type>(a) -= b;
}

/* ------------------------------------------------------------------------- */

// multiply point by a number
template <class Type>
inline Point<Type> operator *(const Point<Type> & a, Type b)
{
	return Point<Type>(a) *= b;
}

/* ------------------------------------------------------------------------- */

// multiply point by a number
template <class Type>
inline Point<Type> operator *(Type a, const Point<Type> & b)
{
	return Point<Type>(b) *= a;
}

/* ------------------------------------------------------------------------- */

// divide point by a number
template <class Type>
inline Point<Type> operator /(const Point<Type> & a, Type b)
{
	return Point<Type>(a) /= b;
}

/* ------------------------------------------------------------------------- */

// scalar multiplication of two points
template <class Type>
inline Type operator *(const Point<Type> & a, const Point<Type> & b)
{
	return a.x*b.x + a.y*b.y;
}

/* ------------------------------------------------------------------------- */

// compare two points for equality
template <class Type>
inline bool operator ==(const Point<Type> & a, const Point<Type> & b)
{
	return a.x == b.x && a.y == b.y;
}

/* ------------------------------------------------------------------------- */

// compare two points for inequality
template <class Type>
inline bool operator !=(const Point<Type> & a, const Point<Type> & b)
{
	return a.x != b.x || a.y != b.y;
}

/* ------------------------------------------------------------------------- */

// z-axis of vector multiplication of two points
// positive for counterclockwise orientation of a->b
// negative for clockwise orientation of a->b
// zero     for linearly dependent a & b
template <class Type>
inline Type operator ^(const Point<Type> & a, const Point<Type> & b)
{
	return a.x*b.y - a.y*b.x;
}

/* ------------------------------------------------------------------------- */

// return norm (squared distance) between two points
template <class Type>
inline Type Norm(const Point<Type> & a, const Point<Type> & b)

{
	return Sqr(a.x-b.x) + Sqr(a.y-b.y);
}

/* ------------------------------------------------------------------------- */

// return distance between two points
template <class Type>
inline Type Dist(const Point<Type> & a, const Point<Type> & b)

{
	return sqrt(Norm(a,b));
}


// -------------------------------------------------------------------------

template <class Type>
inline Point<Type> Abs(Point<Type> a)
{
	//return Point<Type>((a.x < 0) ? -a.x : a.x, (a.y < 0) ? -a.y : a.y);
	return Point<Type>(Abs(a.x), Abs(a.y));
}

// -------------------------------------------------------------------------

template <class Type>
inline Point<Type> Avg(Point<Type> a, Point<Type> b)
{
	return Point<Type>((a + b)/2.0f);
}

#endif
