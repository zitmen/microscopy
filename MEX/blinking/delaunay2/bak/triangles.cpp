#include "triangles.h"



// ---------------------------------------------------------------------------

Triangles::Triangles(uint32 npts, float * const datax, float * const datay)
:npts(npts), ptx(datax), pty(datay)
{
}

// ---------------------------------------------------------------------------

Triangles::~Triangles()
{
	ll.DeleteAll();
}

// ---------------------------------------------------------------------------

void Triangles::Add(uint32 idxP1, uint32 idxP2, uint32 idxP3)
{
	ll.AddHead(TriItem(idxP1, idxP2, idxP3));
}

// ---------------------------------------------------------------------------

int Triangles::Render(double *roi, matrixf& image)
{
	listtri::CNode tri;
	pointf pts[3];
	float area;
	uint32 i;
	float *ptr, scale, offset;

	ptr = ptx;
	offset = (float) roi[0];
	scale = (float) (image.W()/roi[2]);
	for(i = 0; i < npts; i++, ptr++)
		*ptr = (*ptr - offset) * scale - 0.5f;

	ptr = pty;
	offset = (float) roi[1];
	scale = (float) (image.H()/roi[3]);
	for(i = 0; i < npts; i++, ptr++)
		*ptr = (*ptr - offset) * scale - 0.5f;

	for(tri = ll.Head(); tri != ll.Tail(); tri++)
	{
		getpoints(*tri, pts);	// idx -> x,y		
		area = getarea(pts);
		filltriangle(image, pts, 1/area);
	}

	return 0;
}

// ---------------------------------------------------------------------------

void Triangles::getpoints(const TriItem *item, pointf *pts)
{
	int i;
	uint32 idx;
	
	for(i = 0; i < 3; i++)
	{
		idx = item->index[i];
		pts[i].x = ptx[idx];
		pts[i].y = pty[idx];
	}
}

// ---------------------------------------------------------------------------
// compute area using semi-perimeter
float Triangles::getarea(pointf *pts)
{
	float a,b,c,s;

	a = Dist(pts[0], pts[1]);
	b = Dist(pts[1], pts[2]);
	c = Dist(pts[0], pts[2]);
	s = 0.5f * (a + b + c);
	return sqrt(s * (s - a) * (s - b) * (s - c));
}

// ---------------------------------------------------------------------------
void Triangles::filltriangle(matrixf& image, const pointf *pts, const float val)
{
	int i, ix, iy;
	float minx = FLT_MAX, maxx = -FLT_MAX, miny = FLT_MAX, maxy = -FLT_MAX;
	float *ptr;
	pointf pt, cog, s[3];
	float c[3];
	bool isin;
	
	// find center of gravity
	for(i = 0; i < 3; i++)
		cog += pts[i];
	cog /= 3;
	
	for(i = 0; i < 3; i++)
	{
		pt = pts[i];

		// find min, max
		if (minx > pt.x)
			minx = pt.x;
		if (maxx < pt.x)
			maxx = pt.x;

		// find min, max		
		if (miny > pt.y)
			miny = pt.y;
		if (maxy < pt.y)
			maxy = pt.y;
		
		// find line coordinates
		s[i] = pts[(i < 2)?i+1:0] - pt;
		s[i].Normalize();			// normalize
		if (((cog - pt)^s[i]) > 0)	// cog & s  -> clockwise direction
			s[i] = -s[i];
		c[i] = pt^s[i];				// offset: ax*by+c=0
	}	

	//maxy = (float)Min<int>((int)ceil(maxy),image.H());
	maxy = ceil(maxy);
	//for(iy = Max<int>((int)floor(miny),0); iy < maxy; iy++)
	for(iy = (int)floor(miny); iy < maxy; iy++)
	{		
		//maxx = (float)Min<int>((int)ceil(maxx),image.W());
		maxx = ceil(maxx);
		//ix = Max<int>((int)floor(minx),0);
		ix = (int)floor(minx);
		ptr = image.Mem() + iy * image.MemW() +  ix;
		for(; ix < maxx; ix++, ptr++)
		{
			for(i = 0, isin = true; i < 3; i++)
				if (!(isin &= (s[i]^pointf((float)ix,(float)iy)) + c[i] >= 0))
					break;
			if (isin)
				*ptr = val;
		}
	}
}

// ---------------------------------------------------------------------------
