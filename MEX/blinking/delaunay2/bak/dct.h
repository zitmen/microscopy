#ifndef __DCT_H__
#define __DCT_H__

/* ------------------------------------------------------------------------ */
// dc.h
/* ------------------------------------------------------------------------ */

#define Vector(p1,p2,u,v) (u = p2->x - p1->x, v = p2->y - p1->y)

#define Cross_product_2v(u1,v1,u2,v2) (u1 * v2 - v1 * u2)

#define Cross_product_3p(p1,p2,p3) ((p2->x - p1->x) * (p3->y - p1->y) - (p2->y - p1->y) * (p3->x - p1->x))

#define Dot_product_2v(u1,v1,u2,v2) (u1 * u2 + v1 * v2) 

/* ------------------------------------------------------------------------ */
// defs.h
/* ------------------------------------------------------------------------ */

#define SYSV
#define OUTPUT
#define TIME

#ifndef NULL
#define  NULL  0
#endif
#define  TRUE  1
#define  FALSE  0

/* Edge sides. */
typedef enum {right, left} side;

/* Geometric and topological entities. */
typedef  unsigned int index;
typedef  unsigned int cardinal;
typedef  int integer;
typedef  float  real;
typedef  float  ordinate;
typedef  unsigned char boolean;
typedef  struct   point   point;
typedef  struct  edge  edge;

struct point {
  ordinate x,y;
  edge *entry_pt;
};

struct edge {
  point *org;
  point *dest;
  edge *onext;
  edge *oprev;
  edge *dnext;
  edge *dprev;
}; 

/* ------------------------------------------------------------------------ */
// decl.h
/* ------------------------------------------------------------------------ */

/* divide_and_conquer.c */
void divide(point *p_sorted[], index l, index r, edge **l_ccw, edge **r_cw);

/* edge.c */
edge *join(edge *a, point *u, edge *b, point *v, side s);
void delete_edge(edge *e);
void splice(edge *a, edge *b, point *v);
edge *make_edge(point *u, point *v);

/* error.c */
void panic(char *m);

/* i_o.c */
void read_points(cardinal n);
void print_results(cardinal n, char o);

/* memory.c */
void alloc_memory(cardinal n);
void free_memory();
edge *get_edge();
void free_edge(edge *e);

/* sort.c */
void merge_sort(point *p[], point *p_temp[], index l, index r); 

/* ------------------------------------------------------------------------ */
// edge.h
/* ------------------------------------------------------------------------ */

#define  Org(e)    ((e)->org)
#define  Dest(e)    ((e)->dest)
#define  Onext(e)  ((e)->onext)
#define  Oprev(e)  ((e)->oprev)
#define  Dnext(e)  ((e)->dnext)
#define  Dprev(e)  ((e)->dprev)

#define  Other_point(e,p)  ((e)->org == p ? (e)->dest : (e)->org)
#define  Next(e,p)  ((e)->org == p ? (e)->onext : (e)->dnext)
#define  Prev(e,p)  ((e)->org == p ? (e)->oprev : (e)->dprev)

#define  Visited(p)  ((p)->f)

#define Identical_refs(e1,e2)  (e1 == e2) 

/* ------------------------------------------------------------------------ */
// extern.h
/* ------------------------------------------------------------------------ */

extern point *p_array; 

#endif