#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include <float.h>
#include <math.h>
#include "point.h"
#include "listsingle.h"
#include "matrix.h"

typedef unsigned int uint32;
typedef Point<float> pointf;
typedef Matrix<float> matrixf;


struct TriItem {
	uint32 index[3];

	TriItem(uint32 idxA, uint32 idxB, uint32 idxC)
	{
		index[0] = idxA;
		index[1] = idxB;
		index[2] = idxC;
	};
};

typedef ListSingle<TriItem> listtri;

class Triangles
{
private:

	float *ptx, *pty;	// points coordinates
	uint32 npts;	// number of points
	listtri ll;		// list of triangle indices

	void getpoints(const TriItem *item, pointf *pts);
	float getarea(pointf *pts);
	void filltriangle(matrixf& image, const pointf *pts, const float val);
	//bool isin(const pointf P, const pointf *pts);

public:

	Triangles(const uint32 npts, float * const datax, float * const datay);
	~Triangles();
	void Add(uint32 idxP1, uint32 idxP2, uint32 idxP3);

	// render triangulation into pre-allocated memmory
	int Render(double *roi, matrixf& image);
};


#endif


