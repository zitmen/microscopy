#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include "point.h"
#include <float.h>

typedef Point<float> pointf;

class Triangle
{
private:
	pointf pt[3];
	pointf cog;
	float area;

public:
	Triangle(const pointf P1, const pointf P2, const pointf P3);
	~Triangle() {};

	float getarea() const { return area; };
	pointf getcog() const { return cog; };
	bool isin(pointf P);
	pointf minpt();
	pointf maxpt();
};

// ---------------------------------------------------------------------------
Triangle::Triangle(const pointf P1, const pointf P2, const pointf P3)
{
	float a,b,c,s;
	int i;

	pt[0] = P1;
	pt[1] = P2;
	pt[2] = P3;

	// compute area with semi-perimeter
	// see http://www.btinternet.com/~se16/hgb/triangle.htm
	a = Dist(pt[0], pt[1]);
	b = Dist(pt[1], pt[2]);
	c = Dist(pt[0], pt[2]);
	s = 0.5f * (a + b + c);
	area = sqrt(s * (s - a) * (s - b) * (s - c));

	// compute center of gravity
	for (i = 0; i < 2; i++)
		cog += pt[i];	
	cog /= 3;
};

// ---------------------------------------------------------------------------
// Check if point is inside triangle
// see http://www.blackpawn.com/texts/pointinpoly/default.html
inline bool Triangle::isin(pointf P)
{
	pointf v0, v1, v2;
	float dot00, dot01, dot02, dot11, dot12, invDenom, u, v;

	// Compute vectors
	v0 = pt[2] - pt[0];
	v1 = pt[1] - pt[0];
	v2 = P - pt[0];

	// Compute dot products
	dot00 = v0 * v0;
	dot01 = v0 * v1;
	dot02 = v0 * v2;
	dot11 = v1 * v1;
	dot12 = v1 * v2;

	// Compute barycentric coordinates
	invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
	u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	v = (dot00 * dot12 - dot01 * dot02) * invDenom;

	// Check if point is in triangle
	return (u > 0) && (v > 0) && (u + v < 1);
};

// ---------------------------------------------------------------------------
pointf Triangle::minpt()
{
	int i;
	pointf P(FLT_MAX,FLT_MAX);

	for(i = 0; i < 2; i++)
	{
		if (pt[i].x < P.x)
			P.x = pt[i].x;
		if (pt[i].y < P.y)
			P.y = pt[i].y;
	}
	return P;
};

// ---------------------------------------------------------------------------
pointf Triangle::maxpt()
{
	int i;
	pointf P(-FLT_MAX,-FLT_MAX);

	for(i = 0; i < 2; i++)
	{
		if (pt[i].x > P.x)
			P.x = pt[i].x;
		if (pt[i].y > P.y)
			P.y = pt[i].y;
	}
	return P;
};

#endif


