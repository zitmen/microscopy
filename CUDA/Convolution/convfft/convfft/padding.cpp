#include "padding.h"

void padding_symmetric(int dataW, int dataH, const float *data, int paddingW, int paddingH, int paddedDataW, int paddedDataH, int squareSize, float *padded_data)
{	// mirror-reflecting the array across the array border
	// top
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < dataW; j++)
			padded_data[MAP(paddingW+j, i, squareSize)] = data[MAP(j, paddingH-i-1, dataH)];
	// bottom
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < dataW; j++)
			padded_data[MAP(paddingW+j, paddedDataH-paddingH+i, squareSize)] = data[MAP(j, dataH-i-1, dataH)];
	// left
	for(int i = 0; i < dataH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, paddingH+i, squareSize)] = data[MAP(paddingW-j-1, i, dataH)];
	// right
	for(int i = 0; i < dataH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, paddingH+i, squareSize)] = data[MAP(dataW-j-1, i, dataH)];
	// top-left corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, i, squareSize)] = data[MAP(0, 0, dataH)];
	// top-right corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, i, squareSize)] = data[MAP(dataW-1, 0, dataH)];
	// bottom-left corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, paddedDataH-paddingH+i, squareSize)] = data[MAP(0, dataH-1, dataH)];
	// bottom-right corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, paddedDataH-paddingH+i, squareSize)] = data[MAP(dataW-1, dataH-1, dataH)];
}

void padding_circular (int dataW, int dataH, const float *data, int paddingW, int paddingH, int paddedDataW, int paddedDataH, int squareSize, float *padded_data)
{	// assuming the input array is periodic
	// top
	int start_padding_H = dataH - (paddingH % dataH);
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < dataW; j++)
			padded_data[MAP(paddingW+j, i, squareSize)] = data[MAP(j, (start_padding_H+i)%dataH, dataH)];
	// bottom
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < dataW; j++)
			padded_data[MAP(paddingW+j, paddedDataH-paddingH+i, squareSize)] = data[MAP(j, i%dataH, dataH)];
	// left
	int start_padding_W = dataW - (paddingW % dataW);
	for(int i = 0; i < dataH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, paddingH+i, squareSize)] = data[MAP((start_padding_W+j)%dataW, i, dataH)];
	// right
	for(int i = 0; i < dataH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, paddingH+i, squareSize)] = data[MAP(j%dataW, i, dataH)];
	// top-left corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, i, squareSize)] = padded_data[MAP(j, ((start_padding_H+i)%dataH)+paddingH, paddedDataH)];
	// top-right corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, i, squareSize)] = padded_data[MAP(paddedDataW-paddingW+j, ((start_padding_H+i)%dataH)+paddingH, paddedDataH)];
	// bottom-left corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, paddedDataH-paddingH+i, squareSize)] = padded_data[MAP(j, (i%dataH)+paddingH, paddedDataH)];
	// bottom-right corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, paddedDataH-paddingH+i, squareSize)] = padded_data[MAP(paddedDataW-paddingW+j, (i%dataH)+paddingH, paddedDataH)];
}

void padding_replicate(int dataW, int dataH, const float *data, int paddingW, int paddingH, int paddedDataW, int paddedDataH, int squareSize, float *padded_data)
{	// the nearest array border value
	// top
	int start_padding_H = dataH - (paddingH % dataH);
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < dataW; j++)
			padded_data[MAP(paddingW+j, i, squareSize)] = data[MAP(j, 0, dataH)];
	// bottom
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < dataW; j++)
			padded_data[MAP(paddingW+j, paddedDataH-paddingH+i, squareSize)] = data[MAP(j, dataH-1, dataH)];
	// left
	int start_padding_W = dataW - (paddingW % dataW);
	for(int i = 0; i < dataH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, paddingH+i, squareSize)] = data[MAP(0, i, dataH)];
	// right
	for(int i = 0; i < dataH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, paddingH+i, squareSize)] = data[MAP(dataW-1, i, dataH)];
	// top-left corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, i, squareSize)] = data[MAP(0, 0, dataH)];
	// top-right corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, i, squareSize)] = data[MAP(dataW-1, 0, dataH)];
	// bottom-left corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(j, paddedDataH-paddingH+i, squareSize)] = data[MAP(0, dataH-1, dataH)];
	// bottom-right corner
	for(int i = 0; i < paddingH; i++)
		for(int j = 0; j < paddingW; j++)
			padded_data[MAP(paddedDataW-paddingW+j, paddedDataH-paddingH+i, squareSize)] = data[MAP(dataW-1, dataH-1, dataH)];
}