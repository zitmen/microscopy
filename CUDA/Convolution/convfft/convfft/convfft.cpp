#include "convfft.h"

#include <cufft.h>
#include <cutil_inline.h>

#include "convolutionFFT2D_common.h"
#include "mex.h"

int snapTransformSize(int dataSize)
{
    int hiBit;
    unsigned int lowPOT, hiPOT;

    dataSize = iAlignUp(dataSize, 16);

    for(hiBit = 31; hiBit >= 0; hiBit--)
        if(dataSize & (1U << hiBit)) break;

    lowPOT = 1U << hiBit;
    if(lowPOT == (unsigned int)dataSize)
        return dataSize;

    hiPOT = 1U << (hiBit + 1);
    if(hiPOT <= 1024)
        return hiPOT;
    else 
        return iAlignUp(dataSize, 512);
}

// Parameters retval, retvalH and retvalW are outpu parameters.
// - memory for retval is allocated inside of this function, so you needs to put there address of a pointer, f.e.: float *result; int resH, resW; convolve(...,&result, resH, resW);
void convolveFFT2D(float *h_Data, int dataH, int dataW, float *h_Kernel, int kernelH, int kernelW, int kernelY, int kernelX, float **retval, int &retvalH, int &retvalW)
{
    float
        *h_ResultGPU;

    float
        *d_Data,
        *d_PaddedData,
        *d_Kernel,
        *d_PaddedKernel;

    fComplex
        *d_DataSpectrum,
        *d_KernelSpectrum;

    cufftHandle
        fftPlanFwd,
        fftPlanInv;

#ifdef _DEBUG
    unsigned int hTimer;
    cutilCheckError( cutCreateTimer(&hTimer) );

    mexPrintf("Testing built-in R2C / C2R FFT-based convolution\n");
#endif  // _DEBUG

        const int fftH = snapTransformSize(dataH + kernelH - 1);
        const int fftW = snapTransformSize(dataW + kernelW - 1);
        
#ifdef _DEBUG
    mexPrintf("...allocating memory\n");
#endif  // _DEBUG
        h_ResultGPU = (float *)mxMalloc(fftH    * fftW * sizeof(float));

        cutilSafeCall( cudaMalloc((void **)&d_Data,   dataH   * dataW   * sizeof(float)) );
        cutilSafeCall( cudaMalloc((void **)&d_Kernel, kernelH * kernelW * sizeof(float)) );

        cutilSafeCall( cudaMalloc((void **)&d_PaddedData,   fftH * fftW * sizeof(float)) );
        cutilSafeCall( cudaMalloc((void **)&d_PaddedKernel, fftH * fftW * sizeof(float)) );

        cutilSafeCall( cudaMalloc((void **)&d_DataSpectrum,   fftH * (fftW / 2 + 1) * sizeof(fComplex)) );
        cutilSafeCall( cudaMalloc((void **)&d_KernelSpectrum, fftH * (fftW / 2 + 1) * sizeof(fComplex)) );

#ifdef _DEBUG
    mexPrintf("...creating R2C & C2R FFT plans for %i x %i\n", fftH, fftW);
#endif  // _DEBUG
        cufftSafeCall( cufftPlan2d(&fftPlanFwd, fftH, fftW, CUFFT_R2C) );
        cufftSafeCall( cufftPlan2d(&fftPlanInv, fftH, fftW, CUFFT_C2R) );

#ifdef _DEBUG
    mexPrintf("...uploading to GPU and padding convolution kernel and input data\n");
#endif  // _DEBUG
        cutilSafeCall( cudaMemcpy(d_Kernel, h_Kernel, kernelH * kernelW * sizeof(float), cudaMemcpyHostToDevice) );
        cutilSafeCall( cudaMemcpy(d_Data,   h_Data,   dataH   * dataW *   sizeof(float), cudaMemcpyHostToDevice) );
        cutilSafeCall( cudaMemset(d_PaddedKernel, 0, fftH * fftW * sizeof(float)) );
        cutilSafeCall( cudaMemset(d_PaddedData,   0, fftH * fftW * sizeof(float)) );

        padKernel(
            d_PaddedKernel,
            d_Kernel,
            fftH,
            fftW,
            kernelH,
            kernelW,
            kernelY,
            kernelX
        );

        padDataClampToBorder(
            d_PaddedData,
            d_Data,
            fftH,
            fftW,
            dataH,
            dataW,
            kernelH,
            kernelW,
            kernelY,
            kernelX
        );

    //Not including kernel transformation into time measurement,
    //since convolution kernel is not changed very frequently
#ifdef _DEBUG
    mexPrintf("...transforming convolution kernel\n");
#endif  // _DEBUG
        cufftSafeCall( cufftExecR2C(fftPlanFwd, (cufftReal *)d_PaddedKernel, (cufftComplex *)d_KernelSpectrum) );

#ifdef _DEBUG
    mexPrintf("...running GPU FFT convolution: ");
#endif  // _DEBUG
        cutilSafeCall( cutilDeviceSynchronize() );
#ifdef _DEBUG
        cutilCheckError( cutResetTimer(hTimer) );
        cutilCheckError( cutStartTimer(hTimer) );
#endif  // _DEBUG
        cufftSafeCall( cufftExecR2C(fftPlanFwd, (cufftReal *)d_PaddedData, (cufftComplex *)d_DataSpectrum) );
        modulateAndNormalize(d_DataSpectrum, d_KernelSpectrum, fftH, fftW, 1);
        cufftSafeCall( cufftExecC2R(fftPlanInv, (cufftComplex *)d_DataSpectrum, (cufftReal *)d_PaddedData) );

        cutilSafeCall( cutilDeviceSynchronize() );
#ifdef _DEBUG
        cutilCheckError( cutStopTimer(hTimer) );
        double gpuTime = cutGetTimerValue(hTimer);
    mexPrintf("%f MPix/s (%f ms)\n", (double)dataH * (double)dataW * 1e-6 / (gpuTime * 0.001), gpuTime);

    mexPrintf("...reading back GPU convolution results\n");
#endif  // _DEBUG
        cutilSafeCall( cudaMemcpy(h_ResultGPU, d_PaddedData, fftH * fftW * sizeof(float), cudaMemcpyDeviceToHost) );

#ifdef _DEBUG
    mexPrintf("...shutting down\n");
        cutilCheckError( cutDeleteTimer(hTimer) );
#endif  // _DEBUG

        cufftSafeCall( cufftDestroy(fftPlanInv) );
        cufftSafeCall( cufftDestroy(fftPlanFwd) );

        cutilSafeCall( cudaFree(d_DataSpectrum)   );
        cutilSafeCall( cudaFree(d_KernelSpectrum) );
        cutilSafeCall( cudaFree(d_PaddedData)   );
        cutilSafeCall( cudaFree(d_PaddedKernel) );
        cutilSafeCall( cudaFree(d_Data)   );
        cutilSafeCall( cudaFree(d_Kernel) );

		// return
		(*retval) = h_ResultGPU;
		retvalH = fftH;
		retvalW = fftW;
}

void InitCUDA()
{
    // Use device with highest Gflops/s
    cudaSetDevice(cutGetMaxGflopsDeviceId());
}

void ClearCUDA()
{
    cutilDeviceReset();
}