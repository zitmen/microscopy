// TODO: alokace!

#include "mex.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "convfft.h"
#include "padding.h"

#define max(a,b) (((a) > (b)) ? (a) : (b))

// Those two flags determine whether the convolution would be computed on CPU or GPU (only one must be selected)
#define GPU
//#define CPU

void printHelp();
bool checkInput(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

// ------------------------------------------------------------------------
// MAIN 
// ------------------------------------------------------------------------
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[])
{
#ifdef GPU
	InitCUDA();
#endif  // GPU

    if(!checkInput(nlhs, plhs, nrhs, prhs))
        return;

	int data_height = mxGetM (prhs[0]);
	int data_width  = mxGetN (prhs[0]);
	float *data     = (float *)mxGetData(prhs[0]);
	
	int kernel_height = mxGetM (prhs[1]);
	int kernel_width  = mxGetN (prhs[1]);
	float *kernel   = (float *)mxGetData(prhs[1]);

	int paddingW = (kernel_width  / 2) + (kernel_width  % 2);
	int paddingH = (kernel_height / 2) + (kernel_height % 2);

	// PADDING
	int padded_data_height = data_height + 2*paddingH;	// top_border + height + bottom_border
	int padded_data_width  = data_width  + 2*paddingW;	// left_border + width + right_border
    // The matrix MUST be SQUARE, so let's make it happen:
    int square_size = max(padded_data_height, padded_data_width);

    // -- alloc & fill with zeros...it's the same as FFT padding
	float *padded_data = (float *)mxMalloc(sizeof(float) * square_size * square_size);	// alloc
	memset(padded_data, 0, sizeof(float) * square_size * square_size);	// fill with zeros
	// -- do the convolution padding
	if(mxIsSingle(prhs[2]))
	{
		float padding_val = mxGetScalar(prhs[2]);
		for(int i = 0; i < padded_data_height; i++)
			for(int j = 0; j < padded_data_width; j++)
				padded_data[j*square_size+i] = padding_val;
	}
	else	// if(mxIsChar(prhs[2]))
	{
		char *border_option = mxArrayToString(prhs[2]);
		if(border_option == NULL)
		{
			mexPrintf("convfft::mxArrayToString: unknown error!\n");
			return;
		}
		if(strcmp(border_option, "symmetric") == 0)
		    padding_symmetric(data_width, data_height, data, paddingW, paddingH, padded_data_width, padded_data_height, square_size, padded_data);
		else if(strcmp(border_option, "circular") == 0)
		    padding_circular(data_width, data_height, data, paddingW, paddingH, padded_data_width, padded_data_height, square_size, padded_data);
		else if(strcmp(border_option, "replicate") == 0)
		    padding_replicate(data_width, data_height, data, paddingW, paddingH, padded_data_width, padded_data_height, square_size, padded_data);
		else
		{
			mexPrintf("Unknown value of `border_option` parameter!\n\n");
			printHelp();
			return;
		}
	}
	// -- copy the original data into the padded area
	for(int i = 0; i < data_height; i++)
		for(int j = 0; j < data_width; j++)
			padded_data[(j+paddingW)*square_size + (i+paddingH)] = data[j*data_height + i];

#ifdef _DEBUG
    mexPrintf("These are the padded data (not squared - just to see the kernel padding):\n");
    for(int i = 0; i < padded_data_height; i++)
    {
		for(int j = 0; j < padded_data_width; j++)
			mexPrintf("%f,", padded_data[j*square_size + i]);
        mexPrintf("\n");
    }
    mexPrintf("Square size = %d\n", square_size);
#endif  // _DEBUG

#ifdef GPU
    // run FFT-based convolution on GPU
	int resultW, resultH;
	float *result = NULL;	// result is padded (bacause of the FFT) to a power of 2
	convolveFFT2D(padded_data, square_size, square_size, kernel, kernel_height, kernel_width, paddingH, paddingW, &result, resultH, resultW);
	if(!result)
	{
		mexPrintf("ERROR!\n");
		return;
	}

    #ifdef _DEBUG
        mexPrintf("This is result of FFT:\n");
        for(int i = 0; i < resultH; i++)
        {
		    for(int j = 0; j < resultW; j++)
			    mexPrintf("%f,", result[j*resultH + i]);
            mexPrintf("\n");
        }
    #endif

#endif  // GPU

	// return data back to MATLAB
	// - create a M-by-N real float (single)
    plhs[0] = mxCreateNumericMatrix(data_height, data_width, mxSINGLE_CLASS, mxREAL);
    float *retval = (float *)mxGetData(plhs[0]);

#ifdef GPU
	// - copy data out to MATLAB without any padding (both kernel and FFT)
	// -- kernel-padded data are aligned at top left corner and rest of the memory is filled with zeros
	for(int i = 0; i < data_height; i++)
		for(int j = 0; j < data_width; j++)
			retval[j*data_height + i] = result[(j+paddingW-1)*resultH + (i+paddingH-1)];	// I don't really know why it's -1, but it works fine ;)
#endif  // GPU
#ifdef CPU
	// CPU based convolution
	convolutionClampToBorderCPU(ret_ptr, padded_data, kernel, padded_data_height, padded_data_width, kernel_height, kernel_width, paddingH, paddingW);
#endif  // CPU

	//mxFree(data);		// causes segfault? -- MATLAB is most likely to dealloc this
    //mxFree(kernel);	// causes segfault? -- MATLAB is most likely to dealloc this
	mxFree(padded_data);

#ifdef GPU
    mxFree(result);
	ClearCUDA();
#endif  // GPU
}

// ------------------------------------------------------------------------
// PRINT HELP
// ------------------------------------------------------------------------
void printHelp()
{
	mexPrintf("Usage: convfft(image, [convolution_kernel], [boundary_option]);\n");
	mexPrintf("- parameters [param_name] are optional\n");
	mexPrintf("- parameter convolution_kernel must be specified at least in the first call of the convfft function!\n");
	mexPrintf("- parameter boundary_option can be one of the following values: X, 'symmetric', 'replicate', 'cirtular'\n");
	mexPrintf("-- X: Input array values outside the bounds of the array are implicitly assumed to have the value X. When no boundary option is specified, imfilter uses X = 0.\n");
	mexPrintf("-- 'symmetric': 	Input array values outside the bounds of the array are computed by mirror-reflecting the array across the array border.\n");
	mexPrintf("-- 'replicate': 	Input array values outside the bounds of the array are assumed to equal the nearest array border value.\n");
	mexPrintf("-- 'circular': Input array values outside the bounds of the array are computed by implicitly assuming the input array is periodic.\n");
}

// ------------------------------------------------------------------------
// CHECK INPUT 
// ------------------------------------------------------------------------
bool checkInput(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[])
{
    // Test number of input arguments
	if((nrhs < 2) || (nrhs > 3))
	{
		mexPrintf("Wrong number of parameters!\n\n");
		printHelp();
		return false;
	}
	if(nlhs > 1)
	{
		mexPrintf("This function returns only one value!\n");
		return false;
	}
	if(!mxIsSingle(prhs[0]) || !mxIsSingle(prhs[1]))
	{
		mexPrintf("The function expects arguments `image` and `convolution_kernel` to be SINGLE!\n");
		return false;
	}
	if(mxIsSingle(prhs[2]))
	{
		if((mxGetM(prhs[2]) != 1) || (mxGetN(prhs[2]) != 1))
		{
			mexPrintf("If the `boundary_option` is SINGLE then it must be a single value (1x1 matrix)!\n");
			return false;
		}
	}
	else if(!mxIsChar(prhs[2]))	// !string && !single
	{
		mexPrintf("The function expects argument `boundary_option` arguments to be STRING or SINGLE!\n");
		return false;
	}
    return true;
}