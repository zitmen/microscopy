#ifndef _PADDING_H_
#define _PADDING_H_

// map 2D array indices into 1D array
#define MAP(x,y,height) ((x)*(height)+(y))

// padding methods
void padding_symmetric(int dataW, int dataH, const float *data, int paddingW, int paddingH, int paddedDataW, int paddedDataH, int squareSize, float *padded_data);
void padding_circular (int dataW, int dataH, const float *data, int paddingW, int paddingH, int paddedDataW, int paddedDataH, int squareSize, float *padded_data);
void padding_replicate(int dataW, int dataH, const float *data, int paddingW, int paddingH, int paddedDataW, int paddedDataH, int squareSize, float *padded_data);

#endif  // _PADDING_H_