#ifndef _CONV_FFT_H_
#define _CONV_FFT_H_

void InitCUDA();
void convolveFFT2D(float *h_Data, int dataH, int dataW, float *h_Kernel, int kernelH, int kernelW, int kernelY, int kernelX, float **retval, int &retvalH, int &retvalW);
void ClearCUDA();

#endif  // _CONV_FFT_H_