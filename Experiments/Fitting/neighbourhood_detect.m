%{
Comparison of maxima detectors
==============================
MORPHO=morpho_detect(imread('snowflakes.png'), 5, 100);
N4=neighbourhood_detect(imread('snowflakes.png'), false, 100);
N8=neighbourhood_detect(imread('snowflakes.png'), true, 100);

if N4==N8; ok=1; else ok=0; end;
if MORPHO==N8; ok=1; else ok=0; end;
if MORPHO==N4; ok=1; else ok=0; end;

==> Result: N4 != N8 != MORPHO...not even for MORPHO::radius = {1,3,5}
%}
function [cimmx] = neighbourhood_detect(cim, eight_neighbours, thresh)

    cimmx = false(size(cim));
    xm = size(cim,1);
    ym = size(cim,2);
    for xi=1:xm
        for yi=1:ym
            maximum = true;
            if xi >  1 && cim(xi-1,yi  ) > cim(xi,yi); maximum = false; end;
            if xi < xm && cim(xi+1,yi  ) > cim(xi,yi); maximum = false; end;
            if yi >  1 && cim(xi  ,yi-1) > cim(xi,yi); maximum = false; end;
            if yi < ym && cim(xi  ,yi+1) > cim(xi,yi); maximum = false; end;
            %
            if eight_neighbours
                if xi >  1 && yi >  1 && cim(xi-1,yi-1) > cim(xi,yi); maximum = false; end;
                if xi >  1 && yi < ym && cim(xi-1,yi+1) > cim(xi,yi); maximum = false; end;
                if xi > xm && yi >  1 && cim(xi+1,yi-1) > cim(xi,yi); maximum = false; end;
                if xi > xm && yi < ym && cim(xi+1,yi+1) > cim(xi,yi); maximum = false; end;
            end
            %
            cimmx(xi,yi) = maximum && cim(xi,yi) > thresh;
        end
    end

end