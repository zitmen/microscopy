% cim = obrazek, radius = velikost masky pro dilataci, thresh = threshold
% Funkce vraci binarni obrazek (0/1); pro ziskani seznamu souradnic staci
% zavolat [r,c] = find(cimmx);
%
% example: imshow(morpho_detect(imread('snowflakes.png'), 5, 100))
function [cimmx] = morpho_detect(cim, radius, thresh)
    
    % Extract local maxima by performing a grey scale morphological
    % dilation and then finding points in the corner strength image that
    % match the dilated image and are also greater than the threshold.
    
    mx = imdilate(cim,strel('square',radius)); % Grey-scale dilate.
    % --> to je ekvivalent volani:
    %sze = 2*radius+1;                   % Size of dilation mask.
    %mx = ordfilt2(cim,sze^2,ones(sze)); % Grey-scale dilate.
    % --> dilatacni maska je matice jednicek (radius x radius)

    % Make mask to exclude points within radius of the image boundary. 
    bordermask = zeros(size(cim));
    bordermask(radius+1:end-radius, radius+1:end-radius) = 1;
    
    % Find maxima, threshold, and apply bordermask
    cimmx = (cim==mx) & (cim>thresh) & bordermask;
    
end