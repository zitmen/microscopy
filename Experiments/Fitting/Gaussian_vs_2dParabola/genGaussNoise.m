function noise = genGaussNoise(cfg, mean, sigma)

% random data from normal distribution
noise = sigma .* randn(cfg.imsize) + mean;

% save to file
imwrite(uint16(65535*noise),[cfg.datapath 'noise_sigma-' num2str(sigma) '.tiff'],'Compression','none');
