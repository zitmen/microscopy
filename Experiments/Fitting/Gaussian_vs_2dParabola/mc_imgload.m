function images = mc_imgload(cfg, noise)

images = cell(cfg.numimgs, 1);
norm = double(2^cfg.bitdepth);

% load all images
for frame=1:cfg.numimgs
    images{frame} = double(imread(([cfg.datapath cfg.filename '.tiff']), frame)+noise)/norm;
end


% save image + noise
%image = uint16(65535*img.imraw);
%imwrite(image,[cfg.datapath 'noisy-image_sigma-' noise_sigma_str '.tiff'],'Compression','none');