function [results] = compute(cfg,images,params_cells,noise_sigma)

fprintf(['Running ' cfg.fitmethod ' fitting method...' ]);

errors = cell(cfg.numimgs, 1);

%matlabpool local 8
parfor frame = 1:cfg.numimgs
%for frame = 1:cfg.numimgs
    
    %addpath('..\..\OriginalWork\utils');
    
    % detect molecules
    [loc,imb] = mc_molecule_detect(images{frame}, cfg);

    % store image
    [imbuf,loc] = mc_molecule_storeim(frame, images{frame}, loc, [], cfg);

    % fit model
    fits = molecule_fitting(images{frame}, imbuf, [], cfg);
    
    % evaluate results for the current noise level
    errors{frame} = evaluate_fit(noise_sigma, params_cells{frame}, fits);

    %progress
    %if mod(img.frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end

results.errors = errors;

%intervals = [0:0.5:10];
intervals = [0:1:10];
errors = cell2mat(results.errors);
results.means = zeros(size(intervals,2)-1,1);
results.stds = zeros(size(intervals,2)-1,1);
for i=1:(size(intervals,2)-1)
    errs = errors(errors(:,1)>intervals(i) & errors(:,1)<=intervals(i+1),2);
    results.means(i) = mean(errs);
    results.stds(i) = std(errs);
    errs = errs(find(errs<=(2.7*results.stds(i)))); % remove outliers
    results.means(i) = mean(errs);  % recalc
    results.stds(i) = std(errs);    % recalc
end

fprintf('done\n');
