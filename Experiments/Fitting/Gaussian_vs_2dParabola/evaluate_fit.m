% The method evaluates precision of the fits related to the
% generated data. If there is a FN or FP, it's skipped, because
% this method does not evaluate localization results.
%
% Algorithm:
%   1. make pairs of [orig fits] and discard FP and FN molecules
%   2. square root errors for x, y, A
%   3. store the result for each molecule for later
%
% Params:
%   noise_sigma - sigma parameter of the noise
%   orig        - original data parameters - x0, y0, A0
%   fits        - approximation of the patemeters - x0, y0, A0
%
% Returns:
%   Returns Square Root Error of estimates of X, Y amd Amplitude for each molecule.
function [SRE] = evaluate_fit(noise_sigma, orig, fits)

	% pairing of the molecules
	O = orig;
	F = fits.fits;
	
	%     x       y       A
	o = [ O(:,1), O(:,2), O(:,4) ];
	f = [ F(:,1), F(:,2), F(:,3) ];
    
    % find the closest candidates
	[D,I] = pdist2(o,f,'euclidean','Smallest',1); DI = [D',I'];
	% remove FN
	[D,I] = sortrows(DI,1); DI = [D,I]; % sort by euclidean distance
	[C,ia,ic] = unique(DI(:,2),'first'); DI = DI(ia,:);
	% remove FP
	[D,I] = sortrows(DI,1); DI = [D,I]; % sort by euclidean distance
	[C,ia,ic] = unique(DI(:,3),'first'); DI = DI(ia,:);
	% apply changes (FP,FN) to the data
	o = o(DI(:,2),:);
	f = f(DI(:,3),:);
	
	% [ Signal to Noise Ratio , Square Root Error   ] for each molecule
	%SRE = [ o(:,3) / noise_sigma, sqrt(sum((o-f).^2,2)) ];
    SRE = [ o(:,3) / noise_sigma, sqrt(sum((o(:,1:2)-f(:,1:2)).^2,2)) ];
end