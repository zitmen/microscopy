function [fits, imbuf] = molecule_fitting(img, imbuf, fits, cfg)

persistent X;

if isempty(fits)
  resupdate('free');
  fits = struct('num', 0, 'discard', 0); X = [];
  [X(:,:,2),X(:,:,1)] = meshgrid(-cfg.box:cfg.box,-cfg.box:cfg.box);
end

if strcmp(cfg.fitmethod, 'GAUSSIAN')
  % find molecules that disapeared
  idx = find(cat(1,imbuf.active) == 0);
  npts = (2*cfg.box+1)^2;

  for I = 1:length(idx)
    
    % "pointer" to current detection buffer
    tmp = imbuf(idx(I));
    
    % fit 2D gaussian bell shaped function
    %    [ x,  y, sigma       , Amplitude , background]
    a0 = [ 0,  0, cfg.sigmapsf, max(tmp.im(:)), min(tmp.im(:))];
    %[a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);  
    [a,exitflag,chi2] = lmfit('fun_gauss2D',a0, reshape(X(:),[npts, 2]), tmp.im(:));
      
    % position
    a(1:2) = a(1:2)+ tmp.pos;
    a(4) = a(4) * 2*pi*a(3)^2;
    
    % save result
    fits.num = resupdate('add', single([ a(1), a(2), a(4) ]));
  end
elseif strcmp(cfg.fitmethod, 'PARABOLA')
    pos = cat(1, imbuf.pos);
    [xs, ys, As] = subpix2d(pos(:,1), pos(:,2), img, cfg.box);
    a = [ xs, ys, As ];
      
    % save result
    fits.num = size(a, 1);
    for I = 1:fits.num
        resupdate('add', single(a(I,:)));
    end
else
      error(strcat('Method "', cfg.fitmethod, '" does not exist!'));
end
  
  %save results
  fits.fits = resupdate('save');

function y = gauss2d(X,a)

y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);

