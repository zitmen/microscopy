% Test performance of the localization on the static dots

addpath('..\..\OriginalWork\utils');
clear

cfg = cfg_mcstatic();

% initialize
img = mc_imginit(cfg);
%resupdate('free');  % ????

% load parameters
load([cfg.datapath cfg.filename '.mat']);

disp(cfg.method(cfg.methodrun).name);
fprintf('Analyzing\nProgress ');

% load noisy background
noise_sigma = 0.05;
noise_sigma_str = '0.05';
imgbkg = imread([cfg.datapath 'noise_sigma-' noise_sigma_str '.tiff']);

%cfg.numimgs = 8; %%debug

% load all images at once
images = mc_imgload(cfg, imgbkg);

cfg.fitmethod = 'PARABOLA';
res_par = compute(cfg, images, params_cells, noise_sigma);
errorbar(res_par.means,res_par.stds,'ob')
hold on
cfg.fitmethod = 'GAUSSIAN';
res_gauss = compute(cfg, images, params_cells, noise_sigma);
errorbar(res_gauss.means,res_gauss.stds,'or')
hold off
legend('Parabola','Gaussian')
%axis([0 10 -10 20])
xlabel('Signal to Noise Ratio');
ylabel('Square Root Error');
title(['Dependence of fitting methods error on a SNR.']);

%par_errors = cell2mat(res_par.errors);
%plot(par_errors(:,1),par_errors(:,2),'xb')
%axis([0 10 0 150])
%gauss_errors = cell2mat(res_gauss.errors);
%plot(gauss_errors(:,1),gauss_errors(:,2),'xr')
%axis([0 10 0 150])
