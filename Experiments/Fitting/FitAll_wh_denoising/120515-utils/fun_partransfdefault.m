function varargout = fun_paramtransfdefault(par)
% like num2cell
varargout = cell(size(par));
for I = 1:numel(par)
    varargout{I} = par(I);
end 
