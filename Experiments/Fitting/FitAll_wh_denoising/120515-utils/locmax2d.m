function [val,idx] = locmax2d(x, thr, mindist)
% Find local maxima in a 4-neighbourhood
%
% USE:
%   [val,idx] = locmax2d(X)               ... all local maxima
%   [val,idx] = locmax2d(X, thr)          ... local maxima above threshold
%   [val,idx] = locmax2d(X, thr, mindist) ... local maxima above threshold and with minimum distance
%
% IN:
%   x       ... [m x n] matice
%   thr     ... local maxima threshold 
%   mindist ... local maxima minimum distance
%
% OUT:
%  val      ... functional values
%  idx      ... index coordinates

% 23.4.2010, PK, using "findlocmax2d.mexw32" makes it about 15x faster
% 6.10.2009, PK

[m,n] = size(x);

% find all local maxima in 4-neighbourhood
idx = 1:(size(x,1)*size(x,2));

% sort maxima according to their value
[foo,sortidx] = sort(x(idx),'descend');
idx = double(idx(sortidx));

% functional value of points
val = x(idx);

%eof