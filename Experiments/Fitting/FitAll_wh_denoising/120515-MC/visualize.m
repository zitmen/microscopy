function visualize(img, results)

   figure(12); clf
   hold on
   imagesc(img.imraw,[0 0.1]);
   colormap gray(256)
   axis ij image off;
   set(gca,'Position',[0 0 1 1])
   
   if ~isempty(results.ok)
    ok = results.ok(1:results.cok,12:13);    % xo,yo
    plot(ok(:,2),ok(:,1),'go','MarkerSize',10)    % green  - correct localization
   end
   
   if ~isempty(results.fp)
    fp = results.fp(1:results.cfp,1:2);      % xl,xl
    plot(fp(:,2),fp(:,1),'ro','MarkerSize',10)    % red    - localized point does not exist!
   end
   
   if ~isempty(results.fn)
    fn = results.fn(1:results.cfn,1:2);      % xo,yo
    plot(fn(:,2),fn(:,1),'yo','MarkerSize',10)    % yellow - point was not detected!
   end
 
   drawnow;
