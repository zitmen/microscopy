function [imbuf,location] = mc_molecule_storeim2(img, loc, cfg)

% imbuf.active = -1         ... free buffer index
% imbuf.active =  0         ... ready for fitting
% imbuf.active =  1         ... active item
% imbuf.active =  2,3,4,..  ... timeout - 

box = -cfg.box:cfg.box;

im = img.imraw;

% remove points which are at the border location
valid = ~( (loc(:,1)-cfg.box) < 1 | (loc(:,2)-cfg.box) < 1 | ...
           (loc(:,1)+cfg.box) > img.siz(1) | (loc(:,2)+cfg.box) > img.siz(2) );
loc = loc(valid,:);
%loc = loc(1:cfg.numdots,:);
numloc = size(loc,1);
location = loc;

% list of image boxes for all detected points 
imbuf = struct('im',{}, 'pos', {}, 'frame', {});
for I = 1:numloc
    
    pt = loc(I,1:2);
     
    % image and bacground in the box at position [u,v]
    imbuf(I,1).im = im(pt(1)+box,pt(2)+box);
    imbuf(I).pos = pt;
    imbuf(I).frame = img.frame;  
end

%eof