function  results = mc_evaluate3(img, loc, fits, results, cfg)
%%

thr = 4;%(3)^2;%4;%(3)^2;

npts = size(img.params,1);
nloc = size(loc,1);
nfits = size(fits.fits,1);

% allocate memory
if isempty(results)  
  %results.ok = zeros(2000000,15);  % [xl yl Al xf yf sf Af bf chi2 stdb Nim xo yo so Ao]; dot was generated and corectly located
  results.ok = [];  % [xl yl Al xf yf sf Af bf chi2 stdb Nim xo yo so Ao]; dot was generated and corectly located
  results.cok = 0;
  %results.fp = zeros(1000000,11);  % [xl yl Al xf yf sf Ab bf chi2 stdb Nim]; type I error, dot was located but not generated
  results.fp = [];  % [xl yl Al xf yf sf Ab bf chi2 stdb Nim]; type I error, dot was located but not generated
  results.cfp = 0;
  %results.fn = zeros(1000000,4);   % [xo yo so Ao]; type II error, dot was generated but not located
  results.fn = [];   % [xo yo so Ao]; type II error, dot was generated but not located
  results.cfn = 0;
end

% assign fits to location
locfit = NaN(nloc,11);
locfit(:,1:3) = loc;
for I = 1:nfits
  tmp = fits.fits(I,:);
  dst = ptdist2(tmp(1:2), loc);
  [val,idx] = min(dst);
  locfit(idx,4:11) = tmp([1:6 9 7]);
end

% for all localized dots
p = zeros(npts,1);
for I = 1:nloc

  % assign localized position to generated position
  
  % ---------------------------------------------------------
  % fitting failed -> false negatives       (example: 500 localized -> 392 fitted -> 391 OK, 1 FP, 109 FN)
%   if isnan(locfit(I,4)), continue; end;
%   dst = ptdist2(locfit(I,4:5), img.params);
  % ---------------------------------------------------------
  % detectin only                           (example: 500 localized -> 397 OK, 103 FP, 103 FN)
  dst = ptdist2(locfit(I,1:2), img.params);
  % ---------------------------------------------------------
    
  [val,idx] = min(dst);
 
  % false positives - out of localization radius
  if val > thr,
    results.cfp = results.cfp+1;
    results.fp(results.cfp,:) = locfit(I,:);
    continue;
  end
  
  % count OK
  results.cok = results.cok+1;
  results.ok(results.cok,:) = [locfit(I,:) img.params(idx,:)];
  
  % track localized points
  p(idx) = p(idx)+1;
  
end

% false negatives - dots were not found
num = sum(~p);
results.fn(results.cfn+(1:num),:) = img.params(~p,:);
results.cfn = results.cfn + num;

