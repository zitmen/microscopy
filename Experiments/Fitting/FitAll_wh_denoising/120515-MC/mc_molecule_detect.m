function [loc,proctime] = mc_molecule_detect(img, cfg)
%function [loc,thr] = mc_molecule_detect(img, cfg)

% remove noise
t = clock;
evalc(cfg.method(cfg.methodrun).exec);
proctime = etime(clock,t);

% candidate pixels for a molecule
% find local maxima above threshold + min. distance + sort
[val,idx] = locmax2d(imb);%, thr, cfg.mindist);
% !!! THR !!!
thr = 0.005;%val(round(3*cfg.numdots));    % vezme 1500tej neijintenzivnejsi pixel a pouzije ho jako threshold
idx = idx(val > thr);
%idx = circfilter(idx, img.siz, cfg.mindist); 
[u,v] = ind2sub(img.siz,idx);
loc = [u', v', imb(idx)'];
 
% if cfg.setup || (img.frame < cfg.displayfirst) || (mod(img.frame,cfg.displayevery) == 0)
%   h=figure(12); clf
%   hold on
%   imagesc(img.imraw,[0 0.1]);%0.95*max(img.im(:))])
%   %  imagesc(imb,[0 0.03])
%   colormap gray(256)
%   axis ij image off;
%   set(gca,'Position',[0 0 1 1])
%   plot(v,u,'ro','MarkerSize',20) 
% 
%   drawnow;
% text(10,10,sprintf('thr=%.3e\nframe: %4d',loc.thr.mu,img.frame),'Color','y','VerticalAlignment','top')
% end
