function res = render_hist3(results, cfg)


if (cfg.render.average < 1)
  error('render_hist: cfg.render.average must be >= 1');
end

% rendering rules & decode results
[xpos,ypos,zpos,sigmaa,sigmab,N,b,dx,dz,a,s,chi2,SNR,idx] = render_rules3(results, cfg.render);
npts = size(xpos,1);

%% generate histograms
%  dx = 20;
dxstd = dx/a;
%dxstd = sqrt(2*log(2))*dx/a;

newimsize = round(cfg.render.scale * results.imsize);% - cfg.render.scale + 1;
%histogram edges
xbin = linspace(0,results.imsize(2),newimsize(2)+1);
ybin = linspace(0,results.imsize(1),newimsize(1)+1);
zbinw = diff(cfg.render.zlim(1:2));
zbin = [cfg.render.zlim, cfg.render.zlim(end)+zbinw]-zbinw/2;
numz = length(zbin)-1;

fprintf('Generating %d histogram(s)\n', cfg.render.average);
fprintf('Progress ');
IM = zeros([newimsize numz]+1,'uint16');
for I = 1:cfg.render.average

  xp = xpos;
  yp = ypos;
  zp = zpos;
  
  % random jitter - add normal noise to localized points
  if (cfg.render.average > 1)
    xp = xp + dxstd .* randn(npts,1);
    yp = yp + dxstd .* randn(npts,1);
    zp = zp + dz .* randn(npts,1);
  end
 
  % accumulate histograms
  IM = IM + hist3(yp,xp,zp,ybin,xbin,zbin);
  
  if (mod(I,round(cfg.render.average/10)) == 0), fprintf('.'); end;   
end
fprintf('\n');
% compute average
if cfg.render.average > 1
   IM = IM/cfg.render.average;
end


%% results
res.name = 'hist3d';
res.im = IM;

