#include "mex.h"
#include "dct.h"
#include "triangles.h"

// im = delaunay([yp,xp], [1 1 imsize], newimsize);

void read_points(uint32 npts, float *ptx, float *pty);
//void plot_triangles(uint32 n, Triangles *TRI);
void plot_triangles(Triangles *TRI);
float getarea(pointf *pts);

/* ------------------------------------------------------------------------ */
// M A I N
/* ------------------------------------------------------------------------ */

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	
    uint32	i, npts;		// number of data points
	float	*ptx, *pty;		// data pointers
	double *roi = NULL, *imsize = NULL;

	Triangles *TRI;

	edge *l_cw, *r_ccw;
	point **p_sorted, **p_temp;
	
	// check number of I/O arguments
	if (nrhs != 3)
		mexErrMsgTxt("Wrong number of input arguments.");
	if ( (nlhs < 1) || (nlhs > 3) )
		mexErrMsgTxt("Wrong number of output arguments.");

	// initialize input parameters
	if ( mxIsEmpty(prhs[0]) || !mxIsSingle(prhs[0]) )
		mexErrMsgTxt("Points must be stored in a single matrix.");
	if (mxGetN(prhs[0]) != 2)
		mexErrMsgTxt("Points must be 2D data.");
	if ((npts = mxGetM(prhs[0])) <= 3)
		mexErrMsgTxt("Number of points has to be greater than 3.");

	// get ROI
	if ( !mxIsDouble(prhs[1]) || (mxGetNumberOfElements(prhs[1]) != 4) )
		mexErrMsgTxt("ROI must be a vector [x, y, w, h].");
	roi = mxGetPr(prhs[1]);	
	if ( (roi[2] <= 0) || (roi[3] <=0 ) )
		mexErrMsgTxt("Not a valid ROI.");

	// get output image size
	if ( !mxIsDouble(prhs[2]) || (mxGetNumberOfElements(prhs[2]) != 2) )
		mexErrMsgTxt("imsize must be a vector [w, h].");
	imsize = mxGetPr(prhs[2]);
	if ( (imsize[0] <= 0) || (imsize[1] <=0 ) )
		mexErrMsgTxt("Not a valid imsize.");

	// read data points
    ptx = (float *) mxGetData(prhs[0]);
	pty = ptx + npts;
	alloc_memory(npts);
	read_points(npts, ptx, pty);

	// sort
	p_sorted = (point **)mxMalloc(npts*sizeof(point *));
	if (p_sorted == NULL)
		mexErrMsgTxt("triangulate: not enough memory\n");
	p_temp = (point **)mxMalloc(npts*sizeof(point *));
	if (p_temp == NULL)
		mexErrMsgTxt("triangulate: not enough memory\n");
	for (i = 0; i < npts; i++)
		p_sorted[i] = p_array + i;
	merge_sort(p_sorted, p_temp, 0, npts-1);
	mxFree((char *)p_temp);

	// create triangulation
	divide(p_sorted, 0, npts-1, &l_cw, &r_ccw);

	// plot traingles
	plhs[0] = mxCreateNumericMatrix((int) imsize[0], (int) imsize[1], mxSINGLE_CLASS, mxREAL);
	TRI = new Triangles(npts, ptx, pty);
	TRI->SetROI(roi, matrixf((float *) mxGetData(plhs[0]), (int) imsize[0], (int) imsize[1], (int) imsize[0]));
	plot_triangles(TRI);
	
	// free memory
	delete TRI;
	mxFree((char *)p_sorted);
	free_memory();
}

// ---------------------------------------------------------------------------
void read_points(uint32 npts, float *ptx, float *pty)
{
	for (uint32 i = 0; i < npts; i++)
	{
		// read x&y
		p_array[i].x = *ptx++;
		p_array[i].y = *pty++;

		// Initialise entry edge pointers
		p_array[i].entry_pt = NULL;
	}	
} 

// ---------------------------------------------------------------------------
void plot_triangles(Triangles *TRI)
{
  edge *e_start, *e, *next;
  point *u, *v, *w;
  uint32 i, n = TRI->NumPts();
  point *t;
  uint32 index[3];
  pointf pts[3];
  float area;

  for (i = 0; i < n; i++) {
    u = &p_array[i];
    e_start = e = u->entry_pt;
    do
    {
      v = Other_point(e, u);
      if (u < v) {
		  next = Next(e, u);
		  w = Other_point(next, u);
		  if (u < w)
			  if (Identical_refs(Next(next, w), Prev(e, v))) {  
				  // Triangle
				  if (v > w) { t = v; v = w; w = t; }
				  // mexPrintf("%d %d %d\n", u - p_array, v - p_array, w - p_array);
				  index[0] = u - p_array;
				  index[1] = v - p_array;
				  index[2] = w - p_array;
				  TRI->getpoints(index, pts);
				  area = getarea(pts);
				  //if (area < 1E-6)
				  	//  area = (float) 1E-6;
				  TRI->Plot(pts,1/area);
			  }
	  }
      /* Next edge around u. */
      e = Next(e, u);
    } while (!Identical_refs(e, e_start));
  }
} 

// ---------------------------------------------------------------------------
// compute area of a triangle using semi-perimeter
float getarea(pointf *pts)
{
	float a,b,c,s;

	a = Dist(pts[0], pts[1]);
	b = Dist(pts[1], pts[2]);
	c = Dist(pts[0], pts[2]);
	s = 0.5f * (a + b + c);
	return sqrt(s * (s - a) * (s - b) * (s - c));
}
