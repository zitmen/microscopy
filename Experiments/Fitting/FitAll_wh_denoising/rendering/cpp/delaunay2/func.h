//	Useful global functions

#ifndef __FUNC_H
#define __FUNC_H

#include <math.h>
/* ------------------------------------------------------------------------- */

// return minimal value

template <class Type>
inline Type Min(Type a, Type b)
{
	return (a < b) ? a : b;
}

/* ------------------------------------------------------------------------- */

// return maximal value

template <class Type>
inline Type Max(Type a, Type b)
{
	return (a > b) ? a : b;
}

/* ------------------------------------------------------------------------- */

// crop value into the interval

template <class Type>
inline Type Crop(Type x, Type a, Type b)
{
	return (x < a) ? a : ((x > b) ? b : x);
}

/* ------------------------------------------------------------------------- */

// swap two values

template <class Type>
inline void Swap(Type & a, Type & b)
{
	Type t = a;

	a = b;
	b = t;
}

/* ------------------------------------------------------------------------- */

// return average value

template <class Type>
inline Type Avg(Type a, Type b)
{
	return (a + b)/2;
}

/* ------------------------------------------------------------------------- */

// square function

template <class Type>
inline Type Sqr(Type x)
{
	return x*x;
}

/* ------------------------------------------------------------------------- */

// signum function

template <class Type>
inline int Sgn(Type x)
{
	return (x < 0) ? -1 : 1;
}

/* ------------------------------------------------------------------------- */

// absolute value function

template <class Type>
inline Type Abs(Type x)
{
	return (x < 0) ? -x : x;
}

/* ------------------------------------------------------------------------- */

// provide faster specialization for floats

template <>
inline float Abs(float x)
{
	return fabsf(x);
}

/* ------------------------------------------------------------------------- */

// provide faster specialization for doubles

template <>
inline double Abs(double x)
{
	return fabs(x);
}

/* ------------------------------------------------------------------------- */

// round positive "value" to nearest higher boundary of positive "modul"

inline int RoundUp(int value, unsigned modul)
{
	return modul*((value+modul-1)/modul);
}

/* ------------------------------------------------------------------------- */

// round positive "value" to nearest lower boundary of positive "modul"

inline int RoundDown(int value, unsigned modul)
{
	return modul*((value)/modul);
}

/* ------------------------------------------------------------------------- */

// round double value to nearest integer one

inline int Round(double value)
{
	return int(floor(value+0.5));
}

/* ------------------------------------------------------------------------- */

// calculate largest common divisor (faster a bit if a >= b)

inline unsigned ComDiv(unsigned a, unsigned b)
{
	// simple euclidean algorithm

	for(;;)
	{
		if(!(a %= b))
			return b;

		if(!(b %= a))
			return a;
	}
}

/* ------------------------------------------------------------------------- */

// calculate smallest common multiplication 

inline unsigned ComMult(unsigned a, unsigned b)
{
	// this may overflow if a and/or b are too large!

	return a*b/ComDiv(a,b);
}

/* ------------------------------------------------------------------------- */

#endif
