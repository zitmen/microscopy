function cfg = cfg_v3
% Config file for Matlab release 3

% cfg = cfg_v2_nmeth();
%cfg = cfg_v2_ACTIN11();
cfg = cfg_v1_caveolin();
% cfg = cfg_v2_ACTIN11_9x9();
%cfg = cfg_v2_PALM17();
% cfg = cfg_v2_tubulinblink1();
%cfg = cfg_v2_tubulinblink2();
% cfg = cfg_v2_tubulinblink3();
% cfg = cfg_v2_tubulinblink4();

%cfg = cfg_v2_atto565phalloidin01();

% cfg = cfg_IGF1Ra_serum01();
%cfg = cfg_IGF1Ra_serum02();
%cfg = cfg_IGF1Ra_serum_starved01();
%cfg = cfg_IGF1Ra_serum_starved02();
%cfg = cfg_IGF1Ra_serum_starved03();

%cfg = cfg_IRa_serum01();
%cfg = cfg_IRa_serum02();

%cfg = cfg_IRa_serum_starved01();
%cfg = cfg_IRa_serum_starved02();

% cfg = cfg_phalloidin_serum_starved01();

%cfg = cfg_IRa_ss02();

%cfg = cfg_newErbB3_08();
%cfg = cfg_newErbB3_12();
