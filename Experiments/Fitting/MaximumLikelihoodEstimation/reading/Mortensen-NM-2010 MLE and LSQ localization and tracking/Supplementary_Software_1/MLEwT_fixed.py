# Python (python.org) code for the calculation of the theoretical PSF
# (Eq.(3) in Mortensen et al.) for fixed dipoles, maximum likelihood
# estimator for the position and orientation of the probe,
# and the calculation of the covariance matrix for the estimated
# parameters.
# Uses matplotlib, scipy.
#
#
# 16th of December 2009
#
# Kim I. Mortensen

from scipy.optimize import *
from pylab import *
from scipy.special import i1,jn,gamma
from scipy.stats import erfc
from numint import *

def erf(x):
    return 1-erfc(x)

def gauss(x,s):
    return exp(-x**2/(2*s**2))/sqrt(2*pi*s**2)

def fac(n):
    return gamma(n+1)

class dipdistr:

    """

    Calculates the theoretical point spread function (PSF) for fixed dipoles.
    
    The PSF is the distribution of photons in the image plane from
    a fixed dipole emitter close to the coverslip surface,
    when imaged by a large aperture objective.

    Input:
    -------------------------------------------------------------------------
    
    wl (float) : Wavelength [nm] of emission peak in buffer.
    NA (float) : Numerical aperture of the objective
    n (float) : Refractive index of glass/immersion oil
    n0 (float) : Refractive index of buffer (water)
    M (float) : Magnification of the objective
    
    Functions:
    -------------------------------------------------------------------------
    PSF_exact (float) : Takes the coordinates in the image and the angles of the
    probe. Returns the value of the exact PSF at that point.

    PSF_approx (float): Takes the coordinates in the image and the angles of the
    probe. Returns the value of the approximated PSF at that point.

    """

    def __init__(self,wl,n,n0,M,NA):

        self.wl=wl          #Wavelength in nm (emission peak in buffer)
        self.NA=NA          #Numerical aperture
        
        self.M=M            #Magnification
        self.n=n            #Refractive index of glass/oil
        self.np=1.0         #Refractive index of air in lab (vacuum)
        self.n0=n0          #refractive index of sample medium (water)

        self.kp=2*pi/wl     #Wavevector amplitude in air (vacuum)
        self.k0=n0*self.kp  #Wavevector amplitude in buffer
        self.k=n*self.kp    #Wavevector amplitude in glass

        self.etapmed=n0/M   #Integration limits
        self.etapmax=NA/M

        # Calculate or load normalization constants
        try:
            normdata=load('dipolenorm.dat')
            
            try: shape(normdata)[1]
            except IndexError: normdata=array([normdata])
            
            if wl in normdata[:,0]:
                self.norm=normdata[normdata[:,0]==wl,1:3][0]
            else:
                self.norm=self.Normalization()
                norm=array([wl,self.norm[0],self.norm[1]])
                norm=norm.reshape((1,3))
                normdata=append(normdata,norm,0)
                save('dipolenorm.dat',normdata)
        except IOError:
            self.norm=self.Normalization()
            normdata=array([wl,self.norm[0],self.norm[1]])
            normdata=normdata.reshape((1,3))
            save('dipolenorm.dat',normdata)

        # Calculate the zeroth, first, and second moments of eta' used in the
        # cumulant approximation of the true PSF. Do this separately for the
        # sub- and supercritical regions. This calculation is only needed when
        # the class is initiated.
        
        def Integrand0_sub(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.Eppar(etap)-self.Espar(etap))
            return integrand

        self.b0sub_norm=qromb(Integrand0_sub,0.0,self.etapmed,1e-4)
        self.b0sub_mean=qromb(lambda etap: Integrand0_sub(etap)*etap,0.0,self.etapmed,1e-4)\
                         /self.b0sub_norm
        self.b0sub_var=qromb(lambda etap: Integrand0_sub(etap)*etap**2,0.0,self.etapmed,1e-4)\
                        /self.b0sub_norm-self.b0sub_mean**2

        def Integrand0_real(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc3(etap)-self.sc1(etap))
            return integrand

        self.b0real_norm=qromb(Integrand0_real,self.etapmed,self.etapmax,1e-4)
        self.b0real_mean=qromb(lambda etap: Integrand0_real(etap)*etap,self.etapmed,self.etapmax,1e-4)\
                          /self.b0real_norm
        self.b0real_var=qromb(lambda etap: Integrand0_real(etap)*etap**2,self.etapmed,self.etapmax,1e-4)\
                         /self.b0real_norm-self.b0real_mean**2

        def Integrand0_imag(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc4(etap)-self.sc2(etap))
            return integrand

        self.b0imag_norm=qromb(Integrand0_imag,self.etapmed,self.etapmax,1e-4)
        self.b0imag_mean=qromb(lambda etap: Integrand0_imag(etap)*etap,self.etapmed,self.etapmax,1e-4)\
                          /self.b0imag_norm
        self.b0imag_var=qromb(lambda etap: Integrand0_imag(etap)*etap**2,self.etapmed,self.etapmax,1e-4)\
                         /self.b0imag_norm-self.b0imag_mean**2

        def Integrand1_sub(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*self.Epperp(etap)
            return integrand

        self.b1sub_norm=qromb(Integrand1_sub,0.0,self.etapmed,1e-4)
        self.b1sub_mean=qromb(lambda etap: Integrand1_sub(etap)*etap,0.0,self.etapmed,1e-4)\
                         /self.b1sub_norm
        self.b1sub_var=qromb(lambda etap: Integrand1_sub(etap)*etap**2,0.0,self.etapmed,1e-4)\
                        /self.b1sub_norm-self.b1sub_mean**2

        def Integrand1_real(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*self.sc5(etap)
            return integrand

        self.b1real_norm=qromb(Integrand1_real,self.etapmed,self.etapmax,1e-4)
        self.b1real_mean=qromb(lambda etap: Integrand1_real(etap)*etap,self.etapmed,self.etapmax,1e-4)\
                          /self.b1real_norm
        self.b1real_var=qromb(lambda etap: Integrand1_real(etap)*etap**2,self.etapmed,self.etapmax,1e-4)\
                         /self.b1real_norm-self.b1real_mean**2

        def Integrand1_imag(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*self.sc6(etap)
            return integrand

        self.b1imag_norm=qromb(Integrand1_imag,self.etapmed,self.etapmax,1e-4)
        self.b1imag_mean=qromb(lambda etap: Integrand1_imag(etap)*etap,self.etapmed,self.etapmax,1e-4)\
                          /self.b1imag_norm
        self.b1imag_var=qromb(lambda etap: Integrand1_imag(etap)*etap**2,self.etapmed,self.etapmax,1e-4)\
                         /self.b1imag_norm-self.b1imag_mean**2

        def Integrand2_sub(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.Eppar(etap)+self.Espar(etap))
            return integrand

        self.b2sub_norm=qromb(Integrand2_sub,0.0,self.etapmed,1e-4)
        self.b2sub_mean=qromb(lambda etap: Integrand2_sub(etap)*etap,0.0,self.etapmed,1e-4)\
                         /self.b2sub_norm
        self.b2sub_var=qromb(lambda etap: Integrand2_sub(etap)*etap**2,0.0,self.etapmed,1e-4)\
                        /self.b2sub_norm-self.b2sub_mean**2

        def Integrand2_real(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc3(etap)+self.sc1(etap))
            return integrand

        self.b2real_norm=qromb(Integrand2_real,self.etapmed,self.etapmax,1e-4)
        self.b2real_mean=qromb(lambda etap: Integrand2_real(etap)*etap,self.etapmed,self.etapmax,1e-4)\
                          /self.b2real_norm
        self.b2real_var=qromb(lambda etap: Integrand2_real(etap)*etap**2,self.etapmed,self.etapmax,1e-4)\
                         /self.b2real_norm-self.b2real_mean**2

        def Integrand2_imag(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc4(etap)+self.sc2(etap))
            return integrand

        self.b2imag_norm=qromb(Integrand2_imag,self.etapmed,self.etapmax,1e-4)
        self.b2imag_mean=qromb(lambda etap: Integrand2_imag(etap)*etap,self.etapmed,self.etapmax,1e-4)\
                          /self.b2imag_norm
        self.b2imag_var=qromb(lambda etap: Integrand2_imag(etap)*etap**2,self.etapmed,self.etapmax,1e-4)\
                         /self.b2imag_norm-self.b2imag_mean**2


    # Change and retrieve the radial position value
    
    def SetRho(self,rho):
        self.rho=rho

    def GetRho(self):
        return self.rho

    # Connect eta and eta0 to etap via Snell's law

    def Eta(self,etap):
        M=self.M
        np=self.np
        n=self.n
        eta=arcsin(M*np/n*etap)
        return eta

    def Eta0(self,etap):
        M=self.M
        np=self.np
        n0=self.n0
        n=self.n
        eta=self.Eta(etap)
        eta0=arccos(sqrt(fabs(1.0-(n/n0*sin(eta))**2)))
        return eta0
    
    # The Fresnel transmission coefficients
    
    def Ts(self,etap):
        eta=self.Eta(etap)
        eta0=self.Eta0(etap)
        ts=2*cos(eta0)*sin(eta)/sin(eta0+eta)
        return ts

    def Tp(self,etap):
        eta=self.Eta(etap)
        eta0=self.Eta0(etap)
        ts=self.Ts(etap)
        tp=ts/cos(eta0-eta)
        return tp

    # z-components of wavevectors

    def W(self,etap):
        eta=self.Eta(etap)
        k=self.k
        w=cos(eta)*k
        return w

    def W0(self,etap):
        eta0=self.Eta0(etap)
        k0=self.k0
        w0=cos(eta0)*k0
        return w0

    # Support functions

    def com(self,etap):
        n=self.n
        n0=self.n0
        eta=self.Eta(etap)
        value=sqrt(fabs(1-(n/n0*sin(eta))**2))
        return value

    def gamma(self,etap):
        n=self.n
        n0=self.n0
        eta=self.Eta(etap)
        c=self.com(etap)
        value=(n/n0)*cos(eta)/c
        return value

    def delta(self,etap):
        n=self.n
        n0=self.n0
        eta=self.Eta(etap)
        c=self.com(etap)
        value=(n0/n)*cos(eta)/c
        return value

    def epsilon(self,etap):
        n=self.n
        n0=self.n0
        eta=self.Eta(etap)
        c=self.com(etap)
        value=(n/n0)*c/cos(eta)
        return value

    # Integrands for super-critical angles

    def sc1(self,etap):
        n,n0=self.n,self.n0
        k,k0,kp=self.k,self.k0,self.kp
        g=self.gamma(etap)
        c=self.com(etap)
        value=-(n0*k/k0)*2.0*g**2/(1+g**2)
        return value

    def sc2(self,etap):
        n,n0=self.n,self.n0
        k,k0,kp=self.k,self.k0,self.kp
        g=self.gamma(etap)
        c=self.com(etap)
        value=(n0*k/k0)*2.0*g/(1+g**2)
        return value

    def sc3(self,etap):
        n,n0=self.n,self.n0
        k,kp=self.k,self.kp
        d=self.delta(etap)
        c=self.com(etap)
        value=2*(n/n0)*(k/kp)*d*c/(1+d**2)
        return value

    def sc4(self,etap):
        n,n0=self.n,self.n0
        k,kp=self.k,self.kp
        d=self.delta(etap)
        c=self.com(etap)
        value=2*(n/n0)*(k/kp)*c*d**2/(1+d**2)
        return value

    def sc5(self,etap):
        n,n0=self.n,self.n0
        k,k0,kp=self.k,self.k0,self.kp
        eta=self.Eta(etap)
        q=k*sin(eta)
        e=self.epsilon(etap)
        c=self.com(etap)
        value=(n/n0)*(q/kp)*(k/k0)*2/(1+e**2)
        return value

    def sc6(self,etap):
        n,n0=self.n,self.n0
        k,k0,kp=self.k,self.k0,self.kp
        eta=self.Eta(etap)
        q=k*sin(eta)
        e=self.epsilon(etap)
        c=self.com(etap)
        value=-(n/n0)*(q/kp)*(k/k0)*2*e/(1+e**2)
        return value

    # The electric field functions
    
    def Epperp(self,etap):
        n=self.n
        n0=self.n0
        np=self.np
        k0=self.k0
        k=self.k
        kp=self.kp

        eta=self.Eta(etap)
        eta0=self.Eta0(etap)
        q=k*sin(eta)
                        
        epperp=2.0*(q/kp)*(n/n0)*(k/k0)*sin(eta)*cos(eta)\
                /sin(eta+eta0)/cos(eta0-eta)
        return epperp

    def Eppar(self,etap):
        n=self.n
        n0=self.n0
        k=self.k
        k0=self.k0
        kp=self.kp

        eta=self.Eta(etap)
        eta0=self.Eta0(etap)

        eppar=2.0*(n/n0)*(k/kp)*cos(eta)*cos(eta0)*sin(eta)\
               /sin(eta+eta0)/cos(eta0-eta)
        return eppar
        
    def Espar(self,etap):
        n=self.n
        n0=self.n0
        k=self.k
        k0=self.k0
        kp=self.kp

        eta=self.Eta(etap)
        eta0=self.Eta0(etap)
    
        espar=-2.0*n*(k/k0)*cos(eta)*sin(eta)/sin(eta+eta0)
        return espar

    # Bessel functions of appropriate arguments
    
    def J0(self,etap):
        kp=self.kp
        rho=self.GetRho()
        j0=jn(0,kp*rho*etap)
        return j0

    def J1(self,etap):
        kp=self.kp
        rho=self.GetRho()
        j1=jn(1,kp*rho*etap)
        return j1
    
    def J2(self,etap):
        kp=self.kp
        rho=self.GetRho()
        j2=jn(2,kp*rho*etap)
        return j2
    
    # Calculation of the intensity
    
    def Val0_subcrit(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_real(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.Eppar(etap)-self.Espar(etap))*self.J0(etap)
            return integrand

        value=qromb(Integrand_real,0.0,etapmed,eps=1.0e-4)            
        return value

    def Val0_real(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_sc(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc3(etap)-self.sc1(etap))*self.J0(etap)
            return integrand

        value=qromb(Integrand_sc,etapmed,etapmax,eps=1.0e-4)
        return value

    def Val0_imag(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_sc(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc4(etap)-self.sc2(etap))*self.J0(etap)
            return integrand

        value=qromb(Integrand_sc,etapmed,etapmax,eps=1.0e-4)
        return value

    def Val1_subcrit(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_real(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*self.Epperp(etap)*self.J1(etap)
            return integrand

        value=qromb(Integrand_real,0.0,etapmed,eps=1.0e-4)            
        return value

    def Val1_real(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_sc(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*self.sc5(etap)*self.J1(etap)
            return integrand

        value=qromb(Integrand_sc,etapmed,etapmax,eps=1.0e-4)
        return value

    def Val1_imag(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_sc(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*self.sc6(etap)*self.J1(etap)
            return integrand

        value=qromb(Integrand_sc,etapmed,etapmax,eps=1.0e-4)
        return value

    def Val2_subcrit(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_real(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.Eppar(etap)+self.Espar(etap))*self.J2(etap)
            return integrand

        value=qromb(Integrand_real,0.0,etapmed,eps=1.0e-4)            
        return value

    def Val2_real(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_sc(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc3(etap)+self.sc1(etap))*self.J2(etap)
            return integrand

        value=qromb(Integrand_sc,etapmed,etapmax,eps=1.0e-4)
        return value

    def Val2_imag(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        def Integrand_sc(etap):
            eta=self.Eta(etap)
            integrand=etap/sqrt(cos(eta))*(self.sc4(etap)+self.sc2(etap))*self.J2(etap)
            return integrand

        value=qromb(Integrand_sc,etapmed,etapmax,eps=1.0e-4)
        return value

    # Functions for the cumulant expansion
    
    def Bessel0Cum_subcrit(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b0sub_norm
        mom1=self.b0sub_mean
        mom2=self.b0sub_var

        arg=self.kp*rho*mom1
        j0=jn(0,arg)
        j1=jn(1,arg)
        j2=jn(2,arg)
        value=norm*(j0+0.5*(rho*self.kp)**2*mom2*(j2-j1/arg))
        return value

    def Bessel0Cum_real(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b0real_norm
        mom1=self.b0real_mean
        mom2=self.b0real_var

        arg=self.kp*rho*mom1
        value=norm*(jn(0,arg)+0.5*(rho*self.kp)**2*mom2*(jn(2,arg)-jn(1,arg)/arg))
        return value

    def Bessel0Cum_imag(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b0imag_norm
        mom1=self.b0imag_mean
        mom2=self.b0imag_var

        arg=self.kp*rho*mom1
        value=norm*(jn(0,arg)+0.5*(rho*self.kp)**2*mom2*(jn(2,arg)-jn(1,arg)/arg))
        return value
    
    def Bessel1Cum_subcrit(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b1sub_norm
        mom1=self.b1sub_mean
        mom2=self.b1sub_var

        arg=self.kp*rho*mom1
        j1=jn(1,arg)
        value=norm*(j1+0.5*(rho*self.kp)**2*mom2*(2*j1/arg**2-jn(0,arg)/arg-j1))
        return value

    def Bessel1Cum_real(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b1real_norm
        mom1=self.b1real_mean
        mom2=self.b1real_var

        arg=self.kp*rho*mom1
        j1=jn(1,arg)
        value=norm*(j1+0.5*(rho*self.kp)**2*mom2*(2*j1/arg**2-jn(0,arg)/arg-j1))
        return value

    def Bessel1Cum_imag(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b1imag_norm
        mom1=self.b1imag_mean
        mom2=self.b1imag_var

        arg=self.kp*rho*mom1
        j1=jn(1,arg)
        value=norm*(j1+0.5*(rho*self.kp)**2*mom2*(2*j1/arg**2-jn(0,arg)/arg-j1))
        return value

    def Bessel2Cum_subcrit(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b2sub_norm
        mom1=self.b2sub_mean
        mom2=self.b2sub_var

        arg=self.kp*rho*mom1
        j2=jn(2,arg)
        value=norm*(j2+0.5*(rho*self.kp)**2*mom2*\
                    (jn(0,arg)-3*jn(1,arg)/arg+6*j2/arg**2))
        return value

    def Bessel2Cum_real(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        kp=self.kp
        rho=self.GetRho()

        norm=self.b2real_norm
        mom1=self.b2real_mean
        mom2=self.b2real_var

        arg=self.kp*rho*mom1
        j2=jn(2,arg)
        value=norm*(j2+0.5*(rho*self.kp)**2*mom2*\
                    (jn(0,arg)-3*jn(1,arg)/arg+6*j2/arg**2))
        return value

    def Bessel2Cum_imag(self):
        etapmax=self.etapmax
        etapmed=self.etapmed
        M=self.M
        np=self.np
        n=self.n

        rho=self.GetRho()

        norm=self.b2imag_norm
        mom1=self.b2imag_mean
        mom2=self.b2imag_var

        arg=self.kp*rho*mom1
        j2=jn(2,arg)
        value=norm*(j2+0.5*(rho*self.kp)**2*mom2*\
                    (jn(0,arg)-3*jn(1,arg)/arg+6*j2/arg**2))
        return value

    # Functions for calculation of the normalization constants
    
    def Intensity_norm_par(self,rho):
        self.SetRho(rho)
        par= ((self.Val0_subcrit()+self.Val0_real())**2+self.Val0_imag()**2\
              +(self.Val2_subcrit()+self.Val2_real())**2+self.Val2_imag()**2)/4.0
        return par

    def Intensity_norm_vert(self,rho):
        self.SetRho(rho)
        vert=((self.Val1_subcrit()+self.Val1_real())**2+self.Val1_imag()**2)
        return vert

    def NormIntegrand_par(self,r):
        rho=self.M*r
        value=zeros(len(rho))
        for i in range(len(rho)):
            value[i]=r[i]*self.Intensity_norm_par(rho[i])
        value*=(2*pi*self.M**2)
        return value

    def NormIntegrand_vert(self,r):
        rho=self.M*r
        value=zeros(len(rho))
        for i in range(len(rho)):
            value[i]=r[i]*self.Intensity_norm_vert(rho[i])
        value*=(2*pi*self.M**2)
        return value

    def Normalization(self):
        norm_par=qromb(self.NormIntegrand_par,0.0,20000,1e-4)
        norm_vert=qromb(self.NormIntegrand_vert,0.0,20000,1e-4)             
        return (norm_par,norm_vert)
    
    # Calculation of the PSF
    
    def Intensity_exact(self,rho,phip,alpha,beta):
        
        self.SetRho(rho)

        par= sin(beta)**2/4.0*\
             ((self.Val0_subcrit()+self.Val0_real())**2+self.Val0_imag()**2\
              +(self.Val2_subcrit()+self.Val2_real())**2+self.Val2_imag()**2\
               -2*cos(2*(phip-alpha))*((self.Val0_subcrit()+self.Val0_real())*\
              (self.Val2_subcrit()+self.Val2_real())\
               +self.Val0_imag()*self.Val2_imag()))

        mix=sin(beta)*cos(beta)*cos(phip-alpha)*\
             ((self.Val1_subcrit()+self.Val1_real())*self.Val2_imag()-
              (self.Val2_subcrit()+self.Val2_real())*self.Val1_imag()-
              (self.Val1_subcrit()+self.Val1_real())*self.Val0_imag()+
              (self.Val0_subcrit()+self.Val0_real())*self.Val1_imag())

        vert=cos(beta)**2*\
              ((self.Val1_subcrit()+self.Val1_real())**2+self.Val1_imag()**2)
        
        value=par+mix+vert

        # Normalization
        value/=(sin(beta)**2*self.norm[0]+cos(beta)**2*self.norm[1])
        return value

    def Intensity_approx(self,rho,phip,alpha,beta):
        
        self.SetRho(rho)

        par= sin(beta)**2/4.0*\
             ((self.Bessel0Cum_subcrit()+self.Bessel0Cum_real())**2+self.Bessel0Cum_imag()**2\
              +(self.Bessel2Cum_subcrit()+self.Bessel2Cum_real())**2+self.Bessel2Cum_imag()**2\
               -2*cos(2*(phip-alpha))*((self.Bessel0Cum_subcrit()+self.Bessel0Cum_real())*\
              (self.Bessel2Cum_subcrit()+self.Bessel2Cum_real())\
               +self.Bessel0Cum_imag()*self.Bessel2Cum_imag()))

        mix=sin(beta)*cos(beta)*cos(phip-alpha)*\
             ((self.Bessel1Cum_subcrit()+self.Bessel1Cum_real())*self.Bessel2Cum_imag()-
              (self.Bessel2Cum_subcrit()+self.Bessel2Cum_real())*self.Bessel1Cum_imag()-
              (self.Bessel1Cum_subcrit()+self.Bessel1Cum_real())*self.Bessel0Cum_imag()+
              (self.Bessel0Cum_subcrit()+self.Bessel0Cum_real())*self.Bessel1Cum_imag())

        vert=cos(beta)**2*\
              ((self.Bessel1Cum_subcrit()+self.Bessel1Cum_real())**2+self.Bessel1Cum_imag()**2)
        
        value=par+mix+vert

        # Normalization
        value/=(sin(beta)**2*self.norm[0]+cos(beta)**2*self.norm[1])
        return value

    def PSF_approx(self,x,y,alpha,beta):
        r=sqrt(x**2+y**2)
        if (x<0.0): phip=pi-arctan(-y/x)
        else: phip=arctan(y/x)
        rho=self.M*r
        value=self.M**2*self.Intensity_approx(rho,phip,alpha,beta)
        return value

    def PSF_exact(self,x,y,alpha,beta):
        r=sqrt(x**2+y**2)
        if (x<0.0): phip=pi-arctan(-y/x)
        else: phip=arctan(y/x)
        rho=self.M*r
        value=self.M**2*self.Intensity_exact(rho,phip,alpha,beta)
        return value


class LogLikelihood:
    """ Class defining the log-likelihood function maximized in MLE."""

    def __init__(self,counts,a,wl,n,n0,M,NA,alpha,Sfloor,sigma,pinit):
        self.counts=counts
        self.a=a
        self.wl=wl
        self.NA=NA
        self.n=n
        self.n0=n0
        self.M=M

        self.alpha=alpha
        self.Sfloor=Sfloor
        self.sigma=sigma

        self.pinit=pinit
        
        self.npix=shape(counts)[0]
        self.posvec=arange(-(self.npix-1.0)/2.0,(self.npix)/2.0,1.0)*a

        self.DD=dipdistr(self.wl,self.n,self.n0,self.M,self.NA)
    
    def Value(self,x):

        counts=self.counts
        npix=self.npix
        posvec=self.posvec

        alpha=self.alpha
        Sfloor=self.Sfloor
        sigma=self.sigma
        
        pij=zeros((npix,npix))

        mux,muy,b,N,theta,phi=x

        # Convert parameters
        b=b**2
        N=N**2

        # Calculate probabilities for all pixels (small pixel approximation)
        for i in range(npix):
            for j in range(npix):
                pij[j,i]=self.DD.PSF_approx(posvec[i]-mux,posvec[j]-muy,theta,phi)
        pij*=(self.a**2)

        # Subtract noise floor
        effcounts=counts-Sfloor

        # Calculate log-likelihood
        value=0.0
        for i in range(npix):
            for j in range(npix):
                eta=N*pij[j,i]+b
                
                f0=alpha*exp(-eta)*eta
                fp0=f0*0.5*alpha*(eta-2)

                cij=effcounts[j,i]
                
                conv0=0.5*(1+erf(cij/(sqrt(2*sigma**2))))
                conv1=sigma*exp(-cij**2/(2*sigma**2))/sqrt(2*pi)+cij*conv0
                temp=(f0*conv0+fp0*conv1+exp(-eta)*gauss(cij,sigma))
                
                if (cij>0.0):
                    nij=alpha*cij
                    if eta*nij>10**5:
                        transform=0.5*log(alpha*eta/cij)-nij-eta+2*sqrt(eta*nij)\
                                   -log(2*sqrt(pi)*(eta*nij)**0.25)
                        temp+=(exp(transform)-f0-fp0*cij)
                    else:
                        temp+=(sqrt(alpha*eta/cij)*exp(-nij-eta)*i1(2*sqrt(eta*nij))\
                            -f0-fp0*cij)

                value+=log(temp)
                
        value*=-1.0
        print "%7.5f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f" %\
              (value,mux,muy,b,N,theta,phi)
        return value


class MLEwT:
    """
    Estimates the center coordinates (x and y) and the orientation (theta and phi)
    of a fixed dipole using MLEwT.
    
    Input:
    -------------------------------------------------------------------------

    wl (float)      : Wavelength [nm] of emission peak in buffer
    a (float)       : Width of pixels [nm] (assumed small)
    M (float)       : Magnification of the objective
    NA (float)      : Numerical aperture of the objective
    n (float)       : Refractive index of glass/immersion oil
    n0 (float)      : Refractive index of buffer
    alpha (float)   : Inverse gain of the EMCCD chip
    Sfloor (float)  : Constant offset of the EMCCD output
    sigma (float)   : Width of the noise distribution in the EMCCD output
    initvals (array): Array of length 6 of initial values for mux,muy,b,N,theta,phi
    initpix (array) : Array of length 2 of initial values for the center pixel (ypixel,xpixel)
    deltapix (int)  : The half width of the array to be analyzed
        
    Functions:
    -------------------------------------------------------------------------
    Estimate (array) : Takes a full pixel array and uses MLEwT to return an
    array of estimates for x,y,b,N,theta,phi where b is the number of background
    photons per pixel and N is the photon number.

    Kim I. Mortensen
    """

    def __init__(self,wl,a,M,NA,n,n0,alpha,Sfloor,sigma,\
                initvals,initpix,deltapix):

        # Store user input
        self.wl=wl
        self.a=a
        self.M=M
        self.NA=NA
        self.n=n
        self.n0=n0

        self.alpha=alpha
        self.Sfloor=Sfloor
        self.sigma=sigma

        self.initvals=initvals
        self.initpix=initpix

        self.deltapix=deltapix
        

    def Estimate(self,datamatrix):

        ypix=self.initpix[0]
        xpix=self.initpix[1]
        deltapix=self.deltapix

        # Entire data matrix
        counts=datamatrix

        # Extract pixel array around initial pixel
        counts=counts[ypix-deltapix:ypix+deltapix,xpix-deltapix:xpix+deltapix]

        # Transformation of initial values
        pinit=zeros(6)
        pinit[0:2]=self.initvals[0:2]   # mux,muy
        pinit[2]=sqrt(self.initvals[2]) # b
        pinit[3]=sqrt(self.initvals[3]) # N
        pinit[4]=self.initvals[4]       # theta
        pinit[5]=self.initvals[5]       # phi

        # Create instance of LogLikelihood object
        ll=LogLikelihood(counts,self.a,self.wl,self.n,self.n0,self.M,NA,\
                         self.alpha,self.Sfloor,self.sigma,pinit)

        # Perform maximization of the log-likelihood using Powell's method
        #xopt, fopt, xi, direc, iter, funcalls, warnflag=\
        res=fmin_powell(ll.Value,pinit,ftol=0.0001,maxiter=15,full_output=1)
        est=res[0]
        warnflag=res[5]

        # Store position estimates relative to initial pixel
        self.mux=est[0]
        self.muy=est[1]

        # Convert estimates
        est[2]=est[2]**2
        est[3]=est[3]**2

        # Calculate covariance matrix of estimates of position coordinates and angles
        covar=MLEwTcovar(self.a,self.deltapix*2,self.wl,self.n,self.n0,self.M,self.NA)

        covarmatrix=covar.CovarianceMatrix(est[3],est[2],\
                                     array([self.mux,self.muy]),est[4],est[5])

        # Add escess noise
        covarmatrix*=2.0
 
        errorbars=sqrt(diag(covarmatrix))

        print "\nx coordinate [nm] = ", around(self.a*xpix+est[0],1),'+/-',around(errorbars[0],1)
        print "y coordinate [nm] = ", around(self.a*ypix+est[1],1),'+/-',around(errorbars[1],1)
        print "azimuthal angle [rad] = ", around(est[4],2),'+/-',around(errorbars[2],3)
        print "polar angle [rad] = ", around(est[5],2),'+/-',around(errorbars[3],3)
        print
        print covarmatrix

        return est

class MLEwTcovar:
    """
    Calculates the covariance matrix for the estimated parameters in MLEwT.
    
    The PSF is the distribution of photons in the image plane from
    a fixed dipole emitter close to the coverslip surface,
    when imaged by a large aperture objective.

    Input:
    -------------------------------------------------------------------------

    a (float)  : Width of pixels [nm] (assumed small)
    npix (int) : Number of analyzed pixels along one dimension
    wl (float) : Wavelength [nm] of emission peak in buffer.
    NA (float) : Numerical aperture of the objective
    n (float) : Refractive index of glass/immersion oil
    n0 (float) : Refractive index of buffer (water)
    M (float) : Magnification of the objective
    
    Functions:
    -------------------------------------------------------------------------
    CovarianceMatrix (float) : Takes the photon number, number of background photons
    per pixel, the center coordinates (array([x,y])), the azimuthal angle (theta), and the
    polar angle (phi) of the probe as arguments. Returns the covariance matrix for the estimated
    parameters x,y,theta, and phi.

    Kim I. Mortensen
    """

    def __init__(self,a,npix,wl,n,n0,M,NA):

        self.a=a
        self.npix=npix
        self.wl=wl
        self.NA=NA
        self.n=n
        self.n0=n0
        self.M=M
        

        # Define instance of dipole PSF
        self.dip=dipdistr(self.wl,self.n,self.n0,self.M,self.NA)

    def Probability(self,mu,theta,phi):

        a=self.a
        npix=self.npix

        # Center pixel coordinates in one dimension
        posvec=arange(-(npix-1.0)/2.0,npix/2.0,1.0)*a
        # Matrix of distances from PSF center to pixel centers
        distmat=zeros((npix,npix))
        # Matrix of expected photon counts 
        p=zeros((npix,npix))

        # -- Calculate expected photon values in small pixels
        #    These are calculated by approximating the integral of
        #    the PSF over a pixel to leading order in the pixel width.
 
        for i in range(npix):
            for j in range(npix):
                x=posvec[i]-mu[0]
                y=posvec[j]-mu[1]
        
                p[j,i]=self.dip.PSF_approx(x,y,theta,phi)

        # Expected photon counts (using 1. order approximation over small pixels)
        p*=(a**2)
        return p

    def Derivatives(self,mu,theta,phi):

        delta=1e-6
        f0=(self.Probability(mu+array([delta,0.0]),theta,phi)\
             -self.Probability(mu-array([delta,0.0]),theta,phi))/(2*delta)
        f1=(self.Probability(mu+array([0.0,delta]),theta,phi)\
             -self.Probability(mu-array([0.0,delta]),theta,phi))/(2*delta)
        delta=1e-8
        f2=(self.Probability(mu,theta+delta,phi)\
             -self.Probability(mu,theta-delta,phi))/(2*delta)
        f3=(self.Probability(mu,theta,phi+delta)\
             -self.Probability(mu,theta,phi-delta))/(2*delta)

        return (f0,f1,f2,f3)

    def FisherMatrix(self,N,b,mu,theta,phi):

        p=self.Probability(mu,theta,phi)
        f1,f2,f3,f4=self.Derivatives(mu,theta,phi)

        I=zeros((4,4))

        denom=p+1.0*b/N

        I[0,0]=sum(ravel(f1**2/denom))
        I[0,1]=I[1,0]=sum(ravel(f1*f2/denom))
        I[0,2]=I[2,0]=sum(ravel(f1*f3/denom))
        I[0,3]=I[3,0]=sum(ravel(f1*f4/denom))
        I[1,1]=sum(ravel(f2**2/denom))
        I[1,2]=I[2,1]=sum(ravel(f2*f3/denom))
        I[1,3]=I[3,1]=sum(ravel(f2*f4/denom))
        I[2,2]=sum(ravel(f3**2/denom))
        I[2,3]=I[3,2]=sum(ravel(f3*f4/denom))
        I[3,3]=sum(ravel(f4**2/denom))

        I*=N
        return I

    def CovarianceMatrix(self,N,b,mu,theta,phi):
        return inv(self.FisherMatrix(N,b,mu,theta,phi))


if __name__=='__main__':

    close('all')

    datamatrix=load('data_fixed.txt')

    wl=578.0
    a=44.0
    M=375.0
    NA=1.49
    n=1.52
    n0=1.33
    alpha=0.09
    Sfloor=399.0
    sigma=1.55
    
    deltapix=6

    mux=0.1
    muy=0.1
    b=100.0
    N=20000.0
    theta=2.0
    phi=2.3

    initvals = array([mux,muy,b,N,theta,phi])
    initpix=(261,47)
    
    track=MLEwT(wl,a,M,NA,n,n0,alpha,Sfloor,sigma,\
                     initvals,initpix,deltapix)

    track.Estimate(datamatrix)


