%% This script demonstrates the use of GPUgaussMLE
%run('C:\Program Files (x86)\DIPimage 2.4.1\dipstart.m')


Nfits=1000;     %number of images to fit
bg=1;           %background fluorescence in photons/pixel/frame
Nphotons=500;   %expected photons/frame
Npixels=7;      %linear size of fit region in pixels. 
PSFsigma=1;     %PSF sigma in pixels

%generate a stack of images
coords=Npixels/2-1+rand([Nfits 2]);
[out] = finiteGaussPSFerf(Npixels,PSFsigma,Nphotons,bg,coords);

%corrupt with Poisson noise 
data=noise(out,'poisson',1)

%fit and calculate speed
tic;
[X Y N BG S CRLBx CRLBy CRLBn CRLBb CRLBs LogL]=mGPUgaussMLE(data,PSFsigma);
t=toc;

fprintf('GPUgaussMLE has performed %g fits per second.\n',Nfits/t)

%report some details
s_x_found=std(X-coords(:,1));
meanCRLBx=mean(CRLBx);

fprintf('The standard deviation of x-position error is %g \n',s_x_found)
fprintf('The mean returned CRLB based x-position uncertainty is %g \n',meanCRLBx)


