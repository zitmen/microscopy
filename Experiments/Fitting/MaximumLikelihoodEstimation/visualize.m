function visualize(img, results)

   figure(12); clf(12,'reset')
   hold on
   imagesc(img);
   colormap gray(256)
   axis ij image off;
   set(gca,'Position',[0 0 1 1])
   
   ok = results.ok(1:results.cok,12:13);    % xo,yo
   fp = results.fp(1:results.cfp,1:2);      % xl,xl
   fn = results.fn(1:results.cfn,1:2);      % xo,yo
   
   plot(ok(:,2),ok(:,1),'go','MarkerSize',10)    % green  - correct localization
   plot(fp(:,2),fp(:,1),'ro','MarkerSize',10)    % red    - localized point does not exist!
   plot(fn(:,2),fn(:,1),'yo','MarkerSize',10)    % yellow - point was not detected!
 
   drawnow;
