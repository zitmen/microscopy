% Generate monte-carlo images of dots on noisy background

clear

cfg = cfg_mcstatic();

% initialize
imgpath = [cfg.datapath cfg.filename '.tiff'];
if ~isdir(cfg.datapath), mkdir(cfg.datapath); end;
if exist(imgpath, 'file')
    delete(imgpath);
end;

params_cells = cell(cfg.numimgs, 1);
images_cells = cell(cfg.numimgs, 1);

% montecarlo simulation of noisy background
sigma = 0.05; % percents of the scale [0,65535]
genGaussNoise(cfg, 5*sigma, sigma); % G+5*sigma -> 99% of values is in [0,1] interval

% generate 
fprintf('Generating %d static monte-carlo images\nProgress ', cfg.numimgs);
%matlabpool local 4
parfor frame = 1:cfg.numimgs
%for frame = 1:cfg.numimgs

  % montecarlo simulation of dots
  [im,params] = gendotsstatic(cfg);
  
  % save dots and parameters
  params_cells{frame} = params;
  images_cells{frame} = im;
  
   % progress
  %if mod(frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end

% save to file
for frame=1:cfg.numimgs
    imwrite(uint16(65535*images_cells{frame}),imgpath,'WriteMode','append','Compression','none');
end

save([cfg.datapath cfg.filename '.mat'],'-v6','params_cells');

fprintf('\ndone\n\n');
