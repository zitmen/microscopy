% -------------------------------------------------------------------------
function [xs, ys, As] = subpix2d(r, c, cim, nbr)
% -------------------------------------------------------------------------
% Compute local maxima to subpixel accuracy by fitting a quadric
% function ax^2 + by^2 + cx + dy + e = z in neighbourhood
%
% return values are xs (X subpixel), ys (Y subpixel), As (Amplitude subpixel)
% -------------------------------------------------------------------------

if (nargin < 4), nbr = 1; end;

if isempty(r)
   xs = []; ys = []; As = [];
   return;
end

[rows,cols] = size(cim);

% indices of feature points
ind = sub2ind([rows,cols],r,c);

% 8-neibourhood coordinates
[x,y] = meshgrid(-nbr:nbr,-nbr:nbr); x = x(:); y = y(:);
numnbrs = length(x);
indnbr = repmat(ind,1,numnbrs) + repmat((x*rows+y)',length(ind),1);

% local parabola fitting by LSQ
par = cim(indnbr) * pinv([x.^2, y.^2, x, y, ones(numnbrs,1)])';

% subpixel corrections to original row and column coords
x0 = -par(:,3) ./ par(:,1) / 2;
y0 = -par(:,4) ./ par(:,2) / 2;

% Add subpixel corrections
xs = c + x0;
ys = r + y0;

% interpolated value at subpixel coordinates
As = sum([x0.^2, y0.^2, x0, y0] .* par(:,1:4),2) + par(:,5);
