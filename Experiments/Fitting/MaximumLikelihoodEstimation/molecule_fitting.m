function [fits, imbuf] = molecule_fitting(img, imbuf, fits, cfg)

persistent X;

if isempty(fits)
  resupdate('free');
  fits = struct('num', 0, 'discard', 0); X = [];
  [X(:,:,2),X(:,:,1)] = meshgrid(-cfg.box:cfg.box,-cfg.box:cfg.box);
end

if strcmp(cfg.fitmethod, 'LM')
  % find molecules that disapeared
  idx = find(cat(1,imbuf.active) == 0);
  npts = (2*cfg.box+1)^2;

  for I = 1:length(idx)
    
    % "pointer" to current detection buffer
    tmp = imbuf(idx(I));
    
    % fit 2D gaussian bell shaped function
    %    [ x,  y, sigma       , Amplitude , background]
    a0 = [ 0,  0, cfg.sigmapsf, max(tmp.im(:)), min(tmp.im(:))];
    %[a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);  
    [a,exitflag,chi2] = lmfit('fun_gauss2D',a0, reshape(X(:),[npts, 2]), tmp.im(:));
      
    % position
    a(1:2) = a(1:2)+ tmp.pos;
    a(4) = a(4) * 2*pi*a(3)^2;
    
    % save result
    fits.num = resupdate('add', single([ a(1), a(2), a(4) ]));
  end
elseif strcmp(cfg.fitmethod, 'MLE')
  % find molecules that disapeared
  idx = find(cat(1,imbuf.active) == 0);
  npts = (2*cfg.box+1)^2;
  
  % make stack of the points to run the CUDA function faster
  images = zeros(size(X,1),size(X,2),length(idx));  % alloc  
  for I = 1:length(idx)
    images(:,:,I) = imbuf(idx(I)).im();
  end
  
  % fit 2D gaussian bell shaped function
  [aa] = mle_fit(cfg.sigmapsf, images);
  
  % process the results
  for I = 1:length(idx)
    
    tmp = imbuf(idx(I));    % "pointer" to current detection buffer
    a = aa(I,:);
    
    % position
    a(1:2) = a(1:2) - cfg.box + tmp.pos;  % subtract cfg.box from x and y because this script uses the center as start of the coordinate system [0,0]; it should be actualy sub(cfg.box+1) but the +1 is missing because of zero-indexing in C
    a(4) = a(4) * 2*pi*a(3)^2;
    
    % save result
    fits.num = resupdate('add', single([ a(1), a(2), a(4) ]));
    
  end
else
      error(strcat('Method "', cfg.fitmethod, '" does not exist!'));
end
  
  %save results
  fits.fits = resupdate('save');

function y = gauss2d(X,a)

y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);

