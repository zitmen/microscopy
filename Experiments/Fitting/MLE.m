% minimizing minus log likelihood (MLE)
function MLE()
    Gsize = [11 11];
    G=gaussian(Gsize,6,6,2);

    best_x = 1; best_y = 1; best_sigma = 1;
    best_logL = logLikelihood(G,gaussian(Gsize,best_x,best_y,best_sigma));
    for sigma_est = 1:0.5:3
        for x_est = 1:0.5:11
            for y_est = 1:0.5:11
                G_est=gaussian(Gsize,x_est,y_est,sigma_est);
                logL=logLikelihood(G,G_est);
                if(-logL < -best_logL)
                    best_logL = logL;
                    best_x = x_est;
                    best_y = y_est;
                    best_sigma = sigma_est;
                end
            end
        end
    end

    % output
    fprintf('X=%0.1f, Y=%0.1f, sigma=%0.1f\nlogL=%f\n\n',best_x,best_y,best_sigma,best_logL);
    
    G_est=gaussian(Gsize,best_x,best_y,best_sigma);
    figure(1)
    subplot(2,2,1)
    imagesc(G)
    title('G')
    subplot(2,2,2)
    imagesc(log(G_est))
    title('log(G_{est}) (logaritmus to opticky roztahne)')
    subplot(2,2,3)
    imagesc(G.*log(G_est))
    title('G_{est}.*log(G)')
    subplot(2,2,4)
    imagesc(G.*log(G_est)-G_est)
    title('G_{est}.*log(G)-G')
end

function h = gaussian(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = 1/(2*pi*sigma*sigma)*exp(-((((xi-x0)^2)/(2*sigma*sigma)) + (((yi-y0)^2)/(2*sigma*sigma))));
        end
    end

end

function logL = logLikelihood(h, h_est)

    logL = sum(sum(h.*log(h_est) - h_est));

end