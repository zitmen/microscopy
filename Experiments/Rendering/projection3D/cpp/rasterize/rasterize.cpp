#include "mex.h"
#include "CImg.h"

/* ------------------------------------------------------------------------ */
// R A S T E R I Z A T I ON
/* ------------------------------------------------------------------------ */
cimg_library::CImg<float> rasterize(int im_w, int im_h, int n_poly, const int *n_pts_per_poly, const int *poly_pts, const float *poly_I)
{
	cimg_library::CImg<float> img(im_w, im_h, 1, 1, 0);	// single channel (grayscale) image filled with 0s
	for(int p = 0; p < n_poly; p++)
	{
		cimg_library::CImg<int> points = cimg_library::CImg<int>(n_pts_per_poly[p], 2);
		for(int v = 0; v < n_pts_per_poly[p]; v++)
		{
			points(v,0) = poly_pts[v*n_poly+p] % im_w;	// x (column)
			points(v,1) = poly_pts[v*n_poly+p] / im_w;	// y (row)
		}
		img.draw_polygon<int,float>(points, &poly_I[p]);
	}
	return img;
}

/* ------------------------------------------------------------------------ */
// M A I N
/* ------------------------------------------------------------------------ */
// call: rasterize(width,height,polygons_count,polygons_vertices_counts,vertices,intensities)
void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// check number of I/O arguments
	if(nrhs != 6)
		mexErrMsgTxt("Wrong number of input arguments!\nCall: IM = rasterize(width,height,polygons_count,polygons_vertices_counts,vertices,intensities);");
	if(nlhs != 1)
		mexErrMsgTxt("Wrong number of output arguments!\nCall: IM = rasterize(width,height,polygons_count,polygons_vertices_counts,vertices,intensities);");

	// width & height
	if(!mxIsInt32(prhs[0]) || !mxIsInt32(prhs[1]))
		mexErrMsgTxt("Width and height must be positive INTEGERS (int32)!");
	int img_width = *((int *)mxGetData(prhs[0]));
	int img_height = *((int *)mxGetData(prhs[1]));
	if((img_width <= 0) || (img_height <= 0))
		mexErrMsgTxt("Width and height must be POSITIVE integers(int32)!");

	// polygons count
	if(!mxIsInt32(prhs[2]))
		mexErrMsgTxt("Polygons count must be positive INTEGER(int32)!");
	int polygons_count = *((int *)mxGetData(prhs[2]));
	if(polygons_count <= 0)
		mexErrMsgTxt("Polygons count must be POSITIVE integer(int32)!");

	// polygons vertices counts
	if(mxIsEmpty(prhs[3]) || !mxIsInt32(prhs[3]))
		mexErrMsgTxt("Polygons' vertices counts must be stored in a vector of INTEGERS(int32).");
	if(mxGetN(prhs[3]) != 1)
		mexErrMsgTxt("Polygons' vertices counts must be stored in a VECTOR of integers(int32).");
	const int *polygon_vertices_counts = (const int *)mxGetData(prhs[3]);
	
	// vertices
	if(mxIsEmpty(prhs[4]) || !mxIsInt32(prhs[4]))
		mexErrMsgTxt("Vertices must be stored in a matrix of INTEGERS(int32).");
	const int *vertices = (const int *)mxGetData(prhs[4]);

	// intensities
	if(mxIsEmpty(prhs[5]) || !mxIsSingle(prhs[5]))
		mexErrMsgTxt("Intensities must be stored in a SINGLE matrix.");
	const float *intensities = (const float *)mxGetData(prhs[5]);

	// render the image
	cimg_library::CImg<float> img = rasterize(img_width, img_height, polygons_count, polygon_vertices_counts, vertices, intensities);
	img.transpose();	// CImg stores data in rows, but Matlab stores data in columns => transpose!

	// return the rendered image back to Matlab!
	plhs[0] = mxCreateNumericMatrix(img_width*img_height, 1, mxSINGLE_CLASS, mxREAL);
	memcpy((float *)mxGetData(plhs[0]), img.data(), img_width*img_height*sizeof(float));
}