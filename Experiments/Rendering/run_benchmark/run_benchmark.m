% Note: the difference between 'density' and 'gauss' is that 'gauss' is
%       using amplitude parameter and the 'density' does not
%
function run_benchmark
    %
    % config
    imsiz = [ 128 128 ];   % [rows cols] => [height width]
    resolution = 0.1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    %
    % load data - [X Y sigma I]
    %{
    % Octane
    dataset = 'octane';
    data = csvread('data/localization-octane.csv',1,0);
    data(:,3) = data(:,4)/resolution;
    data(:,4) = 1;
    % --------------
    %}
    %{
    % Quickpalm
    dataset = 'quickpalm';
    qp = dlmread('data/localization-quickpalm.csv','\t',1,1);
    data(:,1) = qp(:,2);
    data(:,2) = qp(:,3);
    data(:,3) = 0;%(0.05/resolution)*rand(size(qp,1),1)+(0.1/resolution);
    data(:,4) = qp(:,1);
    % --------------
    %}
    %{
    % TODO: RapidSTORM - bude asi potreba prepocitat X,Y z nm na px!
    dataset = 'rapidstorm';
    data = csvread('data/localization-octane.csv',1,0);
    data(:,3) = data(:,4)/resolution;
    data(:,4) = 1;
    % --------------
    %}
    %
    % CSSTORM - triangulation and quadtree did the best
    dataset = 'csstorm';
    data = csvread('data/localization-csstorm.csv',1,0);
    thr = 1.0;
    %data(:,3) = (0.01/resolution)*rand(size(data,1),1)+(0.05/resolution); % this is setting for GAUSS rendering!!
    data(:,3) = 1./data(:,4); % this is setting for the other methods
    data = data(data(:,4)>thr,:);
    % --------------
    %
    % TODO: Pavel's SM fitting
    % TODO: DAOSTORM
    % TODO: v jinym skriptu porovat CSSTORM/DAOSTORM/SM v zavislosti na
    % poctu zpracovanych snimku - bud razeno od nejpresvicenejsich, nebo
    % proste jen vzit v poradi tech 500 vybranych znimku, nebo vzit proste
    % zacatek filmu
	
	% TODO: ulozit obrazky bez transformaci a otevrit v ImageJ!! tam lze pak hodne jednoduse vizualne najit "spravny" thresholdy!!

    %
    % rendering
    images{1} = transform(dataset,'density', smlm_2Drendering(data, roi, resolution, 'density'));
    draw_figure('Rendering: Density', images{1});
    
    images{2} = transform(dataset,'scatter', smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',50)));
    draw_figure('Rendering: Scatter', images{2});
    
    images{3} = transform(dataset,'quadtree', smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',50)));
    draw_figure('Rendering: Quadtree', images{3});
    %{
    titles{4} = 'triangulation (intensity = circ; avg = 10)';
    images{4} = transform(dataset,'triangulation', smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference')));
    draw_figure('Rendering: Triangulation (circumference)', images{4});
    %}
    images{4} = transform(dataset,'triangulation', smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',50,'delaunay_thr',10,'intensity','area')));
    draw_figure('Rendering: Triangulation (area)', images{4});
    
    images{5} = transform(dataset,'histogram', smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',50)));
    draw_figure('Rendering: Histogram', images{5});
    
    % gauss needs to have different approach for CSSTORM data
    if strcmp(dataset,'csstorm'), data(:,3) = (0.01/resolution)*rand(size(data,1),1)+(0.05/resolution); end;
    images{6} = transform(dataset,'gauss', smlm_2Drendering(data, roi, resolution, 'gauss'));
    draw_figure('Rendering: Gauss', images{6});
end
%%
% helper functions
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function draw_figure(figid, IM)
    figure('Name',figid,'NumberTitle','off')
    % rendered image
    imagesc(minmax_norm(IM))
    colormap(hot)
end
%
function IM = transform(dataset,method,IM)
    if strcmp(dataset,'csstorm')
        if strcmp(method,'density')
            %blur
            h = fspecial('gaussian',[5 5],1.3);
            IM = imfilter(IM,h,'same','replicate');
            %thr
            IM(IM(:)<=0.02) = 0;
            IM(IM(:)>0.2) = 0.2;
        elseif strcmp(method,'scatter')
            %blur
            h = fspecial('gaussian',[5 5],1.3);
            IM = imfilter(IM,h,'same','replicate');
            %thr
            IM(IM(:)<=0.06) = 0;
            IM(IM(:)>0.3) = 0.3;
        elseif strcmp(method,'quadtree')
            %thr
            IM(IM(:)>0.15) = 0.15;
        elseif strcmp(method,'triangulation')
            %thr
            IM(IM(:)<=0.02) = 0;
            IM(IM(:)>0.4) = 0.4;
        elseif strcmp(method,'histogram')
            %blur
            h = fspecial('gaussian',[5 5],1.3);
            IM = imfilter(IM,h,'same','replicate');
            %thr
            IM(IM(:)<=0.04) = 0;
            IM(IM(:)>0.3) = 0.3;
        elseif strcmp(method,'gauss')
            %thr
            IM(IM(:)<=0.06) = 0;
            IM(IM(:)>0.8) = 0.8;
        end
    end
end