% Note: the difference between 'density' and 'gauss' is that 'gauss' is
%       using amplitude parameter and the 'density' does not
%
function transform_find_polynomial(recompute_results)
    if (recompute_results == 1) || ~exist('results_transform_find_polynomial.mat', 'file')
        compute_results();
    end
    show_results();
end
%%
function compute_results()
    %
    % config
    imsiz = [ 600 600 ];   % [rows cols] => [height width]
    Xd = 15; % X - distance
    Yc = 5000;  % Y - count of points
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    Xfl = 40;   % first line
    %
    % datagen
    numpts = 2*Yc;
    data = zeros(numpts,4); % [x y precision amplitude]
    data(:,1) = PSM*randn(numpts,1);
    data(:,2) = randi(imsiz(1),numpts,1);
    data(:,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
    data(:,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;

    % 1st line
    idx = 1:Yc;
    data(idx,1) = data(idx,1) + Xfl;
    % 2nd line
    idx = (Yc+1):(2*Yc);
    data(idx,1) = data(idx,1) + Xfl + Xd;

    %
    % rendering
    title{1} = 'density'; title{2} = 'scatter'; title{3} = 'quadtree';
    title{4} = 'triangulation'; title{5} = 'histogram'; title{6} = 'gauss';
    %
    IM{1} = smlm_2Drendering(data, roi, resolution, 'density');
    IM{2} = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',10));
    IM{3} = smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',10));
    %IM{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference'));
    IM{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','area'));
    IM{5} = smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',10));
    IM{6} = smlm_2Drendering(data, roi, resolution, 'gauss');

    coef = -1:0.5:+1;
    for imi=1:length(IM)
        fprintf('================== Running: %s ==================\n',title{imi});
        % init for the current method
        SI_best = 0;
        P_best = [ coef(1) coef(1) coef(1) coef(1) coef(1) ];
        % run
        for c5=1:length(coef)
            for c4=1:length(coef)
                for c3=1:length(coef)
                    fprintf('=== (%.1f)*x^5 + (%.1f)*x^4 + (%.1f)*x^3 + ?*x^2 + ?*x^1 ===\n',coef(c5),coef(c4),coef(c3));
                    for c2=1:length(coef)
                        for c1=1:length(coef)
                            SI = saddle_index(transform(IM{imi},coef(c5),coef(c4),coef(c3),coef(c2),coef(c1)), [Xfl Xfl+Xd], PSM);
                            if SI > SI_best
                                SI_best = SI;
                                P_best = [ coef(c5) coef(c4) coef(c3) coef(c2) coef(c1) ];
                                fprintf('Best: SI=%.2f; P=[%.1f,%.1f,%.1f,%.1f,%.1f]\n',SI_best,coef(c5),coef(c4),coef(c3),coef(c2),coef(c1));
                            end
                        end
                    end
                end
            end
        end
        % remember the winner
        results{imi}.method = title{imi};
        results{imi}.SI = SI_best;
        results{imi}.polynomial = P_best;
    end
    
    save('results_transform_find_polynomial.mat', 'results'); % save for later
end
%%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%%
function si = saddle_index(IM, Xg, PSM)
    IM = sum(integral_norm(IM,length(Xg)/2), 1);
    for Xgi=1:length(Xg)
        if mod(Xgi,2) == 0, continue, end;
        % saddle minimum
        im3=IM(Xg(Xgi):Xg(Xgi+1));
        [c3,i3]=min(im3);
        i3=i3+Xg(Xgi)-1;
        % left from the saddle
        im1=IM((Xg(Xgi)-3*PSM):i3);
        [c1,i1]=max(im1);
        i1=i1+Xg(Xgi)-3*PSM-1;
        % right from the saddle
        im2=IM(i3:((Xg(Xgi+1)+3*PSM)));
        [c2,i2]=max(im2);
        i2=i2+i3-1;
        % saddle index
        if (i1 < i3) && (i3 < i2) % => (i1 < i2)
            si = (min(c1,c2) - c3) / max(c1,c2);
        else
            si = 0;
        end
    end
end
%%
function show_results()
    load('results_transform_find_polynomial.mat');
    fprintf('method: SI=si, P=[p5 p4 p3 p2 p1]\n');
    fprintf('=================================\n');
    for i=1:length(results)
        fprintf('%s: SI=%.2f, P=[%.1f %.1f %.1f %.1f %.1f]\n',results{i}.method,results{i}.SI, ...
                results{i}.polynomial(1),results{i}.polynomial(2),results{i}.polynomial(3), ...
                results{i}.polynomial(4),results{i}.polynomial(5));
    end
end
%%
function IM = transform(IM,p5,p4,p3,p2,p1)
    IM = (p5*(IM.^5)) + (p4*(IM.^4)) + (p3*(IM.^3)) + (p2*(IM.^2)) + (p1*(IM.^1));
    IM = IM.^2;%lepsi nez abs(IM), ne? ...posledni pokus, jak se zachranit se zapornejma koeficientama...
end