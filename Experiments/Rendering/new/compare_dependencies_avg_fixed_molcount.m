% Note: the difference between 'density' and 'gauss' is that 'gauss' is
%       using amplitude parameter and the 'density' does not
%
function compare_dependencies_avg_fixed_molcount(recompute_results)
    if (recompute_results == 1) || ~exist('results_avg_fixed_molcount.mat', 'file')
        compute_results();
    end
    analysis();
end
%%
function compute_results()
    %
    % config
    imsiz = [ 600 600 ];   % [rows cols] => [height width]
    Xdr = 1:40; % X - distances range
    avg = 100;   % averaging range - 1:avg (total of 101 elements)
    Yc = 5000;  % Y - count of points
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    Xfl = 40;   % first line
    %
    results = zeros(length(Xdr),avg,6); % 6 rendering methods
    for Xdi = 1:length(Xdr)
        fprintf('\n============== Progress: Xd=%d ==============\n\n',Xdr(Xdi));
        % datagen
        numpts = 2*Yc;
        data = zeros(numpts,4); % [x y precision amplitude]
        data(:,1) = PSM*randn(numpts,1);
        data(:,2) = randi(imsiz(1),numpts,1);
        data(:,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
        data(:,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;

        % 1st line
        idx = 1:Yc;
        data(idx,1) = data(idx,1) + Xfl;
        % 2nd line
        idx = (Yc+1):(2*Yc);
        data(idx,1) = data(idx,1) + Xfl + Xdr(Xdi);

        % rendering
        IM = smlm_2Drendering2(data, roi, resolution, 'scatter', struct('average',avg));
        for avgi=1:avg,results(Xdi,avgi,2) = saddle_index(IM{avgi}, [Xfl Xfl+Xdr(Xdi)], PSM);end;
        %
        IM = smlm_2Drendering2(data, roi, resolution, 'quadtree', struct('average',avg));
        for avgi=1:avg,results(Xdi,avgi,3) = saddle_index(IM{avgi}, [Xfl Xfl+Xdr(Xdi)], PSM);end;
        %
        %IM = smlm_2Drendering2(data, roi, resolution, 'triangulation', struct('average',avg,'delaunay_thr',10,'intensity','circumference'));
        %for avgi=1:avg,results(Xdi,avgi,4) = saddle_index(IM{avgi}, [Xfl Xfl+Xdr(Xdi)], PSM);end;
        %
        IM = smlm_2Drendering2(data, roi, resolution, 'triangulation', struct('average',avg,'delaunay_thr',10,'intensity','area'));
        for avgi=1:avg,results(Xdi,avgi,4) = saddle_index(IM{avgi}, [Xfl Xfl+Xdr(Xdi)], PSM);end;
        %
        IM = smlm_2Drendering2(data, roi, resolution, 'histogram', struct('average',avg));
        for avgi=1:avg,results(Xdi,avgi,5) = saddle_index(IM{avgi}, [Xfl Xfl+Xdr(Xdi)], PSM);end;
    end
    
    save('results_avg_fixed_molcount.mat', 'results'); % save for later analysis
end
%%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%%
function si = saddle_index(IM, Xg, PSM)
    IM = sum(integral_norm(IM,length(Xg)/2), 1);
    for Xgi=1:length(Xg)
        if mod(Xgi,2) == 0, continue, end;
        % saddle minimum
        im3=IM(Xg(Xgi):Xg(Xgi+1));
        [c3,i3]=min(im3);
        i3=i3+Xg(Xgi)-1;
        % left from the saddle
        im1=IM((Xg(Xgi)-3*PSM):i3);
        [c1,i1]=max(im1);
        i1=i1+Xg(Xgi)-3*PSM-1;
        % right from the saddle
        im2=IM(i3:((Xg(Xgi+1)+3*PSM)));
        [c2,i2]=max(im2);
        i2=i2+i3-1;
        % saddle index
        if (i1 < i3) && (i3 < i2) % => (i1 < i2)
            si = (min(c1,c2) - c3) / max(c1,c2);
        else
            si = 0;
        end
    end
end
%%
function analysis()
    load('results_avg_fixed_molcount.mat');    % results matrix (40x20x6) --> (separation in pixels [1:40] * powers [1:20] * rendering methods [1:6])
    titles{1} = 'density'; titles{2} = 'scatter'; titles{3} = 'quadtree';
    titles{4} = 'triangulation'; titles{5} = 'histogram'; titles{6} = 'gauss';
    plot_compare_avgs(results, titles);
end
%%
function plot_compare_avgs(results, titles)
    figure('name','Compare averaging dependency');
    cc = hsv(length(titles));
    hold on
    for fi=2:(length(titles)-1) % this gows through indices 2-5, because the 1st anf 6th methods don't use averaging
        plot(1:100, mean(results(:,:,fi), 1), 'color', cc(fi,:))
    end
    plot([1 100],[0.5 0.5],'--g','LineWidth',2,'Color',[0 0 0]+0.8)
    title('Compare averaging dependency')
    xlabel('Averaging')
    ylabel('Separation Index averaged over separation distances')
    titles{length(titles)+1} = 'Distinguishability boundary';
    l{1}=titles{2}; l{2}=titles{3}; l{3}=titles{4};
    l{4}=titles{5}; l{5}=titles{7};
    legend(l, 'Location', 'SouthEast')
    hold off
end