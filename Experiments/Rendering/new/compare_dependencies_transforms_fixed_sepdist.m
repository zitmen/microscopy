% Note: the difference between 'density' and 'gauss' is that 'gauss' is
%       using amplitude parameter and the 'density' does not
%
function compare_dependencies_transforms_fixed_sepdist(recompute_results)
    if (recompute_results == 1) || ~exist('results_transforms_fixed_sepdist.mat', 'file')
        compute_results();
    end
    analysis();
end
%%
function compute_results()
    %
    % config
    imsiz = [ 600 600 ];   % [rows cols] => [height width]
    Xd = 15; % X - distances range
    power = 1:20;   % transformation --> IM.^power
    Ycr = 100:100:10000;  % Y - count of points range; [100 200 ... 9900 10000]
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    Xfl = 40;   % first line
    %
    results = zeros(length(Ycr),length(power),6); % 6 rendering methods
    for Yci = 1:length(Ycr)
        fprintf('\n============== Progress: Yc=%d ==============\n\n',Ycr(Yci));
        % datagen
        numpts = 2*Ycr(Yci);
        data = zeros(numpts,4); % [x y precision amplitude]
        data(:,1) = PSM*randn(numpts,1);
        data(:,2) = randi(imsiz(1),numpts,1);
        data(:,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
        data(:,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;

        % 1st line
        idx = 1:Ycr(Yci);
        data(idx,1) = data(idx,1) + Xfl;
        % 2nd line
        idx = (Ycr(Yci)+1):(2*Ycr(Yci));
        data(idx,1) = data(idx,1) + Xfl + Xd;

        %
        % rendering
        IM{1} = smlm_2Drendering(data, roi, resolution, 'density');
        IM{2} = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',10));
        IM{3} = smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',10));
        %IM{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference'));
        IM{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','area'));
        IM{5} = smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',10));
        IM{6} = smlm_2Drendering(data, roi, resolution, 'gauss');

        for imi=1:length(IM)
            for poweri = 1:length(power)
                results(Yci,poweri,imi) = saddle_index(transform(power(poweri),IM{imi}), [Xfl Xfl+Xd], PSM);
            end
        end
    end
    
    save('results_transforms_fixed_sepdist.mat', 'results'); % save for later analysis
end
%%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%%
function si = saddle_index(IM, Xg, PSM)
    IM = sum(integral_norm(IM,length(Xg)/2), 1);
    for Xgi=1:length(Xg)
        if mod(Xgi,2) == 0, continue, end;
        % saddle minimum
        im3=IM(Xg(Xgi):Xg(Xgi+1));
        [c3,i3]=min(im3);
        i3=i3+Xg(Xgi)-1;
        % left from the saddle
        im1=IM((Xg(Xgi)-3*PSM):i3);
        [c1,i1]=max(im1);
        i1=i1+Xg(Xgi)-3*PSM-1;
        % right from the saddle
        im2=IM(i3:((Xg(Xgi+1)+3*PSM)));
        [c2,i2]=max(im2);
        i2=i2+i3-1;
        % saddle index
        if (i1 < i3) && (i3 < i2) % => (i1 < i2)
            si = (min(c1,c2) - c3) / max(c1,c2);
        else
            si = 0;
        end
    end
end
%%
function analysis()
    load('results_transforms_fixed_sepdist.mat');    % results matrix (100x20x6) --> (molecules per 600px line [100:100:10000] * powers[1:20] * rendering methods [1:6])
    titles{1} = 'density'; titles{2} = 'scatter'; titles{3} = 'quadtree';
    titles{4} = 'triangulation'; titles{5} = 'histogram'; titles{6} = 'gauss';
    plot_compare_powers(results, titles);
end
%%
function plot_compare_powers(results, titles)
    figure('name','Compare powers dependency');
    cc = hsv(length(titles));
    hold on
    for fi=1:length(titles)
        plot(1:20, mean(results(:,:,fi), 1), 'color', cc(fi,:))
        title('Compare powers dependency')
        xlabel('Transform power (IM.^{power})')
        ylabel('Separation Index averaged over separation distances')
    end
    plot([1 20],[0.5 0.5],'--g','LineWidth',2,'Color',[0 0 0]+0.8)
    titles{length(titles)+1} = 'Distinguishability boundary';
    legend(titles, 'Location', 'SouthEast')
    hold off
end
%%
function IM = transform(power,IM)
    IM = IM .^ power;            % transform
end