% Note: the difference between 'density' and 'gauss' is that 'gauss' is
%       using amplitude parameter and the 'density' does not
%
function compare_profiles
    %
    % config
    imsiz = [ 600 600 ];   % [rows cols] => [height width]
    Xg = [ 40 50, 140 155, 240 260, 340 365, 440 470, 530 565 ]; % X - grid points (the commas are only for visual reasons)
    Yc = 5000;  % Y - count of points
    numpts = length(Xg) * Yc;
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    %
    % datagen
    data = zeros(numpts,4); % [x y precision amplitude]
    data(:,1) = PSM*randn(numpts,1);
    data(:,2) = randi(imsiz(1),numpts,1);
    data(:,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
    data(:,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;

    for i=1:length(Xg)
        idx = ((i-1)*Yc+1):(i*Yc);
        data(idx,1) = data(idx,1) + Xg(i);
    end

    %
    % rendering
    titles{1} = 'density';
    images{1} = transform3(smlm_2Drendering(data, roi, resolution, 'density'));
    draw_figures('Rendering: Density', images{1}, imsiz, PSM, Xg);
    
    titles{2} = 'scatter';
    images{2} = transform3(smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',50)));
    draw_figures('Rendering: Scatter', images{2}, imsiz, PSM, Xg);
    
    titles{3} = 'quadtree';
    images{3} = transform3(smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',50)));
    draw_figures('Rendering: Quadtree', images{3}, imsiz, PSM, Xg);
    %{
    titles{4} = 'triangulation (intensity = circ; avg = 10)';
    images{4} = transform(5,3e-3,3e-1,smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference')));
    draw_figures('Rendering: Triangulation (circumference)', images{4}, imsiz, PSM, Xg);
    %}
    titles{4} = 'triangulation (intensity = area; avg = 50)';
    images{4} = transform3(smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',50,'delaunay_thr',10,'intensity','area')));
    draw_figures('Rendering: Triangulation (area)', images{4}, imsiz, PSM, Xg);
    
    titles{5} = 'histogram';
    images{5} = transform3(smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',50)));
    draw_figures('Rendering: Histogram', images{5}, imsiz, PSM, Xg);
    
    titles{6} = 'gauss';
    images{6} = transform3(smlm_2Drendering(data, roi, resolution, 'gauss'));
    draw_figures('Rendering: Gauss', images{6}, imsiz, PSM, Xg);
    
    compare_distributions('Compare distibutions (si = saddle index) [si -> 0 : no separation] [si -> 1 : full separation]', images, titles, imsiz, PSM, Xg);
end
%%
% helper functions
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%
function draw_figures(figid, IM, imsiz, PSM, Xg)
    figure('Name',figid,'NumberTitle','off')
    subplot(2,1,1)
    % rendered image
    imagesc(minmax_norm(IM))
    colormap(hot)
    subplot(2,1,2)
    % ideal
    hold on
    for i=1:length(Xg)
        y = normpdf(1:imsiz(2), Xg(i), PSM);
        plot(1:imsiz(2),integral_norm(y, 0.5),'-g')
    end
    % measured
    plot(sum(integral_norm(IM,length(Xg)/2), 1),'-r')
    hold off
end
%
function compare_distributions(figid, images, titles, imsiz, PSM, Xg)
    figure('Name',figid,'NumberTitle','off')
    for imi=1:length(images)
        subplot(3,2,imi)
        % ideal
        hold on
        for i=1:length(Xg)
            y = normpdf(1:imsiz(2), Xg(i), PSM);
            plot(1:imsiz(2),integral_norm(y, 0.5),'-g')
        end
        % measured
        IM = sum(integral_norm(images{imi},length(Xg)/2), 1);
        plot(IM,'-r')
        title(titles{imi})
        % saddle index
        for Xgi=1:length(Xg)
            if mod(Xgi,2) == 0, continue, end;
            % saddle minimum
            im3=IM(Xg(Xgi):Xg(Xgi+1));
            [c3,i3]=min(im3);
            i3=i3+Xg(Xgi)-1;
            % left from the saddle
            im1=IM((Xg(Xgi)-3*PSM):i3);
            [c1,i1]=max(im1);
            i1=i1+Xg(Xgi)-3*PSM-1;
            % right from the saddle
            im2=IM(i3:((Xg(Xgi+1)+3*PSM)));
            [c2,i2]=max(im2);
            i2=i2+i3-1;
            % saddle index
            if (i1 < i3) && (i3 < i2) % => (i1 < i2)
                saddle_index = (min(c1,c2) - c3) / max(c1,c2);
            else
                saddle_index = 0;
            end
            text(Xg(Xgi)-3*PSM, 0.05, sprintf('si = %.2f', saddle_index));
        end
        hold off
    end
end
%%
function si = saddle_index(IM, Xg, PSM)
    IM = sum(integral_norm(IM,length(Xg)/2), 1);
    for Xgi=1:length(Xg)
        if mod(Xgi,2) == 0, continue, end;
        % saddle minimum
        im3=IM(Xg(Xgi):Xg(Xgi+1));
        [c3,i3]=min(im3);
        i3=i3+Xg(Xgi)-1;
        % left from the saddle
        im1=IM((Xg(Xgi)-3*PSM):i3);
        [c1,i1]=max(im1);
        i1=i1+Xg(Xgi)-3*PSM-1;
        % right from the saddle
        im2=IM(i3:((Xg(Xgi+1)+3*PSM)));
        [c2,i2]=max(im2);
        i2=i2+i3-1;
        % saddle index
        if (i1 < i3) && (i3 < i2) % => (i1 < i2)
            si = (min(c1,c2) - c3) / max(c1,c2);
        else
            si = 0;
        end
    end
end
%%
function IM = transform(power,thr_low,thr_high,IM)
    IM = IM .^ power;            % transform
    IM(IM<thr_low) = thr_low;    % threshold (low)
    IM(IM>thr_high) = thr_high;  % threshold (high)
    IM = minmax_norm(IM);        % normalize
    %IM = atan(IM);  % transform intensities into <0;1> range (don't worry about negative values - there shouldn't be any)
end
%%
function IM = transform2(IM)
    %
    % prepare
    %IM(:,1:120) = 0.0; % because of oversaturation of the first "double-line"
    K = zeros(7,7);
    K(4,:) = [ 0.0625 0.125 0.5 1 0.5 0.125 0.0625 ];
    K(:,4) = K(4,:)';
    %
    % transform
    IM=IM.^3;
    IM = imresize(IM,4,'nearest');
    IM = imerode(IM,strel('rectangle',[5,5]));
    IM = imdilate(IM,strel('rectangle',[3,3]));
    IM = conv2(IM,K,'same');
    IM = imresize(IM,0.25,'nearest');
    %
    % results
    %fprintf('SI=%.2f\n',saddle_index(IM,[140 155],5));
    %figure(95);imagesc(IM);
end
%%
function IM = transform3(IM)
    IM = IM.^3;
end