% Note: the difference between 'density' and 'gauss' is that 'gauss' is
%       using amplitude parameter and the 'density' does not
%
function compare_dependencies(recompute_results)
    if (recompute_results == 1) || ~exist('results.mat', 'file')
        compute_results();
    end
    analysis();
end
%%
function compute_results()
    %
    % config
    imsiz = [ 600 600 ];   % [rows cols] => [height width]
    Xdr = 1:40; % X - distances range
    Ycr = 100:100:10000;  % Y - count of points range; [100 200 ... 9900 10000]
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    Xfl = 40;   % first line
    %
    results = zeros(length(Xdr),length(Ycr),6); % 6 rendering methods
    for Xdi = 1:length(Xdr)
        for Yci = 1:length(Ycr)
            fprintf('\n============== Progress: Xd=%d,Yc=%d ==============\n\n',Xdr(Xdi),Ycr(Yci));
            % datagen
            numpts = 2*Ycr(Yci);
            data = zeros(numpts,4); % [x y precision amplitude]
            data(:,1) = PSM*randn(numpts,1);
            data(:,2) = randi(imsiz(1),numpts,1);
            data(:,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
            data(:,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;

            % 1st line
            idx = 1:Ycr(Yci);
            data(idx,1) = data(idx,1) + Xfl;
            % 2nd line
            idx = (Ycr(Yci)+1):(2*Ycr(Yci));
            data(idx,1) = data(idx,1) + Xfl + Xdr(Xdi);

            %
            % rendering
            IM = smlm_2Drendering(data, roi, resolution, 'density');
            results(Xdi,Yci,1) = saddle_index(IM, [Xfl Xfl+Xdr(Xdi)], PSM);

            IM = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',10));
            results(Xdi,Yci,2) = saddle_index(IM, [Xfl Xfl+Xdr(Xdi)], PSM);

            IM = smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',10));
            results(Xdi,Yci,3) = saddle_index(IM, [Xfl Xfl+Xdr(Xdi)], PSM);

            IM = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference'));
            %IM = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','area'));
            results(Xdi,Yci,4) = saddle_index(IM, [Xfl Xfl+Xdr(Xdi)], PSM);

            IM = smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',10));
            results(Xdi,Yci,5) = saddle_index(IM, [Xfl Xfl+Xdr(Xdi)], PSM);

            IM = smlm_2Drendering(data, roi, resolution, 'gauss');
            results(Xdi,Yci,6) = saddle_index(IM, [Xfl Xfl+Xdr(Xdi)], PSM);
        end
    end
    
    save('results.mat', 'results'); % save for later analysis
end
%%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%%
function si = saddle_index(IM, Xg, PSM)
    IM = sum(integral_norm(IM,length(Xg)/2), 1);
    for Xgi=1:length(Xg)
        if mod(Xgi,2) == 0, continue, end;
        % saddle minimum
        im3=IM(Xg(Xgi):Xg(Xgi+1));
        [c3,i3]=min(im3);
        i3=i3+Xg(Xgi)-1;
        % left from the saddle
        im1=IM((Xg(Xgi)-3*PSM):i3);
        [c1,i1]=max(im1);
        i1=i1+Xg(Xgi)-3*PSM-1;
        % right from the saddle
        im2=IM(i3:((Xg(Xgi+1)+3*PSM)));
        [c2,i2]=max(im2);
        i2=i2+i3-1;
        % saddle index
        if (i1 < i3) && (i3 < i2) % => (i1 < i2)
            si = (min(c1,c2) - c3) / max(c1,c2);
        else
            si = 0;
        end
    end
end
%%
function analysis()
    % analysis
    %{
    load('results.mat');    % results matrix (40x100x6) --> (separation in pixels [1:40] * molecules per 600px line [100:100:10000] * rendering methods [1:6])
    titles{1} = 'density'; titles{2} = 'scatter'; titles{3} = 'quadtree';
    titles{4} = 'triangulation'; titles{5} = 'histogram'; titles{6} = 'gauss';
    plot_compare_dependencies(results, titles);
    plot_compare_separation_distances(results, titles);
    plot_compare_molecules_counts(results, titles);
    %}
    % verze s pouzitim nove verze triangulace
    load('results_triangulation_circumference.mat');
    results_tc = results;
    %
    load('results.mat');    % results matrix (40x100x6) --> (separation in pixels [1:40] * molecules per 600px line [100:100:10000] * rendering methods [1:6])
    results(:,:,7) = results_tc(:,:,4);
    titles{1} = 'density'; titles{2} = 'scatter'; titles{3} = 'quadtree';
    titles{4} = 'triangulation (area)'; titles{5} = 'histogram'; titles{6} = 'gauss';
    titles{7} = 'triangulation (circ)';
    %plot_compare_dependencies(results, titles);
    plot_compare_separation_distances(results, titles);
    plot_compare_molecules_counts(results, titles);
end
%%
function plot_compare_dependencies(results, titles)
    %f = fspecial('gaussian',[5 5],1);
    for fi=1:size(results,3)
        subplot(3,2,fi)
        %imagesc(imfilter(results(:,:,fi),fi,'replicate','same'))
        imagesc(results(:,:,fi))
        title(titles{fi})
    end
end
%%
function plot_compare_molecules_counts(results, titles)
    figure('name','Compare molecules counts dependency');
    cc = hsv(length(titles));
    hold on
    for fi=1:length(titles)
        plot(1:100:10000, mean(results(:,:,fi), 1), 'color', cc(fi,:))
        title('Compare molecules counts dependency')
        xlabel('Molecules per line')
        ylabel('Separation Index averaged over separation distances')
    end
    plot([1 10000],[0.5 0.5],'--g','LineWidth',2,'Color',[0 0 0]+0.8)
    titles{length(titles)+1} = 'Distinguishability boundary';
    legend(titles, 'Location', 'SouthEast')
    hold off
end
%%
function plot_compare_separation_distances(results, titles)
    figure('name','Compare separation distances dependency');
    cc = hsv(length(titles));
    hold on
    for fi=1:length(titles)
        plot(1:40, mean(results(:,:,fi), 2), 'color', cc(fi,:))
        title('Compare separation distances dependency')
        xlabel('Distance between centers of lines [px]')
        ylabel('Separation Index averaged over molecules counts')
    end
    plot([1 40],[0.5 0.5],'--g','LineWidth',2,'Color',[0 0 0]+0.8)
    titles{length(titles)+1} = 'Distinguishability boundary';
    legend(titles, 'Location', 'SouthEast')
    hold off
end