density_coef = 1.5;
margin = 40;
IM = zeros(500,1000);

i = margin;
xi = margin;
while xi < size(IM,2)-margin
    idy = randi(size(IM,1),2000,1);
    idx = xi + 10.*randn(length(idy),1);
    %scatter(idx,idy,'x');
    %hold on
    idxy = sub2ind(size(IM), round(idy), round(idx));
    IM(idxy) = 1;
    i = i * density_coef;
    xi = xi + i;
end
%hold off
imagesc(IM)
colormap(gray)




% zmena: udelat proste vektor X hodnot a je to...neni potreba resit naky
% koeficienty a podobny ptakoviny
% todo: vzit tenhle kod a dat ho do testu renderovaciho
% todo: generovat profily intenzit