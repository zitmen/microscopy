clear

imsiz = [255 255];
cnt = imsiz/2 + [0 0];
delta = 0*pi/180;
nthr = 4;

thr = linspace(-pi+pi/nthr,pi,2*nthr)+delta;
thr =[thr-2*pi thr thr+2*pi];
thr = reshape(thr,2,length(thr)/2);
[X,Y] = meshgrid(1:imsiz(2),1:imsiz(1));
[fi,rad] = cart2pol(X-cnt(2),Y-cnt(1));
mask = false(imsiz);
for I = 1:size(thr,2)
    mask = mask | (fi > thr(1,I) & fi <= thr(2,I));
end
mask = mask & rad>0 & rad<100;

%imagesc(mask)
imshow(mask)