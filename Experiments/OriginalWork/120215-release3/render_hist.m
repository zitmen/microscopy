function res = render_hist(results, imsize, cfg)

% 7.10.2010, PK, saving moved to res_render
% 25.5.2010, PK, averaging and random jitter, blure filter, saving
% 27.4.2010, PK, in: results, cfg; out: IM; print & save results

if (cfg.render.average < 1)
  error('render_hist: cfg.render.average must be >= 1');
end

%% rendering rules & decode results

a = cfg.resolution;
xpos = results(:,1);
ypos = results(:,2);
sigma = results(:,3);
N = results(:,4);
dx = results(:,5);
npts = length(xpos);


%% generate histograms
%  dx = 20;
dxstd = dx/a;
% dxstd = sqrt(2*log(2))*dx/a;

newimsize = cfg.render.scale * imsize;% - cfg.render.scale + 1;
xbin = linspace(0,imsize(2),newimsize(2)+1);
ybin = linspace(0,imsize(1),newimsize(1)+1);

fprintf('Generating %d histogram(s)\n', cfg.render.average);
fprintf('Progress ');
IM = zeros(newimsize,'single');
for I = 1%:cfg.render.average

  xp = xpos;
  yp = ypos;
  
%   % random jitter - add normal noise to localized points
%   if (cfg.render.average > 1)
%     xp = xp + (dxstd .* randn(npts,1));
%     yp = yp + (dxstd .* randn(npts,1));
%   end
 
  % accumulate histograms
  IM = IM + hist2(xp,yp,xbin,ybin);
  
  if (mod(I,round(cfg.render.average/10)) == 0), fprintf('.'); end;   
end
fprintf('\n');
% compute average
if cfg.render.average > 1
  IM = IM/cfg.render.average;
end

%% blure filter
if isfield(cfg.render,'blure') && ~isempty(cfg.render.blure)
  h=fspecial(cfg.render.blure{1},cfg.render.blure{2}{:});
  IM = imfilter(IM,h);
end;

%% results
res.name = 'hist';
res.im = IM;

