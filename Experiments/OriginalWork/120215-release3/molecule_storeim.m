function imbuf = molecule_storeim(img, location, imbuf, cfg)

% imbuf.active = -1         ... free buffer index
% imbuf.active =  0         ... ready for fitting
% imbuf.active =  1         ... active item
% imbuf.active =  2,3,4,..  ... timeout - 

persistent box;

if isempty(imbuf)
  imbuf = struct('im',{}, 'pos', {}, 'frame', {}, 'numframes',{}, 'active', {});
  box = -cfg.box:cfg.box;
end

im = img.imraw;
loc = location.loc;

% remove points which are at the border location
valid = ~( (loc(:,1)-cfg.box) < 1 | (loc(:,2)-cfg.box) < 1 | ...
           (loc(:,1)+cfg.box) > img.siz(1) | (loc(:,2)+cfg.box) > img.siz(2) );
loc = loc(valid,:);
numloc = size(loc,1);

% initialize - empty buffer, create first item
K = 1;
update = false;
mindistsq = cfg.mindist^2;

% timeout between frames for undetected molecules: if (active>0), active++
idx = [imbuf.active]>0;
tmp = num2cell([imbuf(idx).active]+1);
[imbuf(idx).active] = tmp{:};

% update list of image boxes for all detected points 
active = cat(1,imbuf.active);  % items activity
pos = cat(1,imbuf.pos);        % [u,v] box position
idxact = active > 0;
for I = 1:numloc

  pt = loc(I,1:2);
  
  if ~isempty(imbuf)
    
    % is the point in the list of active items    
    [val,idx] = min(ptdist2(pt, pos(idxact,:)));
    
    % update an existing item if distance is smaller than threshold
    if val < mindistsq
      update = true;
      tmp = find(idxact);
      K = tmp(idx);
    else      
      update = false;
      % locate first empty position in the list
      idx = find(active == -1, 1, 'first');
      if isempty(idx)
        K = length(imbuf) + 1;   % create new item 
      else
        K = idx;                  % use allocated memory: faster + no memory defragmentation
      end
    end
  end
    
  if update
    % update an existing item - integrate image and bacground in the box at position [u,v]
    imbuf(K).numframes = imbuf(K).numframes + 1;
    imbuf(K).im = imbuf(K).im + im(imbuf(K).pos(1)+box, imbuf(K).pos(2)+box);
    imbuf(K).active = 1;
  else    
    % create a new item - image and bacground in the box at position [u,v]
    imbuf(K,1).im = im(pt(1)+box,pt(2)+box);
    imbuf(K).pos = pt;
    imbuf(K).numframes = 1;
    imbuf(K).frame = img.frame;
    imbuf(K).active = 1;
    active(K) = 1;
    pos(K,:) = pt;
    idxact(K) = true;
  end
  
end

% timeout between frames for undetected molecules
% if timeout
idx = find([imbuf.active] > cfg.maxtimeout);
for I = 1:length(idx)
  J = idx(I);
  imbuf(J).active = 0;
end

%eof