function img = imgnext(img)

img.imraw = iq2imgload(img, [], img.frame);
img.frame = img.frame + 1;
