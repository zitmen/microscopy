% Test performance of the localization on the static dots

%  addpath('..\120215-release3');
%  addpath('..\utils');
clear

cfg = cfg_mcstatic();

% initialize
img = mc_imginit(cfg);
results = [];
resupdate('free');

disp(cfg.method(cfg.methodrun).name);
fprintf('Analyzing\nProgress ');
while img.frame < cfg.numimgs 

  % load next image
  img = mc_imgload(img,cfg);

  % detect molecules
  loc = mc_molecule_detect(img, cfg);

  % store image
  [imbuf,loc] = mc_molecule_storeim(img, loc, [], cfg);
      
  % fit model
  fits = molecule_fitting(img, imbuf, [], cfg);
  fits.fits = resupdate('save');
  
  % evaluate results
  results = mc_evaluate3(img, loc, fits, results, cfg);
  
  %progress
  if mod(img.frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end
fprintf('\ndone\n\n');

results.ok = results.ok(1:results.cok,:);
results.fn = results.fn(1:results.cfn,:);
results.fp = results.fp(1:results.cfp,:);

if ~isdir(cfg.respath), mkdir(cfg.respath); end
save([cfg.respath 'res_dst2_' cfg.method(cfg.methodrun).name '.mat'],'-struct','results');
