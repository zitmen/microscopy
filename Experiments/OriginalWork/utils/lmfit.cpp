/*
 * Levenberg-Marquardt Least Squares Fitting
 *
 *  [par, exitflag, residua, numiter, message] = lmfit(funstr, par0, X, y, varargin)
 *
 */

#include <math.h>
#include <float.h>
#include "mex.h"
#include "lmmin.h"

typedef struct {

	char *funstr;		// string with function name

	size_t numpar;		// number of function parameters
	double *par0;		// initial parameters
	double *par;		// actual parameters

	size_t numdata;		// number of data points
	size_t numvar;		// number of input variables
	double *vardata;	// given data variables	       x1,x2,x3, ...
	double *fundata;	// given function values       fun(x1,x2,x3, ...)

	size_t numfunargs;
	mxArray **funargs;

} my_data_type;

// ------------------------------------------------------------------------
// Compute residua for all data points
// ------------------------------------------------------------------------
void my_evaluate(double *params, int m_dat, double *fvec, void *data, int *info)
{
    my_data_type *mydata = (my_data_type *) data;
	mxArray *lhs;
	double *funvec, *fundata = mydata->fundata, *par = mydata->par;
	int		status, i;
	
	// update function parameters
	if (par != params)
		for (i = 0; i < (int) mydata->numpar; i++)
			*(par++) = *(params++);

	// Y = fun(X,par);	
	status = mexCallMATLAB(1, &lhs, mydata->numfunargs, mydata->funargs, mydata->funstr);
	    
	// F = Y - fun(X,par)
	funvec = mxGetPr(lhs);		// computed data
    for (i = 0; i < m_dat; i++)
		*(fvec++) = *(fundata++) - *(funvec++);

	// info
	if (status)
		*info = -1;
	else
		*info = *info;
}

// ------------------------------------------------------------------------
// Control parameters
// ------------------------------------------------------------------------

void init_control_default( lm_control_type * control )
{
    control->maxcall = 100;
    control->epsilon = LM_USERTOL;
    control->stepbound = 100.;
    control->ftol = LM_USERTOL;
    control->xtol = LM_USERTOL;
    control->gtol = LM_USERTOL;
}

void init_control_userdef(lm_control_type * control, const mxArray *options)
{
	mxArray *ptr;

	if ((ptr = mxGetField(options, 0, "maxcall")) != NULL)
		control->maxcall = (int) mxGetScalar(ptr);

	if ((ptr = mxGetField(options, 0, "epsilon")) != NULL)
		control->epsilon = mxGetScalar(ptr);

	if ((ptr = mxGetField(options, 0, "stepbound")) != NULL)
		control->stepbound = mxGetScalar(ptr);

	if ((ptr = mxGetField(options, 0, "ftol")) != NULL)
		control->ftol = mxGetScalar(ptr);

	if ((ptr = mxGetField(options, 0, "xtol")) != NULL)
		control->xtol = mxGetScalar(ptr);

	if ((ptr = mxGetField(options, 0, "gtol")) != NULL)
		control->gtol = mxGetScalar(ptr);
}

// ------------------------------------------------------------------------
// MAIN 
// ------------------------------------------------------------------------

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]) {

  // data and pameter arrays:
    lm_control_type		control;
    my_data_type		data;
	size_t len, i;
	
	// test number of input arguments
	if (nrhs < 4) {
		mexPrintf("\nLevenberg-Marquardt Least Squares Fitting\n\n");
		mexPrintf(" mininize  sum((y-fun(X,par))^2) over a set of parameters \n\n");
		mexPrintf(" Usage:\n\n");
		mexPrintf(" [par, exitflag, residua, numiter, message] = lmfit(funstr, par0, X, y, options, varargin);\n\n");
		mexPrintf("   funstr   ...                 string with function name\n");
		mexPrintf("   par0     ... [1 x numpar]    initial function parameters\n");
		mexPrintf("   X        ... [npts x numvar] given data variables   x1,x2,x3,...\n");
		mexPrintf("   y        ... [npts x 1]      given function values  fun(x1,x2,x3...)\n");
		mexPrintf("   options  ... struct          fileds: maxcall  / epsilon / stepbound / ftol / xtol / gtol\n");
		mexPrintf("   varargin ...                 other parameters: fun(X,par,varargin{:})\n");
		mexPrintf("   par      ...                 optimized parameters\n");
		mexPrintf("   exitflag ...                 status\n");
		mexPrintf("   residua  ... [npts x 1]      y-fun(X,par)\n");
		mexPrintf("   numiter  ...                 number of iterations\n");
		mexPrintf("   message  ...                 string with status message\n\n");
		return;
	}

	// prhs[0]: read string with function name
	if (mxIsEmpty(prhs[0]) || !mxIsChar(prhs[0]))
		mexErrMsgTxt("Function name has to be a string.");
	len = mxGetNumberOfElements(prhs[0]) + 1;
	data.funstr = (char *) mxCalloc(len, sizeof(char));
	if (mxGetString(prhs[0], data.funstr, len) != 0)
		mexErrMsgTxt("Could not convert string data.");

	// prhs[1]: initial parameters and output value
	if (mxIsEmpty(prhs[1]) || !mxIsDouble(prhs[1]))
		mexErrMsgTxt("Initial parameters par0 has to be double vector.\n");
	data.numpar = mxGetNumberOfElements(prhs[1]);
	data.par0 = mxGetPr(prhs[1]);			// save par0

	// prhs[2]: given data variables      x1,x2,x3, ...
	if (mxIsEmpty(prhs[2]) || !mxIsDouble(prhs[2]))
		mexErrMsgTxt("Variables must be stored in double matrix.\n");
	data.numvar = mxGetN(prhs[2]);
	data.numdata = mxGetM(prhs[2]);
	data.vardata = mxGetPr(prhs[2]);

	// prhs[3]: given function values     fun(x1,x2,x3, ...)
	if (mxIsEmpty(prhs[3]) || !mxIsDouble(prhs[3]))
		mexErrMsgTxt("Function values must be double vector.\n");
	if (data.numdata != mxGetM(prhs[3]))
		mexErrMsgTxt("Input data are inconsistent.\n");
	data.fundata = mxGetPr(prhs[3]);

	// prhs[4]: options
	init_control_default(&control);
	if ((nrhs > 4) && !mxIsEmpty(prhs[4]) && mxIsStruct(prhs[4]))
		init_control_userdef(&control,prhs[4]);

	// initialize output par
	plhs[0] = mxDuplicateArray(prhs[1]);	
	data.par = mxGetPr(plhs[0]);

	// function arguments
	data.numfunargs = __max(2,2+nrhs-5);
	data.funargs = (mxArray**) mxMalloc(data.numfunargs * sizeof(mxArray*));
	data.funargs[0] = prhs[2];				// X
	data.funargs[1] = plhs[0];				// par
	for(i = 2; i < data.numfunargs; i++)	// varargin
		data.funargs[i] = prhs[i+3];
	
	// do the fitting
	lm_minimize(data.numdata, data.numpar, data.par, my_evaluate, NULL, &data, &control);

	// exit flag
	if (nlhs > 1)
		plhs[1] = mxCreateDoubleScalar(control.info);

	// residual error  F = Y - fun(X,par)
	if (nlhs > 2) {
		plhs[2] = mxCreateDoubleMatrix(data.numdata,1,mxREAL);
		double *ptr = mxGetPr(plhs[2]);
		my_evaluate(data.par, data.numdata, ptr, &data, &control.info);
	}

	// # iterations
	if (nlhs > 3)
		plhs[3] = mxCreateDoubleScalar(control.nfev);

	// message
	if (nlhs > 4)
		plhs[4] = mxCreateString(lm_infmsg[control.info]);

	mxFree(data.funargs);
}
