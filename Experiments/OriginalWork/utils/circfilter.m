function idx = circfilter(idx,imsize,mindist)

d = size(mindist,2);
if d == 1
  mindist(2) = mindist(1);
elseif d~= 2
  error('localmax2d: Vector mindist has to be 2D.');
end

% invalidate all points in neighbourhood of [v(I),u(I)]
[u,v] = ind2sub(imsize,idx);
valid = true(length(v),1);
for I = 1:length(v)
  if ~valid(I), continue; end;
  ind = ((v(I)-v)/mindist(2)).^2 + ((u(I)-u)/mindist(1)).^2 > 1;
  ind(I) = true;
  valid = valid & ind;
end

% take only valid points
idx = idx(valid);   