function y = fun_gauss(x,par,partransform)

if nargin < 3, partransform = @fun_partransfdefault; end;
[x0,sigma,A,b] = partransform(par);

y = A * exp(-0.5 * ((x-x0)/sigma).^2 ) + b;
% y = A * exp(-0.5 * ((x-x0)/sigma).^2 ) / (sigma*sqrt(2*pi)) + b;
