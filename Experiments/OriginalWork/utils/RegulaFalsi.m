function [c, err, exitflag, numiter] = RegulaFalsi(fnc, par, a, b, delta, epsilon, maxiter)
% The Regula Falsi Method (or False Position Method) is a root-finding algorithm
% that combines features from the bisection method and the secant method.
% 
%
% [c, err, exitflag, numiter] = RegulaFalsi(fnc, par, a, b, delta, epsilon, maxiter)
%
%    fnc       ... function handle
%    par       ... function parameters
%    a, b      ... b > a is an interval, where to find roots
%    delta     ... convergence tolerance for c (default 1E-3)
%    maxiter   ... max number of iteratins (default 100)
%    c         ... solution, e.i., where f(c) = 0
%    err       ... error of estimate for c
%    exitflag  ... 0 for ok, -1 in numiter exeeds maxiter
%    numiter   ... number of iterations

% 2.6.2011, PK

if (nargin < 5), delta = 1E-6; end;
if (nargin < 6), epsilon = 1E-6; end;
if (nargin < 7), maxiter = 100; end;

fa = fnc(a,par);
fb = fnc(b,par);
numiter = 0; exitflag = 0;
for numiter = 1:maxiter
  
  c = (a*fb-b*fa)/(fb-fa);
  fc = fnc(c,par);
  
  if fc == 0
    break;
  elseif fb*fc > 0
    b = c; fb = fc;
  else
    a = c; fa = fc;
  end

  if ((b-a) < delta), break; end;
  if (abs(fc) < epsilon), break; end;
  
end

if (numiter == maxiter), exitflag = -1; end;
err = (b-a)/2;
