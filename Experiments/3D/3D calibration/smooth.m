function xf = smooth(x,n)
% vyhlazeni funkce plovoucim prumerem z 2n+1 bodu

% 8.8.2008, KR

npts = size(x,2);
xf = double(x);


for I = n+1:(npts-n-1)
  xf(:,I) = mean(x(:,I-n:I+n),2);
end



% function xout = smooth(xin,sigma)
% % Smooth input function with Gaussian
% % IN:
% %   xin    ... input vector
% %   sigma  ... kernel size is 2*3*sigma+1
% % OUT:
% %   xout   ... smoothed function
% 
% % 26.3.2009, KR
% 
% if nargin < 1, sigma = 1; end;
% 
% % 3*sigma neighbour
% width = round(3*sigma);
% g = gauss(-width:width,0,sigma);
% 
% % convolution with gauss
% xout = conv(xin,g);
% xout = xout(width+1:end-width);
% 
% function y = gauss(x,mu,sigma)
% 
% y = 1/sqrt(2*pi)/sigma * exp(-0.5*((x-mu)./sigma).^2);



%eof