function varargout = cylinder_calibrate2(filename,varargin)
%
% 
%

cmd = struct(varargin{:});

[pathstr, fname, ext] = fileparts(filename);
info = loadPAMinfo([pathstr '/' fname '.txt']);

img.datapath = [pathstr '/'];
img.filename = fname;
img.ext = ext;
img.norm = 2^info.bitdepth;
img.imsize = [info.y info.x];
img.blure = fspecial('gauss',11,1.3);
img.info = info;


switch cmd.cmd
  case 'setthr'         % 1) find threshold
    varargout{1} = showthr(img,cmd);
    
  case 'getangle'          % 2) find angle
    varargout{1} = getangle(img,cmd);
    
  case 'getzmap'          % 3) z calibration
    varargout{1} = getzcalibration(img,cmd);
    
  otherwise
    error('Unknown command');
end
    

% -----------------------------------------------
function img = imgload(img,Z)

img.im = double(imread([img.datapath img.filename img.ext],Z))/img.norm;

% -----------------------------------------------
function [u,v] = finddots(img,cmd)

imb = imfilter(img.im,img.blure,'same','replicate');
[val,idx] = locmax2d(imb,cmd.thr);
[u,v] = ind2sub(img.imsize,idx);

% -----------------------------------------------
function thr = showthr(img,cmd)

if ~isfield(cmd,'thr'), cmd.thr = 0; end;
if ~isfield(cmd,'Z'), cmd.Z = 1; end;

img = imgload(img,cmd.Z);
[u,v] = finddots(img,cmd);

thr = cmd.thr;

figure(1); clf
hold on
imagesc(img.im,[0 0.2]);
colormap gray
axis image off
set(gca,'Position',[0 0 1 1])
plot(v,u,'rx')

% -----------------------------------------------
function alfa = getangle(img,cmd)

info = img.info;
%if ~isfield(cmd,'thr'), cmd.thr = 0; end;
if ~isfield(cmd,'Z'), cmd.Z = 1; end;

% find beads centers just once in the middle image
img = imgload(img,round(mean(cmd.Z)));
[u,v] = finddots(img,cmd);
npts = length(u);

box = -9:9;
[X(:,:,2),X(:,:,1)] = meshgrid(box,box);
fi = (0:89)*pi/180; %brute force - try every angle, resolution 1 deg
numfi = length(fi);
numz = length(cmd.Z);
resupdate('free'); 

for Z = 1:numz
  img = imgload(img,cmd.Z(Z));
  chi2 = zeros(1,90,'single');
  for I = 1:npts
    try
      im = img.im(u(I)+box,v(I)+box);
      for J = 1:numfi
        a0 = [0 0 max(im(:)) min(im(:)) 2 2];
        [a,chi2(J),exitflag] = lmfit2Dgausselliptic(a0, X, im, fi(J));
      end %J
      resupdate('add', chi2);
    catch
    end %try
  end %I
end %Z

chi2 = resupdate('save');

figure(2);plot(fi*180/pi,chi2')

% chi2 is a periodic function (T=pi/2), it is minimal when the fit is ok
% here we take maxima and minima to increase number of measurements
T = pi/2;
[val,idx1] = min(chi2,[],2);
[val,idx2] = max(chi2,[],2);
theta = [fi(idx1) fi(idx2)-T/2];
theta(theta<0) = theta(theta<0)+T;
% this extends range by T/2 - helps with angles near 0 and T
% fi = [fi, fi(1:45)+T];
% theta = [theta, theta(theta<T/2)+T];

xbin = fi(1:1:end);
cnt = hist(theta,xbin);
[foo,idx] = max(cnt);
alfa = xbin(idx);
%alfa = (xbin * cnt') / sum(cnt); % COG

figure(3)
bar(xbin*180/pi,cnt);

% -----------------------------------------------
function zmap = getzcalibration(img,cmd)

info = img.info;
%if ~isfield(cmd,'thr'), cmd.thr = 0; end;
%if ~isfield(cmd,'alfa'), cmd.alfa = 0; end;
if ~isfield(cmd,'Z'), cmd.Z = 1:info.z; end;

% find beads centers just once in the middle image
img = imgload(img,round(mean(cmd.Z)));
[u,v] = finddots(img,cmd);
npts = length(u);

box = -9:9;
[X(:,:,2),X(:,:,1)] = meshgrid(box,box);
z = info.z_start:info.z_step:info.z_end;
z = 1000*z(cmd.Z);
numz = length(z);

% compute params for all beads and for all layers
params = zeros(npts, 8, numz);
for Z = 1:numz
  img = imgload(img,cmd.Z(Z));

  for I = 1:npts
    try
      im = img.im(u(I)+box,v(I)+box);

      a0 = [0 0 max(im(:)) min(im(:)) 2 2];
      [a,chi2,exitflag] = lmfit2Dgausselliptic(a0, X, im, cmd.angle);
      
      params(I,:,Z) = [a,chi2,exitflag];
    catch
    end %try
  end %I
end %Z

% extract axis lengths
a = squeeze(params(:,5,:));  %a(a<0.5) = NaN;
b = squeeze(params(:,6,:));  %b(b<0.5) = NaN;
%----------- should interpolate/extrapolate or fix some otherway
idx = [find(a<0.5); find(b<0.5)];
[r,c] = ind2sub(size(a),idx);
idx = unique(r);
a(idx,:)=[];b(idx,:)=[]; npts = size(a,1);
%-----------
c = a./b;
% compute center point, here c = a/b = 1
center = zeros(npts,1);
for I = 1:npts
  center(I) = interp1(smooth(c(I,:),1),z,1,'linear');
end

zout = -400:20:400;
numz = length(zout);
aout = zeros(npts,numz);
bout = zeros(npts,numz);
cout = zeros(npts,numz);
for I = 1:npts
  aout(I,:) = interp1(z - center(I), a(I,:), zout,'linear');
  bout(I,:) = interp1(z - center(I), b(I,:), zout,'linear');
  cout(I,:) = interp1(z - center(I), c(I,:), zout,'linear');
end

FONTSIZE = 16;
hndl = figure(4); clf
set(hndl,'InvertHardcopy','off','Color','w')
hold on
errorbar(zout,nanmean(aout,1),nanstd(aout,[],1),'Color','b','LineWidth',1.4)
errorbar(zout,nanmean(bout,1),nanstd(bout,[],1),'Color','r','LineWidth',1.4)
xlabel('{\itz} [nm]', 'FontSize',FONTSIZE)
ylabel('\sigma [pixels]', 'FontSize',FONTSIZE)
title('Calibration curve:  PSF widths', 'FontSize',FONTSIZE)
set(gca,'XLim',[-410 410],'YLim',[1 5], 'FontSize',FONTSIZE)
legend({'\sigma_a','\sigma_b'},'Location','NorthWest','box','off','XColor','w','YColor','w','ZColor','w')
grid on
print(hndl,'-dpng','-r150','psfwidths.png')

hndl = figure(5); clf
errorbar(zout,nanmean(cout,1),nanstd(cout,[],1),'Color','b','LineWidth',1.4)
xlabel('{\itz} [nm]', 'FontSize',FONTSIZE)
ylabel('\sigma_a / \sigma_b  [-]', 'FontSize',FONTSIZE)
title('Calibration curve:  ratio of PSF widths', 'FontSize',FONTSIZE)
set(gca,'XLim',[-410 410],'YLim',[0.5 2], 'FontSize',FONTSIZE)
grid on
print(hndl,'-dpng','-r150','psfwidthsratio.png')


abratio = 0.5:0.01:1.7;
numabr = length(abratio);
zestim = repmat(NaN,npts,numabr);
for I = 1:npts
  [foo,z1idx] = max(c(I,:));
  [foo,z2idx] = min(c(I,:));
  idx = z1idx:z2idx;
  zestim(I,:) = interp1(c(I,idx), z(idx) - center(I), abratio,'linear');
end

zmap = [abratio;nanmean(zestim,1);nanstd(zestim,[],1)]';


hndl = figure(6); clf
errorbar(zmap(:,1),zmap(:,2),zmap(:,3),'Color','b','LineWidth',1.4)
xlabel('\sigma_a / \sigma_b [-]', 'FontSize',FONTSIZE)
ylabel('{\itz} [nm]', 'FontSize',FONTSIZE)
title('Calibration curve:  {\itz}-position estimate', 'FontSize',FONTSIZE)
set(gca,'XLim',[0.49 1.71],'YLim',[-300 300], 'FontSize',FONTSIZE)
grid on
print(hndl,'-dpng','-r150','zcalibration.png')


hndl = figure(7); clf
plot(zmap(:,2),zmap(:,3),'LineWidth',1.4)
xlabel('{\itz} [nm]', 'FontSize',FONTSIZE)
ylabel('STD of the estimate [nm]', 'FontSize',FONTSIZE)
title('Error of the {\itz}-position estimate', 'FontSize',FONTSIZE)
set(gca,'XLim',[-250 250],'YLim',[0 50], 'FontSize',FONTSIZE)
grid on
print(hndl,'-dpng','-r150','zcalibrationerror.png')

dlmwrite('zcal.txt',zmap)
% clr = lines(npts);
% figure(4); clf
% subplot(2,1,1)
% hold on
% for I = 1:npts-1
%   plot(z - center(I), smooth(a(I,:),1),'Color',clr(I,:))
% end
% subplot(2,1,2)
% hold on
% for I = 1:npts-1
%   plot(z - center(I), smooth(b(I,:),1),'Color',clr(I,:))
% end
% figure(5); clf
% hold on
% for I = 1:npts
%   plot(smooth(c(I,:),1),z - center(I),'Color',clr(I,:))
% end
% figure(5)
% plot(smooth(c,1)')





