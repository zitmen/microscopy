function cylinder_calibrate(filename,varargin)
%
% 
%

cmd = struct(varargin{:});

[pathstr, fname, ext] = fileparts(filename);
info = loadPAMinfo([pathstr '/' fname '.txt']);

img.datapath = [pathstr '/'];
img.filename = fname;
img.ext = ext;
img.norm = 2^info.bitdepth;
img.imsize = [info.y info.x];
img.info = info;

switch cmd.cmd
  case 'setthr'         % 1) find threshold
    showthr(img,cmd);
    
  case 'angle'          % 2) find angle
    getangle(img,cmd);
    
  case 'calib'          % 3) z calibration
    getzcalibration(img,cmd);
    
  otherwise
    error('Unknown command');
end
    

% -----------------------------------------------
function img = imgload(img,Z)

hfil = fspecial('gauss',11,1.3);
img.im = double(imread([img.datapath img.filename img.ext],Z))/img.norm;
img.imb = imfilter(img.im,hfil,'same','replicate');

% -----------------------------------------------
function showthr(img,cmd)

if ~isfield(cmd,'thr'), cmd.thr = 0; end;
if ~isfield(cmd,'Z'), cmd.Z = 1; end;

img = imgload(img,cmd.Z);
[val,idx] = locmax2d(img.imb,cmd.thr);
[u,v] = ind2sub(img.imsize,idx);

figure(1); clf
hold on
imagesc(img.im,[0 0.2]);
colormap gray
axis image off
set(gca,'Position',[0 0 1 1])
plot(v,u,'rx')

% -----------------------------------------------
function getangle(img,cmd)

box = -9:9;
[X(:,:,2),X(:,:,1)] = meshgrid(box,box);

info = img.info;
if ~isfield(cmd,'thr'), cmd.thr = 0; end;
if ~isfield(cmd,'Z'), cmd.Z = 1; end;

fi = (0:89)*pi/180;
numfi = length(fi);
numz = length(cmd.Z);
resupdate('free'); 

%params = zeros(90,8,npts);

for Z = 1:numz
  img = imgload(img,cmd.Z(Z));
  [val,idx] = locmax2d(img.imb,cmd.thr);
  [u,v] = ind2sub(img.imsize,idx);
  npts = length(idx);
  chi2 = zeros(1,90,'single');
  for I = 1:npts
    try
      im = img.im(u(I)+box,v(I)+box);
      for J = 1:numfi
        a0 = [0 0 max(im(:)) min(im(:)) 2 2];
        [a,chi2(J),exitflag] = lmfit2Dgausselliptic(a0, X, im, fi(J));
      end %J
      resupdate('add', chi2);
    catch
    end %try
  end %I
end %Z

chi2 = resupdate('save');


figure(2);plot(fi*180/pi,chi2')

[val,idx1] = min(chi2,[],2);
[val,idx2] = max(chi2,[],2);
theta = [fi(idx1) fi(idx2)-pi/4];
theta(theta<0) = theta(theta<0)+pi/2;
mean(theta)*180/pi


xbin = fi(1:4:end);
xedge = [xbin - diff(xbin(1:2))/2, Inf];
cnt = histc(theta,xedge); cnt(end-1) = cnt(end-1) + cnt(end); cnt = cnt(1:end-1);
bar(xbin*180/pi,cnt)


median(fi(idx(val < 0.04)))*180/pi
figure(3);clf
plot(fi*180/pi,chi2')




% % -----------------------------------------------
% function runcalibration(img,cmd)
% 
% info = img.info;
% if ~isfield(cmd,'thr'), cmd.thr = 0; end;
% if ~isfield(cmd,'Z'), cmd.Z = 1:info.z; end;
% 
% box = -9:9;
% [X(:,:,2),X(:,:,1)] = meshgrid(box,box);
% 
% z = info.z_start:info.z_step:info.z_end;
% z = z(cmd.Z);
% 
% numz = length(cmd.Z);
% 
% for I = 1:numz
%   
% end
% 









