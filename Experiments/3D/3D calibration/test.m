clear
addpath('d:\pavel\matlab\utils');

datapath = 'd:\data\blinking\cylinder lens\';
filename = '01.tif';

hfil = fspecial('gauss',11,1.3);
imnorm = 2^14;

box = -9:9;
[X(:,:,2),X(:,:,1)] = meshgrid(box,box); 

%%
I = 26;
IM = single(imread([datapath filename],I))/imnorm;
IMb = imfilter(IM,hfil,'same','replicate');
[m,n] = size(IM);

[val,idx] = locmax2d(double(IMb),0.1);
[u,v] = ind2sub([m,n],idx);

figure(1)
hold on
imagesc(IM,[0 0.2]);
colormap gray
axis image off
set(gca,'Position',[0 0 1 1])
plot(v,u,'rx')


figure(2); clf
subplot(2,1,1)
plot(fi,params{1}(:,5),'r',fi,params{1}(:,6),'b')
subplot(2,1,2)
plot(fi,params{1}(:,7),'b')


npts = length(idx);
params = [];%zeros(npts,7);
fi = linspace(0,pi/2,100);
for I = 1:npts
  try
    im = double(IM(u(I)+box,v(I)+box));

    %a0 = [0 0 3.5 max(im(:)) min(im(:))];
    %[a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);
    for J = 1:100
    a0 = [0 0 max(im(:)) min(im(:)) 4 4];
    [a,chi2,exitflag] = lmfit2Dgausselliptic(a0, X, im, fi(J));
    a(1:2) = a(1:2)+ [u(I) v(I)]; 
    params{I}(J,:) = [a chi2];
    end
        
  catch
    a = NaN;
  end
    
%   params(I,:) = a;  
end

chi2 = [];
for I = 1:npts
  if isempty(params{I}), chi2(I,:) = NaN; continue; end
  chi2(I,:) = params{I}(:,7)';
end

[val,idx] = min(chi2,[],2);

median(fi(idx(val < 0.04)))*180/pi
figure(3);clf
plot(fi*180/pi,chi2')


% plot(params(:,2),params(:,1),'bx')
