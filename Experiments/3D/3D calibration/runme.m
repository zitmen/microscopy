clear;
%addpath('d:\pavel\matlab\utils');
datapath = 'z:\blinking\data\beads\beads-cylinder-lens-20100511\10nm step\z calibration 10 nm step 170 nm beads\';
filename = 'z calibration 03';

bitdepth = 14;
norm = 2^bitdepth;
hfil = fspecial('gauss',11,1.3);
box = -9:9;
[X(:,:,2),X(:,:,1)] = meshgrid(box,box); 

info = loadPAMinfo([datapath filename '.txt']);

%%
Z = 70;

IM = double(imread([datapath filename '.tif'],Z))/norm;
IMb = imfilter(IM,hfil,'same','replicate');
[m,n] = size(IM);

[val,idx] = locmax2d(double(IMb),0.08);
[u,v] = ind2sub([m,n],idx);

figure(1)
hold on
imagesc(IM,[0 0.2]);
colormap gray
axis image off
set(gca,'Position',[0 0 1 1])
plot(v,u,'rx')


npts = length(idx);
params = [];%zeros(npts,7);
fi = linspace(0,pi/2,100);
for I = 1:npts
  try
    im = double(IM(u(I)+box,v(I)+box));

    %a0 = [0 0 3.5 max(im(:)) min(im(:))];
    %[a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);
    for J = 1:100
    a0 = [0 0 max(im(:)) min(im(:)) 4 4];
    [a,chi2,exitflag] = lmfit2Dgausselliptic(a0, X, im, fi(J));
    a(1:2) = a(1:2)+ [u(I) v(I)]; 
    params{I}(J,:) = [a chi2];
    end
        
  catch
    a = NaN;
  end
    
%   params(I,:) = a;  
end

%%

thr = cylinder_calibrate2([datapath filename '.tif'],'cmd','setthr','thr',0.06,'Z',100)
alfa = cylinder_calibrate2([datapath filename '.tif'],'cmd','getangle','thr',0.06,'Z',70:73)

zmap = cylinder_calibrate2([datapath filename '.tif'],'cmd','getzmap','thr',0.06,'angle',28*pi/180,'Z',60:140)

