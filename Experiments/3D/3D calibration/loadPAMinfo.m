function [streaminfo,mask] = loadPAMinfo(filename)
% read stream info
% 13.10.2010, 

vals = { ...
c2s({'name', 'bitdepth', 'strmatch', 'Type :', 'val', NaN}), ...
c2s({'name', 'x',    'strmatch', 'x :', 'val', NaN}), ...
c2s({'name', 'y',    'strmatch', 'y :', 'val', NaN}), ...
c2s({'name', 'z',    'strmatch', 'Z :', 'val', NaN, 'mask', '_t%04d'}), ...
c2s({'name', 'time', 'strmatch', 'Time :', 'val', NaN, 'mask', '_t%04d'}), ...
c2s({'name', 'gainPreAmp', 'strmatch', 'Gains - Pre-Amp-Gain=', 'val', NaN}), ...
c2s({'name', 'gainEM', 'strmatch', 'Gains - EMGain=', 'val', NaN}), ...
c2s({'name', 'z_start', 'strmatch', 'Z Control - Start=', 'val', NaN}),...
c2s({'name', 'z_step', 'strmatch', 'Z Control - Step=', 'val', NaN}),...
c2s({'name', 'z_planes', 'strmatch', 'Z Control - Planes=', 'val', NaN}),...
c2s({'name', 'z_end', 'strmatch', 'Z Control - End=', 'val', NaN})};

mask = [];
streaminfo = [];
numvals = length(vals);
strcheckfor = cell(1,numvals);
for I = 1:numvals
  strcheckfor{I} = upper(vals{I}.strmatch);
  streaminfo.(vals{I}.name) = vals{I}.val;
end

fid = fopen(filename,'rt');
% read file info
while (1)
  
    str = upper(fgetl(fid));      % read one line
    if ~ischar(str) , break, end; % EOF
    
    idx = matchstr(str,strcheckfor);
    if (sum(idx) ~= 1), continue; end; % didn't find a mathch
    
    I = find(idx);
    num = sscanf(str, [strcheckfor{I} ' %f']);
    
    streaminfo.(vals{I}.name) = num;

    if isfield(vals{I}, 'mask')
      mask = [vals{I}.mask, mask];
    end
            
end
fclose(fid);

% ----------------------------------------------------------------------------
% find and match str against template in ptrn
function out = matchstr(str,ptrn)

num = numel(ptrn);
out = zeros(size(ptrn));
for I = 1:num
  tmp = strfind(str,ptrn{I});
  if ~isempty(tmp)
    out(I) = tmp;
  end
end

function out=c2s(in)
% C2S Converts cell to structure array.

% ----------------------------------------------------------------------------
if iscell(in),
  out = struct(in{:});
else
  out = in;
end

return;
% EOF