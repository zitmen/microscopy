% from: http://bigwww.epfl.ch/algorithms/psfgenerator/#g
%
% sigma - vector of sigma values (one value for each z value)
% x,y,z - coordinate vectors
%
% example: PSF3D_AxialGaussian([-5:0.01:+5],[-5:0.01:+5],horzcat(linspace(5,1,21),linspace(1.2,5,20)))
%
function PSF3D_AxialGaussian(x,y,sigma)
    for zi = 1:length(sigma)
        I = zeros(length(x),length(y));
        for xi = 1:length(x)
            for yi = 1:length(y)
                I(xi,yi) = 1/(sigma(zi)^2) * exp(-((x(xi).^2 + y(yi).^2) / (2*sigma(zi)^2)));
            end
        end
        figure(zi),imagesc(I)
    end
end