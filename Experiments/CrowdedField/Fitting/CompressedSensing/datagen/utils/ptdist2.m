function dst2 = ptdist2(pt, pts)

dst2 = (pt(1)-pts(:,1)).^2 + (pt(2)-pts(:,2)).^2;
