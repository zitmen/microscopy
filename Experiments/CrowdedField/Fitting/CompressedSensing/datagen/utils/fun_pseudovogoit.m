function y = fun_pseudovogoit(x,par,partransform)

if nargin < 3, partransform = @fun_partransfdefault; end;
[x0,b,a,d,c] = partransform(par);

c = 0.5*cos(c)+0.5; % 2DO limit "c" to [0 1] in partransform

e = ((x-x0)/b).^2;

y = a * ( c * (1./(1+e)) + (1-c) * exp(-0.5*e)) + d;