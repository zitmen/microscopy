%function results = process(datadir, config)

% addpath('d:\pavel\matlab\blinking\120215-release3\utils')
%% load configuration
% if nargin < 2, cfg = cfg_v3(); else cfg = @config; end;
% if (nargin < 1) || isempty(datadir), datadir = cfg.datadir; end;
% if ~isdir(datadir), error('Data directory does not exist'); end;
clear
cfg = cfg_v3();
%% initialize
fprintf('Start at %s\n', datestr(now));
img = imginit(cfg.datadir,cfg);


fprintf('Progress ...\n');
loc = []; imbuf = []; fits = [];
numframes = img.maxframe-cfg.framestart;
while img.frame < img.maxframe

  % load next image
  img = imgnext(img);
  
  % detect molecules
  loc = molecule_detect8(img, loc, cfg);
 
  % stop if in setup mode
  if cfg.setup, continue; end;

  % store the in the memory
  imbuf = molecule_storeim(img, loc, imbuf, cfg); 
  
  % fit results useing LM - C
  [fits, imbuf] = molecule_fitting(img, imbuf, fits, cfg);
  
  % progress
  if mod(img.frame-cfg.framestart+1,round(numframes/10)) == 0,
    fprintf('  %3d%%  ', round(100*(img.frame-cfg.framestart+1)/numframes));
    fprintf(' #accept %7d   #discard %7d\n', fits.num, fits.discard);
  end;   

end

% stop if in setup mode
if cfg.setup, return; end;

%process the rest of detected molecules
idx = [imbuf.active]>0;
tmp = num2cell(zeros(sum(idx),1));
[imbuf(idx).active] = tmp{:};
fits = molecule_fitting(img, imbuf, fits, cfg);

fprintf('done     %s\n', datestr(now));
%% save results
results = res_save(fits, img, '120215 - release 3', cfg);

% res_analyze(results, cfg);
res_render(results, cfg);

%res_analyze('d:\data\blinking\Actin STORM 11\results\101013-release2.4-detect7-nodrift-timeout1\Actin STORM 11.mat',@cfg_v2_ACTIN11);


% res_render('g:\blinking superresolution data\Actin\Actin STORM 11\results_ver3-detect8-box9x9\Actin STORM 11.mat');


