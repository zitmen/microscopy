function res = render_gauss(results, cfg)

% 7.10.2010, PK, saving moved to res_render
% 27.4.2010, PK, in: results, cfg; out: IM; print & save results

% alocate memmory for reconstructed image
scale = cfg.render.scale;
imsize = scale * results.imsize;
IM  = zeros(imsize,'single');

% rendering rules & decode results
[xpos,ypos,a,sigma,s,N,b,dx,chi2] = render_rules(results, cfg.render);

% rendering coordinates
radius = scale*5;  % rendering radius for a dot
[x,y] = meshgrid(-radius:radius,-radius:radius);
idx = x.^2 + y.^2 < radius^2;
x = x(idx); y = y(idx);

% for all points
npts = length(N);
fprintf('Progress ');
for I = 1:npts

  % get position
  u = floor(scale * ypos(I));
  v = floor(scale * xpos(I));
  du = scale * ypos(I) - u;
  dv = scale * xpos(I) - v;

  % take coordinates only inside the image
  idx = ~(v+x < 1 | v+x > imsize(2) | u+y < 1 | u+y > imsize(1));  
  
  % render
  z = N(I) * exp(-0.5*((x(idx)-dv).^2 + (y(idx)-du).^2)/(1/a*scale*dx(I))^2);
  idx = sub2ind(imsize,u+y(idx),v+x(idx));  
  IM(idx) = IM(idx) + z;
   if (mod(I,npts/10) == 0)
    fprintf('.');
   end;    
end
fprintf('\ndone\n');

% results
res.name = 'gauss';
res.im = IM;
