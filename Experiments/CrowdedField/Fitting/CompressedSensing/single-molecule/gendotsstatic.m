function [im,params] = gendotsstatic(img_w, img_h, dot_pos_x, dot_pos_y, sigma, amplitude)

% create regular grid for positioning dots
[xpos,ypos] = meshgrid(1:img_w, 1:img_h);

% select random subset of dots
idx = (dot_pos_y - 1) * img_w + dot_pos_x;
ndots = 1;
params = zeros(ndots,4,'single'); 

params(:,1) = ypos(idx);
params(:,2) = xpos(idx);
params(:,3) = sigma;
params(:,4) = amplitude;

% create rendering coordinates
radius = 20;
[x,y] = meshgrid(-radius:radius,-radius:radius);
idx = x.^2 + y.^2 < radius^2;
x = x(idx); y = y(idx);
  
% create image of dots
im = zeros([img_w, img_h],'single');
for I = 1:ndots

  % rendering parameters
  u = floor(params(I,1));
  v = floor(params(I,2));
  du = params(I,1) - floor(params(I,1));
  dv = params(I,2) - floor(params(I,2));
  sigma = params(I,3);
  N = params(I,4);

  % take coordinates only inside the image
  idx = ~(v+x < 1 | v+x > img_h | u+y < 1 | u+y > img_w);
  
  % render  
  z = N / (2*pi*sigma^2) * exp(-0.5*((x(idx)-dv).^2 + (y(idx)-du).^2)/sigma^2);
  idx = sub2ind([img_w, img_h],u+y(idx),v+x(idx));  
  im(idx) = im(idx) + z;
    
end
