function noise = gennoise(img_w, img_h)
% Generate an image simulating camera background noise

persistent invFx;

% read inverse cumulative distribution function of noise 
if isempty(invFx)
  invFx = dlmread('noise_invFx.txt');
end

% random data from uniform distribution
X = rand(img_w, img_h);

% transform to noise distribution - interpolation
noise = interp1(invFx(:,1),invFx(:,2),X);
