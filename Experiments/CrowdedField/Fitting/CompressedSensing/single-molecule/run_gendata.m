% Generate monte-carlo images of dots on noisy background

clear

noise = gennoise(11, 11);

% montecarlo simulation of dots
[img,params] = gendotsstatic(11, 11, 6, 6, 1.3, 1);

im = img + noise;

% save dots and parameters 
imwrite(uint16(65535*im),'single-noisy-molecule.png','png','BitDepth',16);
save('single-noisy-molecule.mat','-v6','params');
