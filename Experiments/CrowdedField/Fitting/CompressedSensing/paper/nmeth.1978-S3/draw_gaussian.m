function draw_gaussian()

    %{
    A = 1;
    x0 = 0; y0 = 0;

    sigma_x = 1;
    sigma_y = 2;

    for theta = 0:pi/100:pi
        a = cos(theta)^2/2/sigma_x^2 + sin(theta)^2/2/sigma_y^2;
        b = -sin(2*theta)/4/sigma_x^2 + sin(2*theta)/4/sigma_y^2 ;
        c = sin(theta)^2/2/sigma_x^2 + cos(theta)^2/2/sigma_y^2;

        [X, Y] = meshgrid(-5:.1:5, -5:.1:5);
        Z = A*exp( - (a*(X-x0).^2 + 2*b*(X-x0).*(Y-y0) + c*(Y-y0).^2)) ;
        surf(X,Y,Z);shading interp;view(-36,36);axis equal;drawnow
    end
    %}

    A = 1;
    x0 = 0; y0 = 0;

    sigma_x = 1.5;
    sigma_y = 1.5;

    [X, Y] = meshgrid(-5:.1:5, -5:.1:5);
    %Z = A*exp(-((((X-x0).^2)/(sigma_x^2))+((Y-y0).^2)/(sigma_y^2)));

    mag = 2/7;
    %Z = (exp(-2*(((X-x0)/mag).^2+((Y-y0)/mag).^2)/1.72^2) ...
    %    +0.0208*exp(-2*(sqrt(((X-x0)/mag).^2+((Y-y0)/mag).^2)-2.45).^2/1.10^2));
    sigma = 1.5;    % it is actualy range from 0.3 to 2.2
    N = 80 * (142/65535);  % (rand in range from 1 to 100) * ((142/65535) = 0,0021667811093308918898298619058518)
    N_norm = N / (2*pi*sigma^2);
    %Z = N_norm * exp(-0.5*(((X-x0).^2 + (Y-y0).^2)/(2*sigma^2)));
    Z = exp(-0.5*(((X-x0).^2 + (Y-y0).^2)/(2*sigma^2)));

    surf(X,Y,Z);shading interp;view(-36,36);axis equal;drawnow

end