%
% RUN: CSSTORM('caveolin_795.spe', 0, 1.5, 1, 100)
%{
Error using parallel_function (line 598)

Subscript indices must either be real positive integers or logicals.

Error stack:
CSSTORM>(parfor body) at 192

Error in CSSTORM (line 174)
        parfor PAR_I = 1:(nboxes_x*nboxes_y);
%}
%
%%
% CSSTORM v1.3
% Parameter list:
% [Argument] | (format) <description>
%==========================================================
% inputfile  | (string)  for your *.spe file
% debug_mode | (boolean) 1 will display CS progress; 0 will not
%            |           This shows the box-selection and subtraction
% eps        | (float)   The target (unweighted) chi^2 for the optimization
%            |           Generally a value of 1.5 works for most situations
%            |           A value of 2.1 works with EMCCD images
% ADU        | (float)   Number of photons for each camera count
% base_line  | (float)   Baseline offset of the camera

function CSSTORM(inputfile,debug_mode,eps,ADU,base_line)

    % camera parameters
    photon_per_count = ADU;
    ccd_base_line = base_line;
    
    % optimization parameters
    div = 8; % CS "zoom-factor"
    boxsize = 7; % in pixels
    extramargin = 1;
    scanoverlap = 2;
    margin = 4;
    mag = 2 / boxsize;
    red_chi2 = eps;

    % read input image (WinView .spe format)
    fid = fopen(inputfile, 'r', 'l');
    header = fread(fid,2050,'*uint16');

    % parsing .spe header
    x_dim = double(header(22));
    y_dim = double(header(329));
    z_dim = double(header(724));

    fprintf('Analyzing %s\n', inputfile);
    fprintf('X size .... %d\n', x_dim);
    fprintf('Y size .... %d\n', y_dim);
    fprintf('Frames .... %d\n', z_dim);
    
    if(debug_mode)
        mag_full = 2 / x_dim ;
        min_contrib = 1; % min CS recovered value to be considered as a successful molecule identification
    end

    %determining the data format in the source image
    img_mode = header(55);
    if img_mode == 0
       imgs = fread(fid, inf, 'float32'); 
    elseif img_mode == 1
        imgs = fread(fid, inf, 'uint32');
    elseif img_mode == 2
        imgs = fread(fid, inf, 'int16');
    elseif img_mode == 3
        imgs = fread(fid, inf, 'uint16');
    end

    fclose(fid);

    imgs = reshape(imgs, [x_dim y_dim z_dim]);
    imgs = (imgs - ccd_base_line) * photon_per_count;

    % dimension of of the oversampled super-resolution image
    x_dim_est = x_dim * div;
    y_dim_est = y_dim * div;

    % generate global grid for final image estimate
    % these dimensions should correspond to your raw and final image
    x_pos_est_full = linspace(-1,1,x_dim_est);
    y_pos_est_full = linspace(-1,1,y_dim_est);
    x_inx_full = x_pos_est_full(x_dim_est/x_dim/2 : x_dim_est/x_dim : end);
    y_inx_full = y_pos_est_full(y_dim_est/y_dim/2 : y_dim_est/y_dim : end);

    [x_inx_full y_inx_full] = meshgrid(x_inx_full, y_inx_full);
    [x_pos_est_full y_pos_est_full] = meshgrid(x_pos_est_full, y_pos_est_full);

    % generate grid for local estimation
    x_pos_est = linspace(-1,1, div*boxsize);
    y_pos_est = linspace(-1,1, div*boxsize);

    x_inx = x_pos_est((div/2) : div : end);
    y_inx = y_pos_est((div/2) : div : end);

    % provide pixel padding for local CS estimate
    % This should correspond to the box size around peak intensity
    % -- Begin padding the estimated local patch
    dx = x_pos_est(2)-x_pos_est(1);
    dy = y_pos_est(2)-y_pos_est(1);

    for mm = 1:margin
        x_pos_est = [x_pos_est(1)-dx x_pos_est];
        x_pos_est = [x_pos_est x_pos_est(end)+dx];
        y_pos_est = [y_pos_est(1)-dy y_pos_est];
        y_pos_est = [y_pos_est y_pos_est(end)+dy];
    end

    [x_inx y_inx] = meshgrid(x_inx,y_inx);
    [x_pos_est y_pos_est] = meshgrid(x_pos_est,y_pos_est);

    % generate measurement matrix
    len = length(x_pos_est(:));
    %A = zeros(x_inx, len + 1);
    for ii = 1:len
        img_kernel = MolKernel(x_inx, y_inx, x_pos_est(ii), y_pos_est(ii), mag);
        A(:,ii) = img_kernel(:);
    end
    
    c = sum(A);
    PSF_integ = max(c); % integration of the PSF over space, used for normalization
    c = c./ PSF_integ; % normalize to 1
    A = A./ PSF_integ;
    
    % add the extra optimization variable for the estimation of the background
    len = len + 1;
    c(len) = 0;
    A(:,len) = 1;
    
    len = size(A,2);
    A = sparse(A);
    
    % begin to formulate optimization
    cvx_quiet(true);

    %write file out - header only
    header(22) = uint16(x_dim_est);
    header(329) = uint16(y_dim_est);
    header(55) = 0; %float32 output format

    fid = fopen(strcat(inputfile(1:end-4),'_cs.spe'),'w','l');

    fwrite(fid, header, 'uint16');
 
    % begin analysis of individual frames
    computation_time = zeros(z_dim,1);
    for uu = 1:z_dim,
        newTime = cputime;
        if debug_mode
            fprintf('--- Analyzing Frame %d ---\n',uu);
        else
            fprintf('.');
            if mod(uu,10) == 0
                fprintf('%.0f',floor(uu/10));
            end
            if mod(uu,100) == 0
                fprintf('\n');
            end
        end
        
        % get the corresponding frame
        lowresimg = imgs(:,:,uu);
        
        if debug_mode
           lowresimg_display = lowresimg;
        end
        
        img_recover = zeros(x_dim_est, y_dim_est);
        
        %scan the optimization box throughout image
        pre_xloc = 1 + scanoverlap - boxsize;
        pre_yloc = 1;
        %while true;
        % initialize the parallel computing
        %matlabpool local 8
        %parfor p = 1:8
        %   cd cvx
        %   cvx_setup
        %   cd ..
        %end
        nboxes_x = floor(x_dim / (boxsize - scanoverlap));
        nboxes_y = floor(y_dim / (boxsize - scanoverlap));
        %
        parfor PAR_I = 1:(nboxes_x*nboxes_y);
           pre_xloc = (1 + scanoverlap - boxsize) + (mod(PAR_I, nboxes_x) * (boxsize - scanoverlap));
           pre_yloc = (1 + scanoverlap - boxsize) + (ceil(PAR_I / nboxes_x) * (boxsize - scanoverlap));
           
           % The boundary of the optimization box
           inx_x_l = pre_xloc;
           inx_x_u = pre_xloc + boxsize - 1;
           inx_y_l = pre_yloc;
           inx_y_u = pre_yloc + boxsize - 1;

           %{
            if debug_mode
              imagesc((lowresimg_display)), colormap(gray), hold on;
              rectangle('Position',[inx_x_l-0.5,inx_y_l-0.5,boxsize,boxsize],'linewidth',1,'EdgeColor','y'),pause(0.5);
            end
           %}
           
           % crop from the original image
           cropraw = lowresimg(inx_y_l:inx_y_u, inx_x_l:inx_x_u);
           
           L1sum = norm(cropraw(:),1);
           L2sum = sqrt(L1sum); % Target of least square estimation based on Poisson statistics
           
           if norm(cropraw(:)-L1sum/length(cropraw(:)),2) < L2sum * red_chi2;
               continue;    % no molecules in the patch, just flat background
           end

           % optimization using CVX
           %Weighting = diag(1./sqrt(abs(cropraw(:))+1));
           %img_est = MolEst_eps(Weighting*cropraw(:),len,Weighting*A,c,(boxsize*boxsize-1)*red_chi2);
           img_est = MolEst_eps(cropraw(:),len,A,c,L2sum * red_chi2);
           est_background = img_est(len);
           %{
           if(debug_mode)
               fprintf('BG=%.1f, L1=%.1f (target: %.1f), L2=%.1f (target: %.1f), ',...
                      est_background,...
                      norm(c*img_est,1),             L1sum-(boxsize*boxsize)*est_background,...
                      norm(A*img_est-cropraw(:),2),  L2sum);
           end
           %}
           
           img_est = reshape(img_est(1:len-1), [length(x_pos_est) length(y_pos_est)]);

           % remove margin
           img_est = img_est(margin+1:end-margin, margin+1:end-margin);
           if extramargin > 0
              img_est([1:extramargin*div end-extramargin*div+1:end],:) = 0;
              img_est(:,[1:extramargin*div end-extramargin*div+1:end]) = 0;
           end

           % put this hi-res patch of image back into collection array
           hr_x_l = (inx_x_l-1)*div+1;
           hr_y_l = (inx_y_l-1)*div+1;
           hr_x_u = hr_x_l + (div*boxsize)-1;
           hr_y_u = hr_y_l + (div*boxsize)-1;

           img_recover_temp = zeros(x_dim_est,y_dim_est);
           img_recover_temp(hr_y_l:hr_y_u, hr_x_l:hr_x_u) = img_est;

           % add the optimized patch to the recovered super-resolution image
           img_recover = img_recover + img_recover_temp;

           % display othe optimization procedure for debugging only
           %{
           if debug_mode
               inx_contribute = find(img_recover_temp(:) > min_contrib);
               if isempty(inx_contribute)
                   fprintf('nothing found.\n');
                   continue;
               else
                   fprintf('%d contribution points.\n',length(inx_contribute(:)));
               end

               % calculate the recovered low resolution image based on the optimization results
               lowresimg_temp = zeros(size(lowresimg));
               for jj = 1:length(inx_contribute)
                  h = MolKernel(x_inx_full, y_inx_full, ...
                            x_pos_est_full(inx_contribute(jj)),...
                            y_pos_est_full(inx_contribute(jj)), mag_full);
                  lowresimg_temp = lowresimg_temp + img_recover_temp(inx_contribute(jj))*h;
               end

               lowresimg_display = lowresimg_display - lowresimg_temp / PSF_integ;
           
               imagesc(lowresimg_display); drawnow;
           end
           %}
        end
        
        computation_time(uu) = cputime-newTime;
        %{
        if debug_mode
            fprintf('%1.2f sec.\n', cputime-newTime);
        end
        %}
        
        %Write one frame into the output file
        fwrite(fid,img_recover,'float32','ieee-le');
        
    end
    fprintf('\nComputation time per frame: %.2f +/- %.2f sec\n', mean(computation_time), std(computation_time));
    % close the output file
    fclose(fid);
end
