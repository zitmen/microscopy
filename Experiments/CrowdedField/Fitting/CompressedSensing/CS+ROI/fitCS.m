% based on CSSTORM v1.3
function fitCS(rawimg, loc, debug_mode, eps, ADU, base_line)

    % camera parameters
    photon_per_count = ADU;
    ccd_base_line = base_line;
    
    % optimization parameters
    div = 8; % CS "zoom-factor"
    boxsize = 7; % in pixels
    mag = 2 / boxsize;
    box_off = boxsize / 2;
    red_chi2 = eps;

    img = (rawimg - ccd_base_line) * photon_per_count;

    % dimensions of the image
    x_dim = double(size(rawimg,2));
    y_dim = double(size(rawimg,1));
    
    if(debug_mode)
        mag_full = 2 / x_dim ;
        min_contrib = 1; % min CS recovered value to be considered as a successful molecule identification
    end
    
    % dimension of of the oversampled super-resolution image
    x_dim_est = x_dim * div;
    y_dim_est = y_dim * div;

    % generate global grid for final image estimate
    % these dimensions should correspond to your raw and final image
    x_pos_est_full = linspace(-1,1,x_dim_est);
    y_pos_est_full = linspace(-1,1,y_dim_est);
    x_inx_full = x_pos_est_full(x_dim_est/x_dim/2 : x_dim_est/x_dim : end);
    y_inx_full = y_pos_est_full(y_dim_est/y_dim/2 : y_dim_est/y_dim : end);

    [x_inx_full y_inx_full] = meshgrid(x_inx_full, y_inx_full);
    [x_pos_est_full y_pos_est_full] = meshgrid(x_pos_est_full, y_pos_est_full);

    % generate grid for local estimation
    x_pos_est = linspace(-1,1, div*boxsize);
    y_pos_est = linspace(-1,1, div*boxsize);

    x_inx = x_pos_est((div/2) : div : end);
    y_inx = y_pos_est((div/2) : div : end);

    [x_inx y_inx] = meshgrid(x_inx,y_inx);
    [x_pos_est y_pos_est] = meshgrid(x_pos_est,y_pos_est);

    % generate measurement matrix
    len = length(x_pos_est(:));
    %A = zeros(x_inx, len + 1);
    for ii = 1:len
        img_kernel = MolKernel(x_inx, y_inx, x_pos_est(ii), y_pos_est(ii), mag);
        A(:,ii) = img_kernel(:);
    end
    
    c = sum(A);
    PSF_integ = max(c); % integration of the PSF over space, used for normalization
    c = c./ PSF_integ; % normalize to 1
    A = A./ PSF_integ;
    
    % add the extra optimization variable for the estimation of the background
    len = len + 1;
    c(len) = 0;
    A(:,len) = 1;
    
    len = size(A,2);
    A = sparse(A);
    
    % begin to formulate optimization
    cvx_quiet(true);

    %write file out - header only
    header(22) = uint16(x_dim_est);
    header(329) = uint16(y_dim_est);
    header(55) = 0; %float32 output format

    fid = fopen('result_cs.spe','w','l');

    fwrite(fid, header, 'uint16');
 
    % begin analysis of individual frames
    lowresimg = img;
        
    if debug_mode
       lowresimg_display = lowresimg;
    end

    img_recover = zeros(x_dim_est, y_dim_est);

    %scan the optimization box throughout image ( go through each `loc` positions )
    for ll = 1:size(loc,1)
       
       % Load a position of a located maxima
       pre_xloc = loc(ll, 2) - floor(box_off);
       pre_yloc = loc(ll, 1) - floor(box_off);
        
       % The boundary of the optimization box
       inx_x_l = pre_xloc;
       inx_x_u = pre_xloc + boxsize - 1;
       inx_y_l = pre_yloc;
       inx_y_u = pre_yloc + boxsize - 1;
       
       % check boundaries !!
       if (inx_y_l < 1) || (inx_y_u > y_dim) || (inx_x_l < 1) || (inx_x_u > x_dim)
          continue;
       end

       if debug_mode
          imagesc((lowresimg_display)), colormap(gray), hold on;
          rectangle('Position',[inx_x_l-0.5,inx_y_l-0.5,boxsize,boxsize],'linewidth',1,'EdgeColor','y'),pause(0.5);
       end

       % crop from the original image
       cropraw = lowresimg(inx_y_l:inx_y_u, inx_x_l:inx_x_u);

       L1sum = norm(cropraw(:),1);
       L2sum = sqrt(L1sum); % Target of least square estimation based on Poisson statistics

       if norm(cropraw(:)-L1sum/length(cropraw(:)),2) < L2sum * red_chi2;
           continue;    % no molecules in the patch, just flat background
       end

       % optimization using CVX
       %Weighting = diag(1./sqrt(abs(cropraw(:))+1));
       %img_est = MolEst_eps(Weighting*cropraw(:),len,Weighting*A,c,(boxsize*boxsize-1)*red_chi2);
       img_est = MolEst_eps(cropraw(:),len,A,c,L2sum * red_chi2);
       est_background = img_est(len);
       if(debug_mode)
           fprintf('BG=%.1f, L1=%.1f (target: %.1f), L2=%.1f (target: %.1f), ',...
                  est_background,...
                  norm(c*img_est,1),             L1sum-(boxsize*boxsize)*est_background,...
                  norm(A*img_est-cropraw(:),2),  L2sum);
       end

       img_est = reshape(img_est(1:len-1), [length(x_pos_est) length(y_pos_est)]);

       % put this hi-res patch of image back into collection array
       hr_x_l = (inx_x_l-1)*div+1;
       hr_y_l = (inx_y_l-1)*div+1;
       hr_x_u = hr_x_l + (div*boxsize)-1;
       hr_y_u = hr_y_l + (div*boxsize)-1;

       img_recover_temp = zeros(x_dim_est,y_dim_est);
       img_recover_temp(hr_y_l:hr_y_u, hr_x_l:hr_x_u) = img_est;

       % add the optimized patch to the recovered super-resolution image
       img_recover = img_recover + img_recover_temp;

       % display othe optimization procedure for debugging only
       if debug_mode
           inx_contribute = find(img_recover_temp(:) > min_contrib);
           if isempty(inx_contribute)
               fprintf('nothing found.\n');
               continue;
           else
               fprintf('%d contribution points.\n',length(inx_contribute(:)));
           end

           % calculate the recovered low resolution image based on the optimization results
           lowresimg_temp = zeros(size(lowresimg));
           for jj = 1:length(inx_contribute)
              h = MolKernel(x_inx_full, y_inx_full, ...
                        x_pos_est_full(inx_contribute(jj)),...
                        y_pos_est_full(inx_contribute(jj)), mag_full);
              lowresimg_temp = lowresimg_temp + img_recover_temp(inx_contribute(jj))*h;
           end

           lowresimg_display = lowresimg_display - lowresimg_temp / PSF_integ;

           imagesc(lowresimg_display); drawnow;
       end
    end

    %Write one frame into the output file
    fwrite(fid,img_recover,'float32','ieee-le');

    % close the output file
    fclose(fid);
end
