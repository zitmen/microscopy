clear all

% load next image
imgstack = loadSPE('data/multiple-molecules.spe', 0);

rawimg = imgstack(:, :, 1); % process top image from the stack
%imagesc(rawimg)
%colormap(gray)

% shifted Gaussian
%%h = fspecial('gauss', 11, 1.3);
%h = h - mean(h(:));
%%imb = imfilter(rawimg, h, 'replicate', 'same');
%imagesc(imb)
%colormap(gray)
%%bg_est = rawimg - imb;
%imagesc(bg_est)
%colormap(gray)
%%base_line = mean(bg_est(:));

%%imagesc(rawimg-base_line)
%%colormap(gray)

% detect molecules
loc = mc_molecule_detect(rawimg);

% fit model [CS]
fitCS(rawimg, loc, 1, 6, 1, 0);
