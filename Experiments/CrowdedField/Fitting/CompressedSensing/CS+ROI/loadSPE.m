function imgs = loadSPE(inputfile, normalize)

    % read input image (WinView .spe format)
    fid = fopen(inputfile, 'r', 'l');
    header = fread(fid,2050,'*uint16');

    % parsing .spe header
    x_dim = double(header(22));
    y_dim = double(header(329));
    z_dim = double(header(724));
    
    %determining the data format in the source image
    img_mode = header(55);
    if img_mode == 0
       imgs = fread(fid, inf, 'float32'); 
    elseif img_mode == 1
        imgs = fread(fid, inf, 'uint32');
    elseif img_mode == 2
        imgs = fread(fid, inf, 'int16');
    elseif img_mode == 3
        imgs = fread(fid, inf, 'uint16');
    end

    fclose(fid);
    
    if normalize == 1
        imgs = imgs ./ 65535;   % the img_mode doesn't play a role here...this is about the intensities of the input image and about the camera setup
    end
    
    imgs = reshape(imgs, [x_dim y_dim z_dim])'; % transposed
    