% ======================================================================= %
% function img2list ... takes an SPE image, finds points with intensity >
%                       threshold and saves it into the list of [X,Y,I],
%                       where X and Y are subpixel coordinates
%
% oversampling_factor ... for computation of the subpixel coordinated
% relval ... use 1 for normalized values of intensity (0-1) or 0 use for
%            absolute values (0-65535)
% thr ... it can be 1, because this is only about eliminating the XXXe-8 values
% ======================================================================= %
function list = img2list(inputfile, oversampling_factor, relval, thr)

    % read input image (WinView .spe format)
    fid = fopen(inputfile, 'r', 'l');
    header = fread(fid,2050,'*uint16');

    % parsing .spe header
    x_dim = double(header(22));
    y_dim = double(header(329));
    z_dim = double(header(724));
    
    %determining the data format in the source image
    img_mode = header(55);
    if img_mode == 0
       imgs = fread(fid, inf, 'float32'); 
    elseif img_mode == 1
        imgs = fread(fid, inf, 'uint32');
    elseif img_mode == 2
        imgs = fread(fid, inf, 'int16');
    elseif img_mode == 3
        imgs = fread(fid, inf, 'uint16');
    end

    fclose(fid);
    
    if relval == 1
        imgs = imgs ./ 65535;   % the img_mode doesn't play a role here...this is about the intensities of the input image and about the camera setup
    end
    
    imgs = reshape(imgs, [x_dim y_dim z_dim])'; % transposed
    
    items = 0;
    list = zeros(100000, 3);    % pre-alloc
    
    for uu = 1:z_dim
        
        % get the corresponding frame
        lowresimg = imgs(:,:,uu);
        
        % convert the frame
        I = lowresimg(:);
        idx = (I > thr);
        idx = reshape(idx, [x_dim y_dim]);
        [row,col] = find(idx);
        
        for ii = 1:size(row,1)
            
            items = items + 1;
            % [ X, Y, Intensity ]
            list(items,:) = [ col(ii)/oversampling_factor, ...
                            row(ii)/oversampling_factor, ...
                            lowresimg(row(ii),col(ii))];
            
        end
        
    end
    
    list = list(1:items,:);
    
    % save the list into the MAT format
    %save([inputfile '.mat'],'-struct','list');
    