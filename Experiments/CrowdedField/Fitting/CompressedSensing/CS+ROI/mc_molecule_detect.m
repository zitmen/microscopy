function loc = mc_molecule_detect(img)

    % remove noise
    h = fspecial('gauss', 11, 1.3);
    imb = imfilter(img, h, 'replicate', 'same');

    % candidate pixels for a molecule
    % find local maxima + sort by intensity
    [val,idx] = locmax2d(imb);
    %idx = circfilter(idx, img.siz, cfg.mindist);   % eliminate the closest points
    [u,v] = ind2sub(size(img),idx);
    loc = [u, v, imb(idx)];
