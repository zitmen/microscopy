function img_kernel = MolKernel(x_inx,y_inx,x_pos,y_pos,mag)
%Simulation
%img_kernel = (exp(-2*(((x_inx-x_pos)/mag).^2+((y_inx-y_pos)/mag).^2)/1.72^2) ...
%              +0.0208*exp(-2*(sqrt(((x_inx-x_pos)/mag).^2+((y_inx-y_pos)/mag).^2)-2.45).^2/1.10^2));

% Our simulation
% for sigma=1.4, it finds the molecule only with chi_sq >= 4
sigma = 1.3;    % real sigma = 1.3; it finds 1 contributing point with sigma in range from 1.285 to 1.35
N = 1;          % real N = 1; it finds 1 contributing point everytime? I tried it up to 100 and it still works, so the CS alg. is not so sensitive in this way
N_norm = N / (2*pi*sigma^2);
%img_kernel = N_norm * exp(-0.5*(((x_inx-x_pos).^2 + (y_inx-y_pos).^2)/(2*sigma^2)));
img_kernel = N_norm * exp(-0.5*((((x_inx-x_pos)/mag).^2 + ((y_inx-y_pos)/mag).^2)/sigma^2));

%110123 A647 data
%img_kernel = 0.7082 * exp(-2*(((x_inx-x_pos)/mag).^2+((y_inx-y_pos)/mag).^2)/1.75345^2) ...
%               +0.3209*exp(-2*(sqrt(((x_inx-x_pos)/mag).^2+((y_inx-y_pos)/mag).^2)-0.43046).^2/1.97672^2);
           
%110211 mEos Data
% img_kernel = 0.87694 * exp(-2*(((x_inx-x_pos)/mag).^2+((y_inx-y_pos)/mag).^2)/1.69217^2) ...
%                + 0.12321 * exp(-2*(((x_inx-x_pos)/mag).^2+((y_inx-y_pos)/mag).^2)/2.38311);
end