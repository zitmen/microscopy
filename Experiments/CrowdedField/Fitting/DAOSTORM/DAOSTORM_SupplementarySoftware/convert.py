#!/usr/bin/env python
import pyfits
import numpy
import Image

#get the image and color information
image = Image.open('dense5-snr50.png')
xsize, ysize = image.size
data = image.getdata() # data is now an array of length ysize\*xsize

# create numpy array
np = numpy.reshape(data, (ysize, xsize))
np = numpy.flipud(np);

l = pyfits.PrimaryHDU(data=np)
l.header.update('BITPIX', 16)
l.writeto('dense5-snr50.fits')
