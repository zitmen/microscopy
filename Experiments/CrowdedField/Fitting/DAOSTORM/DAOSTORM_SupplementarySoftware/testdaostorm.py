#! /usr/bin/python
import daostorm, os.path

testMovie = 'dense5-snr50'#'multiple-molecules'#'stormData1.519'
#Test the PSF generation
print('Running PSF generation test\n')
psfFile = daostorm.genPSFmodel(testMovie,sigmaBG=20,thresh=15,frameNo=0,gain=3.1,datamax=14000)
#check whether we get an output

#Test the localization
print('\nRunning DAOSTORM localization test\n')
daostorm.fitMovie(testMovie,psfFile,sigmaBG=20,thresh=15,nIter=4,fwhmpsf=3.3,datamin=400)

#check whether we get an output
expectedPsfName = testMovie+'.0.psf.3.fits'
expectedLocalizationName = testMovie + '.pos.out'

if os.path.isfile(expectedPsfName):
  print('PSF generation test successful')
else:
  print('Warning: PSF generation test failed')

if os.path.isfile(expectedLocalizationName):
  print('DAOSTORM localization test successful')
else:
  print('Warning: DAOSTORM localization test failed')


