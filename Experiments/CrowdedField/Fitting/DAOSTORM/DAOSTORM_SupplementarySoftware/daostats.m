ground_truth = load('dense5.mat','params');
tmp = ground_truth.params(:,1);
ground_truth.params(:,1) = ground_truth.params(:,2);
ground_truth.params(:,2) = tmp;

fits = dlmread('dense5.pos.out', '\t', 1, 0);
DAO_mol_list = fits(:,2:3);
DAO_mol_list(:,3) = 1.3;  % ? where is this number in DAOSTORM results? PSF gen?
DAO_mol_list(:,4) = fits(:,4) ./ 65535;

%{
multiple-molecules
---------------------
max(FP(:,4)) = 0.0754
min(TP(:,8)) = 0.0941
threshold = 0.08; => F1 = 1
%}
threshold = 0;
idx = (DAO_mol_list(:,4) > threshold);
[F1,TP,FP,FN] = statistics(DAO_mol_list(idx,:), ground_truth.params);
sprintf('F1=%f, TP=%d, FP=%d, FN=%d\n', F1, length(TP), length(FP), length(FN))
