%{
[images,params] = datagen();
imwrite(uint16(65535*images{1}),'data/dense5.png','Compression','none');
params = params{1};
save('data/dense5.mat','-v6','params');

for t=1:1000,imwrite(uint16(65535*images{t}),['data/dense5/' int2str(t) '.tiff']),end;
%}
function [images,params] = datagen()
    
    cfg = config();
    
    params = cell(cfg.datagen.img_dim(3), 1);
    images = cell(cfg.datagen.img_dim(3), 1);
    
    %matlabpool('local', 8);
    %parfor t = 1:cfg.datagen.img_dim(3)
	for t = 1:cfg.datagen.img_dim(3)
        img_noise = gennoise();
        [img_molecules,params{t}] = gendotstatic();
        images{t} = cfg.datagen.background + img_noise + img_molecules;
		images{t} = images{t} ./ cfg.datagen.maxval;	% normalize
    end
    %matlabpool('close');

end
%%
function noise = gennoise()

    cfg = config();

    % Generate an image simulating camera background noise
    persistent invFx;

    % read inverse cumulative distribution function of noise 
    if isempty(invFx)
      invFx = dlmread('noise_invFx.txt');
    end

    % random data from uniform distribution
    X = rand(cfg.datagen.img_dim(1:2));

    % transform to noise distribution - interpolation
    noise = interp1(invFx(:,1),invFx(:,2),X);
	noise = noise .* 100 .* cfg.datagen.noise_sigma;	% coefficient 100 is there because of the low values of Poisson distributed noise

end
%%
function [img,params] = gendotstatic()
    
    cfg = config();
    
    % Generate image of monte-carlo dots
    [xpos,ypos] = meshgrid(16:16:cfg.datagen.img_dim(2)-16,16:16:cfg.datagen.img_dim(1)-16);
    xpos = xpos(:);
    ypos = ypos(:);
    npts = numel(xpos);

    % select random subset of dots
    idx = repmat(randperm(npts),1,cfg.datagen.mol_per_intersect)';
    ndots = cfg.datagen.mol_per_intersect * npts;
    idx = sort(idx(1:ndots));
    params = zeros(ndots,4,'single'); 

    % generate dots with random parameters [x0, y0, sigma, amplitude]
    r = 4*sqrt(rand(ndots,1));
    fi = 2*pi*rand(ndots,1);
    params(:,1) = ypos(idx) + r.*sin(fi);   % y0
    params(:,2) = xpos(idx) + r.*cos(fi);   % x0
    params(:,3) = cfg.datagen.psf_sigma(1) + diff(cfg.datagen.psf_sigma)*rand(ndots,1);   % sigma (random)
	params(:,4) = cfg.datagen.I_total + cfg.datagen.I_sigma_total*rand(ndots,1);
	params(:,4) = params(:,4) ./ cfg.datagen.maxval;	% norm

    % create rendering coordinates
    radius = 20;
    [x,y] = meshgrid(-radius:radius,-radius:radius);
    idx = x.^2 + y.^2 < radius^2;
    x = x(idx); y = y(idx);

    % create image of dots
    img = zeros(cfg.datagen.img_dim(1:2),'single');
    for I = 1:ndots

        % rendering parameters
        u = floor(params(I,1));
        v = floor(params(I,2));
        du = params(I,1) - floor(params(I,1));
        dv = params(I,2) - floor(params(I,2));
        sigma = params(I,3);
        N = params(I,4);

        % take coordinates only inside the image
        idx = ~(v+x < 1 | v+x > cfg.datagen.img_dim(2) | u+y < 1 | u+y > cfg.datagen.img_dim(1));

        % render  
		z = N / (2*pi*sigma^2) * exp(-0.5*((x(idx)-dv).^2 + (y(idx)-du).^2)/sigma^2);	% normalized Gaussian
		params(I,4) = max(z(:));	% setting the mean value (intensity of the center of the molecule - it is always the highest)
		idx = sub2ind(cfg.datagen.img_dim(1:2),u+y(idx),v+x(idx));  
        img(idx) = img(idx) + z;
    
    end
	
	% denormalize
	params(:,4) = params(:,4) .* cfg.datagen.maxval;
	img = img .* cfg.datagen.maxval;

end