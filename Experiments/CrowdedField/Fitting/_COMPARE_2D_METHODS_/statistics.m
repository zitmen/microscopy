%
% Statistics
% ----------
%
% Parameters:
%   * results ... results of a fitting method [x_pos,y_pos,sigma_psf,intensity]
%                 -> intensity have to be mapped to the (0,1) range
%
%   * ground_truth ... ground truth of the examined dataset [x_pos,y_pos,sigma_psf,intensity]
%                      -> intensity have to be mapped to the (0,1) range
%
% Return:
%   * F1 ... F1 score calculated from the TP,FP,FN
%   * TP ... True Positive [gt_x,gt_y,gt_sigma,gt_I,res_x,res_y,res_sigma,res_I]
%   * FP ... False Positive [res_x,res_y,res_sigma,res_I]
%   * FN ... False Negative [gt_x,gt_y,gt_sigma,gt_I]
%
function [F1,TP,FP,FN] = statistics(results,ground_truth)

    TP = []; FP = []; FN = [];
    ctp = 0; cfp = 0; cfn = 0;
    
    % create a distance matrix
    dist = inf(size(ground_truth,1),size(results,1));
    for gt = 1:size(ground_truth,1)
        dist(gt,:) = ptdist2(ground_truth(gt,1:2), results(:,1:2));
    end
    
    % classification -- this is very strict...i might need to change it
    fres = zeros(size(results,1),1);    % flag vector to determine which results have been already assigned
    fgt = zeros(size(ground_truth,1),1);% flag vector to determine which ground_truth points have been already assigned
    for i = 1:size(ground_truth,1)
        [dst_gt2res,res_idx] = min(dist(i,:));
        [dst_res2gt,gt_idx] = min(dist(:,res_idx));
        if gt_idx == i
            ctp = ctp + 1;
            TP(ctp,1:4) = ground_truth(gt_idx,:);
            TP(ctp,5:8) = results(res_idx,:);
            fres(res_idx) = 1;
            fgt(gt_idx) = 1;
        end
    end
    FP = results(~fres,:);
    cfp = size(FP,1);
    FN = ground_truth(~fgt,:);
    cfn = size(FN,1);
    
    precision = ctp / (ctp + cfp);
    recall = ctp / (ctp + cfn);
    F1 = 2 * precision * recall / (precision + recall);

end
%%
function dst2 = ptdist2(pt, pts)

    dst2 = (pt(1)-pts(:,1)).^2 + (pt(2)-pts(:,2)).^2;

end