function cfg = config()

    % General setting for the experiment
    cfg.run_datagen = false;    % true = run always; false = run only if there isn't the required dataset
    cfg.run_fitting = false;    % true = run always; false = run only if there are no results for the required dataset
    cfg.run_stats = true;
    
    cfg.datapath = 'data/';
    cfg.respath = 'results/';
    cfg.dataset = 'multiple-molecules';
    cfg.image_extension = '.png';
    
    % Data generator
	cfg.datagen.maxval = 65535;
    cfg.datagen.img_dim = [128 128 1000];
    cfg.datagen.psf_sigma = [1.3 1.3];	% fixed
    cfg.datagen.mol_per_intersect = 5;
    cfg.datagen.I_sigma_total = 30000;
    cfg.datagen.I_total = 5000;	% for mean to be N photons, the sum of all photons belonging to a molecule must be set higher!
	cfg.datagen.noise_sigma = 3000;	% noise is from the range [0,1], to multiplying it will get almost the desired sigma
	cfg.datagen.background = 1000;
    
    % Compressed Sensing STORM
    cfg.CSSTORM.debug_mode = 1;
    cfg.CSSTORM.cvx_quiet = true;
    cfg.CSSTORM.eps = 4;
    cfg.CSSTORM.ADU = 1;
    cfg.CSSTORM.base_line = 264;    % min(img_raw(:))
    cfg.CSSTORM.oversampling = 4; % CS "zoom-factor"
    cfg.CSSTORM.boxsize = 11; % in pixels
    cfg.CSSTORM.extramargin = 1;
    cfg.CSSTORM.scanoverlap = 5;    % more than half! (to minimize FN)
    cfg.CSSTORM.margin = 4;
    cfg.CSSTORM.thr_max_I = 1000;
    cfg.CSSTORM.thr_component_I = 1;
    cfg.CSSTORM.thr_molecule_I = 1000;
    cfg.CSSTORM.psf_sigma = 1.3;
    
    % DAOSTORM
    % - the algorithm was adapted from DAOPHOT II algorithm used for
    %   stellar photometry
    % - DAO stands for "Dominion Astrophysical Observatory" located in Canada
    cfg.DAOSTORM.todo = true;
    
    % Single Molecule STORM
    cfg.SMSTORM.todo = true;

end