%
% Compressed Sensing STORM
% ------------------------
%
% Parameters:
%   * image ... path to an image from camera (or simulation)
%               [at this time, the function can read only a single frame images!]
%
% Return:
%   * mol_list ... [Position_X,Position_Y,Intensity]
%
function mol_list = CSSTORM(img_path)

    % load CVX library
    persistent cvx_loaded;
    if isempty(cvx_loaded)
        run('cvx/cvx_setup.m');
        cvx_loaded = true;
    end

    % run the computation
    img_raw = imread(img_path);
    mol_img = CS_fit_at(img_raw, molecule_detect(img_raw));
    %mol_img = CS_fit(img_raw);
    mol_list = CS_postprocessing(mol_img);

end
%%
function img_recover = CS_fit(lowresimg)

    cfg = config();
    debug_mode = cfg.CSSTORM.debug_mode;

    % camera parameters
    photon_per_count = cfg.CSSTORM.ADU;
    ccd_base_line = cfg.CSSTORM.base_line;
    
    % optimization parameters
    div = cfg.CSSTORM.oversampling;
    boxsize = cfg.CSSTORM.boxsize;
    extramargin = cfg.CSSTORM.extramargin;
    scanoverlap = cfg.CSSTORM.scanoverlap;
    margin = cfg.CSSTORM.margin;
    mag = 2 / boxsize;
    red_chi2 = cfg.CSSTORM.eps;

    % image info
    [x_dim,y_dim] = size(lowresimg);

    if(debug_mode)
        mag_full = 2 / x_dim ;
        min_contrib = 1; % min CS recovered value to be considered as a successful molecule identification
    end

    lowresimg = (lowresimg - ccd_base_line) * photon_per_count;

    % dimension of of the oversampled super-resolution image
    x_dim_est = x_dim * div;
    y_dim_est = y_dim * div;

    % generate global grid for final image estimate
    % these dimensions should correspond to your raw and final image
    x_pos_est_full = linspace(-1,1,x_dim_est);
    y_pos_est_full = linspace(-1,1,y_dim_est);
    x_inx_full = x_pos_est_full(x_dim_est/x_dim/2 : x_dim_est/x_dim : end);
    y_inx_full = y_pos_est_full(y_dim_est/y_dim/2 : y_dim_est/y_dim : end);

    [x_inx_full y_inx_full] = meshgrid(x_inx_full, y_inx_full);
    [x_pos_est_full y_pos_est_full] = meshgrid(x_pos_est_full, y_pos_est_full);

    % generate grid for local estimation
    x_pos_est = linspace(-1,1, div*boxsize);
    y_pos_est = linspace(-1,1, div*boxsize);

    x_inx = x_pos_est((div/2) : div : end);
    y_inx = y_pos_est((div/2) : div : end);

    % provide pixel padding for local CS estimate
    % This should correspond to the box size around peak intensity
    % -- Begin padding the estimated local patch
    dx = x_pos_est(2)-x_pos_est(1);
    dy = y_pos_est(2)-y_pos_est(1);

    for mm = 1:margin
        x_pos_est = [x_pos_est(1)-dx x_pos_est];
        x_pos_est = [x_pos_est x_pos_est(end)+dx];
        y_pos_est = [y_pos_est(1)-dy y_pos_est];
        y_pos_est = [y_pos_est y_pos_est(end)+dy];
    end

    [x_inx y_inx] = meshgrid(x_inx,y_inx);
    [x_pos_est y_pos_est] = meshgrid(x_pos_est,y_pos_est);

    % generate measurement matrix
    len = length(x_pos_est(:));
    %A = zeros(x_inx, len + 1);
    for ii = 1:len
        img_kernel = MolKernel(x_inx, y_inx, x_pos_est(ii), y_pos_est(ii), mag);
        A(:,ii) = img_kernel(:);
    end
    
    c = sum(A);
    PSF_integ = max(c); % integration of the PSF over space, used for normalization
    c = c./ PSF_integ; % normalize to 1
    A = A./ PSF_integ;
    
    % add the extra optimization variable for the estimation of the background
    len = len + 1;
    c(len) = 0;
    A(:,len) = 1;
    
    len = size(A,2);
    A = sparse(A);
    
    % begin to formulate optimization
    cvx_quiet(cfg.CSSTORM.cvx_quiet);

    % begin analysis
    if debug_mode
       lowresimg_display = lowresimg;
    end

    img_recover = zeros(x_dim_est, y_dim_est);

    %scan the optimization box throughout image
    pre_xloc = 1 + scanoverlap - boxsize;
    pre_yloc = 1;
    while true;
       pre_xloc = pre_xloc + boxsize - scanoverlap;
       if pre_xloc + boxsize - 1 > x_dim
           pre_yloc = pre_yloc + boxsize - scanoverlap;
           pre_xloc = 1;
           if pre_yloc + boxsize - 1 > y_dim 
               break;
           end
       end

       % The boundary of the optimization box
       inx_x_l = pre_xloc;
       inx_x_u = pre_xloc + boxsize - 1;
       inx_y_l = pre_yloc;
       inx_y_u = pre_yloc + boxsize - 1;

       if debug_mode
          imagesc((lowresimg_display)), colormap(gray), hold on;
          rectangle('Position',[inx_x_l-0.5,inx_y_l-0.5,boxsize,boxsize],'linewidth',1,'EdgeColor','y'),pause(0.5);
       end

       % crop from the original image
       cropraw = double(lowresimg(inx_y_l:inx_y_u, inx_x_l:inx_x_u));

       L1sum = norm(cropraw(:),1);
       L2sum = sqrt(L1sum); % Target of least square estimation based on Poisson statistics

       if norm(cropraw(:)-L1sum/length(cropraw(:)),2) < L2sum * red_chi2;
           continue;    % no molecules in the patch, just flat background
       end

       % optimization using CVX
       %Weighting = diag(1./sqrt(abs(cropraw(:))+1));
       %img_est = MolEst_eps(Weighting*cropraw(:),len,Weighting*A,c,(boxsize*boxsize-1)*red_chi2);
       img_est = MolEst_eps(cropraw(:),len,A,c,L2sum * red_chi2);
       est_background = img_est(len);
       if(debug_mode)
           fprintf('BG=%.1f, L1=%.1f (target: %.1f), L2=%.1f (target: %.1f), ',...
                  est_background,...
                  norm(c*img_est,1),             L1sum-(boxsize*boxsize)*est_background,...
                  norm(A*img_est-cropraw(:),2),  L2sum);
       end

       img_est = reshape(img_est(1:len-1), [length(x_pos_est) length(y_pos_est)]);

       % remove margin
       img_est = img_est(margin+1:end-margin, margin+1:end-margin);
       if extramargin > 0
          img_est([1:extramargin*div end-extramargin*div+1:end],:) = 0;
          img_est(:,[1:extramargin*div end-extramargin*div+1:end]) = 0;
       end

       % put this hi-res patch of image back into collection array
       hr_x_l = (inx_x_l-1)*div+1;
       hr_y_l = (inx_y_l-1)*div+1;
       hr_x_u = hr_x_l + (div*boxsize)-1;
       hr_y_u = hr_y_l + (div*boxsize)-1;

       img_recover_temp = zeros(x_dim_est,y_dim_est);
       img_recover_temp(hr_y_l:hr_y_u, hr_x_l:hr_x_u) = img_est;

       % add the optimized patch to the recovered super-resolution image
       img_recover = img_recover + img_recover_temp;

       % display othe optimization procedure for debugging only
       if debug_mode
           inx_contribute = find(img_recover_temp(:) > min_contrib);
           if isempty(inx_contribute)
               fprintf('nothing found.\n');
               continue;
           else
               fprintf('%d contribution points.\n',length(inx_contribute(:)));
           end

           % calculate the recovered low resolution image based on the optimization results
           lowresimg_temp = zeros(size(lowresimg));
           for jj = 1:length(inx_contribute)
              h = MolKernel(x_inx_full, y_inx_full, ...
                        x_pos_est_full(inx_contribute(jj)),...
                        y_pos_est_full(inx_contribute(jj)), mag_full);
              lowresimg_temp = lowresimg_temp + img_recover_temp(inx_contribute(jj))*h;
           end

           lowresimg_display = lowresimg_display - uint16(lowresimg_temp) / PSF_integ;

           imagesc(lowresimg_display); drawnow;
       end
    end

end
%%
function img_recover = CS_fit_at(lowresimg,candidates)
    
    cfg = config();
    debug_mode = cfg.CSSTORM.debug_mode;

    % camera parameters
    photon_per_count = cfg.CSSTORM.ADU;
    ccd_base_line = cfg.CSSTORM.base_line;
    
    % optimization parameters
    div = cfg.CSSTORM.oversampling;
    boxsize = cfg.CSSTORM.boxsize;
    extramargin = cfg.CSSTORM.extramargin;
    scanoverlap = cfg.CSSTORM.scanoverlap;
    margin = cfg.CSSTORM.margin;
    mag = 2 / boxsize;
    red_chi2 = cfg.CSSTORM.eps;

    % image info
    [x_dim,y_dim] = size(lowresimg);

    if(debug_mode)
        mag_full = 2 / x_dim ;
        min_contrib = 1; % min CS recovered value to be considered as a successful molecule identification
    end

    lowresimg = (lowresimg - ccd_base_line) * photon_per_count;

    % dimension of of the oversampled super-resolution image
    x_dim_est = x_dim * div;
    y_dim_est = y_dim * div;

    % generate global grid for final image estimate
    % these dimensions should correspond to your raw and final image
    x_pos_est_full = linspace(-1,1,x_dim_est);
    y_pos_est_full = linspace(-1,1,y_dim_est);
    x_inx_full = x_pos_est_full(x_dim_est/x_dim/2 : x_dim_est/x_dim : end);
    y_inx_full = y_pos_est_full(y_dim_est/y_dim/2 : y_dim_est/y_dim : end);

    [x_inx_full y_inx_full] = meshgrid(x_inx_full, y_inx_full);
    [x_pos_est_full y_pos_est_full] = meshgrid(x_pos_est_full, y_pos_est_full);

    % generate grid for local estimation
    x_pos_est = linspace(-1,1, div*boxsize);
    y_pos_est = linspace(-1,1, div*boxsize);

    x_inx = x_pos_est((div/2) : div : end);
    y_inx = y_pos_est((div/2) : div : end);

    % provide pixel padding for local CS estimate
    % This should correspond to the box size around peak intensity
    % -- Begin padding the estimated local patch
    dx = x_pos_est(2)-x_pos_est(1);
    dy = y_pos_est(2)-y_pos_est(1);

    for mm = 1:margin
        x_pos_est = [x_pos_est(1)-dx x_pos_est];
        x_pos_est = [x_pos_est x_pos_est(end)+dx];
        y_pos_est = [y_pos_est(1)-dy y_pos_est];
        y_pos_est = [y_pos_est y_pos_est(end)+dy];
    end

    [x_inx y_inx] = meshgrid(x_inx,y_inx);
    [x_pos_est y_pos_est] = meshgrid(x_pos_est,y_pos_est);

    % generate measurement matrix
    len = length(x_pos_est(:));
    %A = zeros(x_inx, len + 1);
    for ii = 1:len
        img_kernel = MolKernel(x_inx, y_inx, x_pos_est(ii), y_pos_est(ii), mag);
        A(:,ii) = img_kernel(:);
    end
    
    c = sum(A);
    PSF_integ = max(c); % integration of the PSF over space, used for normalization
    c = c./ PSF_integ; % normalize to 1
    A = A./ PSF_integ;
    
    % add the extra optimization variable for the estimation of the background
    len = len + 1;
    c(len) = 0;
    A(:,len) = 1;
    
    len = size(A,2);
    A = sparse(A);
    
    % begin to formulate optimization
    cvx_quiet(cfg.CSSTORM.cvx_quiet);

    % begin analysis
    if debug_mode
       lowresimg_display = lowresimg;
    end

    img_recover = zeros(x_dim_est, y_dim_est);

    %scan the optimization box throughout image ( go through each `candidates` positions )
    for ll = 1:size(candidates,1)
       
       % Load a position of a located maxima
       pre_xloc = candidates(ll, 2) - floor(boxsize/2);
       pre_yloc = candidates(ll, 1) - floor(boxsize/2);

       % The boundary of the optimization box
       inx_x_l = pre_xloc;
       inx_x_u = pre_xloc + boxsize - 1;
       inx_y_l = pre_yloc;
       inx_y_u = pre_yloc + boxsize - 1;
       
       % check boundaries !!
       if (inx_y_l < 1) || (inx_y_u > y_dim) || (inx_x_l < 1) || (inx_x_u > x_dim)
          continue;
       end

       if debug_mode
          imagesc((lowresimg_display)), colormap(gray), hold on;
          rectangle('Position',[inx_x_l-0.5,inx_y_l-0.5,boxsize,boxsize],'linewidth',1,'EdgeColor','y'),pause(0.5);
       end

       % crop from the original image
       cropraw = double(lowresimg(inx_y_l:inx_y_u, inx_x_l:inx_x_u));

       L1sum = norm(cropraw(:),1);
       L2sum = sqrt(L1sum); % Target of least square estimation based on Poisson statistics

       if norm(cropraw(:)-L1sum/length(cropraw(:)),2) < L2sum * red_chi2;
           continue;    % no molecules in the patch, just flat background
       end

       % optimization using CVX
       %Weighting = diag(1./sqrt(abs(cropraw(:))+1));
       %img_est = MolEst_eps(Weighting*cropraw(:),len,Weighting*A,c,(boxsize*boxsize-1)*red_chi2);
       img_est = MolEst_eps(cropraw(:),len,A,c,L2sum * red_chi2);
       est_background = img_est(len);
       if(debug_mode)
           fprintf('BG=%.1f, L1=%.1f (target: %.1f), L2=%.1f (target: %.1f), ',...
                  est_background,...
                  norm(c*img_est,1),             L1sum-(boxsize*boxsize)*est_background,...
                  norm(A*img_est-cropraw(:),2),  L2sum);
       end

       img_est = reshape(img_est(1:len-1), [length(x_pos_est) length(y_pos_est)]);

       % remove margin
       img_est = img_est(margin+1:end-margin, margin+1:end-margin);
       if extramargin > 0
          img_est([1:extramargin*div end-extramargin*div+1:end],:) = 0;
          img_est(:,[1:extramargin*div end-extramargin*div+1:end]) = 0;
       end

       % put this hi-res patch of image back into collection array
       hr_x_l = (inx_x_l-1)*div+1;
       hr_y_l = (inx_y_l-1)*div+1;
       hr_x_u = hr_x_l + (div*boxsize)-1;
       hr_y_u = hr_y_l + (div*boxsize)-1;

       img_recover_temp = zeros(x_dim_est,y_dim_est);
       img_recover_temp(hr_y_l:hr_y_u, hr_x_l:hr_x_u) = img_est;

       % add the optimized patch to the recovered super-resolution image
       img_recover = img_recover + img_recover_temp;

       % display othe optimization procedure for debugging only
       if debug_mode
           inx_contribute = find(img_recover_temp(:) > min_contrib);
           if isempty(inx_contribute)
               fprintf('nothing found.\n');
               continue;
           else
               fprintf('%d contribution points.\n',length(inx_contribute(:)));
           end

           % calculate the recovered low resolution image based on the optimization results
           lowresimg_temp = zeros(size(lowresimg));
           for jj = 1:length(inx_contribute)
              h = MolKernel(x_inx_full, y_inx_full, ...
                        x_pos_est_full(inx_contribute(jj)),...
                        y_pos_est_full(inx_contribute(jj)), mag_full);
              lowresimg_temp = lowresimg_temp + img_recover_temp(inx_contribute(jj))*h;
           end

           lowresimg_display = lowresimg_display - uint16(lowresimg_temp) / PSF_integ;

           imagesc(lowresimg_display); drawnow;
       end
    end

end
%%
function img_est = MolEst_eps(img_raw,len,A,c,eps)

    b = img_raw(:);
    n = len;

    cvx_begin
        variable x(n)
        minimize( norm( A * x - b, 2 ) )
        %minimize(c*x)
        subject to
            x >= 0;
            %norm( A * x - b, 2 ) <= eps;
    cvx_end
    img_est = x;
    
end
%%
function img_kernel = MolKernel(x_inx,y_inx,x_pos,y_pos,mag)

    cfg = config();
    sigma = cfg.CSSTORM.psf_sigma;
    N = 1;  % this actually doesn't matter...it could be any value
    N_norm = N / (2*pi*sigma^2);
    img_kernel = N_norm * exp(-0.5*((((x_inx-x_pos)/mag).^2 + ((y_inx-y_pos)/mag).^2)/sigma^2));

end
%%
function mol_list = CS_postprocessing(mol_image)

    cfg = config();

    max_list = find_max_8(mol_image,cfg.CSSTORM.thr_max_I); % [x,y,I]
    max_list = sort(max_list,3,'descend');
    mol_count = 0;
    mol_list = [];
    for mi = 1:size(max_list,1)
        comp = find_component(max_list(mi,:),mol_image,cfg.CSSTORM.thr_component_I);
        I = sum(comp(:));
        % estimate the exact position
        x = max_list(mi,1);
        y = max_list(mi,2);
        [dx,dy] = center_of_mass(comp);
        % map the fitted parameters back to the original ranges
        mol_x = (x + dx) / double(cfg.CSSTORM.oversampling);
        mol_y = (y + dy) / double(cfg.CSSTORM.oversampling);
        mol_I = I / double(65535);  % max_val(uint16)
        % push to the list
        mol_count = mol_count + 1;
        mol_list(mol_count,:) = [mol_x,mol_y,cfg.CSSTORM.psf_sigma,mol_I];
        % subtract the component from the image
        %mol_image(y-1:y+1,x-1:x+1) = 0;    % not necessary
    end

end
%%
% find maximum relatively to its 8-neighbourhood (it is slow, but it
% doesn't matter, because the whole CSSTORM_fit takes too long)
function max_list = find_max_8(mol_image,thr_I)

    max_count = 0;
    max_list = [];
    for row = 1:size(mol_image,1)
        for col = 1:size(mol_image,2)
            if((mol_image(row,col) >= thr_I) && ...
               (mol_image(row,col) >= mol_image(row-1,col-1)) && ...
               (mol_image(row,col) >= mol_image(row-1,col  )) && ...
               (mol_image(row,col) >= mol_image(row-1,col+1)) && ...
               (mol_image(row,col) >= mol_image(row  ,col-1)) && ...
               (mol_image(row,col) >= mol_image(row  ,col+1)) && ...
               (mol_image(row,col) >= mol_image(row+1,col-1)) && ...
               (mol_image(row,col) >= mol_image(row+1,col  )) && ...
               (mol_image(row,col) >= mol_image(row+1,col+1)))
                max_count = max_count + 1;
                max_list(max_count,:) = [col,row,mol_image(row,col)];  % save [x,y] coords and Intensity
            end
        end
    end

end
%%
% find component connected to the max in the mol_image
% - returns the component stored in 2D matrix
% - for convenience the current version doesn't actually look for the connected component
%   - it just takes the 8-neighbourhood of the maxima
function component = find_component(max,mol_image,thr_I)

    x = max(1); y = max(2);
    component = mol_image(y-1:y+1,x-1:x+1);

end
%%
function [dx,dy] = center_of_mass(component)

    % min-max normalization (transform absolute values to weights)
    c_min = min(component(:));
    c_max = max(component(:));
    c_norm = (component - c_min) ./ (c_max - c_min);
    
    % the maximum is always in the center and, thanks to the normalization,
    % it is always equal to 1
    % --> that's why I don't need to consider it in the following
    %     calculations
    %
    %  .
    %
    
    % margin rows (b=bottom,t=top)
    % - -
    %
    % - -
    tr_dx = c_norm(1,3) - c_norm(1,1);
    br_dx = c_norm(3,3) - c_norm(3,1);
    
    % margin cols (l=left,r=right)
    % | |
    %
    % | |
    lc_dy = c_norm(3,1) - c_norm(1,1);
    rc_dy = c_norm(3,3) - c_norm(1,3);
    
    % how does the margin cols and rows affect the middle ones?
    % and what is the final relative position?
    %  |
    % - -
    %  |
    dx = (c_norm(2,3) - c_norm(2,1)) + tr_dx + br_dx;
    dy = (c_norm(3,2) - c_norm(1,2)) + lc_dy + rc_dy;

end
%%
function loc = molecule_detect(img)

    cfg = config();
    
    % remove noise
    h = fspecial('gauss', 11, 1.3);
    imb = imfilter(img, h, 'replicate', 'same');

    % candidate pixels for a molecule
    % find local maxima + sort by intensity
    [val,idx] = locmax2d(double(imb), 0, 2);    % min_dist = 2px
    [u,v] = ind2sub(size(img),idx);
    loc = [u, v, imb(idx)];
    
end