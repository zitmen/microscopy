cfg = config();

molecules = [];
for frame=1:9990
    resfile = sprintf('%s%s%s%04d%s',cfg.respath,'CSSTORM-',cfg.dataset,frame);
    if ~exist([resfile '.csv'],'file'), continue; end;   % source?
    try
        res = csvread([resfile '.csv']);
    catch err  % empty file
        continue;
    end
    molecules = vertcat(molecules,res);
end

csvwrite([cfg.respath 'CSSTORM-' cfg.dataset '.csv'],molecules);
