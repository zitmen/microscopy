function run_experiment(start,stop)
	cd cvx
	cvx_setup
	cd ..

	cfg = config();

	for frame=start:stop
		if ~exist(sprintf('%s%s%04d%s',cfg.datapath,cfg.dataset,frame,cfg.image_extension),'file'), continue; end; % source exists?
		if exist(sprintf('%s%s%s%04d%s',cfg.respath,'CSSTORM-',cfg.dataset,frame,'.mat'),'file'), continue; end; % result exists?
		fprintf('Frame: %d\n',frame);
		CS_mol_list = CSSTORM(sprintf('%s%s%04d%s',cfg.datapath,cfg.dataset,frame,cfg.image_extension));
		save(sprintf('%s%s%s%04d%s',cfg.respath,'CSSTORM-',cfg.dataset,frame,'.mat'),'CS_mol_list');
	end
end
