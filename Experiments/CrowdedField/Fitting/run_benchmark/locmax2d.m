function [val,idx] = locmax2d(x, thr, mindist)
% Find local maxima in a 4-neighbourhood
%
% USE:
%   [val,idx] = locmax2d(X)               ... all local maxima
%   [val,idx] = locmax2d(X, thr)          ... local maxima above threshold
%   [val,idx] = locmax2d(X, thr, mindist) ... local maxima above threshold and with minimum distance
%
% IN:
%   x       ... [m x n] matice
%   thr     ... local maxima threshold 
%   mindist ... local maxima minimum distance
%
% OUT:
%  val      ... functional values
%  idx      ... index coordinates

% 23.4.2010, PK, using "findlocmax2d.mexw32" makes it about 15x faster
% 6.10.2009, PK

[m,n] = size(x);

% find all local maxima in 4-neighbourhood
idx = findlocmax2d(x);

% get only points above threshold
if nargin > 1 && ~isempty(thr)
  idx = idx(x(idx) > thr);
end;

% sort maxima according to their value
[foo,sortidx] = sort(x(idx),'descend');
idx = double(idx(sortidx));

% get only points with minimal distance
if nargin > 2
  % test na 2D
  d = size(mindist,2);
  if d == 1
    mindist(2) = mindist(1);
  elseif d~= 2
    error('localmax2d: Vector mindist has to be 2D.');
  end
  
  % invalidate all points in neighbourhood of [v(I),u(I)]
  [u,v] = ind2sub([m,n],idx);
  valid = true(length(v),1);
  for I = 1:length(v)
    if ~valid(I), continue; end;
    ind = ((v(I)-v)/mindist(2)).^2 + ((u(I)-u)/mindist(1)).^2 > 1;
    ind(I) = true;
    valid = valid & ind;
  end
  
  % take only valid points
  idx = idx(valid);  
end

% functional value of points
val = x(idx);

%eof