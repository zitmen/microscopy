cfg = config();

for frame=1:9990
    resfile = sprintf('%s%s%s%04d%s',cfg.respath,'CSSTORM-',cfg.dataset,frame);
    if ~exist([resfile '.mat'],'file'), continue; end;  % source?
    if exist([resfile '.csv'],'file'), continue; end;   % dest?
    load([resfile '.mat']);
    csvwrite([resfile '.csv'],CS_mol_list);
end