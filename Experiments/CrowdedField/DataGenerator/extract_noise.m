function noise = extract_noise(cfg, datadir, filemask, frame)

Z = 1;
T = frame;
W = 1;

[info,iqinf] = iq2imginit(datadir, filemask);
noise = iq2imgload(info, Z, T, W, @double);
noise = noise(1:cfg.imsize(1), 1:cfg.imsize(2));
