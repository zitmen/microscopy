% Generate monte-carlo images of dots on noisy background

clear

cfg = cfg_mcstatic();

% initialize
imgpath = [cfg.datapath 'noisy_data' '.tiff'];
moleculepath = [cfg.datapath 'clean_data' '.tiff'];
noisepath = [cfg.datapath 'noise' '.tiff'];
if ~isdir(cfg.datapath), mkdir(cfg.datapath); end;
if exist(imgpath, 'file')
    delete(imgpath);
end;
if exist(moleculepath, 'file')
    delete(moleculepath);
end;
if exist(noisepath, 'file')
    delete(noisepath);
end;

params_cells = cell(cfg.numimgs, 1);
images_cells = cell(cfg.numimgs, 1);

MAX_MOLECULES_PER_INTERSECTION = 5;

% generate 
fprintf('Generating %d static monte-carlo images\nProgress ', cfg.numimgs);
%matlabpool local 8
parfor frame = 1:cfg.numimgs

  % extract a 'noisy frame'
  %imbg = extract_noise(cfg, 'camera_noise', 'iXonAndor_EMCCD_ShutterClosed_PreAmpGain1.0_EMGain100', frame);
  
  imbg = gennoise(cfg);
  
  % montecarlo simulation of dots
  [imfg,params] = gendotsstatic(cfg, uint32(1 + (MAX_MOLECULES_PER_INTERSECTION-1)*rand()));
  
  % dots on background
  im = imbg + imfg;

  % save dots and parameters 
  %imwrite(uint16(65535*im),imgpath,'WriteMode','append','Compression','none');
  %imwrite(uint16(65535*imfg),moleculepath,'WriteMode','append','Compression','none');
  %imwrite(uint16(65535*imbg),noisepath,'WriteMode','append','Compression','none');
  images_cells{frame} = im;%imfg;
  params_cells{frame} = params;
  
  % progress
  %if mod(frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end

%write to file
for frame = 1:cfg.numimgs
    imwrite(uint16(65535*images_cells{frame}),moleculepath,'WriteMode','append','Compression','none');
end

save([cfg.datapath 'clean_data' '.mat'],'-v6','params_cells');

fprintf('\ndone\n\n');
