function noise = gennoise(cfg, reset)
% Generate an image simulating camera background noise

persistent invFx;

% read inverse cumulative distribution function of noise 
if nargin < 2, reset = 0; end;
if isempty(invFx) || reset
  invFx = dlmread('noise_invFx.txt');
end

% random data from uniform distribution
X = rand(cfg.imsize);

% transform to noise distribution - interpolation
noise = interp1(invFx(:,1),invFx(:,2),X);
