addpath('./BM3D');

z = im2double(imread('./data/test.tiff'));

NA = cell(20);
z_est = cell(20);

%matlabpool local 8
parfor i=1:20
    [NA{i}, z_est{i}] = BM3D(1, z, i);
end

for i=1:20
    imwrite(z_est{i}, ['./res/filtered_sigma-' int2str(i) '.tiff'])
end
