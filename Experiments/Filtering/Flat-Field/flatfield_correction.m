% pozn.: median fachci nejlip...variance logicky nedava moc smysl, stredni
% hodnota muze byt hodne ovlivnena blikanim molekul, protoze staci jeden
% jasnej snimek a hned se stredni hodnota posune moc nahoru, modus by byl
% dobrej, ale problem je s implementaci, protoze na 50ti snimcich je dost
% velka pravdepodobnost, ze vyjde jen naka blba nahodna hodnota (sum),
% takze median dava opravdu nejspolehlivejsi vysledky...samozrejme by bylo
% jeste lepsi udelat to na vice snimcich, udealne na vsech, ale co s
% pameti, zejo...pocitat vzdy po jednom pixelu? to by bylo hrozne dlouhy,
% ale zas vysledek by byl nejpresnejsi
function flatfield_correction()
%{
    imrange = 11000:15000;
    %
    minIM = double(imread(sprintf('12 + cyl lens_t%d.tif',imrange(1))));
    %
    fprintf('Loading');
    for ii=2:length(imrange)
        minIM = min(minIM,double(imread(sprintf('12 + cyl lens_t%d.tif',imrange(ii)))));
        if(mod(ii,floor(length(imrange)/10)) == 0), fprintf('.'), end;
    end
    fprintf('done.\n');
    %
    % show example
    IM = double(imread('12 + cyl lens_t0831.tif'));
    subplot(1,3,1),imagesc(IM),subplot(1,3,2),imagesc(minIM),subplot(1,3,3),imagesc(IM-minIM),colormap gray,axis off;
%}
%{
    imrange = 14950:15000;
    %
    fprintf('Allocating memory...');
    IM = zeros(length(imrange),703,515);
    fprintf('done.\n');
    %
    %
    fprintf('Loading');
    for ii=1:length(imrange)
        IM(ii,:,:) = double(imread(sprintf('12 + cyl lens_t%d.tif',imrange(ii))));
        if(mod(ii,floor(length(imrange)/10)) == 0), fprintf('.'), end;
    end
    fprintf('done.\n');
    %
    % mean for each pixel
    EIM = reshape(mean(IM,1),size(IM,2),size(IM,3));
    %
    % variance for each pixel
    varIM = zeros(703,515);
    fprintf('Evaluating the variance');
    for ii=1:length(imrange)
        varIM = varIM + (((IM(ii) - EIM) .^ 2) ./ length(imrange));
        if(mod(ii,floor(length(imrange)/10)) == 0), fprintf('.'), end;
    end
    fprintf('done.\n');
    %
    % show example
    IM = double(imread('12 + cyl lens_t0831.tif'));
    subplot(1,3,1),imagesc(IM),subplot(1,3,2),imagesc(varIM),subplot(1,3,3),imagesc(IM./varIM),colormap gray,axis off;
%}
%
    h = fspecial('gauss',11,1.3);
%
    fprintf('Allocating memory...');
    IIM = 70;
    JJM = 70;
    medIMs = zeros(JJM,703,515);
    imrange = 10001:14900;  % 70x70=4900 frames
    %
    IM = zeros(IIM,703,515);
    fprintf('done.\n');
    %
    %
    for jj=1:JJM
        fprintf('Loading[%d]',jj);
        for ii=1:IIM
            IM(ii,:,:) = double(imread(sprintf('12 + cyl lens_t%d.tif',imrange((IIM*(jj-1))+ii))));
            IM(ii,:,:) = imfilter(IM(ii,:,:),h,'replicate','same');
            if(mod(ii,floor(IIM/10)) == 0), fprintf('.'), end;
        end
        fprintf('done.\n');
        %
        % mean for each pixel
        medIMs(jj,:,:) = reshape(median(IM,1),size(IM,2),size(IM,3));
    end
    %
    % mean for each pixel
    medIM = reshape(median(medIMs,1),size(medIMs,2),size(medIMs,3));
    save('medIM.mat','medIM');
    %
    % show example
    IM = double(imread('12 + cyl lens_t0831.tif'));
    figure(1),subplot(1,2,1),imagesc(IM),axis off,subplot(1,2,2),imagesc(medIM),axis off,colormap gray;
    figure(2),subplot(1,2,1),imagesc(IM),axis off,subplot(1,2,2),imagesc(max(IM-medIM,zeros(703,515))),axis off,colormap gray;
    figure(3),subplot(1,2,1),imagesc(IM),axis off,subplot(1,2,2),imagesc(IM./medIM),axis off,colormap gray;
    
    figure(4),subplot(1,3,1),imagesc(IM),axis off,subplot(1,3,2),imagesc(medIM),axis off,subplot(1,3,3),imagesc(max(IM-medIM,zeros(703,515))),axis off,colormap gray;
%

end