function [x,y,c] = findMax(mat,thr)

    x = zeros(1000,1);
    y = zeros(1000,1);
    c = 0;
    for i=2:(size(mat,1)-1)
        for j=2:(size(mat,2)-1)
            
            % 4-neighbourhood
            if (mat(i,j) > mat(i+1,j)) && (mat(i,j) > mat(i-1,j)) && ...
               (mat(i,j) > mat(i,j+1)) && (mat(i,j) > mat(i,j-1)) && ...
               (mat(i,j) > thr)
                
                c = c+1;
                x(c) = j;
                y(c) = i;
                
            end
            
        end
    end

end