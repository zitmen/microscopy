%%
% Parameters settings

% -> dwtmode
padding = 'ppd';    % periodical padding
% -> wpdec2
%corr=0.81842; [3 61]
%corr=0.82017; [2 13]
%corr=0.83170; [1  3]
%wname = 'coif1';    % wavelet type
lev = 3;            % levels of wavelet tree
%corr=0.83448; [1 3]
%corr=0.83159; [1 2]
%corr=0.83276; [1 1]
%corr=0.30758; [1 0], ale zajimavej vystup (takovy velky ctverecky)
wname = 'sym2';    % wavelet type
%
% -> wpcoef
tree_node = [1 3];  % selected node (it will be used as a base frequency band for a filter)
% -> wpbmpen
alpha = 2;  % from Matlab docs: "Typically ALPHA = 2"
% -> wpdencmp
keepapp = 1;    % from Matlab docs: "If KEEPAPP = 1, approximation coefficients cannot be thresholded; otherwise, they can be."
sorh = 's'; % 's' -> soft tresholding; 'h' -> hard tresholding
crit = 'nobest';    % from Matlab docs: "In addition if CRIT = 'nobest' no optimization is done and the current decomposition is thresholded."
% -> findMax
%loc_thr = 1.5*mean(mean(img));


%%
% Image denoising test

%noise = double(imread('noise_auto.tiff'))/double(2^16);
%orig  = double(imread('image_auto.tiff', 1))/double(2^16);
orig  = double(imread('noisy-image.tiff', 1))/double(2^16);
noise = zeros(size(orig,1),size(orig,2));
img   = orig + noise;
%img   = img - mean(mean(img));
%img   = img - max(max(noise));
img   = img .* 2;
%imshow(img)
%imagesc(img),colormap('gray')
%legend('Original - noisy image');
%pause

dwtmode(padding);
tree = wpdec2(img,lev,wname);
%tree = besttree(tree);  % optimize the tree using an entropy criterion
%plot(tree);    % according to this I can choose the wpcoef 2nd parameter (the node)
det1 = wpcoef(tree,tree_node);
sigma = median(median(abs(det1)))/0.6745;
thr = wpbmpen(tree,sigma,alpha);
imgOut = wpdencmp(tree,sorh,crit,thr,keepapp);

[XCF, Lags, Bounds]=crosscorr(orig(:),imgOut(:));  % POZOR! tady to musi byt obrazek bez sumu vs. vyfiltrovany obrazek
[x,y] = meshgrid(1:size(img,1),1:size(img,2));
%grid = [x y];
%[max_x,max_y] =  ind2sub(size(img), grid(D==max(max(D))));

% Finally, we plot three figures.
figure(1)
%imshow(orig)
imagesc(orig),colormap('gray')
legend('Original - image with blobs');

figure(2)
%imshow(imgOut)
imagesc(imgOut),colormap('gray')
legend('Result - denoised image');

figure(3)
plot(Lags,XCF)
%plot3(x,y,D, x(max_i), y(max_i), D(max_i), 'rx')
%text(x(max_i), y(max_i), D(max_i), num2str(D(max_i)))
legend(strcat('Correlation = ', num2str(max(max(XCF)))));

loc_thr = 1.5*mean(mean(img));
[x,y,c] = findMax(imgOut,loc_thr);

figure(4)
plot(x,y,'gx')
legend(strcat('Found dots = ', num2str(c)));

%%
% Sine wave denoising example

% k = 0:9.0703e-005:5;
% w=5*pi;
% 
% h=w.*k;
% x = sin(h);
% 
% y = awgn(x,0,'measured');
% wname = 'coif5'; lev = 10;
% tree = wpdec(y,lev,wname);
% det1 = wpcoef(tree,2);
% sigma = median(abs(det1))/0.6745;
% alpha = 2;
% thr = wpbmpen(tree,sigma,alpha);
% keepapp = 1;
% xd = wpdencmp(tree,'s','nobest',thr,keepapp);
% 
% D=crosscorr(x,xd);
% z=-20:1:20;
% max_i = (D==max(D));
% 
% % Finally, we plot three figures.
% figure(1)
% plot(k,y,'k')
% hold on
% plot(k,x,'g')
% hold on
% plot(k,xd)
% legend('Denoise signal','Signal with AWGN','Original signal');
% figure(2)
% plot(z,D, z(max_i), D(max_i), 'rx')
% text(z(max_i), D(max_i), num2str(D(max_i)))
% legend('Correlation @ 0');
% 
