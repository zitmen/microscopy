function y = fun_gauss2D(X,par,partransform)

if nargin < 3, partransform = @fun_partransfdefault; end;
[x0,y0,sigma,A,b] = partransform(par);


y = A * exp(-0.5 * (  ((X(:,1)-x0)/sigma).^2  + ((X(:,2)-y0)/sigma).^2 )) + b;
%y = A * exp(-0.5 * (  ((x-x0)/sigma).^2  + ((y-y0)/sigma).^2 )) / (2*pi*sigma^2) + b;