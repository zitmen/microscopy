function im = iq2imgload(info, Z, T, W, outputtype)
% load single/multilayer tif

if (nargin < 2 || isempty(Z)), Z = 1; end;
if (nargin < 3 || isempty(T)), T = 1; end;
if (nargin < 4 || isempty(W)), W = 1; end;
if (nargin < 5),  outputtype = @double; end;

if ((Z < 1) || (Z > info.siz(3))), error('iq2imgload:outofrange','Z out of range.'); end;
if ((T < 1) || (T > info.siz(4))), error('iq2imgload:outofrange','T out of range.'); end;
if ((W < 1) || (W > info.siz(5))), error('iq2imgload:outofrange','W out of range.'); end;

% determine frame
frame = 1 + (Z-1) * info.offset.z + (T-1) * info.offset.t + (W-1) * info.offset.w;

% here: info.numframes = [0 cumsum(numpg)] 
idx = info.numframes < frame;
fileno = sum(idx);
frameno = frame - info.numframes(fileno);

im = outputtype(imread([info.datadir filesep info.filelist{fileno}], frameno));

if (isa(outputtype(0),'double') || isa(outputtype(0),'single')), im = im/info.norm; end;
