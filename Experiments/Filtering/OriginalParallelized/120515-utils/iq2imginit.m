function [info, iqinf] = iq2imginit(varargin)
% initialize data with IQ2 info
% Use:
%       [info, iqinf] = iq2imginit(datadir)
%       [info, iqinf] = iq2imginit(datadir, filemask)

% 11/11/2011  PK, code cleaning + bugfix

% read directory info
[info.datadir,info.filemask,info.filelist,numfiles] = initdata(varargin{:});

% read IQ info from the text file or from the first image file
if exist([info.datadir filesep info.filemask '.txt'], 'file')
  [iqinf, iqmode] = iq2parse(fileread([info.datadir filesep info.filemask '.txt']));
else
  iminfo = imfinfo([info.datadir filesep info.filelist{1}]);
  [iqinf, iqmode] = iq2parse(iminfo(1).ImageDescription);
end

% read image size, roi, etc.
info = getiqinfo(info, iqinf, iqmode);

% dimensions order
info = getdimsorder(info,numfiles);

% ============================================================================
function [datadir,filemask,filelist,numfiles] = initdata(varargin)
% ============================================================================

% datadir 
if (nargin < 1 || ~isdir(varargin{1})), 
  error('iq2imginit:nodir','Data directory does not exist.');
end
datadir = strrep(varargin{1},'/',filesep);
if (datadir(end) == filesep), 
  datadir = datadir(1:end-1);   % no '\' in the end
end; 

% filemask
if nargin < 2, filemask = []; else filemask = varargin{2}; end
if isempty(filemask)
  % take filemask from the dir name
  idx = find(datadir == filesep,1,'last');
  filemask = datadir((idx+1):end);
end

% read dir info
dirinfo = dir([datadir filesep filemask '*.tif']);

% sort *.tif file names with _w0000 | _z0000 | _t0000 | _o0000 in name
K = 0;
numfiles = length(dirinfo);
WZT = zeros(numfiles,5,'uint32');
for I = 1:numfiles  
  fn = dirinfo(I).name;
  if ~isempty(strfind(fn,[filemask '_w'])) || ...
     ~isempty(strfind(fn,[filemask '_z'])) || ...
     ~isempty(strfind(fn,[filemask '_t'])) || ...
     ~isempty(strfind(fn,[filemask '_o'])) || ...
     ~isempty(strfind(fn,[filemask '.tif']))
    K = K+1;
    WZT(K,:) = [parsenum(fn,'_w'), parsenum(fn,'_z'), parsenum(fn,'_t'), parsenum(fn,'_o'), I];
  end
end

% sort files, first W then Z and last T/O
if (K == 0), 
  error('iq2imginit:nodatafile','Data file(s) not found in the directory.');
end;
WZT = sortrows(WZT(1:K,:),[1 2 3 4]);

% create a list of data files
filelist = {dirinfo(WZT(:,5)).name};

% count files: numfiles = [T Z W]
numfiles = [max(length(unique(WZT(:,3))), length(unique(WZT(:,4)))), ...
            length(unique(WZT(:,2))), ...
            length(unique(WZT(:,1))) ];
idx = ~(numfiles > 1);
numfiles(idx) = 0;

% ----------------------------------------------------------------------------
function num = parsenum(str, id)
% ----------------------------------------------------------------------------
% parse number according to identifier from a string, e.g., parsenum('aaa_t0005_z0002','_t') = 5
idx1 = strfind(str,id) + length(id);
if isempty(idx1)
  num = NaN;
  return;
end

idx2 = find(str(idx1:end) < '0' | str(idx1:end) > '9', 1, 'first') + idx1 - length(id);

if isempty(idx2)
  num = str2double(str(idx1:end));
else
  num = str2double(str(idx1:idx2));
end

% ============================================================================
function [info, mode] = iq2parse(desc)
% ============================================================================

if isempty(desc), error('iq2parse:nodesc','Missing IQ description.'); end;

% read iq file
lnend = find(desc == 10);
lnbegin = [1, lnend(1:end-1)+1];
lnnum = length(lnbegin);
iqv1 = false;
info = [];
mode = [];
strsec = 'Header';

% parse values
for I = 1:lnnum

  % read one line
  lnstr = desc(lnbegin(I):lnend(I));
  % skip empty line
  if ~any(lnstr > ' '), continue; end;

  % start a new setion
  if lnstr(1) == '['
    if ~isempty(strfind(lnstr, ' End]')) || ~isempty(strfind(lnstr, '[End ')), strsec = ''; continue; end;
    strsec = fixname(lnstr(2:(find(lnstr == ']')-1)));
    if isempty(strsec), error('iq2parse:nosec','Wrong section name.'); end;
    info.(strsec) = [];
    continue;
  end
  
  if isempty(strsec), error('iq2parse:nosec','Missing section name.'); end;
    
  % fork for
  switch (strsec)
    case 'ProtocolDescription'
      [name,val] = readvar(lnstr,'-');
      switch (name)
        case 'RepeatT'
          mode = [mode 't'];
        case 'RepeatZ'
          mode = [mode 'z'];
        case 'MoveChannel'
          mode = [mode 'w'];
        case 'Channel'
          if ~iqv1, mode = [mode 'w']; end;
        case 'Repeat'
          if ~isempty(strfind(val,'times'))
            mode = [mode 't']; iqv1 = true;
          end
      end
    case 'Header'
      [name,val] = readvar(lnstr,':');
    otherwise
      [name,val] = readvar(lnstr,'=');
  end;
  
  % assign value to a field
  if ~isempty(name)
    info.(strsec).(name) = val;
  end
      
end

% ----------------------------------------------------------------------------
function [name,val] = readvar(str,sgn)
% ----------------------------------------------------------------------------
% parse name and value in a line string
idx = find(str == sgn,1,'first');
if isempty(idx), name = ''; val = ''; return; end; %error('iq2parse:novar','Missing operator');
name = fixname(str(1:(idx-1)));
val = fixval(str((idx+1):end));

% ----------------------------------------------------------------------------
function str = fixname(str)
% ----------------------------------------------------------------------------
% string contains only letters and numbers
str( (str < 'A' | str > 'Z') & (str < 'a' | str > 'z') & (str < '0' | str > '9') ) = [];
if ((str(1) >= '0') && (str(1) <= '9')), str = ['x' str]; end;

% ----------------------------------------------------------------------------
function val = fixval(str)
% ----------------------------------------------------------------------------
% out: str or double
str(str == 9) = ' '; % tab
str(str < ' ') = []; % control characters
if (isempty(str)),  val = []; return; end;
idx = find(str > ' ',1,'first'); str = str(idx:end); % no space at the beginning
num = str2double(str);
if isnan(num),
  val = str;
else  
  val = num;
end

% ============================================================================
function info = getiqinfo(info, iqinf, iqmode)
% ============================================================================

% initial dimensions and resulution
info.siz = [NaN NaN 1 1 1];
info.res = repmat(NaN,1,4);
info.roi = repmat(NaN,1,4);

% x,y
if isfield(iqinf,'GrabParameters')
  [info.siz(2),info.siz(1)] = readvals(iqinf.GrabParameters,'ImageWidth','ImageHeight');
end

% ROI [y0,y1,x0,x1];
if isfield(iqinf,'ImageInfo')
  [info.roi(1),info.roi(3)] = readvals(iqinf.ImageInfo,'WindowTop','WindowLeft');
  info.roi([1 3]) = info.roi([1 3]) + 1;
  info.roi([2 4]) = info.roi([1 3]) + info.siz(1:2) - 1;
else
  info.roi = [1 info.siz(1) 1 info.siz(2)];
end

% t,z (for IQ2)  |  t (for IQ1)
if isfield(iqinf, 'ProtocolDescription')
  [tsiz,zsiz,rep] = readvals(iqinf.ProtocolDescription,'RepeatT','RepeatZ','Repeat');
  tsiz = parsevals(tsiz);
  if all(~isnan(tsiz)), info.siz(4) = tsiz(1); info.res(4) = tsiz(2); end;
  zsiz = parsevals(zsiz);
  if all(~isnan(zsiz)), info.siz(3) = zsiz(2); info.res(3) = zsiz(1)/(zsiz(2)-1); end;
  if ~isempty(strfind(rep,'times')) % IQ1's time repeat
    rep = parsevals(rep);
    if all(~isnan(rep)), info.siz(4) = rep(1); end;
  end;
end

if isfield(iqinf, 'TabSequence')
  % z,w    (for IQ1 and only if z or w is present)
  [wsiz1,won,w1st,wsiz2] = readvals(iqinf.TabSequence,'NumberOfLambdas','LControlEnabled','LControlMove1st','LControlNoOfLambdas');
  [zsiz1,zon,zsiz2,zres] = readvals(iqinf.TabSequence,'NumberOfZeds','ZControlEnabled','ZControlPlanes','ZControlStep');
  if ((wsiz1 ~= wsiz2) || (zsiz1 ~= zsiz2)), error('iq2imginit:iqinfocorrupted','IQ protocol corrupted.'); end;  
  info.siz(3) = zsiz1;
  info.siz(5) = wsiz1;
  info.res(3) = zres;
  if strcmp(zon,'Checked') && ~strcmp(won,'Checked')
    iqmode = [iqmode 'z'];
  elseif ~strcmp(zon,'Checked') && strcmp(won,'Checked')
    iqmode = [iqmode 'w'];
  elseif strcmp(zon,'Checked') && strcmp(won,'Checked')
    if strcmp(w1st,'Checked')
      iqmode = [iqmode 'zw'];
    else
      iqmode = [iqmode 'wz'];
    end
  end
else
    % w  (for IQ2)
    idx = find(iqmode == 'w');
    if (length(idx) > 1),
      info.siz(5) = length(idx);
      iqmode(idx(2:end)) = [];
    else
      info.siz(5) = 1;
    end;
end

% adjust iqmode
if (info.siz(5) < 2), iqmode(iqmode == 'w') = []; end;
if (info.siz(4) < 2), iqmode(iqmode == 't') = []; end;
if (info.siz(3) < 2), iqmode(iqmode == 'z') = []; end;
info.mode = iqmode;

% Bit depth
if isfield(iqinf,'TabDeviceInfo')
  info.nbit = readvals(iqinf.TabDeviceInfo,'BitDepth');
else
  info.nbit = 16;
end
info.norm = 2^info.nbit-1;

% Check all assigned values with header - header seems to be present only in the external text file
if isfield(iqinf,'Header')
  [xsiz,ysiz,tsiz,t1siz,zsiz,wsiz] = readvals(iqinf.Header,'x','y','Time','Time1','Z','Wavelength');
  % lateral resolution
  xsiz = parsevals(xsiz); info.res(2) = xsiz(2);
  ysiz = parsevals(ysiz); info.res(1) = ysiz(2);
  if (info.siz(1) ~= ysiz(1)) || ... 
     (info.siz(2) ~= xsiz(1)) || ...
     (~isnan(zsiz) && (info.siz(3) ~= zsiz)) || ...
     (~isnan(tsiz) && (info.siz(4) ~= tsiz)) || ...
     (~isnan(t1siz) && (info.siz(4) ~= t1siz)) || ...
     (~isnan(wsiz) && (info.siz(5) ~= wsiz))
       error('iq2imginit:iqinfocorrupted','IQ protocol corrupted.');
  end;
end
    
% ----------------------------------------------------------------------------
function varargout = readvals(in,varargin)
% ----------------------------------------------------------------------------
% read field values from a structure
nitems = nargin - 1;
varargout = cell(1,nitems);
for I = 1:nitems
  if isfield(in,varargin{I})
    varargout{I} = in.(varargin{I});
  else
    varargout{I} = NaN;
  end
end

% ----------------------------------------------------------------------------
function vals = parsevals(str)
% ----------------------------------------------------------------------------
% parse numbers from a string,  e.g., parsevals('blabla5blabla2.5blabla') = [5 2.5]
idx = isstrprop(str, 'digit')  | str == '.';
idx = diff([0 idx 0]);
ia = find(idx == 1);
ib = find(idx == -1)-1;
if isempty(ia), vals = NaN; return; end;
num = length(ia);  
vals = zeros(1,num);
for I = 1:num
  vals(I) = str2double(str(ia(I):ib(I)));
end

% ============================================================================
function info = getdimsorder(info,numfiles)
% ============================================================================

% count number of images in each data file
numtot = prod(info.siz(3:5));
nfiles = length(info.filelist);
if nfiles > 1
  numpg = zeros(1,nfiles); 
  numpg(1) = tiffnumpg([info.datadir filesep info.filelist{1}]);  % count only first
  numpg(2:nfiles-1) = numpg(1); 
  numpg(nfiles) = numtot - (nfiles-1)*numpg(1);
else
  numpg = numtot;
end
info.numframes = [0 cumsum(numpg)];

% get order of dimensions
info.offset = struct('z',numtot,'t',numtot,'w',numtot);
str = 'tzw';
offset = 1;

% loop within file
loopin  = strintersect(info.mode,str(numfiles == 0));
for I = length(loopin):-1:1
  info.offset.(loopin(I)) = offset;
  offset = offset * dimsiz(info.siz,loopin(I));
end

% loop over files
loopout = strintersect(info.mode,str(numfiles > 0));
for I = 1:3
  idx = str(I) == loopout;
  if ~any(idx), continue; end;
  info.offset.(loopout(idx)) = offset;
  offset = offset * dimsiz(info.siz,loopout(idx));
end

% ----------------------------------------------------------------------------
function strout = strintersect(str,strmask)
% ----------------------------------------------------------------------------
strout = [];
for I = 1:length(str)
  if any(str(I) == strmask)
    strout = [strout str(I)];
  end
end

% ----------------------------------------------------------------------------
function siz = dimsiz(imsiz,id)
% ----------------------------------------------------------------------------
switch (id)
  case 'z'
    siz = imsiz(3);
  case 't'
    siz = imsiz(4);
  case 'w'
    siz = imsiz(5);
  otherwise
    error('iq2imginit:dimsiz','Wrong dimension identifier')
end

% eof