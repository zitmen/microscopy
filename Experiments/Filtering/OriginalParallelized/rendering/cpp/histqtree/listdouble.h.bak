#ifndef __LISTDOUBLE_H__
#define __LISTDOUBLE_H__

// ---------------------------------------------------------------------------
// double-linked list
// ---------------------------------------------------------------------------

template <typename Type>
class ListDouble {

private:
	struct ListItem {
		Type    val;
		ListItem *prev;
		ListItem *next;
			
		ListItem(ListItem *prev, ListItem *next, const Type & val)
		: val(val), prev(prev), next(next)
		{
			prev->next = this;
			next->prev = this;
		}
		
		~ListItem()
		{
			prev->next = next;
			next->prev = prev;
		}

	};
	
	ListItem *list;		 // list->next = head, list->prev = tail, list = temp. item
	int num;			 // number of items

public:
	
	// constant list node access class
	class CNode 
	{
	protected:

		ListItem	*	ptr;		// current list item

		// attach to list item
		CNode(ListItem * ptr)
		: ptr(ptr)
		{}

		friend class ListDouble<Type>;
		
	public:

		// default constructor
		CNode()
		: ptr(NULL)
		{}
	};

	// modifiable list node access class
	class Node : public CNode
	{
	protected:

		// resolve base members
		using CNode::ptr;

		friend class ListDouble<Type>;

	public:

		// attach to list item
		Node(ListItem * ptr)
		: CNode(ptr)
		{}

		// default constructor
		Node() {}
	};

	// default constructor
	ListDouble()
	: num(0)
	{
		list = new ListItem;
		list->next = list->prev = list;
	}


	// cleanup
	~ListDouble()
	{
		DeleteAll();
		delete list;
	}

	void DeleteAll()
	{
		ListItem *p;
		while( (p = first) != NULL )
		{
			first = p->next;
			//delete p->val; ???
			delete p;
		}
		num = 0;
	}

	// get modifiable starting item of list
	Node Head()
	{
		return Node(first);
	}

	// get constant starting item of list
	CNode Head()
	const
	{
		return CNode(first);
	}

	// get ending item
	Node Tail()
	{
		return Node(last);
	}

	// get ending item
	CNode Tail()
	const
	{
		return CNode(last);
	}


	// add item at the beginning
	ListDouble& AddHead(const Type & val)
	{
		//num++;
		//return Node(new ListItem(NULL,first,val));

		
		ListItem *p = new ListItem(NULL,first,val);
		if (first)
			first->prev = p;
		else
			last = p;
		first = p;
		num++;
		return *this;
		
	}

	// add item at the end
	ListDouble& AddTail(const Type & val)
	{
		ListItem *p = new ListItem(last,NULL,val);
		if (last)
			last->next = p;
		else
			first = p;
		last = p;
		num++;
		return *this;
	}

	Type DeleteHead()
	{
		if (!first)
			throw "LINKLIST: List is empty";
		ListItem *p = first;
		first = p->next;
		if (first)
			first->prev = NULL;
		else
			last = NULL;
		Type val = p->val;
		num--;
		delete p;
		return val;
	}

	Type DeleteTail()
	{
		if (!last)
			throw "LINKLIST: List is empty";
		ListItem *p = last;
		last = p->prev;
		if (last)
			last->next= NULL;
		else
			first = NULL;
		Type val = p->val;
		num--;
		delete p;
		return val;
	}

	
	// delete given node from list
	Node Delete(Node node)
	{
		ASSERT(num > 0);

		Node next = node.ptr->next;
		delete node.ptr;
		--num;

		return next;
	}


	// get number of items
	int Num()
	const
	{
		return num;
	}

	// is list empty?
	bool IsEmpty()
	const
	{
		return (first == NULL);
	}


	// cancel copy constructor (Virius 1, p.140)
	ListDouble(const ListDouble&);
	ListDouble& operator=(const ListDouble&);
};

#endif
