function [xpos,ypos,zpos,sigmaa,sigmab,N,b,dx,dz,a,s,chi2,SNR,idx] = render_rules3(results, cfg)

fits = results.fits;

% sigmapsf = results.cfg.psfsigma;
a = results.resolution;

ab2zmap = dlmread('zcal.txt');

% if isfield(results.cfg, 'vrtx') && ~isempty(results.cfg.vrtx)
%   vrtx = results.cfg.vrtx;
%   IN = inpolygon(fits(:,2),fits(:,1),vrtx(:,1),vrtx(:,2));
%   fits = fits(IN,:);
% end

%[ypos, xpos, N, offset, sy ,sx chi2, Nim, bkgmu, bkgstd, frame, numframes]

xpos = fits(:,2);
ypos = fits(:,1);
N = fits(:,3);
sigmaa = fits(:,5);
sigmab = fits(:,6);
sigma = sqrt(sigmaa.*sigmab);
chi2 = fits(:,7);
s = a * sigma;
b = fits(:,10);
dx = sqrt((s.^2 + (a^2)/12) ./ N + 8*pi * s.^4 .* (b.^2) ./ (a^2 .* N.^2));

abratio = sigmaa./sigmab;
zpos = interp1(ab2zmap(:,1),ab2zmap(:,2),abratio,'linear');
dz = interp1(ab2zmap(:,1),ab2zmap(:,3),abratio,'linear');

SNR = cfg.snrgain * N ./ b;

idxsig1 = sigma > 0.7;%*sigmapsf;
idxtmp = dx > 5 & dx < 100;

idxz = ~isnan(zpos);

% what to show
switch cfg.rule
  case 0
    idx = true(length(xpos),1);

%   case 1
%     idx = SNR > 2 & chi2/49 > 4.9;
    
  case 2
    idx = idxsig1 & idxtmp & idxz;
        
    
  otherwise
    error('No rendering rule');
end

fprintf('  %d molecules out of %d\n',sum(idx),length(xpos));

xpos = xpos(idx);
ypos = ypos(idx);
zpos = zpos(idx);
sigmaa = sigmaa(idx);
sigmab = sigmab(idx);
N = N(idx);
b = b(idx);
dx = dx(idx);
dz = dz(idx);
s = s(idx);
chi2 = chi2(idx);
SNR = SNR(idx);
