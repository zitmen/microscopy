clear
data = load('z:\Users\blinking\data\3d_data\IGF1Ra ss blink cyl lens 3.mat');


% [xpos,ypos,dx] = data2var(data,{'xpos','ypos','dx'});

% imres = [20 20 50]; %nm/pixel
% imroi = [min(data.data(:,1)), max(data.data(:,1)); ...
%          min(data.data(:,2)), max(data.data(:,2)); ...
%          min(data.data(:,3)), max(data.data(:,3))];

imroi = [min(data.data(:,2)), max(data.data(:,2)), ...
         min(data.data(:,1)), max(data.data(:,1))];

% [im,xbin,ybin] = render_hist(data.data(:,[2 1 7]), imroi, imres, 100);
%[im,xbin,ybin] = render_gauss(data.data(:,[2 1 7]), imroi, 10);

npts = size(data.data,1);
[im,xbin,ybin] = render_clusters([data.data(:,[2 1 7]) rand(npts,3)], imroi, 10);

im(im>1) = 1;
figure(2); clf
%imagesc(xbin/1000,ybin/1000,im,[0 1]);
image(im);
colormap hot
axis image %off
%set(gca,'Position',[0 0 1 1])
% xlabel('x [\mum]')
% ylabel('y [\mum]')

