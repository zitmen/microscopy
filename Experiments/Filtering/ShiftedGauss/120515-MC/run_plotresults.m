addpath('..\120515-release3');
addpath('..\120515-utils');
clear

cfg = cfg_mcstatic();

noisesigma = cfg.snr;

J =    cfg.methodrun;


res = [];
% for J = 1:18
  xbin = 0:100;%0:0.5:30;%linspace(0,26,53);
  sbin = 0.3:0.05:2.2;% linspace(0.2,2.2,35);
  FONTSIZE = 10;
  % load
  results = load([cfg.respath 'res_dst2_' cfg.method(J).name '.mat']);
  
  disp(cfg.method(J).name);
  
  % histograms
% v2.4a

  % OK: [xl yl Al xf yf sf Af bf chi2 stdbkg Nim xo yo so Ao]; dot was generated and corectly located
  % FP: [xl yl Al xf yf sf Af bf chi2 stdbkg Nim ]; type I error, dot was located but not generated
  % FN: [xo yo so Ao]; type II error, dot was generated but not located

  % if LM fitting failed: assume noisesigma for bkg, ignore FP
  idx = isnan(results.ok(:,10)); results.ok(idx,10) = noisesigma;
  idx = isnan(results.fp(:,10)); results.fp(idx,:) = [];

%   results.ok(:,15) = results.ok(:,15)./(2*pi*results.ok(:,14).^2);
%   results.fp(:,7)= results.fp(:,7)./(2*pi*results.fp(:,6).^2);
%   results.fn(:,4) = results.fn(:,4)./(2*pi*results.fn(:,3).^2);
  
  ok = hist2(results.ok(:,15)./results.ok(:,10), results.ok(:,14), xbin, sbin);
  fp = hist2(results.fp(:,7)./results.fp(:,10), results.fp(:,6), xbin, sbin);
  fn = hist2(results.fn(:,4)/noisesigma, results.fn(:,3), xbin, sbin);

% noisesigma = 0.001;
%   ok = hist2(results.ok(:,15)./noisesigma, results.ok(:,14), xbin, sbin);
%   fp = hist2(results.fp(:,7)./noisesigma, results.fp(:,6), xbin, sbin);
%   fn = hist2(results.fn(:,4)/noisesigma, results.fn(:,3), xbin, sbin);

%  imagesc(xbin, sbin, fp)
%  axis xy
  
%   ok = hist2(results.ok(:,15)/noisesigma, results.ok(:,14), xbin, sbin);
%   fp = hist2(results.fp(:,7)/noisesigma, results.fp(:,6), xbin, sbin);
%   fn = hist2(results.fn(:,4)/noisesigma, results.fn(:,3), xbin, sbin);
   
  
%   idx = xbin > 24; idx(end)=[]; ok(:,idx) = []; fp(:,idx) = []; fn(:,idx) = []; xbin(idx) = [];
%   idx = sbin > 2; idx(end)=[]; ok(idx,:) = []; fp(idx,:) = []; fn(idx,:) = []; sbin(idx) = [];
  
  ok(isnan(ok)) = 0;
  fp(isnan(fp)) = 0;
  
  tot = ok + fp + fn;

  xbin = xbin+ diff(xbin(1:2))/2;
  sbin = sbin+ diff(sbin(1:2))/2;
  
  sensitivity = ok./(ok+fn);  
  precision = ok./(ok+fp);
  precision(:,1) = 1;
  sensitivity(:,1) = 0;
  OK = ok./tot; OK(:,1) = 0;
  FP = fp./tot; FP(:,1) = 0;
  FN = fn./tot; FN(:,1) = 1;
  ERR = (fn+fp)./tot; ERR(:,1) = 1;
  F1 = 2*precision.*sensitivity./(precision+sensitivity); F1(:,1) = 0;

  
  
  
%% ------------------------------------
  
  F1r=imresize(F1,10,'bicubic');
  mask0 = F1r > 0.99; % 0% error
%   mask0 = F1r > 0.9744; % 5% error
%   mask0 = F1r > 0.9474; % 10% error
  mask = mask0 - bwmorph(mask0,'erode'); mask(end,:) = 0; mask(:,end) = 0; mask(1,1:end) = 0; 
  [u,v] = ind2sub(size(F1r), find(mask)); 
  sig = u*(sbin(end)-sbin(1))/10/length(sbin)+sbin(1);    [val1,idx1]=max(sig); [val2,idx2]=min(sig); sig = [sig; val1+0.5; val2-0.04]; 
  snr = v*(xbin(end)-xbin(1))/10/length(xbin)+xbin(1);    snr = [snr; snr(idx1)+80; snr(idx2)+10]; 

  % LSQ fit
  x = linspace(0.2,2.3,100);
  par0 = [1 1 1];
  par = lmfit('fun_poly',par0, sig, snr);
  x(2,:) = fun_poly(x,par);
  
  h=figure(9);clf reset
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,F1,[0 1]) 
   plot(snr,sig,'w.')  
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  axis xy tight
  pos = get(gca,'Position'); %pos(1) = 1.11*pos(1); pos(2) = 1.3*pos(2); pos(3) = 0.905*pos(3);
    set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE, 'XTick',0:25:100, 'TickDir','out','Position',[0.15 0.14 0.69 0.82] )%

%   set(gca,'XTickLabel',[]);
  xlabel('SNR', 'FontSize', FONTSIZE);
%  set(gca,'YTickLabel',[]);
    ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE);


  
  hcb=colorbar('Location','East','Position',[0.78 0.20 0.03 0.3],'FontSize',0.8*FONTSIZE,'Color','w','XColor','w','YColor','w');
%   text(4,2.05,'h','Color','w', 'HorizontalAlignment','left','FontSize', 2.25*FONTSIZE);
  
  %ssbin = linspace(0.5,2,310); idx = ssbin < 0.8 | ssbin > 1.5; mask0(idx,:) = []; % count only within this interval
%   text(4,1.81,sprintf('%.1f%%',100*sum(mask0(:))/numel(mask0)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);  

thr = par(1) + par(2)*(0.95*1.3) + par(3)*(0.95*1.3).^2;
% line([thr thr],[0 2.2],'Color','w','LineWidth',2,'LineStyle','--') %4.75
% line([0 100],[0.91 0.91],'Color','w','LineWidth',2,'LineStyle','--') %0.8
% line([0 100],[1.69 1.69],'Color','w','LineWidth',2,'LineStyle','--') %1.5

  ax1 = gca;
  ax2 = axes('Position',get(ax1,'Position'),...
           'XAxisLocation','top',...
           'YAxisLocation','right',...
           'TickDir','out', ...
           'XTick',[], ...
           'YLim', 80*[0.3 2.2], ...
           'Color','none',...
           'XColor','k','YColor','k','FontSize',FONTSIZE);
% set(ax2,'YTickLabel',[]);         
   ylabel('\sigma_0 [nm]','FontSize',FONTSIZE);


%   print(h,'-dpng','-r100', [cfg.respath 'f1_d2_' cfg.method(J).name '.png'])

% print(h,'-dpng','-r100', [cfg.respath 'f1_d2_nonoiseexample.png'])
  
%%  
  h=figure(1); clf
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,OK,[0 1])
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  xlabel('SNR', 'FontSize', FONTSIZE);
  ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE); 
  title('OK', 'FontSize', FONTSIZE)
  axis xy
  set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE,'TickDir','out')
  text(5,2,sprintf('%.1f%%',100*sum(OK(:)>0.95)/numel(OK)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);
  text(5,2.1,sprintf('%s',cfg.method(J).name ),'Color','w', 'HorizontalAlignment','left','interpreter','none','FontSize', FONTSIZE);  
%   print(h,'-dpng','-r100', [cfg.respath 'ok_d2_' cfg.method(J).name '.png'])

  h=figure(2); clf
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,FP,[0 1])
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  xlabel('SNR', 'FontSize', FONTSIZE);
  ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE); 
  title('FP', 'FontSize', FONTSIZE)
  axis xy
  set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE,'TickDir','out')
  text(5,2,sprintf('%.1f%%',100*sum(FP(:)>0.05)/numel(FP)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);  
  text(5,2.1,sprintf('%s',cfg.method(J).name ),'Color','w', 'HorizontalAlignment','left','interpreter','none','FontSize', FONTSIZE);  
%   print(h,'-dpng','-r100', [cfg.respath 'fp_d2_' cfg.method(J).name '.png'])

  h=figure(3); clf
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,FN,[0 1])
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  xlabel('SNR', 'FontSize', FONTSIZE);
  ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE); 
  title('FN', 'FontSize', FONTSIZE)
  axis xy
  set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE,'TickDir','out')
  text(5,2,sprintf('%.1f%%',100*sum(FN(:)>0.05)/numel(FN)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);  
  text(5,2.1,sprintf('%s',cfg.method(J).name ),'Color','w', 'HorizontalAlignment','left','interpreter','none','FontSize', FONTSIZE);  
%   print(h,'-dpng','-r100', [cfg.respath 'fn_d2_' cfg.method(J).name '.png'])

  h=figure(4); clf
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,ERR,[0 1])
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  xlabel('SNR', 'FontSize', FONTSIZE);
  ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE); 
  title('ERROR', 'FontSize', FONTSIZE)
  axis xy
  set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE,'TickDir','out')
  text(5,2,sprintf('%.1f%%',100*sum(ERR(:)>0.05)/numel(ERR)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);
  text(5,2.1,sprintf('%s',cfg.method(J).name ),'Color','w', 'HorizontalAlignment','left','interpreter','none','FontSize', FONTSIZE);  
%   print(h,'-dpng','-r100', [cfg.respath 'err_d2_' cfg.method(J).name '.png'])

  h=figure(5); clf
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,sensitivity,[0 1])
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  xlabel('SNR', 'FontSize', FONTSIZE);
  ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE); 
  title('SENSITIVITY', 'FontSize', FONTSIZE)
  axis xy
  set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE,'TickDir','out')
  text(5,2,sprintf('%.1f%%',100*sum(sensitivity(:)>0.95)/numel(sensitivity)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);
  text(5,2.1,sprintf('%s',cfg.method(J).name ),'Color','w', 'HorizontalAlignment','left','interpreter','none','FontSize', FONTSIZE);  
%   print(h,'-dpng','-r100', [cfg.respath 'sens_d2_' cfg.method(J).name '.png'])
  
  h=figure(6); clf
  set(h,'InvertHardcopy','off','Color','w')
  hold on
  imagesc(xbin,sbin,precision,[0 1])
  plot(x(2,:), x(1,:),'w','LineWidth',3)
  xlabel('SNR', 'FontSize', FONTSIZE);
  ylabel('\sigma_0 [pixels]', 'FontSize', FONTSIZE); 
  title('PRECISION', 'FontSize', FONTSIZE)
  axis xy
  set(gca,'XLim',[0 100],'YLim',[0.3 2.2], 'FontSize', FONTSIZE,'TickDir','out')
  text(5,2,sprintf('%.1f%%',100*sum(precision(:)>0.95)/numel(precision)),'Color','w', 'HorizontalAlignment','left','FontSize', FONTSIZE);
  text(5,2.1,sprintf('%s',cfg.method(J).name ),'Color','w', 'HorizontalAlignment','left','interpreter','none','FontSize', FONTSIZE);  
%   print(h,'-dpng','-r100', [cfg.respath 'prec_d2_' cfg.method(J).name '.png'])
    
%  end
