function img = mc_imginit(cfg)

% init
img.siz = cfg.imsize;
img.imraw = [];
img.params = [];
img.frame = 0;
img.convert = 1;
img.bitdepth = cfg.bitdepth;
img.norm = 2^img.bitdepth;