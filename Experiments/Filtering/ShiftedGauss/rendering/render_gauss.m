function [IM,xbin,ybin] = render_gauss(data, roi, resolution)
% data       ... [npts x 3] matrix with columns [y x std]
%   x,y      ... position [nm]
%   std      ... localization accuracy [nm]
% roi        ... region of interest [ymin ymax xmin xmax] in [nm]
% resolution ...  output resolution [nm/pixel]

% convert nm -> pixel
ypos = (data(:,1)-roi(1))/resolution;
xpos = (data(:,2)-roi(3))/resolution;
dxstd = data(:,3)/resolution;

% rendering coordinates (20nm/pixel is nominal resolution)
radius = ceil(3*20/resolution);  % rendering radius for a dot
[x,y] = meshgrid(-radius:radius,-radius:radius);
idx = x.^2 + y.^2 < radius^2;
x = x(idx); y = y(idx);

fprintf('Generating density map with Gaussian rendering\n');

% alocate memmory for reconstructed image
xbin = roi(3):resolution:roi(4); n = length(xbin);
ybin = roi(1):resolution:roi(2); m = length(ybin);
IM = zeros([m n],'single');

% for all points
fprintf('Progress ');
npts = size(data,1);
for I = 1:npts

  % get position
  u = floor(ypos(I));
  v = floor(xpos(I));
  du = ypos(I) - u;
  dv = xpos(I) - v;  

  % take coordinates only inside the image
  idx = ~(v+x < 1 | v+x > n | u+y < 1 | u+y > m);
  
  % render
  z = exp(-0.5*((x(idx)-dv).^2 + (y(idx)-du).^2)/dxstd(I)^2)/(2*pi*dxstd(I)^2);
  idx = sub2ind([m n], u+y(idx), v+x(idx));  
  IM(idx) = IM(idx) + z;

  % progress bar
  if (mod(I,round(npts/10)) == 0), fprintf('.'); end;   
end
fprintf('\ndone\n');


