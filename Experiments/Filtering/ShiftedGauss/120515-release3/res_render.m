function res = res_render(fileorresults, config)
% render results using method in config.render

% 7.10.2010, PK,  result is saved with the source data

% load configuration
if nargin < 2,
  cfg = cfg_v2();
else
  if isa(config,'function_handle')
    cfg = config();
  else
    cfg = config;
  end
end;
 
% load data
fprintf('\nRendering ...\n');
if ischar(fileorresults)
  results = res_load(fileorresults, cfg);  
  fprintf('File:                  %s\n', results.filename);
  fprintf('Date:                  %s\n', results.date);
  fprintf('Description:           %s\n', results.description);
  [pathstr, fname, ext] = fileparts(fileorresults);
  pathstr = [pathstr '/'];
  results.cfg = cfg;
  % results.cfg.vrtx = cfg.vrtx;
  % results.cfg.sigmapsf = cfg.sigmapsf;  
else
  results = fileorresults;
  fname = results.filename;
  %pathstr = results.cfg.respath;
   pathstr = strrep(cfg.resdir, '$data$', results.datapath);%(1:end-1));
end

%fname = [pathstr filesep fname];
fname = [pathstr fname];

% file appendix
if ischar(cfg.render.save)
  apdx = cfg.render.save;
else
  apdx = '';
end

% compensate for drift
results = render_driftcompensation(results,cfg);

% apply rules
[xpos,ypos,sigma,N,b,SNR,chi2,dx,s,a,idx] = render_rules(results, cfg.render);

% show what will be displayed
h=figure(7); clf
hold on
xedge = linspace(0,100,100);
yedge = linspace(0,5,100);
xbin = xedge+diff(xedge(1:2))/2;
ybin = yedge+diff(yedge(1:2))/2;
cnt = hist2(SNR,sigma,xedge,yedge);
imagesc(xbin,ybin,cnt); colormap jet
par = [ 31.0379  -15.6772   12.0866];   %    0% of error for sigma = 2.6 
sbin = linspace(0, 6,100);
plot((par(1) + par(2)*sbin + par(3)*sbin.^2), sbin,'w','LineWidth',3)
thr = par(1) + par(2)*(0.95*results.cfg.sigmapsf) + par(3)*(0.95*results.cfg.sigmapsf).^2;
line([thr thr],[0 5],'Color','w','LineWidth',2,'LineStyle','--')
line([0 100],0.7*results.cfg.sigmapsf*[1 1],'Color','w','LineWidth',2,'LineStyle','--')
line([0 100],1.3*results.cfg.sigmapsf*[1 1],'Color','w','LineWidth',2,'LineStyle','--')
axis xy;
xlabel('SNR')
ylabel('\sigma_0')
title('SNR vs. \sigma')
set(gca,'XLim',[0 100],'YLim',[0 5]);
drawnow;

% call rendering
res = cfg.render.method([xpos,ypos,sigma,N,dx], results.imsize(1:2), cfg);

% show and save results
h = figure(12); clf
if isempty(res.im)
  if ~isempty(cfg.render.save) || (isnumeric(cfg.render.save) && (cfg.render.save ~= 0))
    print(h,'-dpng','-r100',[fname '_render_' res.name apdx '.png']);
  end
else
  xedge = linspace(0,max(res.im(:)),32)';
  xbin = xedge + diff(xedge(1:2))/2;
  %cnt = histc(res.im(:),xedge);
  val = 0.3*max(res.im(:));% qntl(cnt,0.999,xbin);
  imagesc(res.im,[0 val]);
  colormap hot(256)
  axis image off;
  set(gca,'Position',[0 0 1 1]);
  text(10,10,sprintf('MAG: %.1fx',cfg.render.scale),'color','y','VerticalAlignment','top');
  if ~isempty(cfg.render.save) || (isnumeric(cfg.render.save) && (cfg.render.save ~= 0))
    %imwrite(uint8(10*IM),[fname '_render_hist' apdx '.png'],'bitdepth',8);
    imwrite(uint16(1000*res.im),[fname '_render_' res.name apdx '.png'],'bitdepth',16);
  end
end
