function results = driftcompensation(results, cfg)


if ~isfield(cfg,'drift') || ~isfield(cfg.drift,'method')
  return
end

fprintf('Drift compensation: %s ...\n', cfg.drift.method);

switch (cfg.drift.method)

  case 'none'
    return
    
  case 'linear'
    xpos = results.fits(:,2);
    ypos = results.fits(:,1);
    frame = results.fits(:,10);
    vx = cfg.drift.params(2)/1000;
    vy = cfg.drift.params(1)/1000;
    angle = cfg.drift.params(3)/1000;

    results.fits(:,2) = xpos + vx*frame + cos(angle*frame);
    results.fits(:,1) = ypos + vy*frame + sin(angle*frame);
    
  otherwise
    error('DRIFTCOMPENSATION: Unknown method.')
end
