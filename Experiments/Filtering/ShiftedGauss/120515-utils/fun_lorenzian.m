function y = fun_lorenzian(x,par,partransform)

if nargin < 3, partransform = @fun_partransfdefault; end;
[x0,sigma,A,b] = partransform(par);

y = A./(1+((x-x0)/sigma).^2) + b;
