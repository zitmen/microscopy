function [info, errmsg] = imginit(filename)

info = [];

if ~exist(filename,'file')  
  errmsg = 'File does not exist.'; return;
end

% parse path and file name
[fdir, fname, fext] = fileparts(filename);
fdir = [fdir filesep];

if ~strcmp(fext,'.txt')
  errmsg = 'Not a valid file.'; return;
end

% ----------------------------------------
% parse protocol info from a text file
% ----------------------------------------
parsevar = {...
  {'name','xsiz','id','x : '}, ...
  {'name','ysiz','id','y : '}, ...
  {'name','tsiz','id','Time : '}, ...
  {'name','tsiz','id','Time1 : '}, ...
  {'name','zsiz','id','Z : '}, ...
  {'name','nchnls','id','Wavelength : '}, ...
  {'name','zres','id','Z Control - Step='}, ...  
  {'name','nbit','id','BitDepth='}};
%  {'name','xsiz','id','Image Width='}, ...
%  {'name','ysiz','id','Image Height='}, ...
%  {'name','zsiz','id','Number Of Zeds='}, ...
%  {'name','nchnls','id','Number Of Lambdas='}, ...  
%  {'name','tsiz','id','Repeat - '}, ...
%  {'name','tsiz','id','Repeat T - '}, ...
%  {'name','preampgain','id','Gains - Pre-Amp-Gain='}, ...
%  {'name','emgain','id','Gains - EMGain='}
iqinf = iqparse([fdir fname '.txt'], parsevar);
if isempty(iqinf)
  errmsg = 'Error parsing IQ setup file.'; return;
end

if ~isfield(iqinf,'zsiz'), iqinf.zsiz = 1; end;
if ~isfield(iqinf,'nchnls'), iqinf.nchnls = 1; end;
if ~isfield(iqinf,'tsiz'), iqinf.tsiz = 1; end;
if ~isfield(iqinf,'zres'), iqinf.zres = NaN; end;
if iqinf.nchnls > 1
   errmsg = 'Cannot process data, too many channels.'; return;
end

% ----------------------------------------
% read data files
% ----------------------------------------
% list all tif
dirinfo = dir([fdir fname '*.tif']);
nfiles = length(dirinfo);
% filter names of appropriate data files
mask = false(nfiles,1);
for I = 1:nfiles
  idx = matchstr(dirinfo(I).name,  {[fname '.tif'], [fname '_t']});
  if ~isempty(idx), mask(I) = true; end;
end
filelist = {dirinfo(mask).name};
nfiles = length(filelist);
if isempty(filelist)
  errmsg = 'Data file(s) not found.'; return;
end

% sort files, first T then Z                                    
idxt = strfind(filelist{1},'_t')+2; % position of a number
idxz = strfind(filelist{1},'_z')+1; % approximate position of a number
if isempty(idxz)
  T = zeros(nfiles,1);
  for I = 1:nfiles
    fn = filelist{I};
    fn(fn<'0' | fn>'9') = ' ';
    T(I) = str2double(fn(idxt:idxt+4));   % NOTE: this could be done with string tokens
  end
  [foo,idx] = sort(T);
else
  TZ = zeros(nfiles,2);
  for I = 1:nfiles
    fn = filelist{I};
    fn(fn<'0' | fn>'9') = ' ';
    TZ(I,:) = [str2double(fn(idxt:idxt+4)), str2double(fn(idxz:idxz+5))];   % NOTE: this could be done with string tokens
  end
  [foo,idx] = sortrows(TZ,[1 2]);
end
filelist = {filelist{idx}};

% count number of images in each data file
numtot = iqinf.zsiz * iqinf.tsiz;
if nfiles > 1
  numpg = zeros(1,nfiles); 
  numpg(1) = tiffnumpg([fdir filelist{1}]);     % count only first
  numpg(2:nfiles-1) = numpg(1); 
  numpg(nfiles) = numtot - (nfiles-1)*numpg(1);
else
  numpg = numtot;
end

% if (iqinf.zsiz * iqinf.tsiz) ~= cumsum(numpg)
%   errmsg = 'Data and protocol setings are not consistent.'; return;
% end

% ----------------------------------------
% output values
% ----------------------------------------
errmsg = '';
info.datadir = fdir;
info.filename = fname;
info.filelist = filelist;
info.numframes = [0 cumsum(numpg)];
info.imsize = [iqinf.ysiz, iqinf.xsiz, iqinf.zsiz, iqinf.tsiz];
info.zres = iqinf.zres;
info.nbit = iqinf.nbit;
info.norm = 2^info.nbit-1;
