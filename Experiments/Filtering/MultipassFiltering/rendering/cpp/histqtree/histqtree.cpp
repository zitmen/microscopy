/*
   MEX FUNCTION
 
   Generates a Quad-tree based histogram

   Syntax:
 
   histqtree(points, roi)
   histqtree(points, roi, imsize)
   histqtree(points, roi, imsize, maxevents)

   [H, maxdepth, numpts] = histqtree(...)

   where

   points      ... [m x 2] single matrix of points
   roi         ... [x, y, w, h] is origin and width and height of the window
   imsize      ... [p, q] output histogram size (default [32, 32])
   maxevents   ... maximum number of events in leaf nodes (default 10)
   H           ... [p x q] single matrix representing quad-tree histogram
   maxdepth    ... maxim tree depth
   numpts      ... number of points in the histogram
   
   
   25.5.2010, PK, histqtree(points, roi, imsize, maxevents)
   14.5.2010, PK, histqtree(points, roi, maxevents, renderdepth)
*/

#include <math.h>
#include "mex.h"
#include "quadtree.h"

static double IMSIZE_DEFAULT[2] = {16.0, 16.0};

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	QuadTree *qt;
	float *ptx, *pty;						// data pointers
	double *roi = NULL, *imsize = NULL;		// roi and output image size
	int maxevents = QT_MAX_EVENTS_DEFAULT;	// max. number of events in leaf nodes
	int i, numpts;
	int renderdepth, size;
	double scale[2]; // w,h - how to fit histogram units to pixels
	

	// check number of I/O arguments
	if ( (nrhs < 2) || (nrhs > 4) )
		mexErrMsgTxt("Wrong number of input arguments.");
	if ( (nlhs < 1) || (nlhs > 3) )
		mexErrMsgTxt("Wrong number of output arguments.");

	// get pointers to data
	if ( mxIsEmpty(prhs[0]) || !mxIsSingle(prhs[0]) )
		mexErrMsgTxt("Points must be stored in a single matrix.");
	if ( mxGetN(prhs[0]) != 2 )
		mexErrMsgTxt("Points must be 2D data.");
	numpts = mxGetM(prhs[0]);
	ptx = (float *) mxGetData(prhs[0]);
	pty = ptx + numpts;

	// get ROI
	if ( !mxIsDouble(prhs[1]) || (mxGetNumberOfElements(prhs[1]) != 4) )
		mexErrMsgTxt("ROI must be a vector [x, y, w, h].");
	roi = mxGetPr(prhs[1]);	
	if ( (roi[2] <= 0) || (roi[3] <=0 ) )
		mexErrMsgTxt("Not a valid ROI.");

	// get output image size
	if (nrhs > 2) {
		if ( !mxIsDouble(prhs[2]) || (mxGetNumberOfElements(prhs[2]) != 2) )
			mexErrMsgTxt("imsize must be a vector [w, h].");
		imsize = mxGetPr(prhs[2]);
		if ( (imsize[0] <= 0) || (imsize[1] <=0 ) )
			mexErrMsgTxt("Not a valid imsize.");
	}
	else
		imsize = IMSIZE_DEFAULT;

	// get maximum number of events
	if (nrhs > 3) {
		if ( !mxIsNumeric(prhs[3]) || (mxGetNumberOfElements(prhs[3]) != 1) )
			mexErrMsgTxt("Maximum number of events in leaf nodes must be a scalar.");
		if ( (maxevents = (int) mxGetScalar(prhs[3])) < 1 )
			mexErrMsgTxt("Maximal number of events must be a positive number.");
	}


	// rendering depth based on the output image size
	renderdepth = (int) ceil(log(Max(imsize[0],imsize[1]))/log(2.0));	
	size = 1 << renderdepth;  // size = 2^renderdepth

	scale[0] = imsize[0]/roi[2];
	scale[1] = imsize[1]/roi[3];


	// create a quad-tree -- QuadTree(A, C, maxevents), where A and C are corners of ROI
	qt = new QuadTree(point((float) roi[0],(float) roi[1]), point((float) (roi[0]+size/scale[0]),(float) (roi[1]+size/scale[1])), maxevents);
	qt->SetROI(point((float) roi[0],(float) roi[1]), point((float) (roi[0]+roi[2]),(float) (roi[1]+roi[3])));

	// and fill it with events
	for (i = 0; i < numpts; i++)
		qt->Add(point(*(ptx++),*(pty++)));

	// render the final result
	plhs[0] = mxCreateNumericMatrix(size, size, mxSINGLE_CLASS, mxREAL);
	qt->Render(matrix((float *) mxGetData(plhs[0]), size, size, size), renderdepth);

	// nepujde tak jednoduse, RenderNode: size = subimage.W() >> 1
	//plhs[0] = mxCreateNumericMatrix((int) imsize[0], (int) imsize[1], mxSINGLE_CLASS, mxREAL);
	//qt->Render(matrix((float *) mxGetData(plhs[0]), (int) imsize[0], (int) imsize[1], (int) imsize[0]), renderdepth);

	
	//set output size - must be done in MATLAB
	//mxSetM(plhs[0],(int) imsize[0]); // does not work
	//mxSetN(plhs[0],(int) imsize[1]);


	// output tree depth
	if (nlhs > 1)
		plhs[1] = mxCreateDoubleScalar((double) qt->Depth());

	// output number of 
	if (nlhs > 2)
		plhs[2] = mxCreateDoubleScalar((double) qt->Num());

	delete qt;
}