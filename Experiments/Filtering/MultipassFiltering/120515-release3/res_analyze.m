function res_analyze(fileorresults, config)
% Makes an analysis of fitted data

% 7.10.2010, PK, result is saved with the source data

% load configuration
if nargin < 2,
  cfg = cfg_v2();
else
  if isa(config,'function_handle')
    cfg = config();
  else
    cfg = config;
  end
end;

FONTSIZE = 12;

fprintf('\nAnalyzing ...\n');
% load data
if ischar(fileorresults)
  results = res_load(fileorresults, cfg);  
  fprintf('File:                  %s\n', results.filename);
  fprintf('Date:                  %s\n', results.date);
  fprintf('Description:           %s\n', results.description);
  [pathstr, fname, ext] = fileparts(fileorresults);
  pathstr = [pathstr '/'];
else
  results = fileorresults;
  fname = results.filename;
  %pathstr = results.cfg.respath;
  pathstr = strrep(cfg.respath, '$data$', results.datapath(1:end-1));
end
% where to save results
fname = [pathstr filesep fname];

if isfield(results,'numimgs') && ~isempty(results.numimgs),  fprintf('# analyzed frames:     %d\n', results.numimgs); end
if ~isempty(results.numaccept), fprintf('# fitted molecules:    %d\n', results.numaccept); end
if ~isempty(results.numdiscard), fprintf('# discarded molecules: %d\n', results.numdiscard); end

%%
% rendering rules & decode results
results.cfg.vrtx = cfg.vrtx;
results.cfg.sigmapsf = cfg.sigmapsf;
[xpos,ypos,sigma,N,b,SNR,chi2,dx,s,a,index]= render_rules(results, cfg.analize);

fits = results.fits; 

h=figure(1); clf
xedge = linspace(0,5,100);
xbin = xedge+diff(xedge(1:2))/2;
cnt=histc(sigma,xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('\sigma','FontSize',FONTSIZE);
ylabel('Number of molecules','FontSize',FONTSIZE)
title(['Size of located molecules (s = \sigma \times ' num2str(a) ' [nm])'],'FontSize',FONTSIZE)
set(gca,'XLim',[0 5],'FontSize',FONTSIZE);
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('\\sigma_{max} = %.2f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_sigma.png']); end

h=figure(2); clf
xedge = linspace(0,1000,100);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(N,xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('N - number of photons','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Number photons per molecule','FontSize',FONTSIZE)
set(gca,'XLim',[0 1000],'FontSize',FONTSIZE);
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('N_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_N.png']); end;

h=figure(3); clf
xedge = linspace(0,1000,100);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(fits(index,7),xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('N - number of photons','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Number photons per molecule - sum(image)','FontSize',FONTSIZE)
set(gca,'XLim',[0 1000],'FontSize',FONTSIZE);
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('N_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_Nsum.png']); end;

h=figure(4); clf
xedge = linspace(0,50,100);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(b,xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('b - number of photons','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Background intensity (std of background noise)','FontSize',FONTSIZE)
set(gca,'XLim',[0 50],'FontSize',FONTSIZE);
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('b_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_b .png']); end

h=figure(5); clf
xedge = linspace(0,70,70);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(dx,xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('\Deltax [nm]','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Localization accuracy','FontSize',FONTSIZE)
set(gca,'XLim',[0 70],'FontSize',FONTSIZE);
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('\\Deltax_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_dx.png']); end

h=figure(6); clf
xedge = linspace(0,150,100);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(SNR,xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('SNR','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('SNR','FontSize',FONTSIZE)
set(gca,'XLim',[0 150],'FontSize',FONTSIZE);
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('SNR_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_snr.png']); end

h=figure(7); clf
hold on
xedge = linspace(0,100,100);
yedge = linspace(0,5,100);
xbin = xedge+diff(xedge(1:2))/2;
ybin = yedge+diff(yedge(1:2))/2;
cnt = hist2(SNR,sigma,xedge,yedge);
imagesc(xbin,ybin,cnt); colormap jet
par = [ 31.0379  -15.6772   12.0866];   %    0% of error for sigma = 2.6 
% par = [27.2521  -12.8095   10.4444];  %    5% of error for sigma = 2.6
% par = [23.7811   -9.2450    8.5744];  %   10% of error for sigma = 2.6
sbin = linspace(0, 6,100);
plot((par(1) + par(2)*sbin + par(3)*sbin.^2), sbin,'w','LineWidth',3)
xlabel('SNR','FontSize',FONTSIZE)
ylabel('\sigma_0','FontSize',FONTSIZE)
title('SNR vs. \sigma','FontSize',FONTSIZE)
axis xy;
set(gca,'XLim',[0 100],'YLim',[0.3 2.2],'FontSize',FONTSIZE);
% [val,idx]=max(cnt);
% valmax=xbin(idx);
% text(1.5*valmax,0.95*val,sprintf('SNR_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
% line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_snr-vs-sigma.png']); end

h=figure(8); clf
chi2red = chi2/44;
xedge = linspace(0,400,100);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(chi2red,xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('\chi^2_{reduced}','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Residual fitting error','FontSize',FONTSIZE)
set(gca,'XLim',[0 400],'FontSize',FONTSIZE)
[val,idx]=max(cnt);
valmax=xbin(idx);
text(1.5*valmax,0.95*val,sprintf('\\chi^2_{max} = %.1f',valmax),'Color','r','FontSize',FONTSIZE);
line([valmax valmax],[0 val],'Color','r','Linestyle','--','LineWidth',1)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_chi2red.png']); end

h=figure(9); clf
xedge = linspace(0,30,30);
xbin = xedge+diff(xedge(1:2))/2;
cnt = histc(fits(index,11),xedge);
bar(xbin,cnt,'hist'); colormap jet
xlabel('Number of frames','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Molecule activity - how many frames in the image','FontSize',FONTSIZE) 
set(gca,'XLim',[0 30],'FontSize',FONTSIZE)
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_framenum.png']); end

h=figure(10); clf
xedge = linspace(0,results.numimgs,30);
xbin = xedge'+diff(xedge(1:2))/2;
cnt = histc(fits(index,10),xedge);
hold on
bar(xbin,cnt,'hist'); colormap jet
xlabel('Frame','FontSize',FONTSIZE)
ylabel('Number of molecules','FontSize',FONTSIZE)
title('Number of detected molecules in sequence of frames','FontSize',FONTSIZE)
set(gca,'XLim',[0 results.numimgs+1],'FontSize',FONTSIZE)
% options = optimset('Display','off','LargeScale','off','LevenbergMarquardt','on','TolFun',1E-8,'TolX',1E-8);
%a = lsqnonlin(@fitexp,[1E4 1E-4 0],[],[],options,xbin',cnt');
% a = lsqnonlin(@fitexp,[cnt(1) 1/cnt(1) 0],[],[],options,xbin',cnt');
% y=a(1)*exp(-a(2)*xbin)+a(3);
% plot(xbin,y,'r--')
% text(xbin(10),0.95*max(cnt),sprintf('f(x) = %.1e * e^{-%.1e * x} + %.1e',a),'Color','r','FontSize',FONTSIZE);
if (cfg.analize.save), print(h,'-dpng','-r100',[fname '_framesq.png']); end


function F = fitexp(a,xi,yi)

yf = a(1)*exp(-a(2)*xi)+a(3);
F = yi-yf;