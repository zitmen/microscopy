function out = res_save(fits, img, description, cfg)
% Save results into a file
% 
% IN:
%  filename    ... string with filename
%  results     ... fitted data
%  imsize      ... size of the image
%  resolution  ... nm/pixel
%  description ... string with description

% 27.4.2010, PK, item names changed
% 8.2.2010, PK


pathstr = strrep(cfg.resdir, '$data$', img.datadir);%(1:end-1));

if ~isdir(pathstr)
  mkdir(pathstr);
end

out.date = datestr(now);
out.description = description;
out.filename = img.filemask;
out.datapath = img.datadir;

out.imsize = img.siz;
out.resolution = img.res;
out.sigmapsf = cfg.sigmapsf;
out.PreAmpGain = img.gain(1);
out.EMGain = img.gain(2);
out.convert = img.convert;

out.numimgs = img.numframes(end) - cfg.framestart;
out.numaccept = fits.num;
out.numdiscard = fits.discard;

out.fits = resupdate('save');

out.cfg = cfg;

save([pathstr img.filemask '.mat'],'-v7','-struct','out');
