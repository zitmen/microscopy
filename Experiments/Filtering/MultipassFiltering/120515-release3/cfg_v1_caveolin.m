function cfg = cfg_v1_caveolin
% ----------------------------------------------------------------------------
% Config file for Matlab release 1
% caveolin
% ----------------------------------------------------------------------------
%
cfg.ver = '3';

cfg.setup = 0;

cfg.framestart = 3700;
cfg.numframes = Inf;                 % maximal number of frames to process, (Inf = all)
cfg.thr = 0.11;

% where to save results
cfg.datadir = '../caveolin/';
cfg.filemask = '2H3 caveolin-1 serum blink +cyl lens 1'; 
cfg.resdir = '$data$/results_ver3-detect8-box9x9/';

% ----------------------------------------------------------------------------
% camera
cfg.resolution = 80;                 % camera resolution [nm/pixel]
cfg.sigmapsf = 1.33;%1.36;
cfg.bitdepth = 14;
cfg.ADconvert = [1, 3.6; 1.9, 2; 3.7, 0.9];  % Conversion from Pre-Amp-Gain to CCD Semsitivity
cfg.vrtx=[77 383; 12 239; 76 102; 204 47; 297 53; 354 81; 403 166; 425 257; 364 357; 237 448];
% cfg.vrtx= [200 110; 200 250; 340 250; 340 110];

% ----------------------------------------------------------------------------
% molecule detection
cfg.displayfirst = cfg.setup;               % display localized molecules in first # frames
cfg.displayevery = 2000;              % display localized molecules every # frames
%cfg.detblure = {'gauss', {11,1.3}};  % blure image for detection, window size, approximate PSF size
cfg.detblure = {'gauss', {19,2.6}};
cfg.mindist = 7;                     % min. distance between located molecules [pixels]
               
% ----------------------------------------------------------------------------
% fitting
cfg.maxtimeout = 1; %2               % maximal timeout for molecule disappearance [frames]
cfg.box = 3;                         % size of the box (# times 2 + 1) [pixels]

% ----------------------------------------------------------------------------
% analyze
cfg.analize.rule = 20;               % rule
cfg.analize.snrgain = 1.3;%1.8;
cfg.analize.save = 1;               % save results [0|1]

% ----------------------------------------------------------------------------
% drift compensation
cfg.drift.method = 'linear'; 
cfg.drift.params = [0.02 -0.15 0]; % [vx vy angle];

% ----------------------------------------------------------------------------
% rendering
%  method  ... function handle to rendering method
%  scale   ... magnification of original resolution
%  average ... >=1; use adaptive jitter and average results (only for hist and quadtree)
%  blure   ... blure the result (only for hist and quadtree); [] no blure; {{'gauss',{11,1}}} blure with specified filter
%  rules   ... function handle to rendering rules
%  rule    ... rule applied (0 = all located points)
%  save    ... save result 0 or 1 for on and off, if string than use it as appendix for output file 'rule1_scale5_average01_blure0'

% cfg.render = struct('method', @render_scatter, 'rules',  0, 'save', 1);
% cfg.render = struct('method', @render_gauss, 'scale', 5, 'rule', 1, 'save', 1);
cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 100, 'rule', 0, 'save', 'av100','snrgain',1);
%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 100, 'rule', 6, 'save', 'av100_mag5x_rule6');
% cfg.render = struct('method', @render_quadtree, 'scale', 5, 'average', 100, 'maxevents', 10,  'rule', 1, 'save', 'av100'); 

%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 100, 'rule', 24, 'snrgain',1.3, 'save','_av100_rule44_miss');
%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 500, 'rule', 0, 'snrgain', 1.3, 'save','_av500_r0' );
%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 10, 'rule', 24, 'snrgain',1.8, 'save',0);%'_av100_rule43_fp');
