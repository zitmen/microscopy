function [im,params] = gendotsstatic(cfg)
% Generate image of monte-carlo dots

% create regular grid for positioning dots 
[xpos,ypos] = meshgrid(16:16:cfg.imsize(2)-16,16:16:cfg.imsize(1)-16);
xpos = xpos(:);
ypos = ypos(:);
npts = numel(xpos);

% select random subset of dots
idx = randperm(npts)';
ndots = cfg.numdots;
idx = sort(idx(1:ndots));
params = zeros(ndots,4,'single'); 

% generate dots with random parameters [x0, y0, sigma, amplitude]
r = 4*sqrt(rand(ndots,1));
fi = 2*pi*rand(ndots,1);
params(:,1) = ypos(idx) + r.*sin(fi);        % y0
params(:,2) = xpos(idx) + r.*cos(fi);        % x0
%params(:,3) = eval(cfg.distribution);        % sigma
params(:,3) = cfg.mcsigma(1)+diff(cfg.mcsigma)*rand(ndots,1);
snr = cfg.mcsnr(1)+diff(cfg.mcsnr)*rand(ndots,1);
params(:,4) = snr * cfg.snr;        % amplitude based on SNR

% create rendering coordinates
radius = 20;
[x,y] = meshgrid(-radius:radius,-radius:radius);
idx = x.^2 + y.^2 < radius^2;
x = x(idx); y = y(idx);
  
% create image of dots
im = zeros(cfg.imsize,'single');
for I = 1:ndots

  % rendering parameters
  u = floor(params(I,1));
  v = floor(params(I,2));
  du = params(I,1) - floor(params(I,1));
  dv = params(I,2) - floor(params(I,2));
  sigma = params(I,3);
  N = params(I,4);

  % take coordinates only inside the image
  idx = ~(v+x < 1 | v+x > cfg.imsize(2) | u+y < 1 | u+y > cfg.imsize(1));  
  
  % render  
  z = N / (2*pi*sigma^2) * exp(-0.5*((x(idx)-dv).^2 + (y(idx)-du).^2)/sigma^2);
  idx = sub2ind(cfg.imsize,u+y(idx),v+x(idx));  
  im(idx) = im(idx) + z;
    
end
