function cfg = cfg_mcstatic()

cfg = cfg_v2();

% data and results
cfg.datapath = 'data/snr1-100sigma0.3-2.2/';
cfg.respath = 'res/snr1-100sigma0.3-2.2/';

% monte-carlo parameters
cfg.snr = 142/65535;      % 1.7 photons - std of the background noise currently used
cfg.imsize = [512 512];   % image size
cfg.numdots = 500;        % number of dots in each image
cfg.numimgs = 3000;       % number of monte-carlo images
cfg.mcsigma = [0.3 2.2];
cfg.mcsnr = [1 100];

cfg.box = 5;
cfg.maxtimeout = 0;

% noise removal method
% Winner: 49 -> GAUSS_11_1.3_SHIFTED+MED_03 (PRECISION=82%,OK=67.6%,FP=16.4%,FN=27.8%,SENSITIVITY=71.4%,ERROR=32.4%)
%            -> F1-score = (2*precision*sensitivity)/(precision+sensitivity) = 0.763337679 (3rd)
cfg.methodrun  = 49;    % multipass filtering range = [40:55]
cfg.method(1) = struct('name','NONOISE','exec','imb = img.imraw;');
cfg.method(2) = struct('name','AV_02','exec','h=fspecial(''average'', 2);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(3) = struct('name','AV_03','exec','h=fspecial(''average'', 3);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(4) = struct('name','AV_04','exec','h=fspecial(''average'', 4);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(5) = struct('name','AV_05','exec','h=fspecial(''average'', 5);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(6)  = struct('name','AV_07','exec','h=fspecial(''average'', 7);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(7)  = struct('name','AV_11','exec','h=fspecial(''average'',11);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(8)  = struct('name','AV_15','exec','h=fspecial(''average'',15);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(9) = struct('name','MED_02','exec','imb = medfilt2(img.imraw,[2 2]);');
cfg.method(10) = struct('name','MED_03','exec','imb = medfilt2(img.imraw,[3 3]);');
cfg.method(11) = struct('name','MED_04','exec','imb = medfilt2(img.imraw,[4 4]);');
cfg.method(12) = struct('name','MED_05','exec','imb = medfilt2(img.imraw,[5 5]);');
cfg.method(13)  = struct('name','MED_07','exec','imb = medfilt2(img.imraw,[7 7]);');
cfg.method(14)  = struct('name','MED_11','exec','imb = medfilt2(img.imraw,[11 11]);');
cfg.method(15)  = struct('name','MED_15','exec','imb = medfilt2(img.imraw,[15 15]);');
cfg.method(16)  = struct('name','GAUSS_07_0.7','exec','h=fspecial(''gauss'', 7,0.7);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(17)  = struct('name','GAUSS_11_1.3','exec','h=fspecial(''gauss'',11,1.3);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(18)  = struct('name','GAUSS_15_2.0','exec','h=fspecial(''gauss'',15,2.0);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(19) = struct('name','GAUSS_19_2.6','exec','h=fspecial(''gauss'',19,2.6);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(20) = struct('name','GAUSS_24_3.5','exec','h=fspecial(''gauss'',24,3.5);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(21) = struct('name','WIENER_03','exec','imb = wiener2(img.imraw,[3 3]);');
cfg.method(22) = struct('name','WIENER_05','exec','imb = wiener2(img.imraw,[5 5]);');
cfg.method(23) = struct('name','WIENER_07','exec','imb = wiener2(img.imraw,[7 7]);');
cfg.method(24) = struct('name','WIENER_11','exec','imb = wiener2(img.imraw,[11 11]);');
cfg.method(25) = struct('name','WIENER_15','exec','imb = wiener2(img.imraw,[15 15]);');
cfg.method(26) = struct('name','ERODE_03','exec','se = strel(''disk'',1); imb = imerode(img.imraw,se);');
cfg.method(27) = struct('name','ERODE_05','exec','se = strel(''disk'',2); imb = imerode(img.imraw,se);');
cfg.method(28) = struct('name','ERODE_07','exec','se = strel(''disk'',3); imb = imerode(img.imraw,se);');
cfg.method(29) = struct('name','BOXSTD_03','exec','[foo,imb] = boxmustd(img.imraw,3,1);');
cfg.method(30) = struct('name','BOXSTD_04','exec','[foo,imb] = boxmustd(img.imraw,4,1);');
cfg.method(31) = struct('name','BOXSTD_05','exec','[foo,imb] = boxmustd(img.imraw,5,1);');
cfg.method(32) = struct('name','BOXSTD_06','exec','[foo,imb] = boxmustd(img.imraw,6,1);');
cfg.method(33) = struct('name','BOXSTD_07','exec','[foo,imb] = boxmustd(img.imraw,7,1);');
cfg.method(34) = struct('name','BOXSTD_08','exec','[foo,imb] = boxmustd(img.imraw,8,1);');
cfg.method(35)  = struct('name','GAUSS_05_0.4','exec','h=fspecial(''gauss'', 5,0.4);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(36)  = struct('name','GAUSS_09_1.0','exec','h=fspecial(''gauss'', 9,1);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(37)  = struct('name','GAUSS_13_1.6','exec','h=fspecial(''gauss'', 13,1.6);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(38)  = struct('name','GAUSS_17_2.3','exec','h=fspecial(''gauss'', 17,2.3);imb=imfilter(img.imraw,h,''replicate'',''same'');');
cfg.method(39) = struct('name','GAUSS_21_3.0','exec','h=fspecial(''gauss'',21,3.0);imb=imfilter(img.imraw,h,''replicate'',''same'');');
%% Multipass filtering
% GAUSS+MED
cfg.method(40)  = struct('name','GAUSS_07_0.7+MED_03','exec','h=fspecial(''gauss'', 7,0.7);imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[3 3]);');
cfg.method(41)  = struct('name','GAUSS_11_1.3+MED_03','exec','h=fspecial(''gauss'',11,1.3);imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[3 3]);');
cfg.method(42)  = struct('name','GAUSS_07_0.7+MED_05','exec','h=fspecial(''gauss'', 7,0.7);imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[5 5]);');
cfg.method(43)  = struct('name','GAUSS_11_1.3+MED_05','exec','h=fspecial(''gauss'',11,1.3);imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[5 5]);');
% MED+GAUSS
cfg.method(44)  = struct('name','MED_03+GAUSS_07_0.7','exec','h=fspecial(''gauss'', 7,0.7);imb = medfilt2(img.imraw,[3 3]);imb=imfilter(imb,h,''replicate'',''same'');');
cfg.method(45)  = struct('name','MED_03+GAUSS_11_1.3','exec','h=fspecial(''gauss'',11,1.3);imb = medfilt2(img.imraw,[3 3]);imb=imfilter(imb,h,''replicate'',''same'');');
cfg.method(46)  = struct('name','MED_05+GAUSS_07_0.7','exec','h=fspecial(''gauss'', 7,0.7);imb = medfilt2(img.imraw,[5 5]);imb=imfilter(imb,h,''replicate'',''same'');');
cfg.method(47)  = struct('name','MED_05+GAUSS_11_1.3','exec','h=fspecial(''gauss'',11,1.3);imb = medfilt2(img.imraw,[5 5]);imb=imfilter(imb,h,''replicate'',''same'');');
% SHIFTED_GAUSS+MED
cfg.method(48)  = struct('name','GAUSS_07_0.7_SHIFTED+MED_03','exec','h=fspecial(''gauss'', 7,0.7);h=h-mean(h(:));imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[3 3]);');
cfg.method(49)  = struct('name','GAUSS_11_1.3_SHIFTED+MED_03','exec','h=fspecial(''gauss'',11,1.3);h=h-mean(h(:));imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[3 3]);');
cfg.method(50)  = struct('name','GAUSS_07_0.7_SHIFTED+MED_05','exec','h=fspecial(''gauss'', 7,0.7);h=h-mean(h(:));imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[5 5]);');
cfg.method(51)  = struct('name','GAUSS_11_1.3_SHIFTED+MED_05','exec','h=fspecial(''gauss'',11,1.3);h=h-mean(h(:));imb=imfilter(img.imraw,h,''replicate'',''same'');imb = medfilt2(imb,[5 5]);');
% MED+SHIFTED_GAUSS
cfg.method(52)  = struct('name','MED_03+GAUSS_07_0.7_SHIFTED','exec','h=fspecial(''gauss'', 7,0.7);h=h-mean(h(:));imb = medfilt2(img.imraw,[3 3]);imb=imfilter(imb,h,''replicate'',''same'');');
cfg.method(53)  = struct('name','MED_03+GAUSS_11_1.3_SHIFTED','exec','h=fspecial(''gauss'',11,1.3);h=h-mean(h(:));imb = medfilt2(img.imraw,[3 3]);imb=imfilter(imb,h,''replicate'',''same'');');
cfg.method(54)  = struct('name','MED_05+GAUSS_07_0.7_SHIFTED','exec','h=fspecial(''gauss'', 7,0.7);h=h-mean(h(:));imb = medfilt2(img.imraw,[5 5]);imb=imfilter(imb,h,''replicate'',''same'');');
cfg.method(55)  = struct('name','MED_05+GAUSS_11_1.3_SHIFTED','exec','h=fspecial(''gauss'',11,1.3);h=h-mean(h(:));imb = medfilt2(img.imraw,[5 5]);imb=imfilter(imb,h,''replicate'',''same'');');
