function loc = molecule_detect8(img, loc, cfg)
% candidate pixels for a molecule

% init
if isempty(loc)
  loc.loc = [];
end

% remove noise (blure image)
h = fspecial(cfg.detblure{1},cfg.detblure{2}{:});
imb = imfilter(img.imraw, h, 'same', 'replicate');

% find local maxima above threshold + min. distance + sort
[val,idx] = locmax2d(imb);


% take only maxima above a threshold
[boxmu,boxstd] = boxmustd(img.imraw,6,1);
% idx = idx(cfg.thr(1)*boxmu(idx) < val);
% idx = idx(boxstd(idx) > cfg.thr(2));
%idx = idx(imb(idx)./(boxstd(idx)+1E-6) > 3.7);
idx = idx(boxstd(idx) > cfg.thr);

% take only maxima not too close to each other
idx = circfilter(idx, img.siz, cfg.mindist);
[u,v] = ind2sub(img.siz,idx);
loc.loc = [u, v, imb(idx)];

if cfg.setup || (img.frame <= cfg.displayfirst) || (mod(img.frame,cfg.displayevery) == 0)
  h=figure(12); clf
  hold on
  xedge = linspace(0,1,512)';
  xbin = xedge + diff(xedge(1:2))/2;
  cnt = histc(img.imraw(:),xedge);
  val = qntl(cnt,0.999,xbin);
  imagesc(img.imraw,[0 val]);%0.8*max(boxmu(:))]);%10*loc.thr.mu]);%0.95*max(img.im(:))])
  colormap gray(256)
  axis ij image off;
  set(gca,'Position',[0 0 1 1])
   plot(v,u,'ro','MarkerSize',20) 
  set(h,'Name', sprintf('Frame %d/%d', img.frame, img.numframes(end)));
  drawnow;
% text(10,10,sprintf('thr=%.3e\nframe: %4d',loc.thr.mu,img.frame),'Color','y','VerticalAlignment','top')
end

%   h=figure(2); clf
%   hold on
%   imagesc(boxmu);%10*loc.thr.mu]);%0.95*max(img.im(:))])
%   colormap gray
%   axis ij image off;
%   set(gca,'Position',[0 0 1 1])
% %    plot(v,u,'ro','MarkerSize',20) 
% 
%   h=figure(3); clf
%   hold on
%   imagesc(boxstd);%10*loc.thr.mu]);%0.95*max(img.im(:))])
%   colormap gray
%   axis ij image off;
%   set(gca,'Position',[0 0 1 1])
% %    plot(v,u,'ro','MarkerSize',20) 
% 
%   h=figure(4); clf
%   hold on
%   imagesc(imb);%10*loc.thr.mu]);%0.95*max(img.im(:))])
%   colormap gray
%   axis ij image off;
%   set(gca,'Position',[0 0 1 1])
% %    plot(v,u,'ro','MarkerSize',20) 

%%


function idx = circfilter(idx,imsize,mindist)

d = size(mindist,2);
if d == 1
  mindist(2) = mindist(1);
elseif d~= 2
  error('localmax2d: Vector mindist has to be 2D.');
end

% invalidate all points in neighbourhood of [v(I),u(I)]
[u,v] = ind2sub(imsize,idx);
valid = true(length(v),1);
for I = 1:length(v)
  if ~valid(I), continue; end;
  ind = ((v(I)-v)/mindist(2)).^2 + ((u(I)-u)/mindist(1)).^2 > 1;
  ind(I) = true;
  valid = valid & ind;
end

% take only valid points
idx = idx(valid);  
