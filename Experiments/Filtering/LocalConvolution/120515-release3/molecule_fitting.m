function [fits, imbuf] = molecule_fitting(img, imbuf, fits, cfg)

% 12.3.2010 removed threshold for noise level - it is not necessary here
%  9.3.2010 moved to utils

persistent X;

if isempty(fits)
  resupdate('free');
  fits = struct('num', 0, 'discard', 0); X = [];
  [X(:,:,2),X(:,:,1)] = meshgrid(-cfg.box:cfg.box,-cfg.box:cfg.box);
end

npts = (2*cfg.box+1)^2;

for I = 1:length(imbuf)
       
  im = imbuf(I).im;

  % fit 2D gaussian bell shaped function
  a0 = [ 0,  0, cfg.sigmapsf, max(im(:))-min(im(:)), min(im(:))];
  %y=X(:,:,1);x=X(:,:,2);
  %[a,chi2,exitflag] = lmfit2DgaussXYSAO(a0, [y(:) x(:)]', im(:)');
  [a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);
  %[a,exitflag,chi2] = lmfit('fun_gauss2D',a0, reshape(X(:),[npts, 2]), im(:));
    
  % fitting error
  if exitflag < 1 || exitflag > 3
    fits.discard = fits.discard + 1;
    continue;
  end
  
  % estimate background at stored position
  imestim = gauss2d(X,a);
  Nim = sum(im(:) - a(5));  % Nimestim = sum(imestim(:) - a(5)); % is the same
  
  bkgestim = im - imestim;
  bkgmu = sum(bkgestim(:))/npts;
  bkgstd = sqrt(sum((bkgestim(:)-bkgmu).^2)/(npts-1));
  
  % position
  a(1:2) = a(1:2)+ imbuf(I).pos;
  a(4) = a(4) * 2*pi*a(3)^2;
  
  % save results
  fits.num = resupdate('add', single([a, sum(chi2), Nim, bkgmu, bkgstd, imbuf(I).frame]));
  
end


function y = gauss2d(X,a)

y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);

