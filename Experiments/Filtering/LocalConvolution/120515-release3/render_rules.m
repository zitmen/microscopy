function [xpos,ypos,sigma,N,b,SNR,chi2,dx,s,a,idx] = render_rules(results, cfg)

%  xpos     ... position x
%  ypos     ... position y
%  a        ... resolution
%  sigma    ... size
%  s        ... resolution * size
%  N        ... number of photons (fitted amplitude)
%  b        ... background level (fitted offset)
%  dx       ... localization accuracy
%  chi2     ... chi
%  idx

% 3.12.2010, PK, v2.4a

fits = results.fits;
sigmapsf = results.cfg.sigmapsf;
a = results.resolution(1);

if isfield(results.cfg, 'vrtx') && ~isempty(results.cfg.vrtx)
  vrtx = results.cfg.vrtx;
  IN = inpolygon(fits(:,2),fits(:,1),vrtx(:,1),vrtx(:,2));
  fits = fits(IN,:);
end

%[y0, x0, s0, N0, z0, chi2, Nim, bkgmu, bkgstd, frame, numframes]

% calculate parameters
xpos = fits(:,2);
ypos = fits(:,1);
sigma = fits(:,3);
N = fits(:,4);
b = fits(:,9);
chi2 = fits(:,6);
s = a * sigma;
% dx = sqrt((s.^2 + a^2/12) ./ N + 4*sqrt(pi)* s.^3 .* (b.^2) ./ (a .* N.^2));
dx = sqrt((s.^2 + a^2/12) ./ N + 8*pi * s.^4 .* (b.^2) ./ (a.^2 .* N.^2));
SNR = cfg.snrgain * N ./ b;


par = [ 31.0379  -15.6772   12.0866];   %    0% of error for sigma = 2.6 
% par = [27.2521  -12.8095   10.4444];  %    5% of error for sigma = 2.6
% par = [23.7811   -9.2450    8.5744];  %   10% of error for sigma = 2.6

thr = par(1) + par(2) * (0.95*sigmapsf) + par(3) * (0.95*sigmapsf).^2;
fprintf('sigmapsf = %.2f, thr = %.1f\n',sigmapsf,thr);
idxthr = SNR > thr;%28;%31; %ACTIN11%  idxthr = SNR > 29;%26; %29 %PALM17
idxopt = par(1) + par(2) * sigma + par(3) * sigma.^2 < SNR;
idxsig1 = sigma > 0.7*sigmapsf;
idxsig2 = sigma > 0.7*sigmapsf & sigma < 1.3*sigmapsf;
idxtmp  = b < 50 & dx > 5 & dx < 100;% & N < 1000;

thrmin = par(1) + par(2) * (0.7*sigmapsf) + par(3) * (0.7*sigmapsf).^2;
thrmax = par(1) + par(2) * (1.3*sigmapsf) + par(3) * (1.3*sigmapsf).^2;
idxallFP   = SNR > thrmin & ~idxopt;
idxallMISS = (SNR < thrmax | ~idxsig2 ) & idxopt ;

% what to show
switch cfg.rule
  case 0
    idx = true(length(xpos),1);

  case 1
    %idx = SNR > 2 & chi2/49 > 4.9;
    idx = chi2/49 > 4.9;
   
  case 4
    idx = idxtmp;
    
  case 5
    idx = idxallFP;
    
  case 6
    idx = idxallMISS;
    
  % tratitional threshold
  case 10
    idx = idxthr;

  case 11  
    idx = idxthr & idxsig1;
    
  case 12  
    idx = idxthr & idxsig2;

  case 13  
    idx = idxthr & idxsig2 & idxtmp;
    
  case 14
    idx = idxthr & idxtmp;
    

  % optimal threshold
  case 20
    idx = idxopt;

  case 21
    idx = idxopt & idxsig1;
    
  case 22 
    idx = idxopt & idxsig2;

  case 23 
    idx = idxopt & idxsig2 & idxtmp;
    
  case 24
    idx = idxopt & idxtmp;  
   
      
  % MISSING - molecules in optimal threshold but missing in traditional threshold
  case 31
    idx1 = idxthr & idxsig1;
    idx2 = idxopt & idxsig1;
    idx = xor(idx1,idx2) & idx2;    

  case 32
    idx1 = idxthr & idxsig2;
    idx2 = idxopt & idxsig2;
    idx = xor(idx1,idx2) & idx2;
    
  case 33
    idx1 = idxthr & idxsig2 & idxtmp;
    idx2 = idxopt & idxsig2 & idxtmp;
    idx = xor(idx1, idx2) & idx2;

  case 34
    idx1 = idxthr & idxsig2 & idxtmp;
    idx2 = idxopt & idxtmp;
    idx = xor(idx1, idx2) & idx2;
    
    
  % FP - false positives molecules in traditional threshold compared to optimal threshold
  case 41
    idx1 = idxthr & idxsig1;
    idx2 = idxopt & idxsig1;
    idx = xor(idx1,idx2) & idx1;
        
  case 42
    idx1 = idxthr & idxsig2;
    idx2 = idxopt & idxsig2;
    idx = xor(idx1,idx2) & idx1;

  case 43
    idx1 = idxthr & idxsig2 & idxtmp;
    idx2 = idxopt & idxsig2 & idxtmp;
    idx = xor(idx1, idx2) & idx1;  
    
  case 44
    idx1 = idxthr & idxsig2 & idxtmp;
    idx2 = idxopt & idxtmp;
    idx = xor(idx1, idx2) & idx1;  

    
    
  otherwise
    error('No rendering rule');
end

fprintf('  %d molecules out of %d\n',sum(idx),length(xpos));

xpos = xpos(idx);
ypos = ypos(idx);
sigma = sigma(idx);
N = N(idx);
b = b(idx);
chi2 = chi2(idx);
dx = dx(idx);
s = s(idx);
SNR = SNR(idx);
