function res = render_scatter(results, cfg)

% 7.10.2010, PK, saving moved to res_render
% 27.4.2010, PK, in: results, cfg; out: image handle; print & save results

imsize = results.imsize;

% rendering rules & decode results
[xpos,ypos,a,sigma,s,N,b,dx,chi2] = render_rules(results, cfg.render);

% show image
k = a/1000;
plot(xpos*k,ypos*k,'.','MarkerSize',1);
set(gca,'XLim',[0 imsize(2)*k],'YLim',[0 imsize(1)*k]);
xlabel('[\mum]')
ylabel('[\mum]')

res.name = 'scatter';
res.im = [];
