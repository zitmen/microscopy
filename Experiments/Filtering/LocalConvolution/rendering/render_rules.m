function [xpos,ypos,a,sigma,s,N,b,dx,chi2,idx] = render_rules(results, cfg)

%  xpos     ... position x
%  ypos     ... position y
%  a        ... resolution
%  sigma    ... size
%  s        ... resolution * size
%  N        ... number of photons (fitted amplitude)
%  b        ... background level (fitted offset)
%  dx       ... localization accuracy
%  chi2     ... chi
%  idx

% 28.4.2010, PK, in: results, cfg; out: decoded and filtered parameters

fits = results.fits;
sigmapsf = results.cfg.psfsigma;

% if isfield(results.cfg, 'vrtx') && ~isempty(results.cfg.vrtx)
%   vrtx = results.cfg.vrtx;
%   IN = inpolygon(fits(:,2),fits(:,1),vrtx(:,1),vrtx(:,2));
%   fits = fits(IN,:);
% end

%[ypos, xpos, N, offset, sy ,sx chi2, Nim, bkgmu, bkgstd, frame, numframes]

% calculate parameters
a = results.resolution;
xpos = fits(:,2);
ypos = fits(:,1);
sigma = sqrt(fits(:,5)*fits(:,6));
s = a * sigma;
N = fits(:,3);
chi2 = fits(:,7);
b = fits(:,10);
%b = noisesigma;
%dx = sqrt((s.^2 + a^2/12) ./ N + 4*sqrt(pi)* s.^3 .* (b.^2) ./ (a .* N.^2));
dx = sqrt((s.^2 + (a^2)/12) ./ N + 8*pi * s.^4 .* (b.^2) ./ (a^2 * N.^2));


SNR = cfg.snrgain * N ./ b;
%SNR = cfg.snrgain*N / noisesigma;


% what to show
switch cfg.rule
  case 0
    idx = true(length(xpos),1);

  case 1
    idx = SNR > 2 & chi2/49 > 4.9;
   
  case 4
    idx = idxtmp;
    
    
  % tratitional threshold
  case 10
    idx = idxthr;

  case 11  
    idx = idxthr & idxsig1;
    
  case 12  
    idx = idxthr & idxsig2;

  case 13  
    idx = idxthr & idxsig2 & idxtmp;
    
  case 14
    idx = idxthr & idxtmp;
       
    
  otherwise
    error('No rendering rule');
end

fprintf('  %d molecules out of %d\n',sum(idx),length(xpos));

xpos = xpos(idx);
ypos = ypos(idx);
sigma = sigma(idx);
s = s(idx);
N = N(idx);
if length(b) > 1,  b = b(idx);end;
dx = dx(idx);
chi2 = chi2(idx);
