
//	Templated single-linked list of objects

//	Struct library
//	Radek Svoboda, March 2000, Neovision Ltd, Prague

#ifndef __SLIST_H
#define __SLIST_H

#define ASSERT(x) (void(0))
/* ------------------------------------------------------------------------- */

// templated single-linked list

template <class Type>
class SList

{
	struct ListItem 
	{
		Type			val;		// list item value
		ListItem	*	next;		// pointer to next item (or NULL)

		// until the MSVC compiler gets matured, use only standard allocator
//		USE_STD_ALLOCATOR(Type,Alloc);

		// attach after given node
		ListItem(ListItem ** prev, const Type & val)
		: val(val), next(*prev)
		{
			*prev = this;
		}
/*
		// allocate new item
		void * operator new(size_t size)
		{
			return Alloc::Allocate(1);
		}

		// free this item
		void operator delete(void * ptr)
		{
			Alloc::Deallocate((ListItem *)ptr,1);
		}*/
	};

	ListItem		*	list;		// start of list
	size_t					num;		// number of items

public:

	// constant list node access class
	class CNode 
	{
	protected:

		ListItem	*	ptr;		// pointer to list item

		CNode(ListItem * ptr)
		: ptr(ptr)
		{
			ASSERT(ptr);
		}

		friend class SList<Type>;

	public:
		
		// default constructor
		CNode()
		: ptr(NULL)
		{}

		// access const node value
		const Type & operator *() 
		const
		{
			ASSERT(ptr);

			return ptr->val;	
		}

		// access const node item
		const Type * operator ->()
		const
		{
			ASSERT(ptr);

			return &(ptr->val);
		}

		// compare operator
		bool operator ==(const CNode & n)
		const
		{
			return n.ptr == ptr;
		}

		// compare operator
		bool operator !=(const CNode & n)
		const
		{
			return n.ptr != ptr;
		}

		// test for end
		operator bool ()
		const
		{
			return ptr != NULL;
		}

		// move to next item
		CNode & operator ++()
		{
			ASSERT(ptr);

			ptr = ptr->next;
			return *this;
		}

		// postfix increment
		CNode operator ++(int)
		{
			ASSERT(ptr);

			CNode tmp = *this;
			ptr = ptr->next;
			return tmp;
		}

		// return next node
		CNode Next()
		const
		{
			ASSERT(ptr);

			return CNode(ptr->next);
		}
	};

	// modifiable list node access class
	class Node : public CNode
	{
		// attach to list item
		Node(ListItem * ptr)
		: CNode(ptr)
		{}

		friend class SList<Type>;

		// resolve base members
		using CNode::ptr;

	public:

		// default constructor
		Node() {}

		// access modifiable node value
		Type & operator *()
		const
		{
			ASSERT(ptr);

			return ptr->val;
		}

		// access modifiable node item
		Type * operator ->()
		const
		{
			ASSERT(ptr);

			return &(ptr->val);
		}

		// compare operator
		bool operator ==(const Node & n)
		const
		{
			return n.ptr == ptr;
		}

		// compare operator
		bool operator !=(const Node & n)
		const
		{
			return n.ptr != ptr;
		}

		// move to next item
		Node & operator ++()
		{
			ASSERT(ptr);

			ptr = ptr->next;
			return *this;
		}

		// postfix increment
		Node operator ++(int)
		{
			ASSERT(ptr);

			Node tmp = *this;
			ptr = ptr->next;
			return tmp;
		}

		// return next node
		Node Next()
		const
		{
			ASSERT(ptr);

			return Node(ptr->next);
		}
	};

	// default constructor
	SList()
	: num(0), list(NULL)
	{}

	// copy constructor 
	SList(const SList<Type> & l)
	: num(0), list(NULL)
	{
		Append(l.Head(),l.Tail());
	}

	// cleanup - destroy all nodes in list
	~SList()
	{
		RemoveAll();
	}

	// assignment operator - replace with other list
	SList & operator =(const SList<Type> & l)
	{
		// prevent self-assignment

		if(&l != this)
		{
			// clear old content

 			RemoveAll();

			// append new one

			Append(l.Head(),l.Tail());
		}

		return *this;
	}

	// swap itself with the other list
	void Swap(SList<Type> & l)
	{
		Swap(list,l.list);
		Swap(num,l.num);
	}

	// get modifiable starting item of list
	Node Head()
	{
		return Node(list);
	}

	// get constant starting item of list
	CNode Head()
	const
	{
		return CNode(list);
	}

	// get ending item of list (one after last)
	Node Tail()
	{
		return Node();
	}

	// get ending item of list (one after last)
	CNode Tail()
	const
	{
		return CNode();
	}

	// get number of items
	size_t Num()
	const
	{
		return num;
	}

	// insert new node after given one 
	Node InsertAfter(Node node, const Type & val)
	{
		ASSERT(node);

		++num;
		return Node(new ListItem(&(node.ptr->next),val));
	};

	// add item before beginning of list
	Node AddHead(const Type & val)
	{
		++num;
		return Node(new ListItem(&list,val));
	}

	// add item before beginning of list			//PK
	Node AddHead(Node node)
	{
		node.ptr->next = list;
		list = node;
		num++;
		return *this;
	}


	// delete first node in list
	void DeleteHead()
	{
		ASSERT(num && list);

		ListItem * tmp = list->next;
		delete list;
		list = tmp;

		--num;
	}

	// delete one node after given one
	void DeleteAfter(Node node)
	{
		ASSERT(num && node.ptr->next);

		ListItem * tmp = node.ptr->next->next;
		delete node.ptr->next;
		node.ptr.next = tmp;

		--num;
	}

	// get previous constant node (searches through all list items!)
	// returns NULL for first item in list
	CNode Prev(CNode node)
	const
	{
		ListItem * ptr = list;

		while(ptr && ptr->next != node.ptr)
			ptr = ptr->next;

		return Node(ptr);
	}

	// get modifiable constant node (searches through all list items!)
	// returns NULL for first item in list
	Node Prev(Node node)
	const
	{
		ListItem * ptr = list;

		while(ptr && ptr->next != node.ptr)
			ptr = ptr->next;

		return Node(ptr);
	}

	// append items at the end of list
	void Append(CNode from, CNode to);

	// delete all nodes in given range
	void Erase(Node from, Node to);

	// empty complete list
	void RemoveAll()
	{
		Erase(Head(),Tail());
	}
};

/* ------------------------------------------------------------------------- */

template <class Type>
void SList<Type>::Append(typename SList<Type>::CNode from, 
						 typename SList<Type>::CNode to)

{
	// hold pointer to end

	ListItem ** ptr = &list;

	// search for end

	while(*ptr != NULL)
		ptr = &((*ptr)->next);

	// append all items

	while(from != to)
	{
		ptr = &((new ListItem(ptr,*from))->next);

		// move 

		++num;
		++from;
	}
}

/* ------------------------------------------------------------------------- */

template <class Type>
void SList<Type>::Erase(typename SList<Type>::Node from, 
						typename SList<Type>::Node to)

{
	ListItem ** ptr;

	// search for first given node

	for(ptr = &list; *ptr && *ptr != from.ptr; ptr = &((*ptr)->next));

	// delete all given nodes

	while(*ptr && *ptr != to.ptr)
	{
		ASSERT(num);

		// delete node

		ListItem * tmp = (*ptr)->next;
		delete *ptr;
		*ptr = tmp;

		--num;
	}
}

/* ------------------------------------------------------------------------- */
/*
template <class Type>
OutStream & operator <<(OutStream & os, const SList<Type> & l)

{
	// write number of items

	os << l.Num();

	// write items

	for(typename SList<Type>::CNode ptr = l.Head(); ptr; ++ptr)
		os << *ptr;

	// return stream

	return os;
}
*/
/* ------------------------------------------------------------------------- */
/*
template <class Type>
InStream & operator >>(InStream & is, SList<Type> & l)

{
	int items;

	// get number of items

	is >> items;
	ASSERT(items >= 0);

	// read items

	Type tmp;

	while(items--)
	{
		is >> tmp;
		l.AddHead(tmp);
	}

	// return stream

	return is;
}
*/
/* ------------------------------------------------------------------------- */

#endif
