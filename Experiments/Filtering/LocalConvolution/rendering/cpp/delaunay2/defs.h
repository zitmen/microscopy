
//	Global type and macro definitions

#ifndef __DEFS_H
#define __DEFS_H

#include <crtdbg.h>

/* ------------------------------------------------------------------------- */

// declare debugging function

//void DebugPrint(const char * fmt, ...);

/* ------------------------------------------------------------------------- */

// declare useful Windows-style types 
/*
typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned long		DWORD;
typedef			 int		BOOL;
*/
/* ------------------------------------------------------------------------- */

// prepare 64-bit type
/*
#ifdef _WIN32
typedef	__int64				LONGLONG;
typedef unsigned __int64	ULONGLONG;
#else
typedef long long			LONGLONG;
typedef unsigned long long  ULONGLONG;
#endif

typedef	ULONGLONG			QWORD;
*/
/* ------------------------------------------------------------------------- */

// Windows-style BOOL values

#ifndef		FALSE
#define		FALSE	0
#endif

#ifndef		TRUE
#define		TRUE	1
#endif

/* ------------------------------------------------------------------------- */

// declare NULL

#ifndef		NULL
#define		NULL	0
#endif

/* ------------------------------------------------------------------------- */

// declare ASSERT macro

#ifndef		ASSERT
#ifdef		_DEBUG
#ifdef		_WIN32
#define		ASSERT(f)	_ASSERTE(f)
#else
#define		ASSERT(f)	assert(f)
#endif
#else
#define		ASSERT(f)	((void)0)
#endif
#endif

/* ------------------------------------------------------------------------- */

// declare VERIFY macro
/*
#ifndef		VERIFY
#ifdef		_DEBUG
#define		VERIFY(f)	ASSERT(f)
#else
#define		VERIFY(f)	((void)f)
#endif
#endif
*/
/* ------------------------------------------------------------------------- */

// declare TRACE macro
/*
#ifndef		TRACE
#ifdef		_DEBUG
#define		TRACE		::DebugPrint
#else
#define		TRACE		1 ? ((void)0) : ::DebugPrint
#endif
#endif
*/
/* ------------------------------------------------------------------------- */

// get rid of stupid Microsoft macros

#undef		min
#undef		max

/* ------------------------------------------------------------------------- */

#endif
