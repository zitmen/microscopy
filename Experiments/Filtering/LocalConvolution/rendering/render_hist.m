function [IM,xbin,ybin] = render_hist(data, roi, resolution, numjitters)
% data: [npts x 3] matrix with columns [y x std] - position and localization accuracy in [nm]
% roi: region of interest [ymin ymax xmin xmax] in [nm]
% resolution: [resy resx] in [nm/pixel]
% numjitters: number of jitters

if (nargin < 4), numjitters = 1; end;
if (numjitters < 1), error('render_hist: numjitters must be >= 1'); end;


fprintf('Generating %d histogram(s)\n', numjitters);
npts = size(data,1);
shift = resolution/2;
yedge = (roi(1)-shift(1)):resolution(1):(roi(2)+shift(1));
xedge = (roi(3)-shift(2)):resolution(2):(roi(4)+shift(2));
ybin = yedge(1:end-1)+shift(1);
xbin = xedge(1:end-1)+shift(2);
% alocate memmory for reconstructed image
IM = zeros([length(ybin) length(xbin)],'single');

% generate histograms
fprintf('Progress ');
for I = 1:numjitters

  % position
  xp = data(:,2);
  yp = data(:,1);
  
  % random jitter - add normal noise to localized points
  if (numjitters > 1)
    xp = xp + data(:,3) .* randn(npts,1);
    yp = yp + data(:,3) .* randn(npts,1);
  end
 
  % accumulate histograms
  IM = IM + hist2(yp,xp,yedge,xedge);
  
  % progress bar
  if (mod(I,round(numjitters/10)) == 0), fprintf('.'); end;   
end
fprintf('\n');

% compute average
if numjitters > 1
  IM = IM/numjitters;
end

