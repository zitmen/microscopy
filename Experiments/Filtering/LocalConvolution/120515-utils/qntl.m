function y = qntl2(pdf,p,xval)
% Computes quantil from a density function
%
%  pdf  ... [1 x npts] density function
%  p    ... type of quantil
%  xval ... values on x axis
%  y    ... quantil value

% 5.10.2009, PK, x vals
% 31.7.2008, PK@Neovision

if nargin < 3, xval = 1:length(pdf); end;

% cumulative sum = distribution function
F = cumsum(pdf);

% normalize
if (F(end) == 0), y = 0; return; end
F = F/F(end);

% find point where F <= p
z = (F < p);
%idx = find(diff(z) == -1);
%if isempty(idx), y = xval(1); return; end;
idx = find(~z,1,'first')-1;
if (idx==0), y = NaN; return; end;

% interpolation
k = (F(idx+1) - F(idx)) /(xval(idx+1) - xval(idx));
q = F(idx) - k .* xval(idx);
y = (p-q) ./ k;
