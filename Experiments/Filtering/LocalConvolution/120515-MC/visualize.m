function visualize(img, results)

   figure(12); clf
   hold on
   imagesc(img.imraw,[0 0.1]);
   colormap gray(256)
   axis ij image off;
   set(gca,'Position',[0 0 1 1])
   
   % green - correct localization
   if results.cok > 0
       ok = results.ok(1:results.cok,1:2);
       plot(ok(:,2),ok(:,1),'go','MarkerSize',10)
   end
   
   % red - localized point does not exist!
   if results.cfp > 0
       fp = results.fp(1:results.cfp,1:2);
       plot(fp(:,2),fp(:,1),'ro','MarkerSize',10)
   end
   
   % yellow - point was not detected!
   if results.cfn > 0
       fn = results.fn(1:results.cfn,1:2);
       plot(fn(:,2),fn(:,1),'yo','MarkerSize',10)
   end
   
   drawnow;
