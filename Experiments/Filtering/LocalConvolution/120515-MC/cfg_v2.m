function cfg = cfg_v2


cfg.ver = '3';

cfg.setup = 0;

cfg.framestart = 1;
cfg.numframes = Inf;  % maximal number of frames to process, (Inf = all)
cfg.thrsigma = 0.1;   % SENSITIVITY of detection (the higher number the less sensitive, 5 - 11 works fine)

% ----------------------------------------------------------------------------
% camera
cfg.resolution = 80;                 % camera resolution [nm/pixel]
cfg.sigmapsf = 1.3;
cfg.bitdepth = 16;
cfg.ADconvert = [1, 3.6; 1.9, 2; 3.7, 0.9];  % Conversion from Pre-Amp-Gain to CCD Semsitivity

% ----------------------------------------------------------------------------
% molecule detection
cfg.displayfirst = cfg.setup;               % display localized molecules in first # frames
cfg.displayevery = 2000;              % display localized molecules every # frames
cfg.detblure = {'gauss', {11,1.3}};  % blure image for detection, window size, approximate PSF size
cfg.mindist = 7;                     % min. distance between located molecules [pixels]

% ----------------------------------------------------------------------------
% fitting
cfg.maxtimeout = 1;                  % maximal timeout for molecule disappearance [frames]
cfg.box = 3;                         % size of the box (# times 2 + 1) [pixels]
