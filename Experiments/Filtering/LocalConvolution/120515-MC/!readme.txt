Monte Carlo simulation
1.12.2010,PK

background noise:
std = 1.7 photons (142 gray levels * 3.6/300)

molecules: (1.5E6 molecules in total)
500 molecules per frame
3000 frames
sigma = [0.3 2.2]
snr = [0.1 30]

PSF(x,mu,sigma,N,z0) = N / (2*pi*sigma^2) * exp(-0.5 * (x(1)-mu(1))^2 + (x(2)-mu(2))^2 ) / sigma^2 ) + z0

PSF has to be normalized -> this makes volume = 1
Multiplied by N -> estimate number of photons correctly

