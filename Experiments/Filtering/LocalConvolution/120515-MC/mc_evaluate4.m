function  results = mc_evaluate3(img, loc, results, cfg)
%%

thr = 4;%(3)^2;%4;%(3)^2;

npts = size(img.params,1);
nloc = size(loc,1);

% allocate memory
if isempty(results)  
  results.ok = [];%zeros(2000000,15);  % [xl yl Al xf yf sf Af bf chi2 stdb Nim xo yo so Ao]; dot was generated and corectly located
  results.cok = 0;
  results.fp = [];%zeros(1000000,11);  % [xl yl Al xf yf sf Ab bf chi2 stdb Nim]; type I error, dot was located but not generated
  results.cfp = 0;
  results.fn = [];%zeros(1000000,4);   % [xo yo so Ao]; type II error, dot was generated but not located
  results.cfn = 0;
end

% for all localized dots
p = zeros(npts,1);
for I = 1:nloc

  % assign localized position to generated position
  % ---------------------------------------------------------
  dst = ptdist2(loc(I,1:2), img.params(:,1:2));
  % ---------------------------------------------------------
    
  [val,idx] = min(dst);
 
  % false positives - out of localization radius
  if val > thr,
    results.cfp = results.cfp+1;
    results.fp(results.cfp,:) = loc(I,1:2);
    continue;
  end
  
  % count OK
  results.cok = results.cok+1;
  results.ok(results.cok,:) = img.params(idx,1:2);
  
  % track localized points
  p(idx) = p(idx)+1;
  
end

% false negatives - dots were not found
num = sum(~p);
results.fn(results.cfn+(1:num),:) = img.params(~p,1:2);
results.cfn = results.cfn + num;

