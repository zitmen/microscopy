function [loc] = mc_molecule_detect(img, cfg)

%imb = img.imraw;

% blur
evalc(cfg.method(cfg.methodrun).exec);

% candidate pixels for a molecule
% find local maxima
[val,idx] = locmax2d(imb);
idx = circfilter(idx, imb, cfg.mindist); % invalidate all points in neighbourhood
[u,v] = ind2sub(img.siz, idx);

% remove noise
I = zeros(size(idx, 1), 1);
rel_diff = zeros(size(idx, 1), 1);
h = fspecial('gauss', 11, 1.3);
mask = h - mean(h(:));
for i = 1:size(idx, 1);

    try
        region = imb(u(i)-5:u(i)+5, v(i)-5:v(i)+5);
        I(i) = sum(sum(bsxfun(@times, region, mask)));
        diff = abs(I(i) - region(6,6));
        rel_diff(i) = diff ./ region(6,6);
    catch ex
        % do nothing -> discard all in 6px border
    end

end

% analyze results
idx = (I > 0);  % select valid points
u = u(idx); v = v(idx); I = I(idx); rel_diff = rel_diff(idx);
idx = (rel_diff < cfg.detection_threshold);   % thresholding
u = u(idx); v = v(idx); I = I(idx);

% return value
loc = [u, v, I];
