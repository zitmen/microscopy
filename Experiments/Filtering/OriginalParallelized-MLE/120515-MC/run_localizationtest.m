% Test performance of the localization on the static dots

addpath('..\120515-release3');
addpath('..\120515-utils');
clear

cfg = cfg_mcstatic();

% initialize
results = cell(cfg.numimgs,1);
resupdate('free');

disp(cfg.method(cfg.methodrun).name);
fprintf('Analyzing\nProgress ');

%matlabpool local 8
parfor frame = 1:cfg.numimgs

  % parallel loop needs to have it's own path included
   addpath('..\120515-release3');
   addpath('..\120515-utils');

  % initialize
  img = mc_imginit(cfg);
  
  % load next image
  img = mc_imgload(img,frame,cfg);

  % detect molecules
  loc = mc_molecule_detect(img, cfg);

  % store image
  [imbuf,loc] = mc_molecule_storeim2(img, loc, cfg);
      
  % fit model
  fits = molecule_fitting2(img, imbuf, [], cfg);
  
  % evaluate results
  results{frame} = mc_evaluate3(img, loc, fits, results{frame}, cfg);
  
  %progress
  if mod(frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end
fprintf('done\n\n');

fprintf('\nSaving');
res = struct('ok',[],'fn',[],'fp',[],'cok',0,'cfn',0,'cfp',0);
for frame = 1:cfg.numimgs
  res.ok = cat(1,res.ok,results{frame}.ok);
  res.fn = cat(1,res.fn,results{frame}.fn);
  res.fp = cat(1,res.fp,results{frame}.fp);
  res.cok = res.cok + results{frame}.cok;
  res.cfn = res.cfn + results{frame}.cfn;
  res.cfp = res.cfp + results{frame}.cfp;
  %progress
  if mod(frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end
fprintf('done\n\n');

fprintf('\nWritting...');
if ~isdir(cfg.respath), mkdir(cfg.respath); end
save([cfg.respath 'res_dst2_' cfg.method(cfg.methodrun).name '.mat'],'-struct','res');
fprintf('done\n\n');