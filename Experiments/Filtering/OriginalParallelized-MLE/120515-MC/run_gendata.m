% Generate monte-carlo images of dots on noisy background

clear

cfg = cfg_mcstatic();

% initialize
if ~isdir(cfg.datapath), mkdir(cfg.datapath); end;

% generate 
fprintf('Generating %d static monte-carlo images\nProgress ', cfg.numimgs);
for frame = 1:10%cfg.numimgs

  % montecarlo simulation of noisy background
  imbkg = gennoise(cfg);
    
  % montecarlo simulation of dots
  [im,params] = gendotsstatic(cfg);
  
  % dots on background
  im = imbkg + im;

  % save dots and parameters 
  imwrite(uint16(65535*im),[cfg.datapath sprintf('tst%04d.png',frame)],'png','BitDepth',16);
  save([cfg.datapath sprintf('tst%04d.mat',frame)],'-v6','params');
  
   % progress
  if mod(frame,cfg.numimgs/10) == 0, fprintf('.'); end;
end

fprintf('\ndone\n\n');
