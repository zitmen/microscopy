function img = mc_imgload(img,frame,cfg)

img.frame = frame;

% load image
img.imraw = double(imread([cfg.datapath sprintf('tst%04d.png',img.frame)]))/img.norm;

% load parameters
load([cfg.datapath sprintf('tst%04d.mat',img.frame)]);
img.params = params;

