//	Templated double-linked list of objects

#ifndef __LISTDOUBLE_H__
#define __LISTDOUBLE_H__

#include "defs.h"

template <class Type>
class ListDouble 

{
	// list item
	struct ListItem
	{
		Type			val;		// list value
		ListItem	*	prev;		// previous item 
		ListItem	*	next;		// next item

		ListItem()
		:  val(0), prev(NULL), next(NULL)
		{}

		// setup both internal and external links
		ListItem(ListItem * prev, ListItem * next, const Type & val)
		:  val(val), prev(prev), next(next)
		{
			prev->next = this;
			next->prev = this;
		}

		// unlink
		~ListItem()
		{
			prev->next = next;
			next->prev = prev;
		}
	};

	ListItem	*list;	// list->next = head, list->prev = tail, list = temporary item
	int			num;	// number of items

public:

	// constant list node access class
	class CNode 
	{
	protected:

		ListItem	*	ptr;		// current list item

		// attach to list item
		CNode(ListItem * ptr)
		: ptr(ptr)
		{}

		friend class ListDouble<Type>;
		
	public:

		// default constructor
		CNode()
		: ptr(NULL)
		{}

		// access const node value
		const Type & operator *() 
		const
		{
			ASSERT(ptr);

			return ptr->val;	
		}

		// access const node item
		const Type * operator ->()
		const
		{
			ASSERT(ptr);

			return &(ptr->val);
		}

		// compare operator
		bool operator ==(const CNode & n)
		const
		{
			return n.ptr == ptr;
		}

		// compare operator
		bool operator !=(const CNode & n)
		const
		{
			return n.ptr != ptr;
		}

		// test for noninitialized 
		operator bool ()
		const
		{
			return ptr != NULL;
		}

		// move to next item
		CNode & operator ++()
		{
			ASSERT(ptr);

			ptr = ptr->next;
			return *this;
		}

		// move to previous item
		CNode & operator --()
		{
			ASSERT(ptr);

			ptr = ptr->prev;
			return *this;
		}

		// postfix increment
		CNode operator ++(int)
		{
			ASSERT(ptr);

			CNode tmp = *this;
			ptr = ptr->next;
			return tmp;
		}

		// postfix decrement
		CNode operator --(int)
		{
			ASSERT(ptr);

			CNode tmp = *this;
			ptr = ptr->prev;
			return tmp;
		}

		// return previous node
		CNode Prev()
		const
		{
			ASSERT(ptr);

			return CNode(ptr->prev);
		}

		// return next node
		CNode Next()
		const
		{
			ASSERT(ptr);

			return CNode(ptr->next);
		}
	};

	// modifiable list node access class
	class Node : public CNode
	{
	protected:

		// resolve base members
		using CNode::ptr;

		friend class ListDouble<Type>;

	public:

		// attach to list item
		Node(ListItem * ptr)
		: CNode(ptr)
		{}

		// default constructor
		Node() {}

		// access modifiable node value
		Type & operator *()
		const
		{
			ASSERT(ptr);

			return ptr->val;
		}

		// access modifiable node item
		Type * operator ->()
		const
		{
			ASSERT(ptr);

			return &(ptr->val);
		}

		// compare operator
		bool operator ==(const Node & n)
		const
		{
			return n.ptr == ptr;
		}

		// compare operator
		bool operator !=(const Node & n)
		const
		{
			return n.ptr != ptr;
		}

		// move to next item
		Node & operator ++()
		{
			ASSERT(ptr);

			ptr = ptr->next;
			return *this;
		}

		// move to previous item
		Node & operator --()
		{
			ASSERT(ptr);

			ptr = ptr->prev;
			return *this;
		}

		// postfix increment
		Node operator ++(int)
		{
			ASSERT(ptr);

			Node tmp = *this;
			ptr = ptr->next;
			return tmp;
		}

		// postfix decrement
		Node operator --(int)
		{
			ASSERT(ptr);

			Node tmp = *this;
			ptr = ptr->prev;
			return tmp;
		}

		// return previous node
		Node Prev()
		const
		{
			ASSERT(ptr);

			return Node(ptr->prev);
		}

		// return next node
		Node Next()
		const
		{
			ASSERT(ptr);

			return Node(ptr->next);
		}
	};

	// default constructor
	ListDouble()
	: num(0)
	{
		list = new ListItem;
		list->next = list->prev = list;
	}

	// copy constructor 
	ListDouble(const ListDouble & l)
	: num(0)
	{
		list = new ListItem;
		list->next = list->prev = list;

		Append(l.Head(),l.Tail());
	}

	// cleanup - destroy all nodes in list
	~ListDouble()
	{
		RemoveAll();
		delete list;
	}

	// assignment operator - replace with other list
	ListDouble & operator =(const ListDouble & l)
	{
		// prevent self-assignment

		if(&l != this)
		{
			// clear old content
 			RemoveAll();

			// append new one
			Append(l.Head(),l.Tail());
		}

		return *this;
	}

	// swap itself with the other list
	void Swap(ListDouble & l)
	{
		::Swap(list,l.list);
		::Swap(num,l.num);
	}

	// get modifiable starting item of list
	Node Head()
	{
		return Node(list->next);
	}

	// get constant starting item of list
	CNode Head()
	const
	{
		return CNode(list->next);
	}

	// get ending item of list (one after last)
	Node Tail()
	{
		return Node(list);
	}

	// get ending item of list (one after last)
	CNode Tail()
	const
	{
		return CNode(list);
	}

	// get number of items
	int Num()
	const
	{
		return num;
	}

	// insert node before given one
	Node Insert(Node node, const Type & val = Type())
	{
		++num;
		return Node(new ListItem(node.Prev().ptr,node.ptr,val));
	}

	// add item before beginning of list
	Node AddHead(const Type & val)
	{
		++num;
		return Node(new ListItem(list,list->next,val));
	}

	// add item after end of list
	Node AddTail(const Type & val)
	{
		++num;
		return Node(new ListItem(list->prev,list,val));
	}

	// delete given node from list
	Node Delete(Node node)
	{
		ASSERT(num > 0);

		Node next = node.ptr->next;
		delete node.ptr;
		--num;

		return next;
	}

	// delete all nodes from first to last
	void Erase(Node from, Node to)
	{
		while(from != to)
			from = Delete(from);
	}

	// empty complete list
	void RemoveAll()
	{
		Erase(Head(),Tail());
	}

	// append a sublist to this one
	void Append(CNode from, CNode to)
	{
		while(from != to)
			AddTail(*(from++));
	}
};	


#endif
