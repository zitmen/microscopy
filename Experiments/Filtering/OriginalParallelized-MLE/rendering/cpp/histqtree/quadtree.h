// 24.5.2010, PK, histqtree(points, roi, imsize, maxevents)
// 14.5.2010, PK, histqtree(points, roi, maxevents, renderdepth)

#ifndef __QUADTREE_H__
#define __QUADTREE_H__

#include <stdlib.h>
#include "func.h"
#include "point.h"
#include "listsingle.h"
#include "matrix.h"

const int QT_IMSIZE_DEFAULT = 16;
const int QT_MAX_EVENTS_DEFAULT = 10;
		
typedef enum {
	qtOK = 0,
	qtNotALeafNode,
	qtOutOfMargins,
	qtFull,
	qtRenderError
} qterror;


typedef Point<float> point;
typedef ListSingle<point> listofevents;
typedef Matrix<float> matrix;

class QuadTreeNode {
private:
	point corners[2];				// corners
	QuadTreeNode **children;		// childrens
	QuadTreeNode *parent;			// parent
	listofevents *events;			// list of events in the node

	point size;						// size of the bin
	int depth;						// depth of the leaf
	int num;						// number of events in the leaf

	friend class QuadTree;

public:
	QuadTreeNode();
	QuadTreeNode(const point& a, const point& c, QuadTreeNode * const parent);
	~QuadTreeNode();
	qterror FindLeafNode(const point &pt, QuadTreeNode **ptr);
	qterror Add(const point& pt);
	qterror Split();
	bool isLeaf() const { return children == NULL; };
	bool isRoot() const { return parent == NULL; };
	bool isIn(const point& pt) const;
	point Size() const { return size; };
	int Depth() const { return depth; };
	int Num() const { return num; };
};



class QuadTree {
private:

	QuadTreeNode *root;		// root of the tree
	
	point roi[2];
	int maxevents;			// maximum number of events in a bin - set by the user
	int depth;				// counts maximum depth of the tree

	qterror RenderNode(const QuadTreeNode * node, matrix& subimage, const int rendertodepth);

public:
	QuadTree();
	~QuadTree();

    // make a root
	QuadTree(const point& a, const point& c, const int maxevents);

	// set ROI if different from root
	qterror SetROI(const point& a, const point& c);

	// add an event
	qterror Add(const point &pt);
	
	// return depth of the tree
	int Depth() const { return depth; };

	// return number of registred events
	int Num() const { return root->Num(); };


	// compute size of the final image based on rendering depth
	//qterror RenderSize(int *rendertodepth, int *imsize);

	// render quad-tree histogram into pre-allocated memmory
	qterror Render(matrix& image, int rendertodepth);
};


#endif
