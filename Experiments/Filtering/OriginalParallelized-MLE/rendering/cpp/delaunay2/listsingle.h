#ifndef __LISTSINGLE_H__
#define __LISTSINGLE_H__

#include "defs.h"

// ---------------------------------------------------------------------------
// templated single-linked list
// ---------------------------------------------------------------------------

template <typename Type>
class ListSingle {

private:
	struct item {
		Type	val;
		item	*next;

		item(item **prev, const Type & val)
		: val(val), next(*prev)
		{
			*prev = this;
		}
	};

	item *list;		// pointer to the list
	size_t num;		// number of items in the list

public:

	// constant node access class
	class CNode {
	protected:
		
		item	*ptr;	// pointer to list item

		CNode(item *ptr)
		: ptr(ptr)
		{
			ASSERT(ptr);
		}

		friend class ListSingle<Type>;
	
	public:

		// default constructor
		CNode()
		: ptr(NULL)
		{}

		// access const node item
		const Type * operator *()
		const
		{
			ASSERT(ptr);

			return &(ptr->val);
		}

		// compare operator
		bool operator ==(const CNode & n)
		const
		{
			return n.ptr == ptr;
		}

		// compare operator
		bool operator !=(const CNode & n)
		const
		{
			return n.ptr != ptr;
		}

		// test for end
		operator bool ()
		const
		{
			return ptr != NULL;
		}

		// move to next item
		CNode & operator ++()
		{
			ASSERT(ptr);

			ptr = ptr->next;
			return *this;
		}

		// postfix increment
		CNode operator ++(int)
		{
			ASSERT(ptr);

			CNode tmp = *this;
			ptr = ptr->next;
			return tmp;
		}


	};

	// modifiable list node access class
	class Node : public CNode
	{
	private:

		// attach to list item
		Node(item * ptr)
		: CNode(ptr)
		{}

		friend class ListSingle<Type>;

		// resolve base members
		using CNode::ptr;

	public:

		// default constructor
		Node() {}
	};


	// default constructor
	ListSingle()
	: list(NULL), num(0)
	{}
	
	~ListSingle()
	{
		DeleteAll();
	}

	// delete all items
	void DeleteAll()
	{
		item *p;
		while ( (p = list) != NULL)
		{
			list = p->next;
			//delete p->val; ??? it seems its deleted automaticaly with p
			delete p;
			num--;
		}
	}

	// get modifiable starting item of list
	Node Head()
	{
		return Node(list);
	}

	// get constant starting item of list
	CNode Head()
	const
	{
		return CNode(list);
	}

	// get ending item of list (one after last)
	Node Tail()
	{
		return Node();
	}

	// get ending item of list (one after last)
	CNode Tail()
	const
	{
		return CNode();
	}

	// number of items in the list
	size_t Num()
	const
	{
		return num;
	}

	// number of items in the list
	bool isEmpty()
	const
	{
		return (list == NULL);
	}

	// add item
	Node AddHead(const Type & val)
	{
		++num;
		return Node(new item(&list, val));
	}

	Type DeleteHead(void)
	{
		ASSERT(num > 0);

		item *p = list;
		list = p->next;
		Type val = p->val;
		num--;
		delete p;
		return val;
	}

	// cancel copy constructor (Virius 1, p.140)
	ListSingle(const ListSingle&);
	ListSingle& operator=(const ListSingle&);
};

// ---------------------------------------------------------------------------
#endif
