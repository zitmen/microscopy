function histmat  = hist3(x, y, z, xedges, yedges, zedges)

if nargin ~= 6
    error('The four input arguments are required!');
end
if any(size(x) ~= size(y)) || any(size(x) ~= size(z))
    error('The size of the three first input vectors should be same!');
end

[xn, xbin] = histc(x,xedges);
[yn, ybin] = histc(y,yedges);
[zn, zbin] = histc(z,zedges);

idx = xbin == 0 | ybin == 0 | zbin == 0;
xbin(idx) = [];
ybin(idx) = [];
zbin(idx) = [];

xnbin = length(xedges);
ynbin = length(yedges);
znbin = length(zedges);

xyz = zbin*ynbin*xnbin + ybin*xnbin + xbin;
indexshift =  ynbin*xnbin; 
xyzuni = unique(xyz);
hstres = histc(xyz,xyzuni);
histmat = zeros(xnbin,ynbin,znbin,'uint16');
histmat(xyzuni-indexshift) = hstres;

% combine last bins
% histmat(:,end-1) = histmat(:,end-1) + histmat(:,end);
% histmat(end-1,:) = histmat(end-1,:) + histmat(end,:);
% histmat = histmat(1:end-1,1:end-1,1:end-1);
