function res = render_delaunay(results, rule, imsize, scaling, numjitters)


if (numjitters < 1)
  error('render_delaunay: numjitters must be >= 1');
end

%% rendering rules & decode results
[xpos,ypos,a,sigma,s,N,b,dx,chi2] = render_rules(results, rule);

npts = length(xpos);

%% output image size
newimsize = scaling * imsize;
% dx = 20;
dxstd = dx/a;

%% generate quad-tree based histogram
fprintf('Generating %d Delaunay triangulation(s)\n', numjitters);
fprintf('Progress ');

IM = zeros(newimsize,'single');
for I = 1:numjitters

  xp = xpos;
  yp = ypos;
  
  % random jitter - add normal noise to localized points
  if (numjitters > 1)
    xp = xp + dxstd .* randn(npts,1);
    yp = yp + dxstd .* randn(npts,1);
  end

  % generate quad-tree
  im = delaunay2([yp,xp], [1 1 imsize], newimsize);
  
  % accumulate the 
  IM = IM + im(1:newimsize(1),1:newimsize(2));
  
  if (mod(I,round(numjitters/10)) == 0), fprintf('.'); end;   
end
fprintf('\n');
if numjitters > 1
  IM = IM/numjitters;
end


%% results
res.name = 'delaunay';
res.im = IM;
