function cfg = cfg_IGF1Ra_serum01
% ----------------------------------------------------------------------------
% Config file for Matlab release 2
% ACTIN 11
% ----------------------------------------------------------------------------
% 13.10.2010, Last change, PK
cfg.ver = '3';

cfg.setup = 0;

cfg.framestart = 1000;%100
cfg.numframes = 1100;%Inf;                 % maximal number of frames to process, (Inf = all)
cfg.thr = 0.018;

% where to save results
%d:\data\blinking\Peter\2H3\IGF1Ra serum\01\IGF1Ra serum blink cyl lens 1\';
cfg.datadir = 'g:\blinking superresolution data\Peter-2H3\IGF1Ra serum blink cyl lens 1\';
cfg.filemask = 'IGF1Ra serum blink cyl lens 1'; 
cfg.resdir = '$data$/results_ver3-detect8-box9x9/';

% ----------------------------------------------------------------------------
% camera
cfg.resolution = 80;                 % camera resolution [nm/pixel]
cfg.psfsigma = 1.29;
cfg.psfangle = 28*pi/180;
cfg.bitdepth = 14;
cfg.ADconvert = [1, 3.6; 1.9, 2; 3.7, 0.9];  % Conversion from Pre-Amp-Gain to CCD Semsitivity
cfg.vrtx=[];

% ----------------------------------------------------------------------------
% molecule detection
cfg.displayfirst = 10;%cfg.setup;               % display localized molecules in first # frames
cfg.displayevery = 2000;              % display localized molecules every # frames
cfg.detblure = {'gauss', {11,1.3}};  % blure image for detection, window size, approximate PSF size
cfg.mindist = 7;                     % min. distance between located molecules [pixels]
               
% ----------------------------------------------------------------------------
% fitting
cfg.maxtimeout = 1;                  % maximal timeout for molecule disappearance [frames]
cfg.box = 4;                         % size of the box (# times 2 + 1) [pixels]

% ----------------------------------------------------------------------------
% analyze
cfg.analize.fontsize = 12;          % font size
cfg.analize.rule = 0;               % rule
cfg.analize.snrgain = 1;
cfg.analize.save = 1;               % save results [0|1]

% ----------------------------------------------------------------------------
% drift compensation
cfg.drift.method = 'none'; 
cfg.drift.params = [0 0 0]; % [vx vy angle];

% ----------------------------------------------------------------------------
% rendering
%  method  ... function handle to rendering method
%  scale   ... magnification of original resolution
%  average ... >=1; use adaptive jitter and average results (only for hist and quadtree)
%  blure   ... blure the result (only for hist and quadtree); [] no blure; {{'gauss',{11,1}}} blure with specified filter
%  rules   ... function handle to rendering rules
%  rule    ... rule applied (0 = all located points)
%  save    ... save result 0 or 1 for on and off, if string than use it as appendix for output file 'rule1_scale5_average01_blure0'

% cfg.render = struct('method', @render_scatter, 'rules',  1, 'save', 1);
% cfg.render = struct('method', @render_gauss, 'scale', 5, 'rule', 1, 'save', 1);
% cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 100, 'rule', 1, 'save', 'av100');
%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 100, 'rule', 6, 'save', 'av100_mag5x_rule6');
% cfg.render = struct('method', @render_quadtree, 'scale', 5, 'average', 100, 'maxevents', 10,  'rule', 1, 'save', 'av100'); 

%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 500, 'rule', 15, 'snrgain',0.7, 'save', '_av500_rule15_fp_');
%cfg.render = struct('method', @render_hist, 'scale', 5, 'average', 500, 'rule', 0, 'snrgain',1, 'save','_av500');
cfg.render = struct('method', @render_hist3, 'scale', 5, 'zlim', -250:25:250, 'average', 1000, 'rule', 2, 'snrgain',1, 'save','_xy12.5nm_z25nm_av1000_r2');
%cfg.render = struct('method', @render_hist3, 'scale', 2.5, 'zlim', -250:25:250, 'average', 1000, 'rule', 2, 'snrgain',1, 'save','_xy25nm_z25nm_av1000_r2');
