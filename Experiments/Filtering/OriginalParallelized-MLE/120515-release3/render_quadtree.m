function res = render_quadtree(results, cfg)

% 7.10.2010, PK, saving moved to res_render
% 24.5.2010, PK, averaging and random jitter, blure filter, saving

if (cfg.render.average < 1)
  error('render_quadtree: cfg.render.average must be >= 1');
end

%% rendering rules & decode results
[xpos,ypos,a,sigma,s,N,b,dx,chi2] = render_rules(results, cfg.render);

npts = length(xpos);

%% output image size
newimsize = cfg.render.scale * results.imsize;
dxstd = dx/a;

%% generate quad-tree based histogram
fprintf('Generating %d quad-tree(s)\n', cfg.render.average);
fprintf('Progress ');

IM = zeros(newimsize,'single');
for I = 1:cfg.render.average

  xp = xpos;
  yp = ypos;
  
  % random jitter - add normal noise to localized points
  if (cfg.render.average > 1)
    xp = xp + dxstd .* randn(npts,1);
    yp = yp + dxstd .* randn(npts,1);
  end

  % generate quad-tree
  im = histqtree([yp,xp], [1 1 results.imsize], newimsize, cfg.render.maxevents);
  
  % accumulate the 
  IM = IM + im(1:newimsize(1),1:newimsize(2));
  
  if (mod(I,round(cfg.render.average/10)) == 0), fprintf('.'); end;   
end
fprintf('\n');
if cfg.render.average > 1
  IM = IM/cfg.render.average;
end

%% blure filter
if ~isempty(cfg.render.blure)
  h=fspecial(cfg.render.blure{1},cfg.render.blure{2}{:});
  IM = imfilter(IM,h);
end;

%% results
res.name = 'quadtree';
res.im = IM;
