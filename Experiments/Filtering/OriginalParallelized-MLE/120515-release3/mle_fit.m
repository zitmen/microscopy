function [a] = mle_fit(startingSigma, im);

[X Y N BG S CRLBx CRLBy CRLBn CRLBb CRLBs LogL]=mGPUgaussMLE(single(im),startingSigma,10,2);
a = [ X Y S N BG ];
