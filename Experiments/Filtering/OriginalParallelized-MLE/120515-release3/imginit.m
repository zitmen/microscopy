function img = imginit(datadir,cfg)

% 13.10.2010, PK, load multiple layer tiff
% 6.10.2010, PK, bitdepth
% 27.4.2010, PK, only a single image
% 11.3.2010, PK, release 1 - average over neighbouring images

% 2DO - fix gains and perhaps loading image list - see sim4sectioning

%%
fprintf('\nInitializing ...\n');
[img, iqinf] = iq2imginit(cfg.datadir,cfg.filemask);

% conversion from gray levels to photons
img.gain = [iqinf.TabExposure.GainsPreAmpGain iqinf.TabExposure.GainsEMGain];
idx = abs(cfg.ADconvert(:,1) - img.gain(1)) < 1e-3;
if sum(idx) ~= 1
  img.convert = 1;
  fprintf(' Conversion to CCD sensitivity failed. Check cfg.ADconvert. Using unity.\n');    
else
  img.convert = img.norm * cfg.ADconvert(idx,2) / img.gain(2);
end

% starting and final frame
img.frame = cfg.framestart;
% if cfg.setup
%   img.maxframe = min(cfg.framestart + cfg.setup, img.siz(4));
% else
  img.maxframe = min(cfg.framestart + cfg.numframes, img.siz(4));
% end

% init data
img.imraw = [];

fprintf(' Directory:        %s\n', img.datadir);
fprintf(' Filename:         %s\n', img.filemask);
fprintf(' Image size:       %d x %d\n', img.siz(1:2));
fprintf(' Number of frames: %d\n', img.maxframe);
fprintf(' Starting frame:   %d\n', img.frame);
fprintf(' Bit depth:        %d\n', img.nbit);
fprintf(' Pre-Amp-Gain:     %.2f\n', img.gain(1));
fprintf(' EMGain:           %d\n', img.gain(2));
%eof