function [fits, imbuf] = molecule_fitting2(img, imbuf, fits, cfg)

persistent X;

if isempty(fits)
  resupdate('free');
  fits = struct('num', 0, 'discard', 0); X = [];
  [X(:,:,2),X(:,:,1)] = meshgrid(-cfg.box:cfg.box,-cfg.box:cfg.box);
end

  % find molecules that disapeared
  idx = 1:length(imbuf);  % proste vsechno true :)
  npts = (2*cfg.box+1)^2;

  % make stack of the points to run the CUDA function faster
  images = zeros(size(X,1),size(X,2),length(idx));  % alloc  
  for I = 1:length(idx)
    images(:,:,I) = imbuf(idx(I)).im();
  end

  % fit 2D gaussian bell shaped function
  [aa] = mle_fit(cfg.sigmapsf, images);

  % process the results
  for I = 1:length(idx)
    
    tmp = imbuf(idx(I));    % "pointer" to current detection buffer
    im = imbuf(I).im;
    a = aa(I,:);
    
    % estimate background at stored position
    imestim = gauss2d(X,a);
    Nim = sum(im(:) - a(5));  % Nimestim = sum(imestim(:) - a(5)); % is the same
    
    bkgestim = im - imestim;
    bkgmu = sum(bkgestim(:))/npts;
    bkgstd = sqrt(sum((bkgestim(:)-bkgmu).^2)/(npts-1));
    
    % position
    a(1:2) = a(1:2) - cfg.box + tmp.pos;  % subtract cfg.box from x and y because this script uses the center as start of the coordinate system [0,0]; it should be actualy sub(cfg.box+1) but the +1 is missing because of zero-indexing in C
    a(4) = a(4) * 2*pi*a(3)^2;
    
    % save results
    fits.num = resupdate('add', single([a, 0, Nim, bkgmu, bkgstd, imbuf(I).frame]));
    %fits.num = resupdate('add', single([a, sum(chi2), Nim, bkgmu, bkgstd, imbuf(I).frame]));
  end

  %save results
  fits.fits = resupdate('save');


function y = gauss2d(X,a)

y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);

