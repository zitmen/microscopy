function im = imgload(img, frame)
% load single/multilayer tif

if (frame < 1) || (frame > img.numframes(end))
  error('Wrong frame number.');
end

% here img.numframes = [0 cumsum(numpg)] 
idx = img.numframes < frame;
fileno = sum(idx);
frameno = frame - img.numframes(fileno);

im = single(imread([img.datadir img.filelist{fileno}], frameno))/img.norm;
