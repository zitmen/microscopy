function y = fun_poly(x,par)

y = par(1) + par(2)*x + par(3)*x.^2;
