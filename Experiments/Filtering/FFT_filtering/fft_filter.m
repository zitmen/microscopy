img=double(imread('noisy-image_sigma-0.05.tiff'))/double(2^16);
imshow(img)

f=fft2(img);
f(abs(f)<50)=0;

imshow(fft2(img))
imshow(abs(f))

imshow(ifft2(fft2(img)))
imshow(ifft2(f))
