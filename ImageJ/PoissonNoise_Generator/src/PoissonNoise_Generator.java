import ij.*;
import ij.gui.*;
import java.awt.*;
import ij.process.*;
import ij.plugin.*;
import ij.plugin.filter.*;
import ij.plugin.frame.*;
import jsc.distributions.Poisson;

// TODO: jeste pred vygenerovanim obrazku vyhodit dialog, ve kterym zadam rozmery (default: w,h=512;lambda=spocitana z obrazku)
//          a bude tam tlacitko GenNoise, kterym se vygeneruje obrazek s sumem

public class PoissonNoise_Generator implements PlugInFilter
{
    ImagePlus imp = null;
    
    @Override
    public int setup(String arg, ImagePlus imp)
    {
        if(arg.equals("about"))
        {
            showAbout();
            return DONE;
        }
        this.imp = imp;
        // Grayscale only
        return DOES_8G | DOES_16 | DOES_32;
    }
    
    @Override
    public void run(ImageProcessor ip)
    {
        //
        // Retrieve image and region of interest, if any
        Rectangle rect = null;
        Roi roi = imp.getRoi();
        if(roi != null) rect = roi.getBounds();
        if(rect == null) rect = new Rectangle(0, 0, imp.getWidth(), imp.getHeight());
        //
        // Open histogram
        IJ.log("Evaluating statistics of the image...");
        HistogramWindow hist = new HistogramWindow(imp);
        //
        // Compute lambda
        IJ.log("Approximating the Poisson's λ parameter...");
        double lambda = 0.0;
        for(int y = rect.y, ym = y + rect.height; y < ym; y++)
            for(int x = rect.x, xm = x + rect.width; x < xm; x++)
                lambda += (double)imp.getPixel(x, y)[0];
        //
        lambda /= (double)(rect.width * rect.height);
        lambda = round(lambda, 2);
        IJ.log("λ = " + lambda + "\n");
        //
        // Show GenNoise Options dialog
        OptionsDialog options = new OptionsDialog(512, 512, lambda);
    }
    
    void showAbout()
    {
        IJ.showMessage("About PoissonNoise_Generator...",
                       "This plugin takes a selected Region Of Interest (or the whole image if there is no ROI), " +
                       "then calulates λ of Poisson distribution and finaly the plugin generates a new noise-only image.");
    }
    
    double round(double value, int digits)
    {
        double d = Math.pow(10.0, (double)digits);
        return ((double)Math.round(value * d) / d);
    }

    static public void genNoise(int width, int height, double lambda)
    {
        IJ.log("Generating the 512x512px noise image (λ = " + lambda + ")...");
        Poisson poisson = new Poisson(lambda);
        ImagePlus noisy = NewImage.createFloatImage("Noisy image", width, height, 1, NewImage.FILL_BLACK);
        ImageProcessor ipn = new FloatProcessor(width, height);
        noisy.setProcessor(ipn);
        float [] pixels = (float [])ipn.getPixels();
        for(int i = 0, im = pixels.length; i < im; i++)
            pixels[i] = (float)poisson.random();
        noisy.show();
    }
}
