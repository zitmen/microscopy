function decloc(datapath,respath,filename,frames,cfg)

    % load CVX library
    persistent cvx_loaded;
    if isempty(cvx_loaded)
        run('cvx/cvx_setup.m');
        cvx_loaded = true;
    end
    
    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), mkdir(respath); end
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        % load image
        imraw = imread(sprintf('%s/%s.%04d.tiff',datapath,filename,frame));
        
        % detect molecule candidates to speed up the CSSTORM routine
        %
        mol_map = detect_molecules(double(imraw)/65535,cfg);
        mol_map = imdilate(mol_map,strel('square',3));  % just to be sure the right pixel is there ;)
        MM = zeros(size(mol_map));
        d = floor(cfg.CSSTORM.boxsize/2);
        MM(1:end-d,1:end-d) = mol_map(1+d:end,1+d:end);
        %
        %MM = ones(size(imraw));

        % run the CSSTORM routine
        mol_img = CS_fit(imraw,MM,cfg);

        % save the superresolution image
        save(sprintf('%s/%s.%04d.mat',respath,filename,frame),'-v6','mol_img');

    end
    fprintf('Done.\n');

end
%%
function img_recover = CS_fit(lowresimg,mol_map,cfg)

    debug_mode = cfg.CSSTORM.debug_mode;

    % camera parameters
    photon_per_count = cfg.CSSTORM.ADU;
    ccd_base_line = cfg.CSSTORM.base_line;
    
    % optimization parameters
    div = cfg.CSSTORM.oversampling;
    boxsize = cfg.CSSTORM.boxsize;
    extramargin = cfg.CSSTORM.extramargin;
    scanoverlap = cfg.CSSTORM.scanoverlap;
    margin = cfg.CSSTORM.margin;
    mag = 2 / boxsize;
    red_chi2 = cfg.CSSTORM.eps;

    % image info
    [x_dim,y_dim] = size(lowresimg);

    if(debug_mode)
        mag_full = 2 / x_dim ;
        min_contrib = 1; % min CS recovered value to be considered as a successful molecule identification
    end

    lowresimg = (lowresimg - ccd_base_line) * photon_per_count;

    % dimension of of the oversampled super-resolution image
    x_dim_est = x_dim * div;
    y_dim_est = y_dim * div;

    % generate global grid for final image estimate
    % these dimensions should correspond to your raw and final image
    x_pos_est_full = linspace(-1,1,x_dim_est);
    y_pos_est_full = linspace(-1,1,y_dim_est);
    x_inx_full = x_pos_est_full(x_dim_est/x_dim/2 : x_dim_est/x_dim : end);
    y_inx_full = y_pos_est_full(y_dim_est/y_dim/2 : y_dim_est/y_dim : end);

    [x_inx_full y_inx_full] = meshgrid(x_inx_full, y_inx_full);
    [x_pos_est_full y_pos_est_full] = meshgrid(x_pos_est_full, y_pos_est_full);

    % generate grid for local estimation
    x_pos_est = linspace(-1,1, div*boxsize);
    y_pos_est = linspace(-1,1, div*boxsize);

    x_inx = x_pos_est((div/2) : div : end);
    y_inx = y_pos_est((div/2) : div : end);

    % provide pixel padding for local CS estimate
    % This should correspond to the box size around peak intensity
    % -- Begin padding the estimated local patch
    dx = x_pos_est(2)-x_pos_est(1);
    dy = y_pos_est(2)-y_pos_est(1);

    for mm = 1:margin
        x_pos_est = [x_pos_est(1)-dx x_pos_est];
        x_pos_est = [x_pos_est x_pos_est(end)+dx];
        y_pos_est = [y_pos_est(1)-dy y_pos_est];
        y_pos_est = [y_pos_est y_pos_est(end)+dy];
    end

    [x_inx y_inx] = meshgrid(x_inx,y_inx);
    [x_pos_est y_pos_est] = meshgrid(x_pos_est,y_pos_est);

    % generate measurement matrix
    len = length(x_pos_est(:));
    %A = zeros(x_inx, len + 1);
    for ii = 1:len
        img_kernel = MolKernel(x_inx, y_inx, x_pos_est(ii), y_pos_est(ii), mag, cfg.CSSTORM.psf_sigma);
        A(:,ii) = img_kernel(:);
    end
    
    c = sum(A);
    PSF_integ = max(c); % integration of the PSF over space, used for normalization
    c = c./ PSF_integ; % normalize to 1
    A = A./ PSF_integ;
    
    % add the extra optimization variable for the estimation of the background
    len = len + 1;
    c(len) = 0;
    A(:,len) = 1;
    
    len = size(A,2);
    A = sparse(A);
    
    % begin to formulate optimization
    cvx_quiet(cfg.CSSTORM.cvx_quiet);

    % begin analysis
    if debug_mode
       lowresimg_display = lowresimg;
    end

    img_recover = zeros(x_dim_est, y_dim_est);

    %scan the optimization box throughout image
    pre_xloc = 1 + scanoverlap - boxsize;
    pre_yloc = 1;
    while true;
       pre_xloc = pre_xloc + boxsize - scanoverlap;
       if pre_xloc + boxsize - 1 > x_dim
           pre_yloc = pre_yloc + boxsize - scanoverlap;
           pre_xloc = 1;
           if pre_yloc + boxsize - 1 > y_dim 
               break;
           end
       end
       
       %if mol_map(pre_yloc,pre_xloc) == 0;
       %    continue;    % no molecules in the patch, just flat background
       %end

       % The boundary of the optimization box
       inx_x_l = pre_xloc;
       inx_x_u = pre_xloc + boxsize - 1;
       inx_y_l = pre_yloc;
       inx_y_u = pre_yloc + boxsize - 1;

       if debug_mode
          imagesc((lowresimg_display)), colormap(gray), hold on;
          rectangle('Position',[inx_x_l-0.5-margin,inx_y_l-0.5-margin,boxsize+2*margin,boxsize+2*margin],'linewidth',1,'EdgeColor','r'),
          rectangle('Position',[inx_x_l-0.5,inx_y_l-0.5,boxsize,boxsize],'linewidth',1,'EdgeColor','y'),pause(0.5);
       end

       % crop from the original image
       cropraw = double(lowresimg(inx_y_l:inx_y_u, inx_x_l:inx_x_u));

       L1sum = norm(cropraw(:),1);
       L2sum = sqrt(L1sum); % Target of least square estimation based on Poisson statistics

       %if norm(cropraw(:)-L1sum/length(cropraw(:)),2) < L2sum * red_chi2;
       %    continue;    % no molecules in the patch, just flat background
       %end

       % optimization using CVX
       %Weighting = diag(1./sqrt(abs(cropraw(:))+1));
       %img_est = MolEst_eps(Weighting*cropraw(:),len,Weighting*A,c,(boxsize*boxsize-1)*red_chi2);
       img_est = MolEst_eps(cropraw(:),len,A,c,L2sum * red_chi2);
       est_background = img_est(len);
       if(debug_mode)
           fprintf('BG=%.1f, L1=%.1f (target: %.1f), L2=%.1f (target: %.1f), ',...
                  est_background,...
                  norm(c*img_est,1),             L1sum-(boxsize*boxsize)*est_background,...
                  norm(A*img_est-cropraw(:),2),  L2sum);
       end

       img_est = reshape(img_est(1:len-1), [length(x_pos_est) length(y_pos_est)]);

       % remove margin
       img_est = img_est(margin+1:end-margin, margin+1:end-margin);
       if extramargin > 0
          img_est([1:extramargin*div end-extramargin*div+1:end],:) = 0;
          img_est(:,[1:extramargin*div end-extramargin*div+1:end]) = 0;
       end

       % put this hi-res patch of image back into collection array
       hr_x_l = (inx_x_l-1)*div+1;
       hr_y_l = (inx_y_l-1)*div+1;
       hr_x_u = hr_x_l + (div*boxsize)-1;
       hr_y_u = hr_y_l + (div*boxsize)-1;

       img_recover_temp = zeros(x_dim_est,y_dim_est);
       img_recover_temp(hr_y_l:hr_y_u, hr_x_l:hr_x_u) = img_est;

       % add the optimized patch to the recovered super-resolution image
       img_recover = img_recover + img_recover_temp;

       % display othe optimization procedure for debugging only
       if debug_mode
           inx_contribute = find(img_recover_temp(:) > min_contrib);
           if isempty(inx_contribute)
               fprintf('nothing found.\n');
               continue;
           else
               fprintf('%d contribution points.\n',length(inx_contribute(:)));
           end

           % calculate the recovered low resolution image based on the optimization results
           lowresimg_temp = zeros(size(lowresimg));
           for jj = 1:length(inx_contribute)
              h = MolKernel(x_inx_full, y_inx_full, ...
                        x_pos_est_full(inx_contribute(jj)),...
                        y_pos_est_full(inx_contribute(jj)), mag_full,...
                        cfg.CSSTORM.psf_sigma);
              lowresimg_temp = lowresimg_temp + img_recover_temp(inx_contribute(jj))*h;
           end

           lowresimg_display = lowresimg_display - uint16(lowresimg_temp) / PSF_integ;

           imagesc(lowresimg_display); drawnow;
       end
    end

end
%%
function img_est = MolEst_eps(img_raw,len,A,c,eps)

    b = img_raw(:);
    n = len;

    cvx_begin
        variable x(n)
        minimize(c*x)
        subject to
            x >= 0;
            norm( A * x - b, 2 ) <= eps;
    cvx_end
    img_est = x;
    
end
%%
function img_kernel = MolKernel(x_inx,y_inx,x_pos,y_pos,mag,sigma)

    img_kernel = exp(-0.5*((((x_inx-x_pos)/mag).^2 + ((y_inx-y_pos)/mag).^2)/sigma^2));

end
%%
function mol_map = detect_molecules(imraw,cfg)

    h = fspecial('average',3);
    im = imfilter(imraw,h,'replicate','same');
    %
    radius = 5;
    mx = imdilate(im,strel('square',radius));
    bordermask = zeros(size(im));
    bordermask(radius+1:end-radius, radius+1:end-radius) = 1;
    [u,v] = find((im==mx) & (im>cfg.thr) & bordermask);
    mol_map = zeros(size(im));
    mol_map(sub2ind(size(im),u,v)) = 1;

end