function fits = CS_postprocessing(hiresim)

    hiresim(isnan(hiresim(:))) = 0;
    hiresim(hiresim(:)<1) = 0;
    %
    thr_mol_energy = 0.1;
    thr_max_I = thr_mol_energy/2*65535;
    oversampling_factor = 4;
    psf_sigma = 1.3;
    %
    max_list = find_max_8(hiresim,thr_max_I); % [x,y,I]
    max_list = sort(max_list,3,'descend');
    mol_count = 0;
    fits = [];
    for mi = 1:size(max_list,1)
        [I,x,y] = find_component(hiresim,max_list(mi,:),floor(FWHM(psf_sigma)/2*oversampling_factor));
        % map the fitted parameters back to the original ranges
        mol_x = double(x) / double(oversampling_factor);
        mol_y = double(y) / double(oversampling_factor);
        mol_I = I / double(65535);  % max_val(uint16)
        % thr
        if mol_I < thr_mol_energy; continue; end;
        % push to the list
        mol_count = mol_count + 1;
        fits(mol_count,:) = [mol_x,mol_y,psf_sigma,mol_I];
        % subtract the component from the image
        %hiresim(y-1:y+1,x-1:x+1) = 0;    % not necessary
    end

end
%%
%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end
%%
% find maximum relatively to its 8-neighbourhood
function max_list = find_max_8(mol_image,thr_I)

    idx = findlocmax2d(mol_image,thr_I,8);
    [u,v] = ind2sub(size(mol_image),idx);
    max_list = [double(u),double(v),double(mol_image(idx))];

end
%%
% find component connected to the max in the mol_image
% - returns the total energy E and a final subpixel position [x,y]
% - max_dist is a maximal possible distance in X or Y from the center that
% will be included in the molecule
% - maximum is a [x,y,I], where [x,y] is position of a maxima, where a
% component has its initial center and I is intensity at the center
% - mol_image is an output image from CSSTORM method
% ================
% note: just for convenience we do not search the space for a connected
% component, as known from graph theory
function [E,x,y] = find_component(mol_image,maximum,max_dist)

    % boundary check
    max_dist = min([max_dist,maximum(1)-1,maximum(2)-1,size(mol_image,1)-maximum(1),size(mol_image,2)-maximum(2)]);
    %
    space = mol_image(maximum(1)-max_dist:maximum(1)+max_dist,maximum(2)-max_dist:maximum(2)+max_dist);
    space(space>(maximum(3)/2)) = 0;    % if the value is greater then half of a maxima, then it is probably another molecule
    %
    [gx,gy] = meshgrid(-max_dist:+max_dist,-max_dist:+max_dist);
    %
    E = maximum(3) + sum(space(:));
    x = maximum(1) + sum(sumNaN(space ./ maximum(3) ./ gx));
    y = maximum(2) + sum(sumNaN(space ./ maximum(3) ./ gy));

end
%%
function s = sumNaN(M)
    M(isnan(M)) = 0;
    M(isinf(M)) = 0;
    s = sum(M(:));
end