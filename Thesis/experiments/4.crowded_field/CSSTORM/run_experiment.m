function run_experiment(nmol,frames)

    % Initialize
	respath = '_results_';
    datapath = sprintf('../_data_/snr100_sigma1.3_mol%d_siz32x32',nmol);
    dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
    
    % Compressed Sensing STORM configuration
    cfg.CSSTORM.debug_mode = 1;
    cfg.CSSTORM.cvx_quiet = true;
    cfg.CSSTORM.ADU = 1;
    cfg.CSSTORM.base_line = 0;
    cfg.CSSTORM.oversampling = 4;%8; % CS "zoom-factor"
    %
    cfg.CSSTORM.boxsize = 11; % pixels
    cfg.CSSTORM.extramargin = 1;
    cfg.CSSTORM.scanoverlap = 2*cfg.CSSTORM.extramargin;
    %
    cfg.CSSTORM.psf_sigma = 1.3;
    cfg.CSSTORM.margin = 6;%ceil(FWHM(cfg.CSSTORM.psf_sigma));
    cfg.CSSTORM.eps = 5;
    %
    cfg.thr = double(1000/65535);
    
    decloc(datapath,respath,dataset,frames,cfg);

end
%%
function w = FWHM(sigma)
    w = 2*sqrt(2*log(2)) .* sigma;
end