%% Analyse performance of the localization on the static dots
function [TP,FN] = analyse_detections(datapath,respath,figpath,filename,frames)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), error('Results path does not exist!'); end
    if ~isdir(figpath), mkdir(figpath); end
    thr_dist = FWHM(1.3);   % sigmapsf=1.3 ==> width ~3px; this is not quite right, but there is no other way...
    TP = []; % true-positives
    FN = []; % false-negatives
    CTP = 0;
    CFN = 0;
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        if ~exist(sprintf('%s/%s.%04d.mat',datapath,filename,frame),'file'); continue; end;
        if ~exist(sprintf('%s/%s.%04d.mat',respath,filename,frame),'file'); continue; end;
        
        % load `dot_params`
        load(sprintf('%s/%s.%04d.mat',datapath,filename,frame));
        
        % load `fits`
        load(sprintf('%s/%s.%04d.mat',respath,filename,frame)); % `mol_img`
        fits = single(CS_postprocessing(mol_img));
        
        if ~isempty(fits)
            % Run the analysis
            % - pair the dots with its fits
            [D1,I1] = pdist2(dot_params(:,1:2),fits(:,1:2),'euclidean','Smallest',1);
            [D2,I2] = pdist2(fits(:,1:2),dot_params(:,1:2),'euclidean','Smallest',1);
            idxDots = ((I2(I1(I2))==I2) & (D2 < thr_dist));
            idxFits = unique(I2(idxDots));
            % =============================
            cid=0;
            indDots = [];
            for fi=1:length(idxFits)
                ii = idxDots & (I2==idxFits(fi));
                [val,ind] = min(D2(ii));
                temp = find(ii,ind,'first');    % transform index in D2(ii) to index in ii!! it's the k-th value
                indDots(cid+1) = temp(end);
                cid = cid + 1;
            end
            idxDots = zeros(size(dot_params,1),1);
            for ii = 1:length(indDots)
                idxDots = idxDots | ([1:size(dot_params,1)] == indDots(ii))';
            end
            % =============================
            % 3. add the TP,FP,FN
            if sum(idxDots)>0  % is there any TP?
                TP(CTP+1:CTP+sum(idxDots)) = dot_params(idxDots,5);   % assign density!
                CTP = CTP + sum(idxDots);
            end
            if sum(~idxDots)>0  % is there any FN?
                FN(CFN+1:CFN+sum(~idxDots)) = dot_params(~idxDots,5);   % assign density!
                CFN = CFN + sum(~idxDots);
            end
        else
            FN(CFN+1:CFN+size(dot_params,1)) = dot_params(:,5);   % assign density!
            CFN = CFN + size(dot_params,1);
        end

    end
    fprintf('Done.\n');

end

%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end