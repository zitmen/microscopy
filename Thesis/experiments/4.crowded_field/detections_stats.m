function [SMSTORM,DAOSTORM,CSSTORM] = detections_stats(nmol)
    addpath('CSSTORM');
    %
    nfits = 0;
    for ii = 1:100
        load(sprintf('C:/Users/Martin/Desktop/microscopy/Thesis/experiments/4.crowded_field/SMSTORM/_results_/snr100_sigma1.3_mol%d_siz32x32.%04d.mat',nmol,ii));
        nfits = nfits + size(fits,1);
    end
    SMSTORM = nfits/100;
    %
    nfits = 0;
    skipped = 0;
    for ii = 1:100
        try
            fits = dlmread(sprintf('C:/Users/Martin/Desktop/microscopy/Thesis/experiments/4.crowded_field/DAOSTORM/_results_/snr100_sigma1.3_mol%d_siz32x32.%04d.pos.out',nmol,ii),'\t',1,1);
            nfits = nfits + size(fits,1);
        catch err
            skipped = skipped + 1;
        end
    end
    DAOSTORM = nfits/(100-skipped);
    %
    nfits = 0;
    for ii = 1:100
        load(sprintf('C:/Users/Martin/Desktop/microscopy/Thesis/experiments/4.crowded_field/CSSTORM/_results_/snr100_sigma1.3_mol%d_siz32x32.%04d.mat',nmol,ii));
        fits = single(CS_postprocessing(mol_img));
        nfits = nfits + size(fits,1);
    end
    CSSTORM = nfits/100;
end