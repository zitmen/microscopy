% SMSTORM,DAOSTORM,CSSTORM must be analyzed first!!
function run_graph(frames)

    figpath = '_figures_tables_';
    methods = {'SMSTORM','DAOSTORM','CSSTORM'};
    
    nbins = 200;
    xlimit = [1 10];
    
    % detection
    subplot(2,2,1),graph_detection_TP_rel(nbins,xlimit,frames,figpath,methods);
    subplot(2,2,2),graph_detection_FP_abs(methods);
    % localization
    subplot(2,2,3),graph_localization(nbins,xlimit,frames,figpath,methods);
    subplot(2,2,4),graph_localization_normalized(nbins,xlimit,frames,figpath,methods);

end

function graph_detection_FP_abs(methods)

    linestyles = {':x', ':+', ':*'};
    colors = lines(length(methods));
    hold on;
    dataset = [2 3 5 7 10 15 20 25 30 35 40];
    detections = cell(length(methods),1);
    for di = 1:length(dataset)
        [detections{1}(di),detections{2}(di),detections{3}(di)] = detections_stats(dataset(di));
    end
    for mi = 1:length(methods)
        plot(dataset,detections{mi},linestyles{mi},'color',colors(mi,:));
    end
    xlabel('molecules per image'),ylabel('detections per image'); 
    legend(methods);
    xlim([2 40]);
    ylim([2 40]);
    hold off;

end

function graph_detection_TP_rel(nbins,xlimit,frames,figpath,methods)

    linestyles = {':x', ':+', ':*'};
    colors = lines(length(methods));
    hold on;
    for mi = 1:length(methods)
        X.TP = [];
        X.FN = [];
        for nmol = [2 3 5 7 10 15 20 25 30 35 40]
            dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
            load(sprintf('%s/%s/detections_res-%d-%d_%s.mat',methods{mi},figpath,frames(1),frames(end),dataset));   % detections
            X.TP = [X.TP(:);detections.TP(:)];
            X.FN = [X.FN(:);detections.FN(:)];
        end
        %
        bins = zeros(nbins,1);
        densities = [X.TP(:);X.FN(:)];
        lenTP = length(X.TP);
        idxBin = ceil(densities ./ (max(densities)/nbins));    % ceil --> 1:nbins
        for bi = 1:nbins
            idx = (idxBin==bi); % indices over the `densities`, i.e. [TP;FN]
            cTP = sum(idx(1:lenTP));
            cFN = sum(idx(lenTP+1:end));
            bins(bi) = cTP / (cTP + cFN);
        end
        %
        X = linspace(0,max(densities),nbins);
        X = 1./X;   % distance to density
        %
        invalid = isnan(bins);
        bins = bins(~invalid);
        X = X(~invalid);
        % ======================
        plot(X,bins,linestyles{mi},'color',colors(mi,:));
    end
    xlabel('density [px^{-2}]'),ylabel('recall = TP/(TP+FN)'); 
    xlim(xlimit);
    ylim([0.2 0.8]);
    legend(methods);
    hold off;

end

function graph_localization(nbins,xlimit,frames,figpath,methods)

    linestyles = {':x', ':+', ':*'};
    colors = lines(length(methods));
    hold on;
    for mi = 1:length(methods)
        X.density = [];
        X.dxy = [];
        for nmol = [2 3 5 7 10 15 20 25 30 35 40]
            dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
            load(sprintf('%s/%s/fits_res-%d-%d_%s.mat',methods{mi},figpath,frames(1),frames(end),dataset));   % precision
            X.density = [X.density(:);precision.density(:)];
            X.dxy = [X.dxy(:);precision.dxy(:)];
        end
        %
        bins = zeros(nbins,1);
        idxBin = ceil(X.density ./ (max(X.density)/nbins));    % ceil --> 1:nbins
        for bi = 1:nbins
            bins(bi) = sum(X.dxy(idxBin==bi)) / sum(idxBin==bi);
        end
        %
        X = linspace(0,max(X.density),nbins);
        X = 1./X;   % distance to density
        %
        invalid = isnan(bins);
        bins = bins(~invalid);
        X = X(~invalid);
        % ======================
        plot(X,bins,linestyles{mi},'color',colors(mi,:));
    end
    xlabel('density [px^{-2}]'),ylabel('<(\Delta [x,y])^2> [px]'); 
    xlim(xlimit);
    ylim([0.5 2.5]);
    legend(methods,'Location','NorthWest');
    hold off;

end

function graph_localization_normalized(nbins,xlimit,frames,figpath,methods)

    linestyles = {':x', ':+', ':*'};
    colors = lines(length(methods));
    hold on;
    for mi = 1:length(methods)
        X.density = [];
        X.dxy = [];
        for nmol = [2 3 5 7 10 15 20 25 30 35 40]
            dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
            load(sprintf('%s/%s/fits_res-%d-%d_%s.mat',methods{mi},figpath,frames(1),frames(end),dataset));   % precision
            X.density = [X.density(:);precision.density(:)];
            X.dxy = [X.dxy(:);precision.dxy(:)];
        end
        %
        bins = zeros(nbins,1);
        idxBin = ceil(X.density ./ (max(X.density)/nbins));    % ceil --> 1:nbins
        for bi = 1:nbins
            bins(bi) = sum(X.dxy(idxBin==bi)) / sum(idxBin==bi);
        end
        %
        X = linspace(0,max(X.density),nbins);
        X = 1./X;   % distance to density
        %
        invalid = isnan(bins);
        bins = bins(~invalid);
        X = X(~invalid);
        % ======================
        plot(X,bins./X',linestyles{mi},'color',colors(mi,:));
    end
    xlabel('density [px^{-2}]'),ylabel('<(\Delta [x,y])^2> [px / molcount]'); 
    xlim(xlimit);
    ylim([0 2]);
    legend(methods,'Location','NorthWest');
    hold off;

end