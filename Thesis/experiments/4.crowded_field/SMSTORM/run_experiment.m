function run_experiment(frames)

    boxsize = 5;
    psfsigma = 1.3;
    respath = '_results_';
    
    for nmol = [15 20 25 30 35 40]%[2 3 5 7 10]
        datapath = sprintf('../_data_/snr100_sigma1.3_mol%d_siz32x32',nmol);
        dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
        
        decloc(datapath,respath,dataset,frames,boxsize,psfsigma);
    end

end