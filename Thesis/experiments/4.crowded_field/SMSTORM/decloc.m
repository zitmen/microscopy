function decloc(datapath,respath,filename,frames,boxsize,sigmapsf)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), mkdir(respath); end
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        % load image
        imraw = double(imread(sprintf('%s/%s.%04d.tiff',datapath,filename,frame)))/65535; % 16b

        % detect molecules
        loc = detectMolecules(imraw);

        % store image
        [imfit,loc] = validateAndExtract(boxsize,imraw,loc);

        % fit model
        fits = fitting(boxsize,sigmapsf,imfit,loc);

        % save fits
        save(sprintf('%s/%s.%04d.mat',respath,filename,frame),'-v6','fits');

    end
    fprintf('Done.\n');

end

%%
function loc = detectMolecules(imraw)

    thr = 0.01;
    %{
    h = fspecial('gauss',11,1.3);
    im = imfilter(imraw,h,'replicate','same');
    idx = findlocmax2d(im);
    idx = idx(im(idx)>thr);
    [u,v] = ind2sub(size(im),idx);
    loc = [u,v];
    %}
    h = fspecial('average',3);
    im = imfilter(imraw,h,'replicate','same');
    %
    radius = 7;
    mx = imdilate(im,strel('square',radius));
    bordermask = zeros(size(im));
    bordermask(radius+1:end-radius, radius+1:end-radius) = 1;
    [u,v] = find((im==mx) & (im>thr) & bordermask);
    loc = [u,v];

end

%%
function [imfit,loc] = validateAndExtract(boxsize,im,loc)

    % init
    box = int32(-boxsize:+boxsize);
    imsize = size(im);
    fitregionsize = 2*boxsize+1;

    % remove points which are at the border location
    valid = ~( (loc(:,1)-boxsize) <        1  | (loc(:,2)-boxsize) <        1  | ...
               (loc(:,1)+boxsize) > imsize(1) | (loc(:,2)+boxsize) > imsize(2) );
    loc = loc(valid,:);
    
    % extract the valid molecules from the image
    imfit = zeros(size(loc,1),fitregionsize,fitregionsize);
    for i = 1:size(loc,1)
        pt = int32(loc(i,1:2));
        imfit(i,:,:) = im(pt(1)+box,pt(2)+box);
    end

end

%%
% OUTPUT: set of vectors - results of localization and fitting: [x_{loc},y_{loc},x_{fit},y_{fit},\sigma_{fit},I_{fit},bkg_{fit},\sum{chi2_{fit}},Nim_{fit},\mu_{bkg_{fit}},\sigma_{bkg_{fit}}]
function fits = fitting(boxsize,sigmapsf,imfit,loc)
    % init
    fitregionsize = 2*boxsize+1;
    npts = fitregionsize^2;
    [X(:,:,2),X(:,:,1)] = meshgrid(-boxsize:+boxsize,-boxsize:+boxsize);
    fits = single(zeros(size(imfit,1),11));
    nfits = 0;

    for i = 1:size(imfit,1)

        % fitting region
        im = reshape(imfit(i,:,:),fitregionsize,fitregionsize);

        % fit 2D gaussian bell shaped function
        a0 = [ 0,  0, sigmapsf, max(im(:)), min(im(:))];    % initial parameters
        [a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);

        % fitting error?
        if exitflag < 1 || exitflag > 3; continue; end;

        % estimate background at stored position
        imestim = gauss2d(X,a);
        Nim = sum(im(:) - a(5));  % Nimestim = sum(imestim(:) - a(5)); % is the same

        bkgestim = im - imestim;
        bkgmu = sum(bkgestim(:))/npts;
        bkgstd = sqrt(sum((bkgestim(:)-bkgmu).^2)/(npts-1));

        % position
        a(1:2) = a(1:2) + single(loc(i,1:2));
        a(4) = a(4) * 2*pi*a(3)^2;  % denormalizing the peak intensity (fitting runs with normalized Gaussian, i.e. I/(2 pi sigma^2))

        % save results
        nfits = nfits + 1;
        fits(nfits,:) = single([single(loc(i,1:2)), a, sum(chi2), Nim, bkgmu, bkgstd]);
  
    end
    
    if nfits > 0
        fits = fits(1:nfits,:);
    else
        fits = [];
    end
    
    %
    function y = gauss2d(X,a)
        y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);
    end

end