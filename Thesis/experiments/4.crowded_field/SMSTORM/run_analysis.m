% analysis of detection rate
% nmol -- datasets: 2,3,5,7,10 molecules
function run_analysis(frames)

    sigma_bkg = 0.0022;
    nbins = 20;
    %
    respath = '_results_';
    figpath = '_figures_tables_';
    %
    for nmol = [2 3 5 7 10 15 20 25 30 35 40]
        datapath = sprintf('../_data_/snr100_sigma1.3_mol%d_siz32x32',nmol);
        dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
        if ~exist(sprintf('%s/detections_res-%d-%d_%s.mat',figpath,frames(1),frames(end),dataset),'file')
            [TP,FN] = analyse_detections(datapath,respath,figpath,dataset,frames);    % diffs=[density0,I0,\sigma0,dx,dy,d\sigma,dI]
            detections.TP = TP;
            detections.FN = FN;
            save(sprintf('%s/detections_res-%d-%d_%s.mat',figpath,frames(1),frames(end),dataset),'-v6','detections');
        end
    end
    %
    % ===============
    % CRLB & Thompson
    SNR = 1:100;
    sigmapsf = 1.3;
    I = SNR .* sigma_bkg;
    a = 1;
    %{
    % ============================
    % Estimators evaluations - SNR
    load(sprintf('%s/fits_%s.mat',figpath,dataset));   % precision
    bins = zeros(nbins,1);
    X = precision;
    idxBin = ceil(X.SNR ./ (100/nbins));    % ceil --> 1:nbins
    for bi = 1:nbins
        bins(bi) = sum(X.dxy(idxBin==bi)) / sum(idxBin==bi);
    end
    %
    X = linspace(0,100,nbins);
    % ======================
    figure(1);
    hold on;
    plot(X,bins,':x');%,'Color',[0.3 0.8 0.8]);
    hold off;
    legend('SMSTORM');
    %axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]'); 
    % ======================
    %}
    % ================================
    % Estimators evaluations - density
    X.TP = [];
    X.FN = [];
    for nmol = [2 3 5 7 10 15 20 25 30 35 40]
        dataset = sprintf('snr100_sigma1.3_mol%d_siz32x32',nmol);
        load(sprintf('%s/detections_res-%d-%d_%s.mat',figpath,frames(1),frames(end),dataset));   % detections
        X.TP = [X.TP(:);detections.TP(:)];
        X.FN = [X.FN(:);detections.FN(:)];
    end
    %
    bins = zeros(nbins,1);
    densities = [X.TP(:);X.FN(:)];
    lenTP = length(X.TP);
    idxBin = ceil(densities ./ (max(densities)/nbins));    % ceil --> 1:nbins
    for bi = 1:nbins
        idx = (idxBin==bi); % indices over the `densities`, i.e. [TP;FN]
        cTP = sum(idx(1:lenTP));
        cFN = sum(idx(lenTP+1:end));
        bins(bi) = cTP / (cTP + cFN);
    end
    %
    X = linspace(0,max(densities),nbins);
    %
    invalid = isnan(bins);
    bins = bins(~invalid);
    X = X(~invalid);
    % ======================
    figure('Name','Detection rate');
    hold on;
    plot(X,bins,':x');%,'Color',[0.3 0.8 0.8]);
    hold off;
    legend('SMSTORM');
    %axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('density [px]'),ylabel('recall = TP/(TP+FN)');
    xlim([0 10]);
    % ======================

end