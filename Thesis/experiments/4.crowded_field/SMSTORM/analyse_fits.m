%% Analyse performance of the localization on the static dots
function precision = analyse_fits(datapath,respath,figpath,filename,frames)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), error('Results path does not exist!'); end
    thr_dist = FWHM(1.3);   % sigmapsf=1.3 ==> width ~3px
    precision = zeros(length(frames)*500,7); % [density0 I0 \sigma0 dX dY d\sigma dI], where 0-values are generated and d-values are differences between fitted and generated
    cprecision = 0;    % count of items in `precision`
    
    % run the analysis
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        if ~exist(sprintf('%s/%s.%04d.mat',datapath,filename,frame),'file'); continue; end;
        if ~exist(sprintf('%s/%s.%04d.mat',respath,filename,frame),'file'); continue; end;
        
        % load `dot_params`
        load(sprintf('%s/%s.%04d.mat',datapath,filename,frame));
        
        % load `fits`
        load(sprintf('%s/%s.%04d.mat',respath,filename,frame));
        
        if ~isempty(fits)
            % Run the analysis
            % 1. threshold
            %fits = thresholding(fits); % this is very problematic!!
            %       because the \sigma estimates of parabola or airy PSFs
            %       are very bad and it is only a guess
            % 2. pair the dots with its fits and get the deltas between them
            [D1,I1] = pdist2(dot_params(:,1:2),fits(:,3:4),'euclidean','Smallest',1);
            [D2,I2] = pdist2(fits(:,3:4),dot_params(:,1:2),'euclidean','Smallest',1);
            %
            diffs = diff_locs_fits(dot_params,fits,I1,I2,D1,D2,thr_dist);
            precision(cprecision+1:cprecision+size(diffs,1),:) = diffs(:,:);
            cprecision = cprecision + size(diffs,1);
        end

    end
    
    precision = precision(1:cprecision,:);  % get rid of unused rows

end

%%
function diffs = diff_locs_fits(dots,fits,idxFits2Dots,idxDots2Fits,distFits2Dots,distDots2Fits,thr_dist)

    idxDots = ((idxDots2Fits(idxFits2Dots(idxDots2Fits))==idxDots2Fits) & (distDots2Fits < thr_dist));
    idxFits = idxDots2Fits(idxDots);
    diffs = zeros(sum(idxDots),7);
    diffs(:,1) = dots(idxDots,5);
    diffs(:,2) = dots(idxDots,4);
    diffs(:,3) = dots(idxDots,3);
    diffs(:,4:7) = fits(idxFits,3:6) - dots(idxDots,1:4);

end

%%
function fits = thresholding(fits)

    a = 31.04;
    b = -15.68;
    c = 12.09;
    
    sigma = fits(:,5);
    I_norm = fits(:,6);% ./ (2*pi.*sigma(:));% s normalizaci to nefachci, protoze a,b,c bylo nafitovano asi bez ni
    SNR = I_norm ./ fits(:,11);
    
    fits = fits(((a + b.*sigma + c.*(sigma.^2)) < SNR),:);

end

%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end