%
% Experiment: Compare 2D Methods
%
% Description:
%   In this experiment I compare SMSTORM, CSSTORM and DAOSTORM
%   methods' F1 measures dependencies on SNR and molecule density.
%

cfg = config();

%% Datagen

run_datagen = cfg.run_datagen;  % get cfg value
if ~isdir(cfg.datapath)         % is there any dataset at all?
    mkdir(cfg.datapath);
    run_datagen = true;
end
if ~exist([cfg.datapath cfg.dataset cfg.image_extension],'file') || ...
   ~exist([cfg.datapath cfg.dataset '.mat'],'file')   % is there the required dataset(data+ground_truth)?
    run_datagen = true;
end

if run_datagen                  % run if necessary
%   datagen(SNR, molecule_density)
%   save image to [cfg.datapath cfg.dataset cfg.image_extension]
end

%% Fitting

run_fitting = cfg.run_fitting;  % get cfg value
if ~isdir(cfg.respath)          % are there any results at all?
    mkdir(cfg.respath);
    run_fitting = true;
end

% TODO: if !run_datagen then load the dataset; SMSTORM(dataset);

%if run_fitting || ~exist([cfg.respath 'SMSTORM-' cfg.dataset '.mat'],'file') % run if necessary (is there the required result file?)
%   SM_mol_list = SMSTORM(dataset_path);
%   save([cfg.respath 'SMSTORM-' cfg.dataset '.mat'],'-struct','SM_mol_list');
%end

CS_mol_list = [];
if run_fitting || ~exist([cfg.respath 'CSSTORM-' cfg.dataset '.mat'],'file') % run if necessary (is there the required result file?)
    CS_mol_list = CSSTORM([cfg.datapath cfg.dataset cfg.image_extension]);
    save([cfg.respath 'CSSTORM-' cfg.dataset '.mat'],'CS_mol_list');
end

%if run_fitting || ~exist([cfg.respath 'DAOSTORM-' cfg.dataset '.mat'],'file') % run if necessary (is there the required result file?)
%   DAO_mol_list = DAOSTORM(dataset_path);
%   save([cfg.respath 'DAOSTORM-' cfg.dataset '.mat'],'-struct','DAO_mol_list');
%end

%% Stats

if cfg.run_stats
    
    % TODO: in datagen rename the params structure to the ground_truth
    %load([cfg.datapath cfg.dataset '.mat'],'ground_truth');
    ground_truth = load([cfg.datapath cfg.dataset '.mat'],'params');
    % CHYBA! data v ground_truth jsou ulozena ve formatu [y,x,sigma,I] => swap
    tmp = ground_truth.params(:,1);
    ground_truth.params(:,1) = ground_truth.params(:,2);
    ground_truth.params(:,2) = tmp;
    % -----
    
    %if isempty(SM_mol_list), load([cfg.respath 'SMSTORM-' cfg.dataset '.mat'],'SM_mol_list'); end   % load if necessary
    %SM_F1 = statistics(SM_mol_list,ground_truth);
    
    if isempty(CS_mol_list), load([cfg.respath 'CSSTORM-' cfg.dataset '.mat'],'CS_mol_list'); end   % load if necessary
    CS_F1 = statistics(CS_mol_list,ground_truth.params);
    
    %if isempty(DAO_mol_list), load([cfg.respath 'DAOSTORM-' cfg.dataset '.mat'],'DAO_mol_list'); end   % load if necessary
    %DAO_F1 = statistics(DAO_mol_list,ground_truth);

    % TODO: plot - 3 in 1 for more convenient comparison
    
end
