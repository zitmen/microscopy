#!/usr/bin/env python
import pyfits
import numpy
import Image

for frame in range(1,10):
  #get the image and color information
  image = Image.open('snr1-100_sigma1.3.%04d.tiff' % frame)
  xsize, ysize = image.size
  data = image.getdata() # data is now an array of length ysize\*xsize

  # create numpy array
  np = numpy.reshape(data, (ysize, xsize))
  np = numpy.flipud(np);

  l = pyfits.PrimaryHDU(data=np)
  l.header.update('BITPIX', 16)
  l.writeto('snr1-100_sigma1.3.%04d.fits' % frame)
