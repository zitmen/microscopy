#! /usr/bin/python
import sys, os, numpy, pyfits,time, string
from pyraf import iraf
iraf.digiphot()
iraf.daophot()

def genPSFmodel(movieName, sigmaBG, thresh, frameNo = 0, nModelMolecule = 100, nIter = 4, fwhmpsf = 3.3, gain = 1, sharplo=0.0, sharphi=1.0, datamin = 1, datamax = 14000  ):
  """
  # genPSFmodel
  #   Description: Automatic generation of a model PSF for use by fitMovie
  #   Inputs:
  #     movieName	- Storm movie filename (NB: omit '.fits' extension)
  #     sigmaBG - Standard deviation of background noise per pixel (digital units)
  #     thresh - Threshold n, where molecules n*sigmaBG are analysed
  #     frameNo (default = 0) - Frame to use for PSF generation (zero-indexed)
  #     nModelMolecule (default = 100) - Maximum number of molecules to use for PSF generation
  #     nIter (default = 4) - Number of times to iterate the algorithm: 3-4 works fine.
  #     fwhmpsf (default = 3.3) - Estimate of full width half maximum of the molecular PSF in units of pixels
  #     gain (default = 1) - Camera gain (digital units per photon) - used in DAOPHOT noise model during least squares minimization
  #     sharplo(default =0.0) - The sharpness is ratio of the amplitude of the best fitting delta function at the
  position of a detected object to the amplitude of the best fitting gaussian at the same posi-
  tion. - Ie this gets rid of dodgy pixels on the camera, not typically a significant problem
  " 
  #     sharphi(default =1.0) - see sharplo. If your pixel size is very large compared to your PSF width you might want to consider increasing this so you dont delete points unnecessarily - or just set it to eg 100 to turn it off.
  #     datamin (default = 1) - Minimum "good" data value (used for example to exclude regions on the camera where light is blocked out, eg to separate emission channels)
  #     datamax (default = 14000) -  Maximum "good" data value
  #   Outputs:
  #     psfimage - Name of the model PSF file (omitting the '.fits' extension)
  #
  #   Example useage:
  #     psfName=genPSFmodel('mystormMovie', sigmaBG=20, thresh=10, frameNo = 509, nModelMolecule = 100, nIter = 4, fwhmpsf = 3.3, gain = 3.1, sharplo=0.0, sharphi=1.0, datamin = 400, datamax = 14000  )
  """

  # IRAF sets parameters as global values
  setStormParam(sigmaBG, thresh, fwhmpsf , gain, sharplo, sharphi, datamin, datamax)

  # open junk steam to throw away unused output
  fnull = open(os.devnull, 'w')
  
  # extract the single frame which we want to analyse
  # and copy it to the 1 frame dummy file
  frameName = movieName+'.'+str(frameNo)
  setupPSFinputImage(movieName, frameNo, frameName)

  print "Generating model PSF from frame %s in movie %s" % (str(frameNo), movieName)
  outputProduced = 1
  lastIterationWorked = 1
  i=0
  while i < nIter and outputProduced and lastIterationWorked : 

    if i > 0:
      #sort out the new and old names
      lastframePSFiter = framePSFiter #last iter substar image
      lastpsfimage = psfimage

    #define all the current filenames
    coordfile = frameName + ".coo." + str(i)
    photfile = frameName + ".mag." +str(i)
    allstarfile = frameName + ".als." +str(i)
    subimage = frameName + ".sub." +str(i)
    rejectfile = frameName + ".arj." +str(i)
    pstfile = frameName + ".pst." +str(i)
    opstfile =  frameName + ".opst." +str(i)
    psfimage = frameName + ".psf." +str(i)
    groupfile = frameName + ".psg." +str(i)
    framePSFiter = frameName + ".psfiter."+str(i)

    #remove the files if they exist - to stop iraf crashing
    try:os.remove(coordfile)
    except:dumVar=0 #do nothing
    try:os.remove(photfile)
    except:dumVar=0 #do nothing
    try:os.remove(allstarfile)
    except:dumVar=0 #do nothing
    try:os.remove(rejectfile)
    except:dumVar=0 #do nothing
    try:os.remove(pstfile)
    except:dumVar=0 #do nothing
    try:os.remove(opstfile)
    except:dumVar=0 #do nothing
    try:os.remove(groupfile)
    except:dumVar=0 #do nothing
    try:os.remove(subimage+".fits")
    except:dumVar=0 #do nothing
    try:os.remove(psfimage+".fits")
    except:dumVar=0 #do nothing
    try:os.remove(framePSFiter+".fits")
    except:dumVar=0 #do nothing
   
    iraf.digiphot.daophot.psf.inter = "no"
    iraf.digiphot.daophot.pstselect.inter = "no"
    
    if i == 0:	#first run
      try:
	iraf.digiphot.daophot.daopars.varorder=-1 # pure analytic psf
	iraf.daofind(frameName, coordfile,verify="no",Stdout=fnull,Stderr=fnull) # detect stars
	iraf.phot(frameName, coordfile, photfile,verify="no",Stdout=fnull,Stderr=fnull) # get rough magnitudes
	iraf.pstselect(frameName, photfile, pstfile, nModelMolecule,verify="no") # get candidate stars
	iraf.psf(frameName, photfile, pstfile, psfimage, opstfile, groupfile, verify="no") # obtain model psf
	iraf.allstar( frameName, photfile, psfimage, allstarfile, rejectfile, subimage, verify="no") # fit the image
	# subtract detected stars EXCEPT THE MODEL PSFS
  	iraf.substar(frameName, allstarfile, opstfile, psfimage, framePSFiter, verify="no")
      except:
	outputProduced = 0
    else:
      try:
	iraf.digiphot.daophot.daopars.varorder=0 # empirical + analytical psf
	iraf.daofind(lastframePSFiter, coordfile,verify="no",Stdout=fnull,Stderr=fnull) # detect stars
	iraf.phot(lastframePSFiter, coordfile, photfile,verify="no",Stdout=fnull,Stderr=fnull) # get rough magnitude
	if os.path.isfile(photfile) :#photfile has been produced ie additional stars detected in the subimage
	  iraf.pstselect(lastframePSFiter, photfile, pstfile, nModelMolecule,verify="no") # get candidate stars
	  iraf.psf(lastframePSFiter, photfile, pstfile, psfimage, opstfile, groupfile, verify="no") #obtain the psf
	  iraf.allstar( lastframePSFiter, photfile, psfimage, allstarfile, rejectfile, subimage, verify="no")# fit the image
	  iraf.substar(lastframePSFiter, allstarfile, opstfile, psfimage, framePSFiter, verify="no") # subtract detected stars EXCEPT THE MODEL PSFS


	  # remove the remaining intermediate files
	  os.remove(lastframePSFiter+'.fits')
	  os.remove(lastpsfimage+'.fits')
	  
	else:# no additional molecules detected
	  psfimage = lastpsfimage
	  lastpsfimage = ""
	  lastIterationWorked = 0
      except:# if the loop crashes (because no more molecules were found) use the data from the last iteration 
	psfimage = lastpsfimage
	lastpsfimage = ""
	lastIterationWorked = 0

    #remove the intermediate files
    try:os.remove(coordfile)
    except:dumVar=0 #do nothing
    try:os.remove(photfile)
    except:dumVar=0 #do nothing
    try:os.remove(rejectfile)
    except:dumVar=0 #do nothing
    try:os.remove(allstarfile)
    except:dumVar=0 #do nothing
    try:os.remove(pstfile)
    except:dumVar=0 #do nothing
    try:os.remove(opstfile)
    except:dumVar=0 #do nothing
    try:os.remove(groupfile)
    except:dumVar=0 #do nothing
    try:os.remove(subimage+".fits")
    except:dumVar=0 #do nothing
    i = i+1
  
  if outputProduced == 0:
    psfimage = ""

  fnull.close()
  return psfimage


def fitMovie(movieName,psfFile, sigmaBG, thresh, nIter=4,fwhmpsf = 3.3, gain = 1, sharplo=0.0, sharphi=1.0, datamin = 1, datamax = 14000  ):
  """
  # fitMovie
  #   Description: Preforms crowded-field super-resolution localization
  #     Results are saved to a text file <moviename>.pos.out.
  #   Inputs:
  #     movieName - Storm movie filename (NB: omit '.fits' extension)
  #     psfFile - Name of the model PSF file (omitting the '.fits' extension)
  #     sigmaBG - Standard deviation of background noise per pixel (digital units)
  #     thresh   - Threshold n, where molecules n*sigmaBG are analysed
  #     nIter(default =4) - Number of times to iterate the algorithm: 4 should work fine.
  #     fwhmpsf (default = 3.3) - Estimate of full width half maximum of the molecular PSF in units of pixels
  #     gain (default = 1)- Camera gain (digital units per photon) - used in DAOPHOT noise model during least squares minimization
  #     sharplo(default =0.0) - The sharpness is ratio of the amplitude of the best fitting delta function at the
  position of a detected object to the amplitude of the best fitting gaussian at the same posi-
  tion. - Ie this gets rid of dodgy pixels on the camera, not typically a significant problem
  #     sharphi(default =1.0) - see sharplo. If your pixel size is very large compared to your PSF width you might want to consider increasing this so you dont delete points unnecessarily - or just set it to eg 100 to turn it off.
  #     datamin (default = 1)  - Minimum "good" data value (used for example to exclude regions on the camera where light is blocked out, eg to separate emission channels)
  #     datamax (default = 14000) -  Maximum "good" data value
  #   Outputs:
  #      outputfile - Name of text file containing DAOSTORM localization results 
  """

  print "Analysis begun at %s" % time.asctime(time.localtime())
  timeStart = time.time()

  setStormParam(sigmaBG, thresh, fwhmpsf, gain, sharplo, sharphi, datamin, datamax)

  outputfile = movieName+".pos.out"
  #open the storm movie
  #TODO: process this frame by frame for memory efficiency
  print "Loading whole movie into memory"
  stormMovieHDU = pyfits.open(movieName+".fits")
  stormdata = stormMovieHDU[0].data

  hasNAXIS3 = 1
  try:
    nFrame = stormMovieHDU[0].header['NAXIS3']
  except: # ie only 1 frame
    nFrame = 1
    hasNAXIS3 = 0
  
  #nframe = 3 #for now
  stormMovieHDU.close()
  
  #set up the 1 frame dum file - this is because IRAF only processes single frame
  # data, not movies so have to copy each frame of the movie to a single frame file
  dumframe = "dumframe"
  dumhdu = makedumframe(stormMovieHDU,dumframe)

  print "Analysing %s frames" % str(nFrame)
  #for all frames
  for i in range(nFrame): # note we address frames 0->nFrame-1
    print "%s\t/\t%s" % (str(i+1), str(nFrame) )
    # copy the current frame to a dum file
    copycurrentframe(stormdata, dumhdu,i, hasNAXIS3)
    
    #analyse the current frame
    alsfile = fit1frame(dumframe,psfFile,nIter)
    #parse the data on the current frame and append it to outputfile
    parsefitresults(alsfile, outputfile, i)

  dumhdu.close() # dont forget to close the dum file
  del stormdata
  
  print "Analysis finished at %s" % time.asctime(time.localtime())
  timeEnd = time.time()
  print "Run time : %s" % str(timeEnd - timeStart)

  return outputfile

def fit1frame(frameName,psfFile, nIter):
  """
  # fit1frame
  #   description: Carry out localization on a single frame.
  #   inputs:
  #     frameName - Name of single frame for DAOSTORM analysis (NB: omit '.fits' extension)
  #     psfFile - Name of the model PSF file (omitting the '.fits' extension)
  #     nIter - Number of times to iterate the algorithm: 4 should work fine.
  #   outputs:
  #     allstarfile - Name of file containing DAOSTORM localization data
  """
 
  #define a null output
  fnull = open(os.devnull, 'w')
  
  # apply the user set threshold

  outputProduced = 1
  lastIterationWorked = 1
  i=0
  while i < nIter and outputProduced and lastIterationWorked : 
    if i > 0:
      #sort out the new and old names
      lastsubimage = subimage #last iter substar image
      lastallstarfile = allstarfile#last iter allstarfile
      updatedalsfile = frameName + ".als.pre." +str(i)
      try:os.remove(updatedalsfile)
      except:dumVar=0 #do nothing

    #define all the current filenames
    coordfile = frameName + ".coo." + str(i)
    photfile = frameName + ".mag." +str(i)
    allstarfile = frameName + ".als." +str(i)
    subimage = frameName + ".sub." +str(i)
    rejectfile = frameName + ".arj." +str(i)

    #remove the files if they exist
    try:os.remove(coordfile)
    except:dumVar=0 #do nothing
    try:os.remove(photfile)
    except:dumVar=0 #do nothing
    try:os.remove(allstarfile)
    except:dumVar=0 #do nothing
    try:os.remove(rejectfile)
    except:dumVar=0 #do nothing
    try:os.remove(subimage+".fits")
    except:dumVar=0 #do nothing
 
    try:
      if i == 0:  # first run - examine the original image
	iraf.daofind(frameName, coordfile,verify="no",Stdout=fnull,Stderr=fnull) # detect stars
	iraf.phot(frameName, coordfile, photfile,verify="no",Stdout=fnull,Stderr=fnull) # get rough magnitudes
	iraf.allstar(frameName, photfile, psfFile,allstarfile,rejectfile,subimage,verify="no",Stdout=fnull,Stderr=fnull)#run full fitting program
      else: # need to examin the residuals image
	iraf.daofind(lastsubimage, coordfile,verify="no",Stdout=fnull,Stderr=fnull) # detect stars
	iraf.phot(frameName, coordfile, photfile,verify="no",Stdout=fnull,Stderr=fnull) # get rough magnitudes
	if os.path.isfile(photfile) :#photfile has been produced ie additional stars detected in the subimage
	  mergelist = lastallstarfile +"," +photfile
	  iraf.pfmerge(mergelist, updatedalsfile,Stdout=fnull)# add the new stars detected in residuals image to original list
	  iraf.prenumber(updatedalsfile,Stdout=fnull)#renumber the stars in updated file
	  iraf.allstar(frameName, updatedalsfile, psfFile,allstarfile,rejectfile,subimage,verify="no",Stdout=fnull,Stderr=fnull)#run fit on updated list
	else:# no additional molecules detected
	  allstarfile = lastallstarfile
	  lastallstarfile = ""
	  lastIterationWorked = 0
    except: # no molecules detected at all
      outputProduced = 0
    print "."  
    
    #remove the intermediate files
    try:os.remove(coordfile)
    except:dumVar=0 #do nothing
    try:os.remove(photfile)
    except:dumVar=0 #do nothing
    try:os.remove(rejectfile)
    except:dumVar=0 #do nothing
    
    if i > 0:
      try:os.remove(updatedalsfile)
      except:dumVar=0 #do nothing
      try:os.remove(lastallstarfile)
      except:dumVar=0 #do nothing
      try:os.remove(lastsubimage+".fits")
      except:dumVar=0 #do nothing
    
    i = i+1

  if outputProduced == 0:
      allstarfile = ""

  #close the null output
  fnull.close()
  return allstarfile

def setStormParam(sigmaBG, thresh, fwhmpsf = 3.3, gain = 1, sharplo=0.0, sharphi=1.0, datamin = 1, datamax = 14000 ):
  """
  # setStormParam
  #   Description: Set the fitting parameters (pyRAF/ IRAF uses global
  #     variables for parameter values).
	These parameters are all based on the recommendations of 
	\"A Reference Guide to the IRAF/DAOPHOT Package\", by Lindsey E Davis,
	http://iraf.net/irafdocs
  #   Inputs:
  #     sigmaBG - Standard deviation of background noise per pixel (digital units)
  #     thresh   - Threshold n, where molecules n*sigmaBG are analysed
  #     fwhmpsf (default = 3.3) - Estimate of full width half maximum of the molecular PSF in units of pixels
  #     gain (default = 1)- Camera gain (digital units per photon) - used in DAOPHOT noise model during least squares minimization
  #     sharplo(default =0.0) - The sharpness is ratio of the amplitude of the best fitting delta function at the
  position of a detected object to the amplitude of the best fitting gaussian at the same posi-
  tion. - Ie this gets rid of dodgy pixels on the camera, not typically a significant problem
  #     sharphi(default =1.0) - see sharplo. If your pixel size is very large compared to your PSF width you might want to consider increasing this so you dont delete points unnecessarily - or just set it to eg 100 to turn it off.
  #     datamin (default = 1)  - Minimum "good" data value (used for example to exclude regions on the camera where light is blocked out, eg to separate emission channels)
  #     datamax (default = 14000) -  Maximum "good" data value
  #   Outputs:
  """
  # load each frame into memory hope this speeds things up
  iraf.digiphot.apphot.cache ="yes"
  iraf.digiphot.daophot.daoedit.cache = "yes"

  # set the appropriate fit parameters - these parameters are all based on the recommendations of (1)
  # "A Reference Guide to the IRAF/DAOPHOT Package", by Lindsey E Davis,
  # http://iraf.net/irafdocs
  iraf.digiphot.daophot.psf.inter="no"
  iraf.digiphot.daophot.daopars.varorder=0 # empirical psf, not spatially variant
  iraf.digiphot.daophot.daopars.psfrad= 4.0*fwhmpsf +1.0 #as per (1)
  iraf.digiphot.daophot.daopars.fitrad= max(fwhmpsf,3.0) #as per (1)
  iraf.digiphot.daophot.findpars.sharplo = sharplo
  iraf.digiphot.daophot.findpars.sharphi = sharphi
  iraf.digiphot.daophot.findpars.roundlo = -100.0 # default is to turn these off as they were 
  iraf.digiphot.daophot.findpars.roundhi = 100.0  # originally intended to filter out galaxies - not a common problem in our case
  iraf.digiphot.daophot.photpar.zmag = 0
  iraf.digiphot.daophot.photpar.apertures = max(fwhmpsf,3.0) #as per (1)
  iraf.digiphot.daophot.datapar.xairmass = 0 
  iraf.digiphot.daophot.datapar.itime = 0 
  iraf.digiphot.daophot.datapar.otime = 'INDEF'
  iraf.digiphot.daophot.datapar.readnoise = 0.1 #emccd so negigible read noise
  iraf.digiphot.daophot.datapar.epadu = gain
  iraf.digiphot.daophot.datapar.fwhmpsf = fwhmpsf #as per (1)

  iraf.digiphot.daophot.datapar.datamax = 14000.0

  iraf.digiphot.daophot.fitskypars.salgorithm= "mode"
  iraf.digiphot.daophot.fitskypars.annulus  = 4*(fwhmpsf-0.1) #very similar to (1) - (1) says 4*fwhmpsf, I found this works slightly better
  iraf.digiphot.daophot.fitskypars.dannulus = 2.5*(fwhmpsf-0.1) #as per (1) - (1) says 2.5*fwhmpsf, I found this works slightly better

  iraf.digiphot.daophot.centerpars.calgorithm = "none"
  iraf.digiphot.daophot.centerpars.cbox = max(2*fwhmpsf,5.0) #as per (1)

  iraf.digiphot.daophot.datapar.sigma = sigmaBG
  iraf.digiphot.daophot.findpars.threshold = thresh
  iraf.digiphot.daophot.datapar.datamin = datamin

  iraf.digiphot.daophot.daopars.maxnstar = 10000 #default daophot value
  iraf.digiphot.daophot.daopars.maxgroup = 60 #default daophot value
  iraf.digiphot.daophot.daopars.maxiter  = 50 #default daophot value
  iraf.digiphot.daophot.daopars.fitsky = "no" # default - changing this to "yes" could potentially increase recall/ RMS precision a little, at cost of fitting speed (Stetson, 1987), havent quantified this yet.
  ########################## 
  

  return 

def parsefitresults(alsfile, outputfile, frameno):
  """
  # parsefitresults
  #   Description: Append the DAOSTORM localization results of a single frame a text file.
  #   Inputs:
  #     alsfile - DAOSTORM output results for a single frame, in DAOPHOT format
  #     outputfile - Output file for localization results
  #     frameno - Current frame of movie (zero-indexed)
  #   Outputs:
  """
  if alsfile == "": # if empty string returned then no points detected for this frame
    return # no mols detected so do nothing
  else:
    # get all the columns from als
    xcenter=parseDaophot(alsfile,"xcenter")
    ycenter=parseDaophot(alsfile,"ycenter")
    mag=parseDaophot(alsfile,"mag")
    merr=parseDaophot(alsfile,"merr")
    msky=parseDaophot(alsfile,"msky")
    niter=parseDaophot(alsfile,"niter")
    sharpness=parseDaophot(alsfile,"sharpness")
    chi=parseDaophot(alsfile,"chi")
    pier=parseDaophot(alsfile,"pier")
    
    # work out how many localizations we have
    nmol = len(xcenter)

    # open the file for writing
    if frameno == 0: #first frame
      f = open(outputfile,"w")
    else:
      f = open(outputfile,"a")

    if frameno == 0:  #add the column names 
      # print all the columns on one line
      linedata= "FRAME\t" 
      linedata= linedata+ "XCENTER\t"
      linedata= linedata+ "YCENTER\t"
      linedata= linedata+ "BRIGHTNESS\t"
      linedata= linedata+ "BERR\t"
      linedata= linedata+ "MSKY\t"
      linedata= linedata+ "NITER\t"
      linedata= linedata+ "SHARP\t"
      linedata= linedata+ "CHI\t"
      linedata= linedata+ "ERR\t"
      linedata= linedata+ "\n"
      f.write(linedata)

    for i in range(nmol): 
      # filter out any indef strings to stop matlab crashing
      if xcenter[i] == "INDEF" : xcenter[i] = "NaN"
      if ycenter[i] == "INDEF" : ycenter[i] = "NaN"
      if mag[i] == "INDEF" : 
	mag[i] = "NaN"
      else: # convert from stellar magnitude to camera counts (digital units)
	mag[i] = str(10**(-float(mag[i])/2.5))
      if merr[i] == "INDEF" : 
	merr[i] = "NaN"
      else: # convert from stellar magnitude to camera counts (digital units)
	merr[i] = str(10**(-float(merr[i])/2.5))
      if msky[i] == "INDEF" : msky[i] = "NaN"
      if niter[i] == "INDEF" : niter[i] = "NaN"
      if sharpness[i] == "INDEF" : sharpness[i]= "NaN"
      if chi[i] == "INDEF" : chi[i] = "NaN"
      if pier[i] == "INDEF" : pier[i] = "NaN"
      # print all the columns on one line
      linedata= "%s\t" % str(frameno) # add the frame number
      linedata= linedata+ "%s\t" % str(xcenter[i])
      linedata= linedata+ "%s\t" % str(ycenter[i])
      linedata= linedata+ "%s\t" % str(mag[i])
      linedata= linedata+ "%s\t" % str(merr[i])
      linedata= linedata+ "%s\t" % str(msky[i])
      linedata= linedata+ "%s\t" % str(niter[i])
      linedata= linedata+ "%s\t" % str(sharpness[i])
      linedata= linedata+ "%s\t" % str(chi[i])
      linedata= linedata+ "%s\t" % str(pier[i])
      linedata= linedata+ "\n"
      f.write(linedata)

    f.close()
    return
   
def makedumframe(stormMovieHDU,dumframe):
  """
  # makedumframe
  #   Description: Create a single frame fits file for temporary storage of each movie frame.
  #   Inputs:
  #     stormMovieHDU - Object containing data for current movie
  #     dumframe - Name of dummy single frame for data analysis.
  #   Outputs:
  """
  #delete dumframe
  try:os.remove(dumframe+".fits")
  except:dumVar=0 #do nothing

  naxis1 = stormMovieHDU[0].header['NAXIS1']
  naxis2 = stormMovieHDU[0].header['NAXIS2']
  
  #construct a single frame of the correct dimensions
  dumdata = numpy.zeros((naxis2,naxis1))
  #wrap an hdu and hdu list around it
  dumhdu = pyfits.PrimaryHDU(dumdata)
  #dumhdu.scale('int16', '', bzero=32768) #not sure whether this is necessary
  dumhdulist = pyfits.HDUList([dumhdu])
  dumhdulist.writeto(dumframe+".fits")
  dumhdu = pyfits.open(dumframe+".fits", mode='update')#open in writable mode 

  return dumhdu
  
def copycurrentframe(stormdata, dumhdu,frameno,hasNAXIS3):
  """
  # copycurrentframe
  #   Description: Copy current frame to single frame file used for daostorm analysis
  #   Inputs:
  #     stormdata - Name of current movie (omit '.fits')
  #     dumhdu - Object containg dummy single frame data
  #     frameno - stormdata frame to be copied (zero-indexed notation)
  #     hasNAXIS3 - this is a sanity check to test whether theres more than 1 frame, otherwise
  #		 have to address the data differently 
  #   Outputs:
  """

  if hasNAXIS3 == 1:
    dumhdu[0].data = stormdata[frameno] # overwrite dumframe with current storm frame
  else:
    dumhdu[0].data = stormdata # overwrite dumframe with current storm frame
  
  dumhdu.flush() #update the dumframe
  return


def parseDaophot(filename,keyword):
  """
  # parseDaophot
  #   Description: Extract data from daophot output files 
  #     nominally, iraf.pdump performs this task, but it crashes occasionally and
  #     unpredictably so i wrote my own version.
  #   Inputs:
  #     filename - name of daophot output file
  #     keyword - keyword of data to extract from output file
  #   Outputs:
  #     dataOut - array of all entries matching keyword
  """

  #this should be really straightforward but daophot decides to put things on 
  #multiple lines so its a bit of a faff
  fid = open(filename)
  
  keylinepos=[]
  keyline = []
  ######find the number of #N lines
  #get first line
  startofline=fid.tell();
  line = fid.readline()
  while line[0:1]=='#': 
    if line[0:2]=='#N': # add the line to the list
      keyline = keyline + [line]
    line = fid.readline()
  nkeyline = len(keyline)

  keywordpos = [-1,-1]
  ###### get the keyword position and line number
  for i in range(nkeyline):
    pos = string.find(keyline[i].lower(),keyword.lower())
    if pos != -1:
      keywordpos[0] = i #line number
      keywordpos[1] = pos #line position
  if keyword.lower()=='id' and keywordpos[1]==3: 
    keywordpos[1]=0# hack because if id is first keyword, its at the start of the line

  if keywordpos == [-1, -1]: 
    raise Exception('Keyword '+ keyword +' not found')
  
  ###### now that we know line number and keyword position find all of the data in that position
  #line is already the first line of data
  dataOut =[]
  while line != '':
    for i in range(keywordpos[0]): # move an extra n lines to find the correct data line
      line = fid.readline()
    # need to find the next space from the keyword pos - results are space delimited
    nextSpacePos = string.find(line[keywordpos[1]:],' ')
    currentData = line[keywordpos[1]:keywordpos[1]+nextSpacePos]
    dataOut = dataOut + [currentData]
    #skip to the next group of data lines
    for i in range(nkeyline-keywordpos[0]):
      line = fid.readline()
  
  return dataOut

def setupPSFinputImage(movieName, frameNo, frameName):
  """
  # setupPSFinputImage 
  #   Description: Make a copy of the single frame used for PSF generation
  #   Inputs:
  #     movieName - name of storm movie
  #     frameNo - frame number for PSF generation (zero-indexed)
  #     frameName - name of single frame file for PSF generation
  #   Outputs:
  """
  # extract the single frame which we want to analyse
  # and copy it to the 1 frame dummy file
  stormMovieHDU = pyfits.open(movieName+".fits")
  stormdata = stormMovieHDU[0].data

  hasNAXIS3 = 1
  try:
    nFrame = stormMovieHDU[0].header['NAXIS3']
  except: # ie only 1 frame
    nFrame = 1
    hasNAXIS3 = 0
  
  stormMovieHDU.close()
  
  if frameNo >= nFrame:
    errorStr='Attempted to access frame '+str(frameNo)+'. '+movieName+' contains only '+str(nFrame)+' frames'
    raise Error(errorStr)

  dumhdu = makedumframe(stormMovieHDU,frameName)
  copycurrentframe(stormdata, dumhdu,frameNo, hasNAXIS3)
  
  return


