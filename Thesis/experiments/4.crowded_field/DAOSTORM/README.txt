README file for DAOSTORM v1.0
Seamus J Holden, 29-10-2010
Copyright (C) 2010, Isis Innovation Limited.
All rights reserved.

A detailed installation guide may be found in 'INSTALL.txt'.
The manual is 'daostorm_users_manual.pdf'.
The DAOSTORM software is a python module 'daostorm.py'.
Test code and data is supplied: 'testdaostorm.py' and 'stormData1.519.fits'. See 'INSTALL.txt' for details.

DAOSTORM is released under an “academic use only” license; for details please see the accompanying ‘DAOSTORM_LICENSE.doc’. Usage of the software requires acceptance of this license.

If you use this software in work leading to scientific publication, please cite:
"Holden, S. J., Uphoff, S. & Kapanidis, A. N. DAOSTORM: an algorithm for high density super-resolution microscopy, Nat. Methods (2011)"
