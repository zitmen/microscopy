#!/usr/bin/env python
import sys, os, numpy, pyfits,time, string, Image


image = pyfits.open("stormData1.519.fits")
frame0_data = numpy.reshape(image[0].data, (256, 256))
image.close()
  
im = Image.frombuffer('L', (256,256), frame0_data, 'raw', 'L', 0, 1)
im.show();

del im    
del frame0_data