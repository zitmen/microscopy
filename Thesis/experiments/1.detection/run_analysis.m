function run_analysis(frames)

    datapath = '_data_';
    respath = '_results_';
    figpath = '_figures_tables_';
    dataset = 'snr1-100_sigma0.3-2.2';
    thresholding = false;

	detectors{1} = 'MAXN4_thr0.0';
    detectors{2} = 'MAXN8_thr0.0';
    detectors{3} = 'MORPHO3_thr0.01';
    detectors{4} = 'MORPHO5_thr0.01';
    detectors{5} = 'MORPHO7_thr0.01';
	%
    filters{1} = 'NONE';
    filters{2} = 'BOX3';
    filters{3} = 'BOX5';
    filters{4} = 'BOX7';
    filters{5} = 'MEDIAN_CROSS';
    filters{6} = 'MEDIAN_BOX3';
    filters{7} = 'MEDIAN_BOX5';
    filters{8} = 'MEDIAN_BOX7';
    filters{9} = 'GAUSSIAN7_sigma0.7';
    filters{10} = 'GAUSSIAN11_sigma1.3';
    filters{11} = 'GAUSSIAN15_sigma2.0';
    filters{12} = 'GAUSSIAN19_sigma2.6';
    filters{13} = 'GAUSSIAN24_sigma3.5';
    filters{14} = 'GAUSSIAN7_LOW_sigma0.7';
    filters{15} = 'GAUSSIAN11_LOW_sigma1.3';
    filters{16} = 'GAUSSIAN15_LOW_sigma2.0';
    filters{17} = 'GAUSSIAN19_LOW_sigma2.6';
    filters{18} = 'GAUSSIAN24_LOW_sigma3.5';
    filters{19} = 'GAUSSIAN7_LOW_TRUNC_sigma0.7';
    filters{20} = 'GAUSSIAN11_LOW_TRUNC_sigma1.3';
    filters{21} = 'GAUSSIAN15_LOW_TRUNC_sigma2.0';
    filters{22} = 'GAUSSIAN19_LOW_TRUNC_sigma2.6';
    filters{23} = 'GAUSSIAN24_LOW_TRUNC_sigma3.5';
    %
    % Butterworth filter
    sigma = [48 49 50 51 52];
    gamma = [3.5 4.0 4.5];
    ii = length(filters);
    for si=1:length(sigma)
        for gi=1:length(gamma)
            ii = ii + 1;
            filters{ii} = sprintf('BLPF_sigma%d_gamma%.1f',sigma(si),gamma(gi));
        end
    end
    %{
    F1 = zeros(length(filters),length(detectors));
    for di=1:length(detectors)
        for fi=1:length(filters)
            F1(fi,di) = analyse(datapath,respath,figpath,dataset,frames,filters{fi},detectors{di},false);
            if isnan(F1(fi,di)); F1(fi,di) = 0; end;
        end
    end
    save(sprintf('%s/%s.mat',figpath,dataset),'-v6','F1');
    %}
    load(sprintf('%s/%s.mat',figpath,dataset));   % F1
    %
    % Results sorted by F1
    [V,I] = sort(F1(:),'descend');
    [fi,di] = ind2sub(size(F1),I);
    % -- save to file & print to stdout
    fid = fopen(sprintf('%s/%s.txt',figpath,dataset),'w+');
        fprintf('\n=============================================\n');
        fprintf(fid,'F1\tFILTER\tDETECTOR\n');
        for ii=1:length(fi)
            fprintf('F1=%.6f -- %s + %s\n',V(ii),filters{fi(ii)},detectors{di(ii)});
            fprintf(fid,'%.6f\t%s\t%s\n',V(ii),filters{fi(ii)},detectors{di(ii)});
        end
    fclose(fid);
    
    % Plot SNR/\sigma graph for the winning combination
    best_idx = 5;
    if thresholding
        % thresholding for box3+morpho7 does perform poorly...better not to use that
        %thr = [31.04,-15.68,12.09];   % from the original paper (for Gauss(2.6))
        [thr,F1] = estimate_threshold(datapath,respath,dataset,frames,filters{fi(best_idx)},detectors{di(best_idx)});
        fprintf('Estimated parameters for nonlinear threshold: a=%.2f, b=%.2f,c=%.2f; F1=%.6f\n',thr(1),thr(2),thr(3),F1);
        figure('Name','Best filter+detector pair F1 (after threshold)'),graph(datapath,respath,dataset,frames,filters{fi(best_idx)},detectors{di(best_idx)},thr);
    else
        figure('Name','Best filter+detector pair F1'),graph(datapath,respath,dataset,frames,filters{fi(best_idx)},detectors{di(best_idx)},[]);
    end
    
    % Best detectors (average F1)
    detectors_avg_F1 = mean(F1,1);
    [V,I] = sort(detectors_avg_F1,'descend');
    % -- print to stdout
    fprintf('\n==== BEST DETECTORS =========================\n');
    for ii=1:length(I)
        fprintf('avg F1=%.6f -- %s\n',V(ii),detectors{I(ii)});
    end
    %figure('Name','Best detectors'),ylabel('F1-score'),bar(detectors_avg_F1),set(gca,'XtickLabel',detectors);
    %figure('Name','Best detectors'),ylabel('F1-score'),bar(V),set(gca,'XtickLabel',{'max_8','max_4','morpho_7','morpho_5','morpho_3'});
    
    % Best filters (average F1)
    filters_avg_F1 = mean(F1,2);
    [V,I] = sort(filters_avg_F1,'descend');
    % -- print to stdout
    fprintf('\n==== BEST FILTERS ===========================\n');
    for ii=1:length(I)
        fprintf('avg F1=%.6f -- %s\n',V(ii),filters{I(ii)});
    end
    %figure('Name','Best filters'),ylabel('F1-score'),bar(filters_avg_F1),set(gca,'XtickLabel',filters);
    %figure('Name','Best filters'),ylabel('F1-score'),bar(filters_avg_F1([10,15,20,38,2,5])),set(gca,'XtickLabel',{'Gauss(\sigma=1.3)','Gauss-low(\sigma=1.3)','Gauss-low-trunc(\sigma=1.3)','BLPF(\sigma=52,\gamma=4.5)','Box(size=3)','Median(pattern=cross)'});

end