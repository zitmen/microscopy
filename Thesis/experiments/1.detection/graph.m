%% Analyse performance of the best filter+detector pair
function graph(datapath,respath,filename,frames,filter,detector,threshold)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), error('Results path does not exist!'); end
    thr_dist = FWHM(1.3);   % sigmapsf=1.3 ==> width ~3px
    CTP = 0; % count of true-positives
    CFP = 0; % count of false-positives
    CFN = 0; % count of false-negative
    TP = zeros(500*length(frames),4); % true-positives, max 500 molecules per frame
    FP = zeros(500*length(frames)*10,4); % false-positives, max 500 molecules per frame
    FN = zeros(500*length(frames),4); % false-negative, max 500 molecules per frame
    TOTAL = zeros(500*length(frames),4);    % list of all generated dots
    
    fprintf('\n%s + %s\n',filter,detector);
    
    % run the analysis
    fprintf('Analyzing ');
    for frame=frames
        
        if ~exist(sprintf('%s/%s.%04d.mat',datapath,filename,frame),'file'); continue; end;
        if ~exist(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector),'file'); continue; end;
        
        % load `dot_params`
        load(sprintf('%s/%s.%04d.mat',datapath,filename,frame));
        
        % load `fits`
        load(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector));
        
        if ~isempty(fits)
            % Run the analysis
            % 1. threshold
            if ~isempty(threshold)
                fits = apply_threshold(fits,threshold);
            end
            % 2. pair the dots with its fits
            [D1,I1] = pdist2(dot_params(:,1:2),fits(:,3:4),'euclidean','Smallest',1);
            [D2,I2] = pdist2(fits(:,3:4),dot_params(:,1:2),'euclidean','Smallest',1);
            idxTP = ((I2(I1(I2))==I2) & (D2 < thr_dist));
            idxFP = ~((I1(I2(I1))==I1) & (D1 < thr_dist));
            % 3. add the TP,FP,FN
            if sum(idxTP) > 0
                TP(CTP+1:CTP+sum(idxTP),:) = dot_params(idxTP,1:4);
                CTP = CTP + sum(idxTP);
            end
            if sum(idxFP) > 0
                FP(CFP+1:CFP+sum(idxFP),:) = fits(idxFP,3:6);
                CFP = CFP + sum(idxFP);
            end
            if sum(~idxTP) > 0
                FN(CFN+1:CFN+sum(~idxTP),:) = dot_params(~idxTP,1:4);
                CFN = CFN + sum(~idxTP);
            end
        else
            FN(CFN+1:CFN+size(dots_params,1),:) = dot_params(:,1:4);
            CFN = CFN + size(dots_params,1);
        end

        if mod(frame-frames(1)+1,floor(length(frames)/10)) == 0; fprintf('.'); end;
    end
    fprintf(' done.\n');
    
    eval_results(TP(1:CTP,:),FP(1:CFP,:),FN(1:CFN,:));

end

%%
function fits = apply_threshold(fits,thr)

    a = thr(1);
    b = thr(2);
    c = thr(3);
    
    SNR = fits(:,6) ./ fits(:,11);
    sigma = fits(:,5);
    
    fits = fits(((a + b.*sigma + c.*(sigma.^2)) < SNR),:);

end

%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end

%%
function eval_results(TP,FP,FN)

    sigma_tp = TP(:,3);
    SNR_tp = TP(:,4) ./ 0.0022;
    %
    sigma_fp = FP(:,3);
    SNR_fp = FP(:,4) ./ 0.0022;
    %
    sigma_fn = FN(:,3);
    SNR_fn = FN(:,4) ./ 0.0022;
    %{
    % do not plot these two...it might not be right :-/
    figure(1);
    n_tp = hist(SNR_tp,100);
    n_fp = hist(SNR_fp,100);
    n_fn = hist(SNR_fn,100);
    p = precision(n_tp,n_fp,n_fn);
    r = recall(n_tp,n_fp,n_fn);
    subplot(1,2,1),plot(r,p,'rx'),axis([0 1 0 1]),title('SNR RP analysis'),xlabel('Recall'),ylabel('Precision');
    %
    n_tp = hist(sigma_tp,100);
    n_fp = hist(sigma_fp,100);
    n_fn = hist(sigma_fn,100);
    p = precision(n_tp,n_fp,n_fn);
    r = recall(n_tp,n_fp,n_fn);
    subplot(1,2,2),plot(r,p,'rx'),axis([0 1 0 1]),title('\sigma_0 RP analysis'),xlabel('Recall'),ylabel('Precision');
    %}
    %figure(2);
    snr_bin = 1:99;
    sigma_bin = 0.3:0.05:2.2;
    imagesc(F1_score(hist2(SNR_tp,sigma_tp,snr_bin,sigma_bin),hist2(SNR_fp,sigma_fp,snr_bin,sigma_bin),hist2(SNR_fn,sigma_fn,snr_bin,sigma_bin))),...
        xlabel('SNR'),ylabel('\sigma_0'),set(gca,'YDir','normal'),...
        set(gca,'YTickLabel',0.3:0.2:2.2);
end

%%
function F1 = F1_score(n_tp,n_fp,n_fn)

    p = precision(n_tp,n_fp,n_fn);
    r = recall(n_tp,n_fp,n_fn);
    F1 = 2.*p.*r./(p+r);

end
%%
function p = precision(n_tp,n_fp,n_fn)

    p = n_tp./(n_tp+n_fp);

end
%%
function r = recall(n_tp,n_fp,n_fn)

    r = n_tp./(n_tp+n_fn);

end