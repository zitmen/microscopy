function [thr,F1] = estimate_threshold(datapath,respath,filename,frames,filter,detector)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), error('Results path does not exist!'); end
    thr_dist = FWHM(1.3);   % sigmapsf=1.3 ==> width ~3px
    CTP = 0; % count of true-positives
    CFP = 0; % count of false-positives
    CFN = 0; % count of false-negative
    TP = zeros(500*length(frames),4); % true-positives, max 500 molecules per frame
    FP = zeros(500*length(frames)*10,4); % false-positives, max 500 molecules per frame
    FN = zeros(500*length(frames),4); % false-negative, max 500 molecules per frame
    TOTAL = zeros(500*length(frames),4);    % list of all generated dots
    
    fprintf('\n%s + %s\n',filter,detector);
    
    % run the analysis
    fprintf('Analyzing ');
    for frame=frames
        
        if ~exist(sprintf('%s/%s.%04d.mat',datapath,filename,frame),'file'); continue; end;
        if ~exist(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector),'file'); continue; end;
        
        % load `dot_params`
        load(sprintf('%s/%s.%04d.mat',datapath,filename,frame));
        
        % load `fits`
        load(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector));
        
        if ~isempty(fits)
            % Run the analysis
            % - pair the dots with its fits
            [D1,I1] = pdist2(dot_params(:,1:2),fits(:,3:4),'euclidean','Smallest',1);
            [D2,I2] = pdist2(fits(:,3:4),dot_params(:,1:2),'euclidean','Smallest',1);
            idxTP = ((I2(I1(I2))==I2) & (D2 < thr_dist));
            idxFP = ~((I1(I2(I1))==I1) & (D1 < thr_dist));
            % 3. add the TP,FP,FN
            if sum(idxTP) > 0
                TP(CTP+1:CTP+sum(idxTP),:) = dot_params(idxTP,1:4);
                CTP = CTP + sum(idxTP);
            end
            if sum(idxFP) > 0
                FP(CFP+1:CFP+sum(idxFP),:) = fits(idxFP,3:6);
                CFP = CFP + sum(idxFP);
            end
            if sum(~idxTP) > 0
                FN(CFN+1:CFN+sum(~idxTP),:) = dot_params(~idxTP,1:4);
                CFN = CFN + sum(~idxTP);
            end
        else
            FN(CFN+1:CFN+size(dots_params,1),:) = dot_params(:,1:4);
            CFN = CFN + size(dots_params,1);
        end

        if mod(frame-frames(1)+1,floor(length(frames)/10)) == 0; fprintf('.'); end;
    end
    fprintf(' done.\n');
    
    fprintf('Estimating the threshold:\n');
    [thr,F1] = est_thr(TP(1:CTP,:),FP(1:CFP,:),FN(1:CFN,:));
    fprintf('Done.\n');

end

%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end

%%
function [thr,F1] = est_thr(TP,FP,FN)

    %thr0 = [10*rand(1) 10*rand(1) 10*rand(1)];
    thr0 = [1 1 1];
    [thr,F1] = fminsearch(@(A)(eva_thr_guess(TP,FP,FN,A)),thr0,optimset('Display','none','TolX',1e-18));
    F1 = -F1;
    %
    function F1 = eva_thr_guess(TP,FP,FN,A)
        cthrTP = size(apply_threshold(TP,A),1);
        cthrFP = size(apply_threshold(FP,A),1);
        F1 = -F1_score(cthrTP,cthrFP,size(FN,1)+size(TP,1)-cthrTP);
        %
        pFP = (size(FP,1)-cthrFP)/size(FP,1);
        pTP = (size(TP,1)-cthrTP)/size(TP,1);
        fprintf('\tF1=%.6f, pFP=%.2f%%, pTP=%.2f%%\n',-F1,pFP*100,pTP*100);
        %fprintf('\tF1=%.6f, cthrTP=%d, cthrFP=%d\n',-F1,cthrTP,cthrFP);
    end
    
end

%%
function F1 = F1_score(n_tp,n_fp,n_fn)

    p = precision(n_tp,n_fp,n_fn);
    r = recall(n_tp,n_fp,n_fn);
    F1 = 2.*p.*r./(p+r);

end
%%
function p = precision(n_tp,n_fp,n_fn)

    p = n_tp./(n_tp+n_fp);

end
%%
function r = recall(n_tp,n_fp,n_fn)

    r = n_tp./(n_tp+n_fn);

end
%%
function fits = apply_threshold(fits,thr)

    a = thr(1);
    b = thr(2);
    c = thr(3);
    
    sigma = fits(:,3);
    SNR = fits(:,4) ./ 0.0022;
    
    fits = fits(((a + b.*sigma + c.*(sigma.^2)) < SNR),:);

end