%% Test performance of the localization on the static dots
% boxsize - size of the fitting region: (length(-boxsize:+boxsize))^2
% sigmapsf - initial sigma estimate for the fitting method
function measure(datapath,respath,filename,frames,filters,detectors,imsize,boxsize,sigmapsf)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), mkdir(respath); end
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        % prealloc -- all this ugly stuff is for speeding up the whole
        %             process by ensuring that one position is not fitted
        %             more than once
        loc_hashmap = single(zeros(imsize(1)*imsize(2),11));    % for all detected positions allocate the fitting vector
        loc_stored = false(imsize(1)*imsize(2),1);          % fit results stored in the hasmap?
        loc_tried = false(imsize(1)*imsize(2),1);           % already tried? (it's either already fitted, or there was a fitting error)

        % load image
        imraw = double(imread(sprintf('%s/%s.%04d.tiff',datapath,filename,frame)))/65535; % 16b

        for fi=1:length(filters)
            
            fprintf('    %s\n',filters{fi}.name);
            
            for di=1:length(detectors)
				
                fprintf('      %s\n',detectors{di}.name);
				
				if exist(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filters{fi}.name,detectors{di}.name),'file'); continue; end;

                % detect molecules
                imblur = filters{fi}.function(imraw);
                loc = detectors{di}.function(imblur);

                % check if the point has already been fitted
                idx = loc_stored(sub2ind(imsize,loc(:,1),loc(:,2)));
                done_locs = loc(idx,:);
                loc = loc(~idx,:);
                %
                idx = loc_tried(sub2ind(imsize,loc(:,1),loc(:,2)));
                loc_tried(sub2ind(imsize,loc(:,1),loc(:,2))) = 1;
                loc = loc(~idx,:);

                % store image
                [imfit,loc] = validateAndExtract(boxsize,imraw,loc);

                % fit model
                fits = fitting(boxsize,sigmapsf,imfit,loc);

                % store fits in hashmap to avoid multiple fitting at the same point
                if ~isempty(fits)
                    loc_stored(sub2ind(imsize,fits(:,1),fits(:,2))) = true;
                    loc_hashmap(sub2ind(imsize,fits(:,1),fits(:,2)),:) = fits(:,:);
                end

                % recall already fitted molecules
                if ~isempty(done_locs)
                    fits = vertcat(fits,loc_hashmap(sub2ind(imsize,done_locs(:,1),done_locs(:,2)),:));
                end

                % save fits
                save(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filters{fi}.name,detectors{di}.name),'-v6','fits');
                
            end

        end

    end
    fprintf('Done.\n');

end

%%
function [imfit,loc] = validateAndExtract(boxsize,im,loc)

    % init
    box = int32(-boxsize:+boxsize);
    imsize = size(im);
    fitregionsize = 2*boxsize+1;

    % remove points which are at the border location
    valid = ~( (loc(:,1)-boxsize) <        1  | (loc(:,2)-boxsize) <        1  | ...
               (loc(:,1)+boxsize) > imsize(1) | (loc(:,2)+boxsize) > imsize(2) );
    loc = loc(valid,:);
    
    % extract the valid molecules from the image
    imfit = zeros(size(loc,1),fitregionsize,fitregionsize);
    for i = 1:size(loc,1)
        pt = int32(loc(i,1:2));
        imfit(i,:,:) = im(pt(1)+box,pt(2)+box);
    end

end

%%
% OUTPUT: set of vectors - results of localization and fitting: [x_{loc}, y_{loc}, x_{fit}, y_{fit}, \sigma_{fit}, I_{fit}, bkg_{fit}, \sum{chi2_{fit}}, Nim_{fit}, \mu_{bkg_{fit}}, \sigma_{bkg_{fit}}]
function fits = fitting(boxsize,sigmapsf,imfit,loc)

    % init
    fitregionsize = 2*boxsize+1;
    npts = fitregionsize^2;
    [X(:,:,2),X(:,:,1)] = meshgrid(-boxsize:+boxsize,-boxsize:+boxsize);
    fits = single(zeros(size(imfit,1),11));
    nfits = 0;

    for i = 1:size(imfit,1)

        % fitting region
        im = reshape(imfit(i,:,:),fitregionsize,fitregionsize);

        % fit 2D gaussian bell shaped function
        a0 = [ 0,  0, sigmapsf, max(im(:)), min(im(:))];    % initial parameters
        [a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);

        % fitting error?
        if exitflag < 1 || exitflag > 3; continue; end;

        % estimate background at stored position
        imestim = gauss2d(X,a);
        Nim = sum(im(:) - a(5));  % Nimestim = sum(imestim(:) - a(5)); % is the same

        bkgestim = im - imestim;
        bkgmu = sum(bkgestim(:))/npts;
        bkgstd = sqrt(sum((bkgestim(:)-bkgmu).^2)/(npts-1));

        % position
        a(1:2) = a(1:2) + single(loc(i,1:2));
        a(4) = a(4) * 2*pi*a(3)^2;  % denormalizing the peak intensity (fitting runs with normalized Gaussian, i.e. I/(2 pi sigma^2))

        % save results
        nfits = nfits + 1;
        fits(nfits,:) = single([single(loc(i,1:2)), a, sum(chi2), Nim, bkgmu, bkgstd]);
  
    end
    
    if nfits > 0
        fits = fits(1:nfits,:);
    else
        fits = [];
    end
    
end

%%
function y = gauss2d(X,a)

    y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);

end