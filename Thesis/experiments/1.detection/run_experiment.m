function run_experiment(frames)

    % Initialize
	datapath = '_data_';
    respath = '_results_';
    dataset = 'snr1-100_sigma0.3-2.2';
	
    imsize = [512 512];
    boxsize = 5;
    psfsigma = 1.3;

	detectors{1} = struct('name', 'MAXN4_thr0.0', 'function', @(img)(neighbourhood_detector(img,4,0.0)));
    detectors{2} = struct('name', 'MAXN8_thr0.0', 'function', @(img)(neighbourhood_detector(img,8,0.0)));
    detectors{3} = struct('name', 'MORPHO3_thr0.01', 'function', @(img)(morpho_detector(img,3,0.01)));
    detectors{4} = struct('name', 'MORPHO5_thr0.01', 'function', @(img)(morpho_detector(img,5,0.01)));
    detectors{5} = struct('name', 'MORPHO7_thr0.01', 'function', @(img)(morpho_detector(img,7,0.01)));
	%
    filters{1} = struct('name', 'NONE', 'function', @(img)(no_filter(img)));
    filters{2} = struct('name', 'BOX3', 'function', @(img)(box_filter(img,3)));
    filters{3} = struct('name', 'BOX5', 'function', @(img)(box_filter(img,5)));
    filters{4} = struct('name', 'BOX7', 'function', @(img)(box_filter(img,7)));
    filters{5} = struct('name', 'MEDIAN_CROSS', 'function', @(img)(median_cross_filter(img)));
    filters{6} = struct('name', 'MEDIAN_BOX3', 'function', @(img)(median_box_filter(img,3)));
    filters{7} = struct('name', 'MEDIAN_BOX5', 'function', @(img)(median_box_filter(img,5)));
    filters{8} = struct('name', 'MEDIAN_BOX7', 'function', @(img)(median_box_filter(img,7)));
    filters{9} = struct('name', 'GAUSSIAN7_sigma0.7', 'function', @(img)(gaussian_filter(img,0.7,7)));
    filters{10} = struct('name', 'GAUSSIAN11_sigma1.3', 'function', @(img)(gaussian_filter(img,1.3,11)));
    filters{11} = struct('name', 'GAUSSIAN15_sigma2.0', 'function', @(img)(gaussian_filter(img,2.0,15)));
    filters{12} = struct('name', 'GAUSSIAN19_sigma2.6', 'function', @(img)(gaussian_filter(img,2.6,19)));
    filters{13} = struct('name', 'GAUSSIAN24_sigma3.5', 'function', @(img)(gaussian_filter(img,3.5,24)));
    filters{14} = struct('name', 'GAUSSIAN7_LOW_sigma0.7', 'function', @(img)(lowered_gaussian_filter(img,0.7,7)));
    filters{15} = struct('name', 'GAUSSIAN11_LOW_sigma1.3', 'function', @(img)(gaussian_filter(img,1.3,11)));
    filters{16} = struct('name', 'GAUSSIAN15_LOW_sigma2.0', 'function', @(img)(gaussian_filter(img,2.0,15)));
    filters{17} = struct('name', 'GAUSSIAN19_LOW_sigma2.6', 'function', @(img)(lowered_gaussian_filter(img,2.6,19)));
    filters{18} = struct('name', 'GAUSSIAN24_LOW_sigma3.5', 'function', @(img)(lowered_gaussian_filter(img,3.5,24)));
    filters{19} = struct('name', 'GAUSSIAN7_LOW_TRUNC_sigma0.7', 'function', @(img)(lowered_truncated_gaussian_filter(img,0.7,7)));
    filters{20} = struct('name', 'GAUSSIAN11_LOW_TRUNC_sigma1.3', 'function', @(img)(gaussian_filter(img,1.3,11)));
    filters{21} = struct('name', 'GAUSSIAN15_LOW_TRUNC_sigma2.0', 'function', @(img)(gaussian_filter(img,2.0,15)));
    filters{22} = struct('name', 'GAUSSIAN19_LOW_TRUNC_sigma2.6', 'function', @(img)(lowered_truncated_gaussian_filter(img,2.6,19)));
    filters{23} = struct('name', 'GAUSSIAN24_LOW_TRUNC_sigma3.5', 'function', @(img)(lowered_truncated_gaussian_filter(img,3.5,24)));
    %
    % Butterworth filter
    sigma = [48 49 50 51 52];
    gamma = [3.5 4.0 4.5];
    ii = length(filters);
    for si=1:length(sigma)
        for gi=1:length(gamma)
            ii = ii + 1;
            filters{ii} = struct('name', sprintf('BLPF_sigma%d_gamma%.1f',sigma(si),gamma(gi)), 'function', @(img)(butterworth_filter(img,sigma(si),gamma(gi))));
        end
    end
    %
    measure(datapath,respath,dataset,frames,filters,detectors,imsize,boxsize,psfsigma);

end

%% No filter
function OUT = no_filter(IN)

    OUT = IN;
    
end

%% Box (average) filter
function OUT = box_filter(IN,size)

    h = fspecial('average',size);
    OUT = imfilter(IN,h,'replicate','same');

end

%% Median filter - box pattern
function OUT = median_box_filter(IN,size)

    OUT = medfilt2(IN,[size size]);

end

%% Median filter - cross pattern
function OUT = median_cross_filter(IN)

    % init
    OUT = IN;
    cx = size(IN,1);
    cy = size(IN,2);
    
    % process without the frame
    for x=2:cx-1
        for y=2:cy-1
            OUT(x,y) = median([IN(x,y) IN(x-1,y) IN(x+1,y) IN(x,y-1) IN(x,y+1)]);
        end
    end
    
    % process the left side of the frame (without the cornders)
    x = 1;
    for y=2:cy-1
        OUT(x,y) = median([IN(1,y) IN(x+1,y) IN(x,y-1) IN(x,y+1)]);
    end
    
    % process the right side of the frame (without the cornders)
    x = cx;
    for y=2:cy-1
        OUT(x,y) = median([IN(x,y) IN(x-1,y) IN(x,y-1) IN(x,y+1)]);
    end
    
    % process the top side of the frame (without the cornders)
    y = 1;
    for x=2:cx-1
        OUT(x,y) = median([IN(x,y) IN(x-1,y) IN(x+1,y) IN(x,y+1)]);
    end
    
    % process the bottom side of the frame (without the cornders)
    y = cy;
    for x=2:cx-1
        OUT(x,y) = median([IN(x,y) IN(x-1,y) IN(x+1,y) IN(x,y-1)]);
    end
    
    % process the corners
    OUT( 1, 1) = median([IN(2,1) IN(1,2)]);
    OUT( 1,cy) = median([IN(2,cy) IN(1,cy-1)]);
    OUT(cx, 1) = median([IN(cx-1,1) IN(cx,2)]);
    OUT(cx,cy) = median([IN(cx-1,cy) IN(cx,cy-1)]);

end

%% Gaussian blur
function OUT = gaussian_filter(IN,sigma,size)

    h = fspecial('gauss',size,sigma);
    OUT = imfilter(IN,h,'replicate','same');

end

%% Gaussian blur - lowered Gaussian
function OUT = lowered_gaussian_filter(IN,sigma,size)

    h = fspecial('gauss',size,sigma);
    h = h - (mean(mean(h)));
    OUT = imfilter(IN,h,'replicate','same');

end

%% Gaussian blur - lowered truncated Gaussian
function OUT = lowered_truncated_gaussian_filter(IN,sigma,size)

    h = fspecial('gauss',size,sigma);
    h = h - (mean(mean(h)));
    h(h(:)<0) = 0;
    OUT = imfilter(IN,h,'replicate','same');

end

%% Butterworth filter
function OUT = butterworth_filter(IN,sigma,gamma)

    % filter the image
    fftI = fft2(double(IN));
    fftI = fftshift(fftI);
    blpfI_Mag = abs(fftI) .* butterworth_gen_filter(size(fftI),sigma,gamma);
    blpfI = ifftshift(blpfI_Mag .* exp(i*angle(fftI))); % i = imaginary unit
    
    % filtered image
    OUT = real(ifft2(blpfI));

end

function mask = butterworth_gen_filter(siz,sigma,gamma)

    mask = zeros(siz);
    u0 = siz(1)/2;
    v0 = siz(2)/2;
    for u = 1:siz(1)
        for v = 1:siz(2)
            mask(u,v) = 1/(1+(sqrt((u0-u).^2+(v0-v).^2)/sigma).^(2*gamma));
        end
    end
    % TODO: vektorizace ??!
    %u = 1:siz(1);
    %v = 1:siz(2);
    %mask(u,v) = 1/(1+(sqrt((u0-u).^2+(v0-v).^2)/sigma).^(2*gamma));

end

%% Neighbourhood detector
function loc = neighbourhood_detector(im,neighbourhood,threshold)

    idx = findlocmax2d(im,threshold,neighbourhood); % the 2nd parameter is threshold, which is not used during the detection/fitting
    [u,v] = ind2sub(size(im),idx);
    loc = [u,v];

end

%% Morphological detector
function loc = morpho_detector(im,radius,threshold)
    
    mx = imdilate(im,strel('square',radius));
    
    bordermask = zeros(size(im));
    bordermask(radius+1:end-radius, radius+1:end-radius) = 1;
    
    [u,v] = find((im==mx) & (im>threshold) & bordermask);
    loc = [u,v];
    
end