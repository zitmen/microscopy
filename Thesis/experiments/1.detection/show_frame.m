function show_frame(fpath)

    fpath = '_data_/snr1-100_sigma0.3-2.2.0001';
    
    im = imread([fpath '.tiff']);
    load([fpath '.mat']);   % dot_params
    
    figure(1);
    hold on;
    imagesc(im),colormap gray;
    idxWide = (dot_params(:,3) > 2);
    plot(dot_params(idxWide,2),dot_params(idxWide,1),'ro','MarkerSize',10);
    axis([1 512 1 512]);
    set(gca,'YDir','reverse');
    hold off;
    
    PARAMS = dot_params(idxWide,:);
    figure(2),hist(PARAMS(:,4),10),colormap jet;

end
