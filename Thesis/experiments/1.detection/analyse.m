%% Analyse performance of the localization on the static dots
function F1 = analyse(datapath,respath,figpath,filename,frames,filter,detector)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), error('Results path does not exist!'); end
    thr_dist = FWHM(1.3);   % sigmapsf=1.3 ==> width ~3px
    TP = 0; % true-positives
    FP = 0; % false-positives
    FN = 0; % false-negatives
    
    fprintf('\n%s + %s\n',filter,detector);
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        if ~exist(sprintf('%s/%s.%04d.mat',datapath,filename,frame),'file'); continue; end;
        if ~exist(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector),'file'); continue; end;
        
        % load `dot_params`
        load(sprintf('%s/%s.%04d.mat',datapath,filename,frame));
        
        % load `fits`
        load(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector));
        
        if ~isempty(fits)
            % Run the analysis
            % - pair the dots with its fits
            [D1,I1] = pdist2(dot_params(:,1:2),fits(:,3:4),'euclidean','Smallest',1);
            [D2,I2] = pdist2(fits(:,3:4),dot_params(:,1:2),'euclidean','Smallest',1);
            idxDots = ((I2(I1(I2))==I2) & (D2 < thr_dist));
            idxFits = I2(idxDots);
            F = fits(idxFits,3:6);
            D = dot_params(idxDots,1:4);
            % 3. add the TP,FP,FN
            TP = TP + size(F,1);
            FP = FP + size(fits,1)-size(F,1);
            FN = FN + size(dot_params,1)-size(F,1);
        else
            FN = FN + size(dot_params,1);
        end

    end
    fprintf('Done.\n');
    
    F1 = eval_results(sprintf('%s/%s_%s_%s.txt',figpath,filename,filter,detector),TP,FP,FN);

end

%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end

%%
function F1 = eval_results(fpath,TP,FP,FN)

    recall = TP/(TP+FN);
    precision = TP/(TP+FP);
    F1 = 2*precision*recall/(precision+recall);
    %
    fid = fopen(fpath,'w+');
        fprintf(fid,'TP\tFP\tFN\trecall\tprecision\tF1\n%d\t%d\t%d\t%.2f\t%.2f\t%.2f\n',TP,FP,FN,recall,precision,F1);
    fclose(fid);
    fprintf('\nResult:\n=======\nTP = %d\nFP = %d\nFN = %d\nrecall = %.2f\nprecision = %.2f\nF1 = %.2f\n\n',TP,FP,FN,recall,precision,F1);

end