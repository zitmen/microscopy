function run_localization(frames)

    % Initialize
	datapath = '_data_\12 cyl lens\12 + cyl lens_20101117_14144 PM';
    respath = '_results_/12 + cyl lens_20101117_14144 PM';
    dataset = '12 + cyl lens';
    
    boxsize = 8;
    psfsigma = 1.3;
    theta0 = 0.997104;  % 57.13 degrees
    
    fprintf('Fitting with the fixed angle (angle=%.2frad, thus %.2f degrees)\n',theta0,theta0/pi*180);
    localization(datapath,respath,dataset,frames,boxsize,psfsigma,theta0);

end