function projection_density_3D
    %
    % config
    imsiz = [ 515 703 ];   % [width height]
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    %thr_I = 0.3;
    %
    % load data - [X Y Z avg(sigma_x_y) I]
	load('_results_/no_flatfied+gauss_g-h+thr_std6+lsqnonlin_bkg/12 + cyl lens_XYZ.mat'); % F_XYZ
    %
    %F_XYZ = F_XYZ((F_XYZ(:,5)<thr_I),:);
    %
    data3D(:,1) = double(F_XYZ(:,2));
    data3D(:,2) = double(F_XYZ(:,1));
    data3D(:,3) = double(F_XYZ(:,3));
    %
    data(:,1:2) = data3D(:,1:2);
    data(:,3) = (0.05/resolution)*rand(size(data,1),1)+(0.2/resolution);     % uncertainity (F_XYZ(:,4))
    data(:,4) = F_XYZ(:,5);%1;  % intensity
    %
    % get 3D coordinates
    molecules(:,1) = data3D(:,1)-(roi(2)/2); % shift X to the origin!
    molecules(:,2) = data3D(:,2)-(roi(4)/2); % shift Y to the origin!
    molecules(:,3) = data3D(:,3)./15.0; % Z[nm]; 1px=15nm
    molecules(:,4) = 1.0;   % homogenous coordinate
    %
    % geometric transformations - rotation
    step_y = (2*pi)/360;
    %
    % geometric transformation - perspective projection
    tx = roi(2)/2; ty = roi(4)/2; tz = 0.0;  % viewer position
	%d = 1.0; --> note: this is not perspective projection, because we dont
	%             use the `d` here, thus this is orthogonal projection!!
    P = ...
    [ ...
        1.0 0.0 0.0    tx; ...
        0.0 1.0 0.0    ty; ...
        0.0 0.0 1.0    tz; ...
        0.0 0.0 0.0   1.0; ...
    ];
    %
    % animate the transformation
    avg = 1;
    thr_I = 10;
	%data(:,1:2) = perspective_projection(P,molecules);
    %IM = render_density(molecules(:,1:3),[data(:,1:2),molecules(:,3)], roi, resolution,avg,thr_I,data(:,3),data(:,4));
    %draw_and_save_color(IM,sprintf('_figures_tables_/seq/12+cyllens%04d.png',1));
    %
    fprintf('\nAngle=%d\n',1);
    % perspective projection
    data(:,1:2) = perspective_projection(P,molecules);
    % render
    IM = render_density(molecules(:,1:3),[data(:,1:2),molecules(:,3)], roi, resolution,avg,thr_I,data(:,3),data(:,4));
    draw_and_save_color(IM,sprintf('_figures_tables_/seq/12+cyllensY%04d.png',1));
    %
    for ii=2:359
        fprintf('\nAngle=%d\n',ii);
        % transform - build rotation quaternion and rotate
        Rq = quat_rot(0,1,0,ii*step_y);
        molP(:,1:3) = qrot3d(molecules(:,1:3),Rq);
        molP(:,4) = molecules(:,4);
        % perspective projection
        data(:,1:2) = perspective_projection(P,molP);
        % render
        IM = render_density(molecules(:,1:3),[data(:,1:2),molP(:,3)], roi, resolution,avg,thr_I,data(:,3),data(:,4));
        draw_and_save_color(IM,sprintf('_figures_tables_/seq/12+cyllensY%04d.png',ii));
    end
    %
end
%%
% helper functions
function [IM_I,IM_z] = rasterize_density(data_bT,data_aT,roi,resolution,thr_I,uncertainity,intensity) % data_[b/a]T = data before/after transform
    xpos = data_aT(:,1);
    ypos = data_aT(:,2);
    zpos = data_bT(:,3);
    [sZv,sZi] = sort(zpos,'ascend'); % draw the furthest points first
    xystd = uncertainity;
    npts = size(data_aT,1);
    imsiz = [roi(4)./resolution,roi(2)./resolution];
    %
    % rendering coordinates for one gaussian
    radius = 8/resolution;  % rendering radius for a dot
    [x,y] = meshgrid(-radius:radius,-radius:radius);
    idx = x.^2 + y.^2 < radius^2;
    x = x(idx); y = y(idx);
    % generate image
    IM_I = zeros(imsiz);
    IM_z = zeros(imsiz);
    for ii = 1:npts
        I = sZi(ii);
        % get position
        u = floor(xpos(I)); du = xpos(I) - u;
        v = floor(ypos(I)); dv = ypos(I) - v;
        % take coordinates only inside the image
        idx = ~(u+x < 1 | u+x > imsiz(2) | v+y < 1 | v+y > imsiz(1));
        % render
        z = kernelgauss2D(x(idx)-du, y(idx)-dv, 2*xystd(I)^2, intensity(I));      
        idx = sub2ind(imsiz,v+y(idx),u+x(idx));
        IM_I(idx) = IM_I(idx) + z;
        IM_z(idx) = zpos(I);
    end
    IM_I(IM_I>thr_I)=thr_I;
    %
    %
    function z = kernelgauss2D(x,y,twotimessigma2,I)
        z = exp(-(x.^2 + y.^2)/twotimessigma2)/(pi*twotimessigma2);
    end
end
%
function IM = render_density(data_bT,data_aT,roi,resolution,average,thr_I,uncertainity,intensity) % data_[b/a]T = data before/after transform
    
    imsiz = [roi(4)./resolution,roi(2)./resolution];
    IM_I = zeros(imsiz);
    IM_z = zeros(imsiz);
    %
    [IMG_I,IMG_z] = rasterize_density(data_bT,data_aT,roi,resolution,thr_I,uncertainity,intensity);
    IM_I = IM_I + IMG_I./average;
    IM_z = IM_z + IMG_z./average;
    for ii = 2:average
        fprintf('.');
        [IMG_I,IMG_z] = rasterize_density(data_bT,data_aT,roi,resolution,thr_I,uncertainity,intensity);
        IM_I = IM_I + IMG_I./average;
        IM_z = IM_z + IMG_z./average;
    end
    fprintf('.\n');
    IM = z_colorize(imsiz,IM_I,IM_z);

end
%
function [xp,yp,zp] = jitterpoints(xp,yp,zp,sigma)
    npts = size(xp,1);
    xp = xp + (sigma .* randn(npts,1));
    yp = yp + (sigma .* randn(npts,1));
    zp = zp + (sigma .* randn(npts,1));
end
%
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function XY = perspective_projection(P,molecules)
    molP = (P * molecules')';
    d_Az = 1.0 ./ molP(:,4);
    molP(:,1) = molP(:,1) .* d_Az;
    molP(:,2) = molP(:,2) .* d_Az;
    molP(:,3) = molP(:,3) .* d_Az;
	molP(:,4) = molP(:,4) .* d_Az;
	XY = molP(:,1:2);
end
%
function quat = quat_rot(x0,y0,z0,angle)
    quat = [cos(angle/2) x0*sin(angle/2) y0*sin(angle/2) z0*sin(angle/2)];
end
%
function quat = quat_mul(q1,q2)
    quat = ...
    [ ...
        q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3) - q1(4)*q2(4), ...
        q1(1)*q2(2) + q1(2)*q2(1) + q1(3)*q2(4) - q1(4)*q2(3), ...
        q1(1)*q2(3) - q1(2)*q2(4) + q1(3)*q2(1) + q1(4)*q2(2), ...
        q1(1)*q2(4) + q1(2)*q2(3) - q1(3)*q2(2) + q1(4)*q2(1) ...
    ];
end
%
function IMclr = z_colorize(imsize,IM_intensity,IM_depth)
    % normalize first!
    IM_I = minmax_norm(IM_intensity(:));
    IM_z = minmax_norm(IM_depth(:));
    % then colorize
    [uIMz,idx1,idx2] = unique(IM_z);
    map = rgb2hsv(jet(length(uIMz)));   % `jet` colormap
    imhsv = map(idx2,:);    % colorize by index to the colormap
    imhsv(:,3) = imhsv(:,3) .* IM_I(:); % set the intensity
    imhsv(imhsv>1)=1;   % out of bounds check
    IMclr = hsv2rgb(reshape(imhsv,[imsize(1) imsize(2) 3]));    % => rgb
end
%
function draw_and_save_color(IM,impath)
    imwrite(IM,impath);
    %figure(99)
    %imshow(IM)
    %drawnow;
end
%
function draw_and_save(IM,impath)
    imwrite(ind2rgb(gray2ind(IM,65536),hot(65536)),impath);
    figure(99)
    imagesc(minmax_norm(IM))
    colormap(hot)
    drawnow;
end