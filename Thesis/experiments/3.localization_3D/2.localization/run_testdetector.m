function run_testdetector(frame)

    % Initialize
	datapath = '_data_\12 cyl lens\12 + cyl lens_20101117_14144 PM';
    respath = '_results_/12 + cyl lens_20101117_14144 PM';
    dataset = '12 + cyl lens';
    
    boxsize = 8;
    psfsigma = 1.3;
    theta0 = 0.99;  % 57 degrees
    
    % load image
    imraw = double(imread(sprintf('%s/%s_t%04d.tif',datapath,dataset,frame)))/65535; % 16b
    loc = detectMolecules(imraw);
    loc = validate(1,imraw,loc);
    
    % draw detections
    IM=zeros(703,515);
    imagesc(imraw),colormap gray,hold on;
    plot(loc(:,2),loc(:,1),'rx'),hold off;
    drawnow;

end
%%
function loc = detectMolecules(imraw)

    %{
    thr = 1800/65535;
    h = fspecial('gauss',15,2.6);
    im = imfilter(imraw,h,'replicate','same');
    idx = findlocmax2d(im);
    idx = idx(im(idx)>thr);
    [u,v] = ind2sub(size(im),idx);
    loc = [u,v];
    %}
    %{
    thr = mean(imraw(:))+sqrt(2)/2*std(imraw(:));
    h = fspecial('average',5);
    im = imfilter(imraw,h,'replicate','same');
    %
    radius = 7;
    mx = imdilate(im,strel('square',radius));
    bordermask = zeros(size(im));
    bordermask(radius+1:end-radius, radius+1:end-radius) = 1;
    [u,v] = find((im==mx) & (im>thr) & bordermask);
    loc = [u,v];
    %}
    h = fspecial('gauss',[15 15],2.0);
    g = fspecial('gauss',[15 15],1.0);
    kernel = g - h;
    im = imfilter(imraw,kernel,'replicate','same');
    thr = mean(im(:))+6*std(im(:));
    idx = findlocmax2d(im);
    idx = idx(im(idx)>thr);
    [u,v] = ind2sub(size(im),idx);
    loc = [u,v];
end
%%
function loc = validate(boxsize,im,loc)

    % init
    box = int32(-boxsize:+boxsize);
    imsize = size(im);
    fitregionsize = 2*boxsize+1;

    % remove points which are at the border location
    valid = ~( (loc(:,1)-boxsize) <        1  | (loc(:,2)-boxsize) <        1  | ...
               (loc(:,1)+boxsize) > imsize(1) | (loc(:,2)+boxsize) > imsize(2) );
    loc = loc(valid,:);
    
    % extract the valid molecules from the image
    imfit = zeros(size(loc,1),fitregionsize,fitregionsize);
    for i = 1:size(loc,1)
        pt = int32(loc(i,1:2));
        imfit(i,:,:) = im(pt(1)+box,pt(2)+box);
    end
    
    % check the imfit and keep only valid locs
    C = imfit(:,boxsize+1,boxsize+1);
    AVG = (sum(sum(imfit(:,:,:),2),3) - imfit(:,boxsize+1,boxsize+1)) ./ 8;
    idx = (C - AVG) > (C ./ 10);
    loc = loc(idx,:);

end