function run_show_results(frames)

    % Initialize
    figpath = '_figures_tables_/no_flatfied+gauss_g-h+thr_std6+lsqnonlin_bkg';
	respath = '_results_/no_flatfied+gauss_g-h+thr_std6+lsqnonlin_bkg'; %12 + cyl lens_20101117_14144 PM
    dataset = '12 + cyl lens';
    %{
    % Evaluate XYZ
    F_XYZ = [];
    for frame=frames
        if ~exist((sprintf('%s/%s_z%04d.mat',respath,dataset,frame)),'file')
            continue;
        end
        load(sprintf('%s/%s_z%04d.mat',respath,dataset,frame)); % fits
		if isempty(fits); continue; end;
        Fi = ((abs(fits(:,1)-fits(:,3))<8) & (abs(fits(:,2)-fits(:,4))<8));
        F = fits(Fi,:);
        valid = sqrt((F(:,1)-F(:,3)).^2 + (F(:,2)-F(:,4)).^2) < 8;
        F = F(valid,:);
        Z = sigma2z(F(:,5)./F(:,6));
        valid = Z>-400 & Z<+400;
		if sum(valid)==0; continue; end;
        F_XYZ = vertcat(F_XYZ,[F(valid,3),F(valid,4),Z(valid),((F(valid,5)+F(valid,6))./2),F(valid,7)]);    % [X,Y,Z,avg(sigma_x_y),I]
        %save(sprintf('%s/%s_XYZ_%04d-%04d.mat',respath,dataset,frame,frames(end)),'F_XYZ','-v6','-mat');
        %fprintf('%d\n',frame);
    end
    %}
    % Render
    load(sprintf('%s/%s_XYZ.mat',respath,dataset)); % F_XYZ
    imsize = [703,515];
    IM_I = zeros(imsize);
    IM_I(sub2ind(imsize,round(F_XYZ(:,1)),round(F_XYZ(:,2)))) = 1;
    IM_z = zeros(703,515);
    IM_z(sub2ind(imsize,round(F_XYZ(:,1)),round(F_XYZ(:,2)))) = F_XYZ(:,3);
    imshow(z_colorize(imsize,IM_I,IM_z));
    %{
    for frame=frames
        load(sprintf('%s/%s_XYZ_%04d-%04d.mat',respath,dataset,frame,frames(end))); % F_XYZ
        imsize = [703,515];
        IM_I = zeros(imsize);
        IM_I(sub2ind(imsize,round(F_XYZ(:,1)),round(F_XYZ(:,2)))) = 1;
        IM_z = zeros(703,515);
        IM_z(sub2ind(imsize,round(F_XYZ(:,1)),round(F_XYZ(:,2)))) = F_XYZ(:,3);
        IM = z_colorize(imsize,IM_I,IM_z);
        imwrite(IM,sprintf('%s/%s_%04d-%04d.tiff',figpath,dataset,frame,frames(end)));
        fprintf('%d\n',frame);
    end
    %}

end

function z = zpos(A,B,C)
    z = max([A B C],[],2);
end

function circ = circumference(A,B,C)
    circ = ptdist(A,B) + ptdist(B,C) + ptdist(C,A);
end

function d = ptdist(a,b)
    d = sqrt((a(:,1)-b(:,1)).^2 + (a(:,2)-b(:,2)).^2);% + (a(:,3)-b(:,3)).^2);
end

function [xp,yp,zp] = jitterpoints(xp,yp,zp,sigma,num,npts)
    if (num > 1),
        xp = xp + (sigma .* randn(npts,1));
        yp = yp + (sigma .* randn(npts,1));
        zp = zp + (sigma .* randn(npts,1));
    end
end

function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end

function z = sigma2z(ratio)
    a = -3467.5;
    b = 20098;
    c = -46385;
    d = 53589;
    e = -31824;
    f = 7989.3;
    x = ratio;
    %
    z = a.*x.^5 + b.*x.^4 + c.*x.^3 + d.*x.^2 + e.*x + f;
end

function IMclr = z_colorize(imsize,IM_intensity,IM_depth)
    % normalize first!
    IM_I = minmax_norm(IM_intensity(:));
    IM_z = 1-minmax_norm(IM_depth(:));  % reverse the z-axis! the furthest should be the lowest value
    % then colorize
    [uIMz,idx1,idx2] = unique(IM_z);
    map = rgb2hsv(jet(length(uIMz)));   % `jet` colormap
    imhsv = map(idx2,:);    % colorize by index to the colormap
    imhsv(:,3) = imhsv(:,3) .* IM_I(:); % set the intensity
    imhsv(imhsv>1)=1;   % out of bounds check
    IMclr = hsv2rgb(reshape(imhsv,[imsize(1:2) 3]));    % => rgb
end