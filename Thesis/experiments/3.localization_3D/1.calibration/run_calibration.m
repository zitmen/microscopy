function run_calibration(frames)

    % Initialize
	datapath = '_data_/3D SMLM calibration/cylinder lens calibration/10nm step/z calibration 10 nm step 01';
    respath = '_results_/10nm step 01';
    dataset = 'z calibration 10 nm step 01';
    
    boxsize = 8;
    psfsigma = 1.3;
    theta0 = pi/4;  % 45 degrees
    
    if ~exist(sprintf('%s/%s_theta0.mat',[respath '/fitting_theta0'],dataset),'file')
    
        % fitting with a variable angle
        fprintf('Fitting with a variable angle:\n');
        calibrate(datapath,[respath '/fitting_theta0'],dataset,frames,boxsize,psfsigma,theta0,true);

        % estimate the angle
        fprintf('Estimating the angle:\n');
        theta0 = est_angle([respath '/fitting_theta0'],dataset,frames);
        save(sprintf('%s/%s_theta0.mat',[respath '/fitting_theta0'],dataset),'-v6','theta0');
        
    else
        
        load(sprintf('%s/%s_theta0.mat',[respath '/fitting_theta0'],dataset));  % theta0
        
    end
    
    % fitting again with fixed angle - this should be more accurate
    fprintf('Fitting with the fixed angle (angle=%.6frad, thus %.2f degrees)\n',theta0,theta0/pi*180);
    calibrate(datapath,respath,dataset,frames,boxsize,psfsigma,theta0,false);

end

%%
function angle = est_angle(respath,dataset,frames)

    fits = cell(length(frames),1);
    fprintf('Loading...');
    for frame=frames
        if ~exist(sprintf('%s/%s_z%04d.mat',respath,dataset,frame),'file'); continue; end;
        
        % load `fits`
        F = load(sprintf('%s/%s_z%04d.mat',respath,dataset,frame));
        
        % normalize all angles - modulo 90 degrees
        while true % negative values
            idx = (F.fits(:,9)<0);
            if sum(idx)==0; break; end;
            F.fits(idx,9) = F.fits(idx,9) + pi/2;
        end
        while true % big positive values
            idx = (F.fits(:,9)>(pi/2));
            if sum(idx)==0; break; end;
            F.fits(idx,9) = F.fits(idx,9) - pi/2;
        end
        
        % keep fits in memory
        fits{frame-frames(1)+1} = F.fits(:,3:9);
    end
    fprintf('done.\n');
    
    % estimate the angle
    % note: this might be little unorthodox, but this approach should be
    % less sensitive to outliers then just calculating mean value
    % 1. get mean and median of each frame
    % 2. get mean of these means and medians (`total_*`)
    % 3. get mean of `total_mean` and `total_median`
    sum_med = 0;
    sum_mean = 0;
    for i=1:length(fits)
        sum_med = sum_med + median(fits{i}(:,7));
        sum_mean = sum_mean + mean(fits{i}(:,7));
    end;
    total_median = sum_med / length(fits);
    total_mean = sum_mean / length(fits);
    angle = double((total_mean + total_median) / 2);

end