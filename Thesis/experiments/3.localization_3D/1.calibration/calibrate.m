function calibrate(datapath,respath,filename,frames,boxsize,sigmapsf,theta0,fit_angle)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), mkdir(respath); end
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        % load image
        imraw = double(imread(sprintf('%s/%s_z%04d.tif',datapath,filename,frame)))/65535; % 16b

        % detect molecules
        loc = detectMolecules(imraw);

        % store image
        [imfit,loc] = validateAndExtract(boxsize,imraw,loc);

        % fit model
        fits = fitting(boxsize,sigmapsf,imfit,loc,theta0,fit_angle);

        % save fits
        save(sprintf('%s/%s_z%04d.mat',respath,filename,frame),'-v6','fits');

    end
    fprintf('Done.\n');

end

%%
function loc = detectMolecules(imraw)

    thr = 1000/65535;
    %
    h = fspecial('gauss',11,1.3);
    im = imfilter(imraw,h,'replicate','same');
    idx = findlocmax2d(im);
    idx = idx(im(idx)>thr);
    [u,v] = ind2sub(size(im),idx);
    loc = [u,v];
    %{
    h = fspecial('average',3);
    im = imfilter(imraw,h,'replicate','same');
    %
    radius = 7;
    mx = imdilate(im,strel('square',radius));
    bordermask = zeros(size(im));
    bordermask(radius+1:end-radius, radius+1:end-radius) = 1;
    [u,v] = find((im==mx) & (im>thr) & bordermask);
    loc = [u,v];
    %}
end

%%
function [imfit,loc] = validateAndExtract(boxsize,im,loc)

    % init
    box = int32(-boxsize:+boxsize);
    imsize = size(im);
    fitregionsize = 2*boxsize+1;

    % remove points which are at the border location
    valid = ~( (loc(:,1)-boxsize) <        1  | (loc(:,2)-boxsize) <        1  | ...
               (loc(:,1)+boxsize) > imsize(1) | (loc(:,2)+boxsize) > imsize(2) );
    loc = loc(valid,:);
    
    % extract the valid molecules from the image
    imfit = zeros(size(loc,1),fitregionsize,fitregionsize);
    for i = 1:size(loc,1)
        pt = int32(loc(i,1:2));
        imfit(i,:,:) = im(pt(1)+box,pt(2)+box);
    end

end

%%
% OUTPUT: set of vectors - results of localization and fitting: [x_{loc},y_{loc},x_{fit},y_{fit},\sigma_{fit1},\sigma_{fit2},I_{fit},bkg_{fit},angle,\sum{chi2_{fit}},Nim_{fit},\mu_{bkg_{fit}}, \sigma_{bkg_{fit}}]
function fits = fitting(boxsize,sigmapsf,imfit,loc,theta0,fit_angle)

    % init
    center = boxsize + 1;
    fitregionsize = 2*boxsize+1;
    fits = single(zeros(size(imfit,1),13));
    nfits = 0;

    for i = 1:size(imfit,1)

        % fitting region
        im = reshape(imfit(i,:,:),fitregionsize,fitregionsize);

        % fit 2D gaussian bell shaped function
        a0 = [ center,  center, sigmapsf, sigmapsf, max(im(:)), min(im(:)), theta0];    % initial parameters (initial theta was estimated by user)
        a = lsqnonlin(@(A_est)(im-PSF_ElipticalGaussian(size(im),A_est)),a0,[],[],optimset('Display','none'));
        
        % position
        a(1:2) = a(1:2) - center + single(loc(i,1:2));

        % save results
        nfits = nfits + 1;
        fits(nfits,:) = single([single(loc(i,1:2)), a, 0, 0, 0, 0.0022]);
  
    end
    
    if nfits > 0
        fits = fits(1:nfits,:);
    else
        fits = [];
    end

    %
    function h = PSF_ElipticalGaussian(size,params)
        % params -- [x0,y0,sigma_1,sigma_2,I,bkg,angle]
        [x,y] = meshgrid(1:size(1),1:size(2));
        %
        sigmax = params(3);
        sigmay = params(4);
        I = params(5);
        bkg = params(6);
        x0 = params(1);
        y0 = params(2);
        if fit_angle
            theta = params(7);
        else
            theta = theta0;
        end
        %
        aa = (cos(theta)^2)./(2*sigmax^2) + (sin(theta)^2)/(2*sigmay^2);
        bb = -sin(2*theta)/(4*sigmax^2) + sin(2*theta)/(4*sigmay^2);
        cc = (sin(theta)^2)/(2*sigmax^2) + (cos(theta)^2)/(2*sigmay^2);
        h  = I.*exp(-(aa.*(x-x0).^2 + 2*bb.*(x-x0).*(y-y0) + cc.*(y-y0).^2)) + bkg;
    end
    
end