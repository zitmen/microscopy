function run_analysis(frames)

    % Initialize
    respath = '_results_/10nm step 01';
    figpath = '_figures_tables_';
    dataset = 'z calibration 10 nm step 01';
    z_step = 10;    % 10nm
    
    % Group the molecules together throughout the Z stack
    if ~exist(sprintf('%s/%s_z-stack.mat',figpath,dataset),'file')
        z_stack = group_molecules(respath,dataset,frames);
        save(sprintf('%s/%s_z-stack.mat',figpath,dataset),'-v6','z_stack');
    else
        load(sprintf('%s/%s_z-stack.mat',figpath,dataset)); % `z_stack`
    end
    
    % Find the molecules z-positions
    z_stack = find_center(z_stack);
    
    % Plot z-->sigma ratio
    [Z,SRM,SRE] = average_zpos(z_stack,-60,+60,2);
    figure,errorbar(z_step.*Z,SRM,SRE./2),axis([-600 +600 0.6 1.8]),grid on,xlabel('z [nm]'),ylabel('\sigma_1 / \sigma_2');
    
    % Plot z-->sigma1,sigma2
    [Z,SM1,SE1,SM2,SE2] = sigma_zpos(z_stack,-60,+60,2);
    figure,errorbar(z_step.*Z,SM1,SE1./2,'b'),hold on,errorbar(z_step.*Z,SM2,SE2./2,'r'),hold off,axis([-600 +600 1.5 6.0]),grid on,xlabel('z [nm]'),ylabel('\sigma [px]'),legend('\sigma_1','\sigma_2','Location','NorthWest');
    
    % HINT: to get better results, we should take only linear sequences of
    % some minimal length; linear sequence = 1,2,3,4,5; but not 1,3,4,5 -->
    % these are two linear sequences 1 and 3,4,5...this should be done in
    % the grouping routine
    
    % Get all the values from linear part and fit a polynomial through it
    [Z,SRM,SRE] = average_zpos(z_stack,-30,+30,1);
    figure,plot(SRM,z_step.*Z,'rx'),grid on,xlabel('\sigma_1 / \sigma_2'),ylabel('z [nm]');
    
%{
% z-positions are limited from -300 to +300 nm

% fitted polynomial:
y = p1*x^5 + p2*x^4 +
      p3*x^3 + p4*x^2 +
      p5*x + p6 

Coefficients:
  p1 = -3467.5
  p2 = 20098
  p3 = -46385
  p4 = 53589
  p5 = -31824
  p6 = 7989.3

Norm of residuals = 
     29.093
%}

end

%%
function [Z,sigma1_mean,sigma1_std2,sigma2_mean,sigma2_std2] = sigma_zpos(z_stack,z_min,z_max,z_nbins)

    idx0 = floor((z_max-z_min)/2)+1;
    sigma = cell(length(-200:+200),1);
    for mi=1:length(z_stack)
        z0_idx = (z_stack{mi}.z==0);
        z0_sigma_ratio = z_stack{mi}.sigma(z0_idx,1)/z_stack{mi}.sigma(z0_idx,2);
        if (abs(1-z0_sigma_ratio) > 0.01) | (z_stack{mi}.sigma(z0_idx,1) > 3)    % threshold - take only the small symmetric ones
            %fprintf('/');
            continue;
        end;
        for zi=1:length(z_stack{mi}.z)
            if z_stack{mi}.z(zi) < z_min; continue; end;
            if z_stack{mi}.z(zi) > z_max; continue; end;
            %
            z = idx0 + z_stack{mi}.z(zi);
            sigma{z}(length(sigma{z})+1,1:2) = z_stack{mi}.sigma(zi,1:2);
        end
    end
    %
    Z = [];
    sigma1_mean = [];
    sigma1_std2 = [];
    sigma2_mean = [];
    sigma2_std2 = [];
    for z=1:z_nbins:length(sigma)
        if(~isempty(sigma{z}))
            % do the same think as they do in iceskating competitions :))
            % --> get rid of the min and max values
            % + merge neighbours into z_nbins to get smoother line
            R = [];
            for zbi=0:z_nbins-1
                S = horzcat(R,sigma{z+zbi});
            end
            if length(S) > 2
                S = sort(S);
                sigma1_mean(length(sigma1_mean)+1) = mean(S(2:end-1,1));
                sigma1_std2(length(sigma1_std2)+1) = std2(S(2:end-1,1));
                sigma2_mean(length(sigma2_mean)+1) = mean(S(2:end-1,2));
                sigma2_std2(length(sigma2_std2)+1) = std2(S(2:end-1,2));
                Z(length(Z)+1) = z - idx0;
            end
        end
    end

end

%% Averages all the molecules parameters at the same z-position
function [Z,sigma_ratio_mean,sigma_ratio_std2] = average_zpos(z_stack,z_min,z_max,z_nbins)

    idx0 = floor((z_max-z_min)/2)+1;
    ratio = cell(length(-200:+200),1);
    for mi=1:length(z_stack)
        z0_idx = (z_stack{mi}.z==0);
        z0_sigma_ratio = z_stack{mi}.sigma(z0_idx,1)/z_stack{mi}.sigma(z0_idx,2);
        if (abs(1-z0_sigma_ratio) > 0.01) | (z_stack{mi}.sigma(z0_idx,1) > 3)    % threshold - take only the small symmetric ones
            %fprintf('/');
            continue;
        end;
        for zi=1:length(z_stack{mi}.z)
            if z_stack{mi}.z(zi) < z_min; continue; end;
            if z_stack{mi}.z(zi) > z_max; continue; end;
            %
            z = idx0 + z_stack{mi}.z(zi);
            ratio{z}(length(ratio{z})+1) = z_stack{mi}.sigma(zi,1)/z_stack{mi}.sigma(zi,2);
        end
    end
    %
    Z = [];
    sigma_ratio_mean = [];
    sigma_ratio_std2 = [];
    for z=1:z_nbins:length(ratio)
        if(~isempty(ratio{z}))
            % do the same think as they do in iceskating competitions :))
            % --> get rid of the min and max values
            % + merge neighbours into z_nbins to get smoother line
            R = [];
            for zbi=0:z_nbins-1
                R = horzcat(R,ratio{z+zbi});
            end
            if length(R) > 2
                R = sort(R);
                sigma_ratio_mean(length(sigma_ratio_mean)+1) = mean(R(2:end-1));
                sigma_ratio_std2(length(sigma_ratio_std2)+1) = std2(R(2:end-1));
                Z(length(Z)+1) = z - idx0;
            end
        end
    end

end

%% Seeks fot the most symmetrix z-position
function z_stack = find_center(z_stack)

    for mi=1:length(z_stack)
        sigma = z_stack{mi}.sigma;
        ds = abs(sigma(:,1)-sigma(:,2));
        [val,idx] = min(ds);
        z_stack{mi}.z = z_stack{mi}.z - z_stack{mi}.z(idx);
    end

end

%%
function z_stack = group_molecules(respath,dataset,frames)

    fits = cell(length(frames),1);
    fprintf('Loading...');
    for frame=frames
        if ~exist(sprintf('%s/%s_z%04d.mat',respath,dataset,frame),'file'); continue; end;
        
        % load `fits`
        F = load(sprintf('%s/%s_z%04d.mat',respath,dataset,frame));
        fits{frame-frames(1)+1} = F.fits(:,3:8);
    end
    fprintf('done.\n');
    % fits = [x,y,z,sigma1,sigma2,I,theta]
    z_stack = grouping(fits);

end

%%
% note: this could be done much faster, but it does not matter so much,
% because the calibration is done only once
function z_stack = grouping(fits)

    fprintf('Grouping...\n');
    thr_dist = FWHM(2)^2;
    z_stack = [];
    add_new_molecule(fits{1},1);
    fprintf('  z=%d\n',1);
    for z=2:length(fits)
        fprintf('  z=%d\n',z);
        for mi=1:size(fits{z},1)
            best_dist = inf;
            best_mli = 0;
            for mli=1:length(z_stack)
                dist = ptdist2(z_stack{mli}.pos,fits{z}(mi,1:2));
                if dist < best_dist
                    best_dist = dist;
                    best_mli = mli;
                end
            end
            if best_dist < thr_dist
                add_z_to_molecule(best_mli,fits{z}(mi,5),fits{z}(mi,6),fits{z}(mi,3:4),z);
            else
                add_new_molecule(fits{z}(mi,:),z);
            end
        end
    end
    fprintf('done.\n');
    
    %
    function add_new_molecule(mol,z)
        start = length(z_stack);
        for ii=1:size(mol,1)
            z_stack{start+ii}.pos = mol(ii,1:2);
            z_stack{start+ii}.sigma(1,:) = mol(ii,3:4);
            z_stack{start+ii}.I = mol(ii,5);
            z_stack{start+ii}.angle = mol(ii,6);
            z_stack{start+ii}.z = z;
        end
    end
    %
    function add_z_to_molecule(mol_list_idx,I,angle,sigma,z)
        pos = 1 + length(z_stack{mol_list_idx}.z);
        z_stack{mol_list_idx}.sigma(pos,:) = sigma;
        z_stack{mol_list_idx}.I(pos) = I;
        z_stack{mol_list_idx}.angle(pos) = angle;
        z_stack{mol_list_idx}.z(pos) = z;
    end

end

%%
function d = ptdist2(p1,p2)
    d = (p1(1)-p2(1))^2 + (p1(2)-p2(2))^2;
end

%%
function width = FWHM(sigma)
    width = 2*sqrt(2*log(2)) * sigma;
end