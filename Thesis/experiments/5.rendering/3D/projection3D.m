function projection3D
    %
    % config
    imsiz = [ 256 256 ];   % [rows cols] => [height width]
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    %
    % load data - [X Y sigma I]
	data3D = dlmread('_data_/particles-50000-large.txt','\t',1,1);
    data(:,1:2) = data3D(:,1:2);
    data(:,3) = (0.05/resolution)*rand(size(data,1),1)+(0.1/resolution);     % uncertainity
    data(:,4) = 1;
    %
    % get 3D coordinates
    molecules(:,1) = data3D(:,1)-(roi(4)/2); % shift X to the origin!
    molecules(:,2) = data3D(:,2)-(roi(2)/2); % shift Y to the origin!
    molecules(:,3) = data3D(:,3)./15.0; % Z[nm]; 1px=15nm
    molecules(:,4) = 1.0;   % homogenous coordinate
    %
    % geometric transformations - rotation
    step_z = (2*pi)/360;
    step_y = (2*pi)/720;
    %
    % geometric transformation - perspective projection
    tx = roi(4)/2; ty = roi(2)/2; tz = 0.0;  % viewer position
	%d = 1.0;
    P = ...
    [ ...
        1.0 0.0 0.0    tx; ...
        0.0 1.0 0.0    ty; ...
        0.0 0.0 1.0    tz; ...
        0.0 0.0 0.0   1.0; ...
    ];
    %
    % animate the transformation
	data(:,1:2) = perspective_projection(P,molecules);
    %IM = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',10));
    %draw_and_save(IM,sprintf('_results_/seq/spiral%04d.png',1));
    IM = render_color_scatter(imsiz,resolution,[data(:,1:2),molecules(:,3)],molecules(:,3));
    draw_and_save_color(IM,sprintf('_results_/seq/spiral%04d.png',1));
    %
    for ii=2:719
        fprintf('\nAngle=%d\n',ii);
        % transform - build rotation quaternion and rotate
        Rq = quat_mul(quat_rot(0,1,0,ii*step_y),quat_rot(0,0,1,ii*step_z));
        molP(:,1:3) = qrot3d(molecules(:,1:3),Rq);
        molP(:,4) = molecules(:,4);
        % perspective projection
        data(:,1:2) = perspective_projection(P,molP);
        % render
        %IM = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',10));
        %draw_and_save(IM,sprintf('_results_/seq/spiral%04d.png',ii));
        IM = render_color_scatter(imsiz,resolution,[data(:,1:2),molP(:,3)],molecules(:,3));
        draw_and_save_color(IM,sprintf('_results_/seq/spiral%04d.png',ii));
    end
    %
end
%%
% helper functions
function IM = render_color_scatter(imsiz,resolution,data3D,clr)
    siz = imsiz./resolution;
    [sZv,sZi] = sort(data3D(:,3),'descend'); % draw the furthest points first
    X = data3D(sZi,1)./resolution;
    Y = data3D(sZi,2)./resolution;
    Z = data3D(sZi,3);
    IM_I = zeros(siz);
    IM_I(sub2ind(siz,round(Y),round(X))) = 1;
    IM_z = zeros(siz);
    IM_z(sub2ind(siz,round(Y),round(X))) = clr(sZi);
    IM = z_colorize(siz,IM_I,IM_z);
end
%
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function XY = perspective_projection(P,molecules)
    molP = (P * molecules')';
    d_Az = 1.0 ./ molP(:,4);
    molP(:,1) = molP(:,1) .* d_Az;
    molP(:,2) = molP(:,2) .* d_Az;
    molP(:,3) = molP(:,3) .* d_Az;
	molP(:,4) = molP(:,4) .* d_Az;
	XY = molP(:,1:2);
end
%
function quat = quat_rot(x0,y0,z0,angle)
    quat = [cos(angle/2) x0*sin(angle/2) y0*sin(angle/2) z0*sin(angle/2)];
end
%
function quat = quat_mul(q1,q2)
    quat = ...
    [ ...
        q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3) - q1(4)*q2(4), ...
        q1(1)*q2(2) + q1(2)*q2(1) + q1(3)*q2(4) - q1(4)*q2(3), ...
        q1(1)*q2(3) - q1(2)*q2(4) + q1(3)*q2(1) + q1(4)*q2(2), ...
        q1(1)*q2(4) + q1(2)*q2(3) - q1(3)*q2(2) + q1(4)*q2(1) ...
    ];
end
%
function IMclr = z_colorize(imsize,IM_intensity,IM_depth)
    % normalize first!
    IM_I = minmax_norm(IM_intensity(:));
    IM_z = 1-minmax_norm(IM_depth(:));  % reverse the z-axis! the furthest should be the lowest value
    % then colorize
    [uIMz,idx1,idx2] = unique(IM_z);
    map = rgb2hsv(jet(length(uIMz)));   % `jet` colormap
    imhsv = map(idx2,:);    % colorize by index to the colormap
    imhsv(:,3) = imhsv(:,3) .* IM_I(:); % set the intensity
    imhsv(imhsv>1)=1;   % out of bounds check
    IMclr = hsv2rgb(reshape(imhsv,[imsize(1:2) 3]));    % => rgb
end
%
function draw_and_save_color(IM,impath)
    imwrite(IM,impath);
    figure(99)
    imshow(IM)
    drawnow;
end
%
function draw_and_save(IM,impath)
    imwrite(ind2rgb(gray2ind(IM,65536),hot(65536)),impath);
    figure(99)
    imagesc(minmax_norm(IM))
    colormap(hot)
    drawnow;
end