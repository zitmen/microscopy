function projection_DT3_3D
    %
    % config
    imsiz = [ 256 256 ];   % [rows cols] => [height width]
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    %
    % load data - [X Y sigma I]
	data3D = dlmread('_data_/particles-50000-large.txt','\t',1,1);
    data(:,1:2) = data3D(:,1:2);
    data(:,3) = (0.05/resolution)*rand(size(data,1),1)+(0.1/resolution);     % uncertainity
    data(:,4) = 1;
    %
    % get 3D coordinates
    molecules(:,1) = data3D(:,1)-(roi(4)/2); % shift X to the origin!
    molecules(:,2) = data3D(:,2)-(roi(2)/2); % shift Y to the origin!
    molecules(:,3) = data3D(:,3)./15.0; % Z[nm]; 1px=15nm
    molecules(:,4) = 1.0;   % homogenous coordinate
    %
    % 3D triangulation
    DT = DelaunayTri(molecules(:,1:3));
    %
    % geometric transformations - rotation
    step_z = (2*pi)/360;
    step_y = (2*pi)/720;
    %
    % geometric transformation - perspective projection
    tx = roi(4)/2; ty = roi(2)/2; tz = 0.0;  % viewer position
	%d = 1.0;
    P = ...
    [ ...
        1.0 0.0 0.0    tx; ...
        0.0 1.0 0.0    ty; ...
        0.0 0.0 1.0    tz; ...
        0.0 0.0 0.0   1.0; ...
    ];
    %
    % animate the transformation
    avg = 10;
	data(:,1:2) = perspective_projection(P,molecules);
    IM = render_DT(DT,[data(:,1:2),molecules(:,3)], roi, resolution,avg,data(:,3));
    draw_and_save(IM,sprintf('_results_/seq/spiral%04d.png',1));
    %draw_and_save_color(IM,sprintf('_results_/seq/spiral%04d.png',1));
    %
    for ii=2:719
        fprintf('\nAngle=%d\n',ii);
        % transform - build rotation quaternion and rotate
        Rq = quat_mul(quat_rot(0,1,0,ii*step_y),quat_rot(0,0,1,ii*step_z));
        molP(:,1:3) = qrot3d(molecules(:,1:3),Rq);
        molP(:,4) = molecules(:,4);
        % perspective projection
        data(:,1:2) = perspective_projection(P,molP);
        % render
        IM = render_DT(DT,[data(:,1:2),molP(:,3)], roi, resolution,avg,data(:,3));
        draw_and_save(IM,sprintf('_results_/seq/spiral%04d.png',ii));
        %draw_and_save_color(IM,sprintf('_results_/seq/spiral%04d.png',ii));
    end
    %
end
%%
% helper functions
function IM = render_DT(DT,data,roi,resolution,average,uncertainity) % data = data after transform, DT = Delaunay Triangulation on the original data
    %
    XYZ = data;
    %
    % extract triangles
    DT_A = XYZ(DT(:,1),1:3); sigmaA = uncertainity(DT(:,1));
    DT_B = XYZ(DT(:,2),1:3); sigmaB = uncertainity(DT(:,2));
    DT_C = XYZ(DT(:,3),1:3); sigmaC = uncertainity(DT(:,3));
    %
    DT_A = vertcat(DT_A,XYZ(DT(:,1),1:3)); sigmaA = vertcat(sigmaA,uncertainity(DT(:,1)));
    DT_B = vertcat(DT_B,XYZ(DT(:,2),1:3)); sigmaB = vertcat(sigmaB,uncertainity(DT(:,2)));
    DT_C = vertcat(DT_C,XYZ(DT(:,4),1:3)); sigmaC = vertcat(sigmaC,uncertainity(DT(:,3)));
    %
    DT_A = vertcat(DT_A,XYZ(DT(:,1),1:3)); sigmaA = vertcat(sigmaA,uncertainity(DT(:,1)));
    DT_B = vertcat(DT_B,XYZ(DT(:,3),1:3)); sigmaB = vertcat(sigmaB,uncertainity(DT(:,2)));
    DT_C = vertcat(DT_C,XYZ(DT(:,4),1:3)); sigmaC = vertcat(sigmaC,uncertainity(DT(:,3)));
    %
    DT_A = vertcat(DT_A,XYZ(DT(:,2),1:3)); sigmaA = vertcat(sigmaA,uncertainity(DT(:,1)));
    DT_B = vertcat(DT_B,XYZ(DT(:,3),1:3)); sigmaB = vertcat(sigmaB,uncertainity(DT(:,2)));
    DT_C = vertcat(DT_C,XYZ(DT(:,4),1:3)); sigmaC = vertcat(sigmaC,uncertainity(DT(:,3)));
    %
    IM_I = rasterization(roi,resolution,DT_A,DT_B,DT_C,1./circumference(DT_A,DT_B,DT_C),0.1);
    %IM_z = rasterization(roi,resolution,DT_A,DT_B,DT_C,zpos(DT_A,DT_B,DT_C),0.1);
    for I = 2:average
        fprintf('.');
        [Axp,Ayp,Azp] = jitterpoints(DT_A(:,1),DT_A(:,2),DT_A(:,3),sigmaA);
        [Bxp,Byp,Bzp] = jitterpoints(DT_B(:,1),DT_B(:,2),DT_B(:,3),sigmaB);
        [Cxp,Cyp,Czp] = jitterpoints(DT_C(:,1),DT_C(:,2),DT_C(:,3),sigmaC);
        IM_I = IM_I + rasterization(roi,resolution,[Axp,Ayp,Azp],[Bxp,Byp,Bzp],[Cxp,Cyp,Czp],1./circumference(DT_A,DT_B,DT_C),0.1);
    end
    fprintf('.\n');
    IM_I = IM_I ./ average;
    %IM = z_colorize(size(IM_I),IM_I,IM_z);
    IM=IM_I;
    %
end
%
function z = zpos(A,B,C)
    z = max([A B C],[],2);
end
%
function [xp,yp,zp] = jitterpoints(xp,yp,zp,sigma)
    npts = size(xp,1);
    xp = xp + (sigma .* randn(npts,1));
    yp = yp + (sigma .* randn(npts,1));
    zp = zp + (sigma .* randn(npts,1));
end
%
function IM = rasterization(roi,resolution,DT_A,DT_B,DT_C,I,thr_I)
    imsiz = [roi(2)./resolution,roi(4)./resolution];
    n_tri = size(DT_A,1);
    n_pts = repmat([3],n_tri,1);   % number of vertices -- all triangles have 3 vertices
    pts = ...   % linear coordinated of the vertices
    [     ...
        sub2ind(imsiz,round(DT_A(:,1)./resolution),round(DT_A(:,2)./resolution)), ...
        sub2ind(imsiz,round(DT_B(:,1)./resolution),round(DT_B(:,2)./resolution)), ...
        sub2ind(imsiz,round(DT_C(:,1)./resolution),round(DT_C(:,2)./resolution))  ...
    ];
    draw_idx = (I>thr_I);
    IM = rasterize(int32(imsiz(1)),int32(imsiz(2)),int32(sum(draw_idx)),int32(n_pts(draw_idx)),int32(pts(draw_idx,:)),single(I(draw_idx)));
    IM = reshape(IM,imsiz(1),imsiz(2));
end
%
function circ = circumference(A,B,C)
    circ = ptdist(A,B) + ptdist(B,C) + ptdist(C,A);
end
%
function d = ptdist(a,b)
    d = sqrt((a(:,1)-b(:,1)).^2 + (a(:,2)-b(:,2)).^2 + (a(:,3)-b(:,3)).^2);
end
%
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function XY = perspective_projection(P,molecules)
    molP = (P * molecules')';
    d_Az = 1.0 ./ molP(:,4);
    molP(:,1) = molP(:,1) .* d_Az;
    molP(:,2) = molP(:,2) .* d_Az;
    molP(:,3) = molP(:,3) .* d_Az;
	molP(:,4) = molP(:,4) .* d_Az;
	XY = molP(:,1:2);
end
%
function quat = quat_rot(x0,y0,z0,angle)
    quat = [cos(angle/2) x0*sin(angle/2) y0*sin(angle/2) z0*sin(angle/2)];
end
%
function quat = quat_mul(q1,q2)
    quat = ...
    [ ...
        q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3) - q1(4)*q2(4), ...
        q1(1)*q2(2) + q1(2)*q2(1) + q1(3)*q2(4) - q1(4)*q2(3), ...
        q1(1)*q2(3) - q1(2)*q2(4) + q1(3)*q2(1) + q1(4)*q2(2), ...
        q1(1)*q2(4) + q1(2)*q2(3) - q1(3)*q2(2) + q1(4)*q2(1) ...
    ];
end
%
function IMclr = z_colorize(imsize,IM_intensity,IM_depth)
    % normalize first!
    IM_I = minmax_norm(IM_intensity(:));
    IM_z = 1-minmax_norm(IM_depth(:));  % reverse the z-axis! the furthest should be the lowest value
    % then colorize
    [uIMz,idx1,idx2] = unique(IM_z);
    map = rgb2hsv(jet(length(uIMz)));   % `jet` colormap
    imhsv = map(idx2,:);    % colorize by index to the colormap
    imhsv(:,3) = imhsv(:,3) .* IM_I(:); % set the intensity
    imhsv(imhsv>1)=1;   % out of bounds check
    IMclr = hsv2rgb(reshape(imhsv,[imsize(1:2) 3]));    % => rgb
end
%
function draw_and_save_color(IM,impath)
    imwrite(IM,impath);
    figure(99)
    imshow(IM)
    drawnow;
end
%
function draw_and_save(IM,impath)
    imwrite(ind2rgb(gray2ind(IM,65536),hot(65536)),impath);
    figure(99)
    imagesc(minmax_norm(IM))
    colormap(hot)
    drawnow;
end