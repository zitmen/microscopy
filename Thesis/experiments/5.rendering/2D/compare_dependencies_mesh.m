function compare_dependencies_mesh(recompute_results)
    if (recompute_results == 1) || ~exist('_results_/mesh_results.mat', 'file')
        compute_results();
    end
    analysis();
end
%%
function compute_results()
    %
    % config
    imsiz = [ 120 120 ];   % [rows cols] => [height width]
    XYg = [ 60 ]; % X,Y - grid points
    margin = 20; % margin - because of jittering, so we dont go out of bounds
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    %
    Ycr = 10:10:1000;
    errors = zeros(6,length(Ycr));
    data = [];
    for Yci = 1:length(Ycr)
        Yc = Ycr(Yci);
        if Yci == 1
            dYc = Ycr(Yci);
        else
            dYc = Ycr(Yci) - Ycr(Yci-1);
        end
        fprintf('\n\n============ Yc: %d ===================\n',Yc);
        % Note: each iteration add new dots instead of generating
        % a brand new set to avoid confusions during the analysis
        %
        % datagen - vertical
        new_data(1:dYc,1) = PSM*randn(dYc,1) + XYg(1);
        new_data(1:dYc,2) = margin+randi(imsiz(1)-2*margin,dYc,1);
        new_data(1:dYc,3) = (loc_precision_sigma/resolution)*rand(dYc,1)+(loc_precision_mu/resolution);
        new_data(1:dYc,4) = ones(dYc,1);%2*rand(numpts,1)+0.1;
        %
        % datagen - horizontal
        new_data(dYc+1:2*dYc,1) = margin+randi(imsiz(2)-2*margin,dYc,1);
        new_data(dYc+1:2*dYc,2) = PSM*randn(dYc,1) + XYg(1);
        new_data(dYc+1:2*dYc,3) = (loc_precision_sigma/resolution)*rand(dYc,1)+(loc_precision_mu/resolution);
        new_data(dYc+1:2*dYc,4) = ones(dYc,1);%2*rand(numpts,1)+0.1;
        %
        % remove the crossings
        radius = 10;%px
        valid = true(size(new_data,1),1);
        valid(:) = valid(:) & (((new_data(:,1)-XYg(1)).^2 + (new_data(:,2)-XYg(1)).^2) > (radius^2));
        if isempty(data)
            data = new_data(valid,:);
        else
            data = vertcat(data,new_data(valid,:));
        end

        %
        % rendering
        titles{1} = 'density';
        images{1} = smlm_2Drendering(data, roi, resolution, 'density');
        errors(1,Yci) = evaluate(images{1}, imsiz, PSM, XYg);
        
        titles{2} = 'scatter';
        images{2} = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',50));
        errors(2,Yci) = evaluate(images{2}, imsiz, PSM, XYg);
        
        titles{3} = 'quadtree';
        images{3} = smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',50));
        errors(3,Yci) = evaluate(images{3}, imsiz, PSM, XYg);
        %{
        titles{4} = 'triangulation (intensity = circ; avg = 10)';
        images{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference'));
        errors(4,Yci) = evaluate(images{4}, imsiz, PSM, XYg);
        %}
        titles{4} = 'triangulation (intensity = area; avg = 50)';
        images{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',50,'delaunay_thr',10,'intensity','area'));
        errors(4,Yci) = evaluate(images{4}, imsiz, PSM, XYg);
        
        titles{5} = 'histogram';
        images{5} = smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',50));
        errors(5,Yci) = evaluate(images{5}, imsiz, PSM, XYg);
        
        titles{6} = 'gauss';
        images{6} = smlm_2Drendering(data, roi, resolution, 'gauss');
        errors(6,Yci) = evaluate(images{6}, imsiz, PSM, XYg);
    end
    %
    save('_results_/mesh_results.mat','-v6','-mat');
end
%%
% helper functions
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%
function [ERR,Xc,Yc] = evaluate(IM, imsiz, PSM, XYg)
    histX = sum(integral_norm(IM,length(XYg)), 2);
    histY = sum(integral_norm(IM,length(XYg)), 1);
    [val,Xc] = max(histX);
    [val,Yc] = max(histY);
    Xc = lsqnonlin(@est_X,Xc,[],[],optimset('Display','none'));
    Yc = lsqnonlin(@est_Y,Yc,[],[],optimset('Display','none'));
    ERR = sqrt((Xc-XYg).^2 + (Yc-XYg).^2);
    
    function err = est_X(center)
        err = double(histX)' - normpdf(1:imsiz(1), center, PSM);
    end
    function err = est_Y(center)
        err = double(histY) - normpdf(1:imsiz(2), center, PSM);
    end
end
%%
function analysis()

    load('_results_/mesh_results.mat'); % errors(method,distance--[50:50:1000])
    titles{1} = 'density'; titles{2} = 'scatter'; titles{3} = 'quadtree';
    titles{4} = 'triangulation'; titles{5} = 'histogram'; titles{6} = 'gauss';
    plot_compare(errors, titles);
end
%%
function plot_compare(results, titles)
    figure('name','Compare molecules counts dependency');
    cc = hsv(length(titles));
    hold on
    for fi=1:length(titles)
        %
        % binning
        res = [];
        for ri=((50:50:1000)./10)
            res(ri/5) = mean(results(fi,ri-5+1:ri));
        end
        plot(50:50:1000, res, '-x', 'color', cc(fi,:))
        %
        %plot(10:10:1000, results(fi,:), 'color', cc(fi,:))
        xlabel('Molecules per line')
        ylabel('Error of localization of the crossing [px]')
    end
    hold off
    legend(titles, 'Location', 'NorthEast')
    xlim([50,1000])
end