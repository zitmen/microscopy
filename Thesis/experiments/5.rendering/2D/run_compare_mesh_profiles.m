function run_compare_mesh_profiles
    %
    % config
    imsiz = [ 120 120 ];   % [rows cols] => [height width]
    XYg = [ 60 ]; % X,Y - grid points
    margin = 20; % margin - because of jittering, so we dont go out of bounds
    Yc = 10;  % Y - count of points per line
    numpts = length(XYg) * Yc;
    PSM = 5;   % Point Spread Multiplier
    resolution = 1;   % (r < 1)=>upscaling; (r > 1)=>downscaling
    roi = [ 0 imsiz(1) 0 imsiz(2) ];
    loc_precision_mu = 2.5;
    loc_precision_sigma = 1.5;
    %
    data = zeros(2*numpts,4); % [x y precision amplitude]
    % datagen - vertical
    data(1:numpts,1) = PSM*randn(numpts,1);
    data(1:numpts,2) = margin+randi(imsiz(1)-2*margin,numpts,1);
    data(1:numpts,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
    data(1:numpts,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;
    for i=1:length(XYg)
        idx = ((i-1)*Yc+1):(i*Yc);
        data(idx,1) = data(idx,1) + XYg(i);
    end
    %
    % datagen - horizontal
    data(numpts+1:end,1) = margin+randi(imsiz(2)-2*margin,numpts,1);
    data(numpts+1:end,2) = PSM*randn(numpts,1);
    data(numpts+1:end,3) = (loc_precision_sigma/resolution)*rand(numpts,1)+(loc_precision_mu/resolution);
    data(numpts+1:end,4) = ones(numpts,1);%2*rand(numpts,1)+0.1;
    for i=1:length(XYg)
        idx = ((i-1)*Yc+1):(i*Yc);
        data(numpts+idx,2) = data(numpts+idx,2) + XYg(i);
    end
    %
    % remove the crossings
    radius = 10;%px
    valid = true(size(data,1),1);
    for i=1:length(XYg)
        for j=1:length(XYg)
            valid(:) = valid(:) & (((data(:,1)-XYg(i)).^2 + (data(:,2)-XYg(j)).^2) > (radius^2));
        end
    end
    data = data(valid,:);

    %
    % rendering
    titles{1} = 'density';
    images{1} = smlm_2Drendering(data, roi, resolution, 'density');
    err = eval_and_draw_figures('Rendering: Density', images{1}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{1},err);
    
    titles{2} = 'scatter';
    images{2} = smlm_2Drendering(data, roi, resolution, 'scatter', struct('average',50));
    err = eval_and_draw_figures('Rendering: Scatter', images{2}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{2},err);
    
    titles{3} = 'quadtree';
    images{3} = smlm_2Drendering(data, roi, resolution, 'quadtree', struct('average',50));
    err = eval_and_draw_figures('Rendering: Quadtree', images{3}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{3},err);
    %{
    titles{4} = 'triangulation (intensity = circ; avg = 10)';
    images{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',10,'delaunay_thr',10,'intensity','circumference'));
    err = eval_and_draw_figures('Rendering: Triangulation (circumference)', images{4}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{4},err);
    %}
    titles{4} = 'triangulation (intensity = area; avg = 50)';
    images{4} = smlm_2Drendering(data, roi, resolution, 'triangulation', struct('average',50,'delaunay_thr',10,'intensity','area'));
    err = eval_and_draw_figures('Rendering: Triangulation (area)', images{4}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{4},err);
    
    titles{5} = 'histogram';
    images{5} = smlm_2Drendering(data, roi, resolution, 'histogram', struct('average',50));
    err = eval_and_draw_figures('Rendering: Histogram', images{5}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{5},err);
    
    titles{6} = 'gauss';
    images{6} = smlm_2Drendering(data, roi, resolution, 'gauss');
    err = eval_and_draw_figures('Rendering: Gauss', images{6}, imsiz, PSM, XYg);
    fprintf('%s: err=%.6f\n',titles{6},err);
end
%%
% helper functions
function IM = minmax_norm(IM)
    mmin = min(IM(:));
    mmax = max(IM(:));
    IM = (IM - mmin) ./ (mmax - mmin);
end
%
function IM = integral_norm(IM,line_pair_count)
    IM = IM / (sum(IM(:)) / line_pair_count); % each pair of lines has to normalize it's integral to 1
end
%
function [ERR,Xc,Yc] = eval_and_draw_figures(figid, IM, imsiz, PSM, XYg)
    figure('Name',figid,'NumberTitle','off')
    subplot(2,2,1)
    hold on
    % rendered image
    imagesc(minmax_norm(IM))
    colormap(hot)
    % real centers
    for i=1:length(XYg)
        for j=1:length(XYg)
            gridX(sub2ind([length(XYg),length(XYg)],i,j)) = XYg(i);
            gridY(sub2ind([length(XYg),length(XYg)],i,j)) = XYg(j);
        end
    end
    plot(gridY,gridX,'gx')
    axis off
    axis([1 imsiz(1) 1 imsiz(2)])
    hold off
    % ----- VERTICAL ------ %
    subplot(2,2,2)
    hold on
    % ideal
    for i=1:length(XYg)
        x = normpdf(1:imsiz(1), XYg(i), PSM);
        plot(integral_norm(x, 0.5),1:imsiz(1),'-g')
    end
    % measured
    histX = sum(integral_norm(IM,length(XYg)), 2);
    plot(histX,1:imsiz(1),'-r')
    set(gca,'YtickLabel',[])
    ylim([1 imsiz(2)])
    ylabel('Y')
    xlabel('Normalized intensity')
    hold off
    % ----- HORIZONTAL ------ %
    subplot(2,2,3)
    hold on
    % ideal
    for i=1:length(XYg)
        y = normpdf(1:imsiz(2), XYg(i), PSM);
        plot(1:imsiz(2),integral_norm(y, 0.5),'-g')
    end
    % measured
    histY = sum(integral_norm(IM,length(XYg)), 1);
    plot(1:imsiz(2),histY,'-r')
    set(gca,'XAxisLocation','top')
    set(gca,'YDir','reverse')
    set(gca,'XtickLabel',[])
    xlim([1 imsiz(1)])
    xlabel('X')
    ylabel('Normalized intensity')
    hold off
    %
    % EVALUATE - FIND THE CENTER
    [val,Xc] = max(histX);
    [val,Yc] = max(histY);
    Xc = lsqnonlin(@est_X,Xc,[],[],optimset('Display','none'));
    Yc = lsqnonlin(@est_Y,Yc,[],[],optimset('Display','none'));
    ERR = sqrt((Xc-XYg).^2 + (Yc-XYg).^2);
    
    function err = est_X(center)
        err = double(histX)' - normpdf(1:imsiz(1), center, PSM);
    end
    function err = est_Y(center)
        err = double(histY) - normpdf(1:imsiz(2), center, PSM);
    end
end