function [IM, roi] = smlm_2Drendering(data, roi, resolution, method, options)
% data       ... [npts x 4] matrix with columns [x y sigma intensity]
%   x,y      ... position [um]
%   dx       ... localization accuracy [um]
%   N        ... intensity
% roi        ... region of interest [xmin xmax ymin ymax] in [um]
% resolution ... output resolution [um/pixel]
% method     ... rendering method
% options    ... options for rendering method

  % convert um -> pixel
  xpos = (data(:,1)-roi(3)) / resolution;
  ypos = (data(:,2)-roi(1)) / resolution;
  xystd = data(:,3) / resolution;
  ampl = data(:,4);
  npts = length(xpos);
  
  % adjust roi with image size and resolution
  imsiz = round([roi(2)-roi(1), roi(4)-roi(3)] / resolution);
  roi([2,4]) = imsiz * resolution + roi([1,3]);

  % options
  if nargin < 5, options = []; end
  if ~isfield(options,'average'), options.average = 1; end;
  if ~isfield(options,'radius'), options.radius = 8 * resolution; end;
  if ~isfield(options,'QTmaxevents'), options.QTmaxevents = 1; end;
  if ~isfield(options,'QTrenderdepth'), options.QTrenderdepth = ceil(log(max(imsiz))/log(2)); end;
  if ~isfield(options,'QTresample'), options.QTresample = 'bilinear'; end;
  if ~isfield(options,'delaunay_thr'), options.delaunay_thr = 10; end;
  if ~isfield(options,'intensity'), options.intensity = 'area'; end;
  if (options.average < 1), error('rendering:average', 'Number of jitters has to be a positive number.'); end; 
  
  % allocate memory for image
  IM = zeros(imsiz,'single');
    
  switch method
    case 'scatter'
      IM = render_scatter(options, IM, imsiz, xpos, ypos, xystd, npts);
    case 'histogram'
      IM = render_hist2(options, IM, imsiz, xpos, ypos, xystd, npts);
    case 'gauss'
      IM = render_gauss(options, IM, imsiz, resolution, xpos, ypos, xystd, ampl, npts);
    case 'density'
      IM = render_density(options, IM, imsiz, resolution, xpos, ypos, xystd, npts);
    case 'quadtree'
      IM = render_quadtree(options, IM, imsiz, xpos, ypos, xystd, npts);
    case 'triangulation'
      IM = render_delaunay(options, IM, imsiz, xpos, ypos, xystd, npts);
    otherwise
      error('rendering:unknownmethod','Unknown rendering method.');
  end

  % ---------------------------------------------
  function IM = render_scatter(options, IM, imsiz, xpos, ypos, xystd, npts)
    fprintf('Generating %d scatter plot(s)\nProgress ', options.average);
    images = cell(options.average,1);
    for I = 1:options.average
      % jitter points
      [xp,yp] = jitterpoints(xpos,ypos,xystd,options.average,npts);
      % accumulate scatter plots
      idx = sub2ind(imsiz, round(yp), round(xp));
      IM(idx) = IM(idx) + 1;
      images{I} = IM;
      % progress
      if (mod(I,round(options.average/10)) == 0), fprintf('.'); end;   
    end
    fprintf(' done\n');
    IM = zeros(imsiz);
    for I = 1:options.average,IM = IM + images{I}/I;end;
  end % render_scater

  % ---------------------------------------------
  function IM = render_hist2(options, IM, imsiz, xpos, ypos, xystd, npts)
    xbin = (0:imsiz(2))+0.5;
    ybin = (0:imsiz(1))+0.5;
    fprintf('Generating %d histogram(s)\nProgress ', options.average);
    images = cell(options.average,1);
    for I = 1:options.average
      % jitter points
      [xp,yp] = jitterpoints(xpos,ypos,xystd,options.average,npts);
      % accumulate histograms
      IM = IM + hist2(yp,xp,ybin,xbin);
      images{I} = IM;
      % progress
      if (mod(I,round(options.average/10)) == 0), fprintf('.'); end;   
    end    
    fprintf(' done\n');
    IM = zeros(imsiz);
    for I = 1:options.average,IM = IM + images{I}/I;end;
  end % render_hist2

  % ---------------------------------------------
  function IM = render_gauss(options, IM, imsiz, resolution, xpos, ypos, xystd, ampl, npts)
    % rendering coordinates for one gaussian
    radius = round(options.radius / resolution);  % rendering radius for a dot
    [x,y] = meshgrid(-radius:radius,-radius:radius);
    idx = x.^2 + y.^2 < radius^2;
    x = x(idx); y = y(idx);
    % generate image
    fprintf('Progress ');
    for I = 1:npts
      % get position
      u = floor(xpos(I)); du = xpos(I) - u;
      v = floor(ypos(I)); dv = ypos(I) - v;
      % take coordinates only inside the image
      idx = ~(u+x < 1 | u+x > imsiz(2) | v+y < 1 | v+y > imsiz(1));
      % render
      z = ampl(I)*kernelgauss2D(x(idx)-du, y(idx)-dv, 2*xystd(I)^2);
      idx = sub2ind(imsiz,v+y(idx),u+x(idx));
      IM(idx) = IM(idx) + z;
      % progress
      if (mod(I,npts/10) == 0), fprintf('.'); end;    
    end
    fprintf(' done\n');
  end % gauss

  % ---------------------------------------------
  function IM = render_density(options, IM, imsiz, resolution, xpos, ypos, xystd, npts)
    % rendering coordinates for one gaussian
    radius = round(options.radius / resolution);  % rendering radius for a dot
    [x,y] = meshgrid(-radius:radius,-radius:radius);
    idx = x.^2 + y.^2 < radius^2;
    x = x(idx); y = y(idx);
    % generate image
    fprintf('Progress ');
    for I = 1:npts
      % get position
      u = floor(xpos(I)); du = xpos(I) - u;
      v = floor(ypos(I)); dv = ypos(I) - v;
      % take coordinates only inside the image
      idx = ~(u+x < 1 | u+x > imsiz(1) | v+y < 1 | v+y > imsiz(2));
      % render
      z = kernelgauss2D(x(idx)-du, y(idx)-dv, 2*xystd(I)^2);      
      idx = sub2ind(imsiz,v+y(idx),u+x(idx));
      IM(idx) = IM(idx) + z;
      % progress
      if (mod(I,npts/10) == 0), fprintf('.'); end;
    end
    fprintf(' done\n');
  end % render_density

  % ---------------------------------------------
  function IM = render_quadtree(options, IM, imsiz, xpos, ypos, xystd, npts)
    % quad-tree histogram must be computed on a square area otherwise
    % you would see rectangle structures instead of squares
    siz = max(imsiz);
    lim = [0, siz]+0.5;
    fprintf('Generating %d quad-tree(s)\nProgress ', options.average);
    images = cell(options.average,1);
    for I = 1:options.average
      % jitter points
      [xp,yp] = jitterpoints(xpos, ypos, xystd, options.average,npts);
      % accumulate quad-tree histograms
      im = histqtree(single(yp), single(xp), lim, lim, options.QTmaxevents, options.QTrenderdepth);
      im = imresize(im, [siz siz], options.QTresample);
      IM = IM + im(1:imsiz(1),1:imsiz(2));
      images{I} = IM;
      % progress
      if (mod(I,round(options.average/10)) == 0), fprintf('.'); end;
    end;
    fprintf(' done\n');
    IM = zeros(imsiz);
    for I = 1:options.average,IM = IM + images{I}/I;end;
  end % render_quadtree

  % ---------------------------------------------
  function IM = render_delaunay(options, IM, imsiz, xpos, ypos, xystd, npts)
    fprintf('Generating %d Delaunay triangulation(s)\nProgress ', options.average);
    images = cell(options.average,1);
    for I = 1:options.average
        % jitter points
        [xp,yp] = jitterpoints(xpos,ypos,xystd,options.average,npts);
        % accumulate Delaunay triangulations
        im = delaunay2(single([yp,xp]), [1 1 imsiz], imsiz, options.intensity);
        im(im > options.delaunay_thr) = options.delaunay_thr;
        IM = IM + im(1:imsiz(1),1:imsiz(2));
        images{I} = IM;
        % progress
        if (mod(I,round(options.average/10)) == 0), fprintf('.'); end;   
    end;
    fprintf(' done\n');
    IM = zeros(imsiz);
    for I = 1:options.average,IM = IM + images{I}/I;end;
  end % render_delaunay

  % ---------------------------------------------
  function z = kernelgauss2D(x,y,twotimessigma2)
    z = exp(-(x.^2 + y.^2)/twotimessigma2)/(pi*twotimessigma2);
  end

  % ---------------------------------------------
  % add normal noise to localized points
  function [xp,yp] = jitterpoints(xp,yp,sigma,num,npts)
    if (num > 1),
      xp = xp + (sigma .* randn(npts,1));
      yp = yp + (sigma .* randn(npts,1));
    end
  end % jitterpoints

end % smlm_2Drendering
