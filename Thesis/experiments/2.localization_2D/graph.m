%% Analyse performance of the best filter+detector pair
function graph(datapath,respath,filename,frames,filter,detector)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), error('Results path does not exist!'); end
    thr_dist = FWHM(1.3);   % sigmapsf=1.3 ==> width ~3px
    CTP = 0; % count of true-positives
    TP = zeros(500*length(frames),4); % true-positives, max 500 molecules per frame
    TOTAL = zeros(500*length(frames),4);    % list of all generated dots
    
    fprintf('\n%s + %s\n',filter,detector);
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        if ~exist(sprintf('%s/%s.%04d.mat',datapath,filename,frame),'file'); continue; end;
        if ~exist(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector),'file'); continue; end;
        
        % load `dot_params`
        load(sprintf('%s/%s.%04d.mat',datapath,filename,frame));
        
        % load `fits`
        load(sprintf('%s/%s_%04d_%s_%s.mat',respath,filename,frame,filter,detector));
        
        if ~isempty(fits)
            % Run the analysis
            % 1. threshold
            fits = thresholding(fits);
            % 2. pair the dots with its fits
            [D1,I1] = pdist2(dot_params(:,1:2),fits(:,3:4),'euclidean','Smallest',1);
            [D2,I2] = pdist2(fits(:,3:4),dot_params(:,1:2),'euclidean','Smallest',1);
            idxTP = ((I2(I1(I2))==I2) & (D2 < thr_dist));
            % 3. add the TP,TOTAL
            TOTAL((frame-frames(1))*500+1:(frame-frames(1))*500+500,:) = dot_params(:,:);
            TP(CTP+1:CTP+sum(idxTP),:) = dot_params(idxTP,:);
            CTP = CTP + sum(idxTP);
        else
            TOTAL((frame-frames(1))*500+1:(frame-frames(1))*500+500,:) = dot_params(:,:);
        end

    end
    fprintf('Done.\n');
    
    eval_results(TP(1:CTP,:),TOTAL);

end

%%
function fits = thresholding(fits)

    a = 31.04;
    b = -15.68;
    c = 12.09;
    
    SNR = fits(:,6) ./ fits(:,11);
    sigma = fits(:,5);
    
    fits = fits(((a + b.*sigma + c.*(sigma.^2)) < SNR),:);

end

%%
function width = FWHM(sigma)

    width = 2*sqrt(2*log(2)) * sigma;

end

%%
function eval_results(TP,TOTAL)

    sigma_0 = TOTAL(:,3);
    SNR_0 = TOTAL(:,4) ./ 0.0022;
    %
    sigma = TP(:,3);
    SNR = TP(:,4) ./ 0.0022;
    %
    figure(1);
    n_0 = hist(SNR_0,100);
    n_detected = hist(SNR,100);
    percentage_detected = n_detected ./ n_0;
    [xx,yy] = smoothLine(1:100,percentage_detected);
    subplot(1,2,1),plot(xx,yy.*100),axis([1 100 0 100]),xlabel('SNR'),ylabel('Molecules detected [%]');
    %
    n_0 = hist(sigma_0,100);
    n_detected = hist(sigma,100);
    percentage_detected = n_detected ./ n_0;
    [xx,yy] = smoothLine(linspace(0.3,2.2,100),percentage_detected);
    subplot(1,2,2),plot(xx,yy.*100),axis([0.3 2.2 0 100]),xlabel('\sigma_0'),ylabel('Molecules detected [%]');
    
    figure(2);
    imagesc(hist2(SNR,sigma,1:99,0.3:0.1:2.2) ./ hist2(SNR_0,sigma_0,1:99,0.3:0.1:2.2)),...
        xlabel('SNR'),ylabel('\sigma_0'),set(gca,'YDir','normal'),...
        set(gca,'YTickLabel',0.3:0.2:2.2);
    %{
    imagesc(hist2(SNR,sigma,1:0.25:19,1.0:0.01:2.0) ./ hist2(SNR_0,sigma_0,1:0.25:19,1.0:0.01:2.0)),...
        xlabel('SNR'),ylabel('\sigma_0'),set(gca,'YDir','normal'),...
        set(gca,'YTickLabel',1.0:0.2:2.0);
    %}
end