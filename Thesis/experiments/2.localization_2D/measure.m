%% Test performance of the localization on the static dots
% boxsize - size of the fitting region: (length(-boxsize:+boxsize))^2
% sigmapsf - initial sigma estimate for the fitting method
function measure(datapath,respath,filename,frames,estimators,boxsize,sigmapsf)

    % initialize
    if ~isdir(datapath), error('Data path does not exist!'); end
    if ~isdir(respath), mkdir(respath); end
    
    % run the analysis
    fprintf('Analyzing...\n');
    for frame=frames
        
        fprintf('  frame %d/%d\n',frame,frames(end));
        
        % load image
        imraw = double(imread(sprintf('%s/%s.%04d.tiff',datapath,filename,frame)))/65535; % 16b

        for ei=1:length(estimators)
            
            if exist(sprintf('%s/%s_%04d_%s.mat',respath,filename,frame,estimators{ei}.name),'file'); continue; end;

            fprintf('    %s\n',estimators{ei}.name);

            % detect molecules
            loc = detectMolecules(imraw);

            % store image
            [imfit,loc] = validateAndExtract(boxsize,imraw,loc);

            % fit model
            fits = estimators{ei}.function(boxsize,sigmapsf,imfit,loc);

            % save fits
            save(sprintf('%s/%s_%04d_%s.mat',respath,filename,frame,estimators{ei}.name),'-v6','fits');

        end

    end
    fprintf('Done.\n');

end

%%
function loc = detectMolecules(imraw)

    h = fspecial('gauss',11,1.3);
    im = imfilter(imraw,h,'replicate','same');
    idx = findlocmax2d(im);
    [u,v] = ind2sub(size(im),idx);
    loc = [u,v];

end

%%
function [imfit,loc] = validateAndExtract(boxsize,im,loc)

    % init
    box = int32(-boxsize:+boxsize);
    imsize = size(im);
    fitregionsize = 2*boxsize+1;

    % remove points which are at the border location
    valid = ~( (loc(:,1)-boxsize) <        1  | (loc(:,2)-boxsize) <        1  | ...
               (loc(:,1)+boxsize) > imsize(1) | (loc(:,2)+boxsize) > imsize(2) );
    loc = loc(valid,:);
    
    % extract the valid molecules from the image
    imfit = zeros(size(loc,1),fitregionsize,fitregionsize);
    for i = 1:size(loc,1)
        pt = int32(loc(i,1:2));
        imfit(i,:,:) = im(pt(1)+box,pt(2)+box);
    end

end