function run_experiment(frames)

    % Initialize
	datapath = '_data_';
    respath = '_results_';
    %dataset = 'snr1-10_sigma1.4';
    dataset = 'snr1-100_sigma1.3-1.5';
    %dataset = 'snr10-20_sigma0.3-2.2';
	
    boxsize = 5;
    psfsigma = 1.3;
    %{
    estimators{ 1} = struct('name', 'LSE_AIRY'     , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_LSE,@PSF_Airy    ,boxsize,sigmapsf,imfit,loc)));
    estimators{ 2} = struct('name', 'LSE_GAUSS'    , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_LSE,@PSF_Gauss   ,boxsize,sigmapsf,imfit,loc)));
    estimators{ 3} = struct('name', 'LSE_PARABOLA' , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_LSE,@PSF_Parabola,boxsize,sigmapsf,imfit,loc)));
    estimators{ 4} = struct('name', 'MLE_AIRY'     , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_MLE,@PSF_Airy    ,boxsize,sigmapsf,imfit,loc)));
    estimators{ 5} = struct('name', 'MLE_GAUSS'    , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_MLE,@PSF_Gauss   ,boxsize,sigmapsf,imfit,loc)));
    estimators{ 6} = struct('name', 'MLE_PARABOLA' , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_MLE,@PSF_Parabola,boxsize,sigmapsf,imfit,loc)));
    estimators{ 7} = struct('name', 'PARABOLA'     , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_QE ,[]           ,boxsize,sigmapsf,imfit,loc)));
    estimators{ 8} = struct('name', 'GRADIENT'     , 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_GFE,[]           ,boxsize,sigmapsf,imfit,loc)));
    %estimators{ 9} = struct('name', 'REF_MLE_GAUSS', 'function', @(boxsize,sigmapsf,imfit,loc)(ref_fit_MLE_Gauss(boxsize,sigmapsf,imfit,loc)));
    %estimators{10} = struct('name', 'REF_LSE_GAUSS', 'function', @(boxsize,sigmapsf,imfit,loc)(ref_fit_LSE_Gauss(boxsize,sigmapsf,imfit,loc)));
    %}
    estimators{1} = struct('name', 'MLE_PARABOLA', 'function', @(boxsize,sigmapsf,imfit,loc)(fitting(@fit_MLE,@PSF_Parabola,boxsize,sigmapsf,imfit,loc)));
    %
    measure(datapath,respath,dataset,frames,estimators,boxsize,psfsigma);

end

%% Estimator wrappers
% OUTPUT: set of vectors - results of localization and fitting: [x_{loc}, y_{loc}, x_{fit}, y_{fit}, \sigma_{fit}, I_{fit}, bkg_{fit}, \sum{chi2_{fit}}, Nim_{fit}, \mu_{bkg_{fit}}, \sigma_{bkg_{fit}}]
function fits = fitting(estimator,PSF_model,boxsize,sigmapsf,imfit,loc)

    % init
    center = boxsize + 1;
    fitregionsize = 2*boxsize+1;
    fits = single(zeros(size(imfit,1),11));
    nfits = 0;

    for i = 1:size(imfit,1)

        % fitting region
        im = reshape(imfit(i,:,:),fitregionsize,fitregionsize);

        % fit 2D gaussian bell shaped function
        a0 = [ center,  center, sigmapsf, max(im(:))];    % initial parameters
        a = estimator(PSF_model,a0,im);

        % position
        a(1:2) = a(1:2) - center + single(loc(i,1:2));
        
        % save results
        nfits = nfits + 1;
        fits(nfits,:) = single([single(loc(i,1:2)), a, 0, 0, 0, 0, 0.0022]);
  
    end
    
    if nfits > 0
        fits = fits(1:nfits,:);
    else
        fits = [];
    end
    
end

function A = fit_LSE(PSF_model,A0,I)
    %A = fminsearch(@(A_est)(LSE(I,A_est,size(I),PSF_model)),A0,optimset('Display','none'));
    A = lsqnonlin(@(A_est)(LSE(I,A_est,size(I),PSF_model)),A0,[],[],optimset('Display','none'));
end

function A = fit_MLE(PSF_model,A0,I)
    A = fminsearch(@(A_est)(MLE(I,A_est,size(I),PSF_model)),A0,optimset('Display','none'));    
end

function A = fit_QE(PSF_model,A0,I)
    [xs,ys,Is] = QE(A0(1),A0(2),I,1);
    A = [xs ys A0(3) Is];
end

function A = fit_GFE(PSF_model,A0,I)
    [xc,yc,sigma] = GFE(I);
    A = [xc yc sigma A0(4)];
end


%% MLE Estimator
function logL = MLE(I,A,fit_region,PSF_model)  % I - data, A - estimated parameters

    logL = -logLikelihood(I,PSF_model(fit_region,A));
    
    %
    function logL = logLikelihood(h, h_est)
        logh = log(h_est);
        logh(logh<-1e6) = -1e6;
        logL = sum(sum(h.*logh - h_est));
    end

end

%% LSE Estimator
function err = LSE(I,A,fit_region,PSF_model)  % I - data, A - estimated parameters

    %sse = sum(sum((I-PSF_model(fit_region,A)).^2));
    err = I - PSF_model(fit_region,A);

end

%% PSF models
function h = PSF_Airy(size,params)

    % params -- [x0,y0,sigma,I]
    [x,y] = meshgrid(1:size(1),1:size(2));
    z = (pi.*distance([x(:) y(:)],[params(1) params(2)]))./(2.3*params(3)); % (pi*q)/(lambda*(R/d)) --> q=dist,divisor=sigma
    h = ((2.*besselj(1,z))./z).^2;
    h(isnan(h)) = 1;    % Nan = 0/0, that is typically z=0, i.e. distance = 0, i.e. center of the Airy disk
    h = reshape(h, size);
    h = (params(4) * (pi/2)) .* h;
    
    %
    function d = distance(P1,P2)
        d = sqrt((P1(:,1)-P2(:,1)).^2 + (P1(:,2)-P2(:,2)).^2);
    end
    
end

function h = PSF_Gauss(size,params)

    % params -- [x0,y0,sigma,I]
    [x,y] = meshgrid(1:size(1),1:size(2));
    h = params(4)*exp(-((((x-params(1)).^2)./(2*params(3)*params(3))) + (((y-params(2)).^2)./(2*params(3)*params(3)))));
    
end

function h = PSF_Parabola(size,params)

    % params -- [x0,y0,sigma,I]
    [x,y] = meshgrid(1:size(1),1:size(2));
    h = -(((x-params(1))./params(3)).^2 + ((y-params(2))./params(3)).^2);
    h = minmax_norm(h);
    h = (params(4) / (pi*params(3)^2)) .* h;    % adjust intensity
    
    %
    function N = minmax_norm(M)
        mmin = min(min(M));
        mmax = max(max(M));
        N = (M - mmin) ./ (mmax - mmin);
    end
    
end

%% Quadratic estimator
function [xs, ys, As] = QE(r, c, cim, nbr)
    % -------------------------------------------------------------------------
    % Compute local maxima to subpixel accuracy by fitting a quadric
    % function ax^2 + by^2 + cx + dy + e = z in neighbourhood
    %
    % return values are xs (X subpixel), ys (Y subpixel), As (Amplitude subpixel)
    % -------------------------------------------------------------------------

    if (nargin < 4), nbr = 1; end;

    if isempty(r)
       xs = []; ys = []; As = [];
       return;
    end

    [rows,cols] = size(cim);

    % indices of feature points
    ind = sub2ind([rows,cols],r,c);

    % 8-neibourhood coordinates
    [x,y] = meshgrid(-nbr:nbr,-nbr:nbr); x = x(:); y = y(:);
    numnbrs = length(x);
    indnbr = repmat(ind,1,numnbrs) + repmat((x*rows+y)',length(ind),1);

    % local parabola fitting by LSQ
    par = cim(indnbr) * pinv([x.^2, y.^2, x, y, ones(numnbrs,1)])';

    % subpixel corrections to original row and column coords
    x0 = -par(:,3) ./ par(:,1) / 2;
    y0 = -par(:,4) ./ par(:,2) / 2;

    % Add subpixel corrections
    xs = c + x0;
    ys = r + y0;

    % interpolated value at subpixel coordinates
    As = sum([x0.^2, y0.^2, x0, y0] .* par(:,1:4),2) + par(:,5);
end

%% Gradient Field Estimator
function [xc,yc,sigma] = GFE(I)
    
    % Number of grid points
    [Ny Nx] = size(I);

    % grid coordinates are -n:n, where Nx (or Ny) = 2*n+1
    % grid midpoint coordinates are -n+0.5:n-0.5;
    % The two lines below replace
    %    xm = repmat(-(Nx-1)/2.0+0.5:(Nx-1)/2.0-0.5,Ny-1,1);
    % and are faster (by a factor of >15 !)
    % -- the idea is taken from the repmat source code
    xm_onerow = -(Nx-1)/2.0+0.5:(Nx-1)/2.0-0.5;
    xm = xm_onerow(ones(Ny-1, 1), :);
    % similarly replacing
    %    ym = repmat((-(Ny-1)/2.0+0.5:(Ny-1)/2.0-0.5)', 1, Nx-1);
    ym_onecol = (-(Ny-1)/2.0+0.5:(Ny-1)/2.0-0.5)';  % Note that y increases "downward"
    ym = ym_onecol(:,ones(Nx-1,1));

    % Calculate derivatives along 45-degree shifted coordinates (u and v)
    % Note that y increases "downward" (increasing row number) -- we'll deal
    % with this when calculating "m" below.
    dIdu = I(1:Ny-1,2:Nx)-I(2:Ny,1:Nx-1);
    dIdv = I(1:Ny-1,1:Nx-1)-I(2:Ny,2:Nx);

    % Smoothing -- 
    h = ones(3)/9;  % simple 3x3 averaging filter
    fdu = conv2(dIdu, h, 'same');
    fdv = conv2(dIdv, h, 'same');
    dImag2 = fdu.*fdu + fdv.*fdv; % gradient magnitude, squared

    % Slope of the gradient .  Note that we need a 45 degree rotation of 
    % the u,v components to express the slope in the x-y coordinate system.
    % The negative sign "flips" the array to account for y increasing
    % "downward"
    m = -(fdv + fdu) ./ (fdu-fdv); 

    % *Very* rarely, m might be NaN if (fdv + fdu) and (fdv - fdu) are both
    % zero.  In this case, replace with the un-smoothed gradient.
    NNanm = sum(isnan(m(:)));
    if NNanm > 0
        unsmoothm = (dIdv + dIdu) ./ (dIdu-dIdv);
        m(isnan(m))=unsmoothm(isnan(m));
    end
    % If it's still NaN, replace with zero. (Very unlikely.)
    NNanm = sum(isnan(m(:)));
    if NNanm > 0
        m(isnan(m))=0;
    end

    %
    % Almost as rarely, an element of m can be infinite if the smoothed u and v
    % derivatives are identical.  To avoid NaNs later, replace these with some
    % large number -- 10x the largest non-infinite slope.  The sign of the
    % infinity doesn't matter
    try
        m(isinf(m))=10*max(m(~isinf(m)));
    catch
        % if this fails, it's because all the elements are infinite.  Replace
        % with the unsmoothed derivative.  There's probably a more elegant way
        % to do this.
        m = (dIdv + dIdu) ./ (dIdu-dIdv);
    end


    % Shorthand "b", which also happens to be the
    % y intercept of the line of slope m that goes through each grid midpoint
    b = ym - m.*xm;

    % Weighting: weight by square of gradient magnitude and inverse 
    % distance to gradient intensity centroid.
    sdI2 = sum(dImag2(:));
    xcentroid = sum(sum(dImag2.*xm))/sdI2;
    ycentroid = sum(sum(dImag2.*ym))/sdI2;
    w  = dImag2./sqrt((xm-xcentroid).*(xm-xcentroid)+(ym-ycentroid).*(ym-ycentroid));  

    % least-squares minimization to determine the translated coordinate
    % system origin (xc, yc) such that lines y = mx+b have
    % the minimal total distance^2 to the origin:
    % See function lsradialcenterfit (below)
    [xc yc] = lsradialcenterfit(m, b, w);



    %
    % Return output relative to upper left coordinate
    xc = xc + (Nx+1)/2.0;
    yc = yc + (Ny+1)/2.0;

    % A rough measure of the particle width.
    % Not at all connected to center determination, but may be useful for tracking applications; 
    % could eliminate for (very slightly) greater speed
    Isub = I - min(I(:));
    [px,py] = meshgrid(1:Nx,1:Ny);
    xoffset = px - xc;
    yoffset = py - yc;
    r2 = xoffset.*xoffset + yoffset.*yoffset;
    sigma = sqrt(sum(sum(Isub.*r2))/sum(Isub(:)))/2;  % second moment is 2*Gaussian width

    %

    function [xc yc] = lsradialcenterfit(m, b, w)
        % least squares solution to determine the radial symmetry center

        % inputs m, b, w are defined on a grid
        % w are the weights for each point
        wm2p1 = w./(m.*m+1);
        sw  = sum(sum(wm2p1));
        smmw = sum(sum(m.*m.*wm2p1));
        smw  = sum(sum(m.*wm2p1));
        smbw = sum(sum(m.*b.*wm2p1));
        sbw  = sum(sum(b.*wm2p1));
        det = smw*smw - smmw*sw;
        xc = (smbw*sw - smw*sbw)/det;    % relative to image center
        yc = (smbw*smw - smmw*sbw)/det; % relative to image center
    end

end

%% Reference estimators
% OUTPUT: set of vectors - results of localization and fitting: [x_{loc}, y_{loc}, x_{fit}, y_{fit}, \sigma_{fit}, I_{fit}, bkg_{fit}, \sum{chi2_{fit}}, Nim_{fit}, \mu_{bkg_{fit}}, \sigma_{bkg_{fit}}]
function fits = ref_fit_MLE_Gauss(boxsize,sigmapsf,imfit,loc)
    % init
    center = boxsize + 1;
    fitregionsize = 2*boxsize+1;
    fits = single(zeros(size(imfit,1),11));
    nfits = 0;

    % create stack of fitting regions - imfit is count*11*11, but i need 11*11*count
    imstack = zeros(fitregionsize,fitregionsize,size(imfit,1));
    for i = 1:size(imfit,1)
        imstack(:,:,i) = reshape(imfit(i,:,:),fitregionsize,fitregionsize);
    end
    
    % run the fitting
    [X Y N BG S CRLBx CRLBy CRLBn CRLBb CRLBs LogL] = mGPUgaussMLE(single(imstack),sigmapsf,10,2);
    A = [ X Y S (N./((2*pi).*S)) BG ];
    
    % position
    A(:,1:2) = A(:,1:2) - center + single(loc(:,1:2)) + 1;  % +1 because the function returns zero-indexed values

    % save results
    for i = 1:size(A,1)
        nfits = nfits + 1;
        fits(nfits,:) = single([single(loc(i,1:2)), A(i,:), 0, 0, 0, 0.0022]);
    end
    
    if nfits > 0
        fits = fits(1:nfits,:);
    else
        fits = [];
    end
end

function fits = ref_fit_LSE_Gauss(boxsize,sigmapsf,imfit,loc)
    % init
    fitregionsize = 2*boxsize+1;
    npts = fitregionsize^2;
    [X(:,:,2),X(:,:,1)] = meshgrid(-boxsize:+boxsize,-boxsize:+boxsize);
    fits = single(zeros(size(imfit,1),11));
    nfits = 0;

    for i = 1:size(imfit,1)

        % fitting region
        im = reshape(imfit(i,:,:),fitregionsize,fitregionsize);

        % fit 2D gaussian bell shaped function
        a0 = [ 0,  0, sigmapsf, max(im(:)), min(im(:))];    % initial parameters
        [a,chi2,exitflag] = lmfit2Dgauss(a0, X, im);

        % fitting error?
        if exitflag < 1 || exitflag > 3; continue; end;

        % estimate background at stored position
        imestim = gauss2d(X,a);
        Nim = sum(im(:) - a(5));  % Nimestim = sum(imestim(:) - a(5)); % is the same

        bkgestim = im - imestim;
        bkgmu = sum(bkgestim(:))/npts;
        bkgstd = sqrt(sum((bkgestim(:)-bkgmu).^2)/(npts-1));

        % position
        a(1:2) = a(1:2) + single(loc(i,1:2));
        a(4) = a(4) * 2*pi*a(3)^2;  % denormalizing the peak intensity (fitting runs with normalized Gaussian, i.e. I/(2 pi sigma^2))

        % save results
        nfits = nfits + 1;
        fits(nfits,:) = single([single(loc(i,1:2)), a, sum(chi2), Nim, bkgmu, bkgstd]);
  
    end
    
    if nfits > 0
        fits = fits(1:nfits,:);
    else
        fits = [];
    end
    
    %
    function y = gauss2d(X,a)
        y = a(4) * exp(-0.5 * ((X(:,:,1) - a(1)).^2 + (X(:,:,2) - a(2)).^2) / a(3).^2 ) + a(5);
    end

end