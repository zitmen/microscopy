function run_analysis(frames)

    sigma_bkg = 0.0022;
    nbins = 20;
    %
    datapath = '_data_';
    respath = '_results_';
    figpath = '_figures_tables_';
    dataset = 'snr1-100_sigma1.3-1.5';
    %dataset = 'snr10-20_sigma0.3-2.2';
    %
	estimators{ 1} = 'LSE_AIRY';
    estimators{ 2} = 'LSE_GAUSS';
    estimators{ 3} = 'LSE_PARABOLA';
    estimators{ 4} = 'MLE_AIRY';
    estimators{ 5} = 'MLE_GAUSS';
    estimators{ 6} = 'MLE_PARABOLA'; % --> this simply does not work,
    %because it truncates margin values to 0 and log(0) is -inf
    estimators{ 7} = 'QE';
    estimators{ 8} = 'GFE';
    estimators{ 9} = 'REF_MLE_GAUSS';
    estimators{10} = 'REF_LSE_GAUSS';
    %
    % localization precision
    precision = cell(length(estimators),1); % TODO: proc je parabola tak skvela? a proc je Gauss tak nahovno??!! thresholding? naky fake hodnoty? WTF?! nechapu...! kdyztak thresholding s pevnou sigmou...asi 1.4...I nevim...fituje se divne, mozna to vzit z pixelovy hodnoty;;;jak by vypadal graf jen nejaky podmnoziny snimku? jaky jsou pocty molekul pro ruzny estimatory v ruznych SNR!!?? hledat nejakou disinterpretaci!!...hrozne divny je, ze ty REF meroty jsou tak monstrozne lepsi...jakto!!!!????? v tom bude nakej figl nekde...!!
    for ei=1:length(estimators)
        diffs = analyse(datapath,respath,figpath,dataset,frames,estimators{ei});    % diffs=[I0,\sigma0,dx,dy,d\sigma,dI]
        precision{ei}.SNR   = diffs(:,1) ./ sigma_bkg;
        precision{ei}.sigma = diffs(:,2);
        precision{ei}.dxy   = diffs(:,3).^2 + diffs(:,4).^2;  % square euklidean distance (pixels)
    end
    %
    save(sprintf('%s/%s.mat',figpath,dataset),'-v6','precision');
    %
    % ===============
    % CRLB & Thompson
    SNR = 1:100;
    sigmapsf = mean([1.3 1.5]);
    %sigmapsf = 0.3:0.1:2.2;
    I = SNR .* sigma_bkg;
    a = 1;
    %
    CRLB = sigmapsf.^2 ./ (2.*SNR);    % var = sigma^2 / (N * SNR) --> N  is number od dimensions, i.e. 2
    Thompson = ((sigmapsf.^2 + a^2/12)./SNR) + ((8*pi*sigmapsf.^4*sigma_bkg^2)./(a^2.*SNR));
    %
    %hold on;
    %plot(SNR,CRLB,'--r');
    %plot(SNR,Thompson,'-m');
    %
    % ======================
    % Estimators evaluations
    load(sprintf('%s/%s.mat',figpath,dataset));   % precision
    est_bins = cell(length(estimators),1);
    for ei = 1:length(estimators)
        bins = zeros(nbins,1);
        X = precision{ei};
        idxBin = ceil(X.SNR ./ (100/nbins));    % ceil --> 1:nbins
        for bi = 1:nbins
            bins(bi) = sum(X.dxy(idxBin==bi)) / sum(idxBin==bi);
        end
        est_bins{ei} = [((sigmapsf.^2)/0.8);bins(:)];
    end
    X = linspace(0,100,nbins+1);
    semilogy(SNR,CRLB,SNR,Thompson,...
         X,est_bins{1},X,est_bins{2},X,est_bins{3},X,est_bins{4},X,est_bins{5},...
         X,est_bins{6},X,est_bins{7},X,est_bins{8});%,X,est_bins{9},X,est_bins{10});
    %hold off;
    legtit{1} = 'CRLB'; legtit{2} = 'Thompson'; legtit{3} = estimators{1}; legtit{4} = estimators{2}; legtit{5} = estimators{3};
    legtit{6} = estimators{4}; legtit{7} = estimators{5}; legtit{8} = estimators{6}; legtit{9} = estimators{7}; legtit{10} = estimators{8};
    %legtit{11} = estimators{9}; legtit{12} = estimators{10};
    legend(legtit),xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    %

end