function run_analysis(frames)

    sigma_bkg = 0.0022;
    nbins = 30;
    %
    datapath = '_data_';
    respath = '_results_';
    figpath = '_figures_tables_';
    dataset1 = 'snr1-10_sigma1.4';
    dataset2 = 'snr1-100_sigma1.3-1.5';
    %dataset = dataset1;
    %
	estimators{ 1} = 'LSE_AIRY';
    estimators{ 2} = 'LSE_GAUSS';
    estimators{ 3} = 'LSE_PARABOLA';
    estimators{ 4} = 'MLE_AIRY';
    estimators{ 5} = 'MLE_GAUSS';
    estimators{ 6} = 'MLE_PARABOLA';
    estimators{ 7} = 'PARABOLA';
    estimators{ 8} = 'GRADIENT';
    %estimators{ 9} = 'REF_MLE_GAUSS';
    %estimators{10} = 'REF_LSE_GAUSS';
    %{
    % localization precision
    precision = cell(length(estimators),1);
    for ei=1:length(estimators)
        diffs = analyse(datapath,respath,figpath,dataset,frames,estimators{ei});    % diffs=[I0,\sigma0,dx,dy,d\sigma,dI]
        precision{ei}.SNR   = diffs(:,1) ./ sigma_bkg;
        precision{ei}.sigma = diffs(:,2);
        precision{ei}.dxy   = diffs(:,3).^2 + diffs(:,4).^2;  % square euklidean distance (pixels)
    end
    save(sprintf('%s/%s.mat',figpath,dataset),'-v6','precision');
    %}
    % ===============
    % CRLB & Thompson
    SNR = 1:100;
    sigmapsf = mean([1.3 1.5]);
    %sigmapsf = 0.3:0.1:2.2;
    I = SNR .* sigma_bkg;
    a = 1;
    %
    CRLB = sigmapsf.^2 ./ (2.*SNR);    % var = sigma^2 / (N * SNR) --> N  is number od dimensions, i.e. 2
    Thompson = ((sigmapsf.^2 + a^2/12)./SNR) + ((8*pi*sigmapsf.^4*sigma_bkg^2)./(a^2.*SNR));
    %
    % ======================
    % Estimators evaluations
    load(sprintf('%s/%s.mat',figpath,dataset1));   % precision
    precision1 = precision;
    load(sprintf('%s/%s.mat',figpath,dataset2));   % precision
    precision2 = precision;
    est_bins = cell(length(estimators),1);
    for ei = 1:length(estimators)
        bins = zeros(nbins,1);
        X1 = precision1{ei};
        X2 = precision2{ei};
        idxBin1 = ceil(X1.SNR ./ (100/nbins));    % ceil --> 1:nbins
        idxBin2 = ceil(X2.SNR ./ (100/nbins));    % ceil --> 1:nbins
        for bi = 1:nbins
            bins(bi) = sum(X1.dxy(idxBin1==bi)) / sum(idxBin1==bi);
            bins(bi) = sum(X2.dxy(idxBin2==bi)) / sum(idxBin2==bi);
        end
        est_bins{ei} = [((sigmapsf.^2)/0.8);bins(:)];
    end
    %
    X = linspace(0,100,nbins+1);
    % ======================
    subplot(3,2,1);
    hold on;
    plot(SNR,CRLB,SNR,Thompson);
    plot(X,est_bins{1},':o','Color',[1 0 0]);
    plot(X,est_bins{4},':x','Color',[0.2 0.7 0.7]);
    hold off;
    legend('Cramer-Rao Bound','Thompson Bound','NLLSE_{Airy}','MLE_{Airy}');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    % ======================
    subplot(3,2,2);
    hold on;
    plot(SNR,CRLB,SNR,Thompson);
    plot(X,est_bins{2},':o','Color',[1 0 0]);
    plot(X,est_bins{5},':x','Color',[0.3 0.8 0.8]);
    hold off;
    legend('Cramer-Rao Bound','Thompson Bound','NLLSE_{Gaussian}','MLE_{Gaussian}');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    % ======================
    subplot(3,2,3);
    hold on;
    plot(SNR,CRLB,SNR,Thompson);
    plot(X,est_bins{3},':o','Color',[1 0 0]);
    plot(X,est_bins{6},':x','Color',[0.3 0.8 0.8]);
    hold off;
    legend('Cramer-Rao Bound','Thompson Bound','NLLSE_{Parabola}','MLE_{Parabola}');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    % ======================
    subplot(3,2,4);
    hold on;
    plot(SNR,CRLB,SNR,Thompson);
    plot(X,est_bins{5},':x','Color',[0.3 0.8 0.8]);
    plot(X,est_bins{7},':k^');
    plot(X,est_bins{8},':m*');
    hold off;
    legend('Cramer-Rao Bound','Thompson Bound','MLE_{Gaussian}','QE','GFE');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    % ======================
    subplot(3,2,5);
    hold on;
    plot(SNR,CRLB,SNR,Thompson);
    plot(X,est_bins{1},':x','Color',[0.2 0.7 0.7]);
    plot(X,est_bins{2},':^','Color',[0.7 0.2 0.7]);
    plot(X,est_bins{3},':*','Color',[0.7 0.7 0.2]);
    hold off;
    legend('Cramer-Rao Bound','Thompson Bound','NLLSE_{Airy}','NLLSE_{Gaussian}','NLLSE_{Parabola}');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    % ======================
    subplot(3,2,6);
    hold on;
    plot(SNR,CRLB,SNR,Thompson);
    plot(X,est_bins{4},':x','Color',[0.2 0.7 0.7]);
    plot(X,est_bins{5},':^','Color',[0.7 0.2 0.7]);
    plot(X,est_bins{6},':*','Color',[0.7 0.7 0.2]);
    hold off;
    legend('Cramer-Rao Bound','Thompson Bound','MLE_{Airy}','MLE_{Gaussian}','MLE_{Parabola}');
    axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]');
    % ======================

end