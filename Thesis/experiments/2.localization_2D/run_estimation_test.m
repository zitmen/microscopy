function run_estimation_test()

    fit_region = [11 11];
    params = [6.2 5.9 1.3 2.5]; % x0 y0 sigma I
    I = PSF_Gauss(fit_region,params);
    options = optimset('Display','none');
    
    params
    
    a0 = [3 3 1 2];
    
    % +MLE je, alespon u Airy, daleko citlivejsi na pocatecni odhad a0...pri
    % spatnym odhadu to odleti nekam do hajzlu...timhle nesvarem LSE netrpi
    % +MLE nefunguje s PSF_Parabola
    a = fminsearch(@(A)(MLE(I,A,fit_region,@PSF_Parabola)),a0,options);
    %addpath('nlopt');
    %a = nlopt_minimize(NLOPT_LN_NEWUOA_BOUND,@(A)(MLE(I,A,fit_region,@PSF_Airy)),{},[0 0 0 0],[12 12 5 142*100],a0,[]);%NLOPT_LN_NEWUOA_BOUND,NLOPT_LN_BOBYQA,NLOPT_LN_NELDERMEAD
    a
    
    a = fminsearch(@(A)(MLE(I,A,fit_region,@PSF_Gauss)),a0,options);
    a
    
    % posledni argument je okoli pro fit -- mensi hodnota je lepsi, sice
    % ovlivnitelnejsi sumem, ale presnejsi, protoze nas zajima jenom spicka
    % paraboly! mozna by bylo dobry vyzkouset, jestli se da ridit velikost
    % okoli podle SNR
    [xs,ys,As] = QE(a0(1),a0(2),I,1);
    %a = [xs ys a0(3) As]
    
    [xc,yc,sigma] = GFE(I);
    %a = [xc yc sigma a0(4)]
    %{
    % CRLB & Thompson
    SNR = 1:100;
    sigma = 0.0022;
    I = SNR .* sigma;
    a = 1;
    sigmapsf = 0.3:0.1:2.2;
    sigmapsf = mean(sigmapsf);
    %
    CRLB = sigmapsf.^2 ./ I;    % var = sigma^2 / N
    Thompson = ((sigmapsf.^2 + a^2/12)./I) + ((8*pi*sigmapsf.^4*sigma^2)./(a^2.*I));
    %
    loglog(SNR,CRLB,'--r'),hold on,loglog(SNR,Thompson,'-m'),hold off,xlabel('SNR'),ylabel('<(\Delta [x,y])^2> [px]'),axis([SNR(1) SNR(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    %plot(sigmapsf, CRLB, sigmapsf, Thompson),xlabel('\sigma_{PSF}'),ylabel('<(\Delta [x,y])^2> [px]'),axis([sigmapsf(1) sigmapsf(end) 0 max([CRLB(1),CRLB(end),Thompson(1),Thompson(end)])]);
    legend('CRLB','Thompson');
    %}
    
end


%% MLE Estimator
function logL = MLE(I,A,fit_region,PSF_model)  % I - data, A - estimated parameters

    logL = -logLikelihood(I,PSF_model(fit_region,A));
    
    %
    function logL = logLikelihood(h, h_est)
        logh = log(h_est);
        logh(logh<-1e6) = -1e6;
        logL = sum(sum(h.*logh - h_est));
    end

end

%% LSE Estimator
function sse = LSE(I,A,fit_region,PSF_model)  % I - data, A - estimated parameters

    sse = sum(sum((I-PSF_model(fit_region,A)).^2));

end

%% PSF models
function h = PSF_Airy(size,params)

    % params -- [x0,y0,sigma,I]
    [x,y] = meshgrid(1:size(1),1:size(2));
    z = (pi.*distance([x(:) y(:)],[params(1) params(2)]))./(2.3*params(3)); % (pi*q)/(lambda*(R/d)) --> q=dist,divisor=sigma
    h = ((2.*besselj(1,z))./z).^2;
    h(isnan(h)) = 1;    % Nan = 0/0, that is typically z=0, i.e. distance = 0, i.e. center of the Airy disk
    h = reshape(h, size);
    h = (params(4) * (pi/2)) .* h;
    
    %
    function d = distance(P1,P2)
        d = sqrt((P1(:,1)-P2(:,1)).^2 + (P1(:,2)-P2(:,2)).^2);
    end
    
end

function h = PSF_Gauss(size,params)

    % params -- [x0,y0,sigma,I]
    [x,y] = meshgrid(1:size(1),1:size(2));
    h = params(4)*exp(-((((x-params(1)).^2)./(2*params(3)*params(3))) + (((y-params(2)).^2)./(2*params(3)*params(3)))));
    
end

function h = PSF_Parabola(size,params)

    % params -- [x0,y0,sigma,I]
    [x,y] = meshgrid(1:size(1),1:size(2));
    h = -(((x-params(1))./params(3)).^2 + ((y-params(2))./params(3)).^2);
    h = minmax_norm(h);
    h = (params(4) / (pi*params(3)^2)) .* h;    % adjust intensity
    
    %
    function N = minmax_norm(M)
        mmin = min(min(M));
        mmax = max(max(M));
        N = (M - mmin) ./ (mmax - mmin);
    end
    
end

%% Quadratic estimator
function [xs, ys, As] = QE(r, c, cim, nbr)
    % -------------------------------------------------------------------------
    % Compute local maxima to subpixel accuracy by fitting a quadric
    % function ax^2 + by^2 + cx + dy + e = z in neighbourhood
    %
    % return values are xs (X subpixel), ys (Y subpixel), As (Amplitude subpixel)
    % -------------------------------------------------------------------------

    if (nargin < 4), nbr = 1; end;

    if isempty(r)
       xs = []; ys = []; As = [];
       return;
    end

    [rows,cols] = size(cim);

    % indices of feature points
    ind = sub2ind([rows,cols],r,c);

    % 8-neibourhood coordinates
    [x,y] = meshgrid(-nbr:nbr,-nbr:nbr); x = x(:); y = y(:);
    numnbrs = length(x);
    indnbr = repmat(ind,1,numnbrs) + repmat((x*rows+y)',length(ind),1);

    % local parabola fitting by LSQ
    par = cim(indnbr) * pinv([x.^2, y.^2, x, y, ones(numnbrs,1)])';

    % subpixel corrections to original row and column coords
    x0 = -par(:,3) ./ par(:,1) / 2;
    y0 = -par(:,4) ./ par(:,2) / 2;

    % Add subpixel corrections
    xs = c + x0;
    ys = r + y0;

    % interpolated value at subpixel coordinates
    As = sum([x0.^2, y0.^2, x0, y0] .* par(:,1:4),2) + par(:,5);
end

%% Gradient Field Estimator
function [xc,yc,sigma] = GFE(I)
    
    % Number of grid points
    [Ny Nx] = size(I);

    % grid coordinates are -n:n, where Nx (or Ny) = 2*n+1
    % grid midpoint coordinates are -n+0.5:n-0.5;
    % The two lines below replace
    %    xm = repmat(-(Nx-1)/2.0+0.5:(Nx-1)/2.0-0.5,Ny-1,1);
    % and are faster (by a factor of >15 !)
    % -- the idea is taken from the repmat source code
    xm_onerow = -(Nx-1)/2.0+0.5:(Nx-1)/2.0-0.5;
    xm = xm_onerow(ones(Ny-1, 1), :);
    % similarly replacing
    %    ym = repmat((-(Ny-1)/2.0+0.5:(Ny-1)/2.0-0.5)', 1, Nx-1);
    ym_onecol = (-(Ny-1)/2.0+0.5:(Ny-1)/2.0-0.5)';  % Note that y increases "downward"
    ym = ym_onecol(:,ones(Nx-1,1));

    % Calculate derivatives along 45-degree shifted coordinates (u and v)
    % Note that y increases "downward" (increasing row number) -- we'll deal
    % with this when calculating "m" below.
    dIdu = I(1:Ny-1,2:Nx)-I(2:Ny,1:Nx-1);
    dIdv = I(1:Ny-1,1:Nx-1)-I(2:Ny,2:Nx);

    % Smoothing -- 
    h = ones(3)/9;  % simple 3x3 averaging filter
    fdu = conv2(dIdu, h, 'same');
    fdv = conv2(dIdv, h, 'same');
    dImag2 = fdu.*fdu + fdv.*fdv; % gradient magnitude, squared

    % Slope of the gradient .  Note that we need a 45 degree rotation of 
    % the u,v components to express the slope in the x-y coordinate system.
    % The negative sign "flips" the array to account for y increasing
    % "downward"
    m = -(fdv + fdu) ./ (fdu-fdv); 

    % *Very* rarely, m might be NaN if (fdv + fdu) and (fdv - fdu) are both
    % zero.  In this case, replace with the un-smoothed gradient.
    NNanm = sum(isnan(m(:)));
    if NNanm > 0
        unsmoothm = (dIdv + dIdu) ./ (dIdu-dIdv);
        m(isnan(m))=unsmoothm(isnan(m));
    end
    % If it's still NaN, replace with zero. (Very unlikely.)
    NNanm = sum(isnan(m(:)));
    if NNanm > 0
        m(isnan(m))=0;
    end

    %
    % Almost as rarely, an element of m can be infinite if the smoothed u and v
    % derivatives are identical.  To avoid NaNs later, replace these with some
    % large number -- 10x the largest non-infinite slope.  The sign of the
    % infinity doesn't matter
    try
        m(isinf(m))=10*max(m(~isinf(m)));
    catch
        % if this fails, it's because all the elements are infinite.  Replace
        % with the unsmoothed derivative.  There's probably a more elegant way
        % to do this.
        m = (dIdv + dIdu) ./ (dIdu-dIdv);
    end


    % Shorthand "b", which also happens to be the
    % y intercept of the line of slope m that goes through each grid midpoint
    b = ym - m.*xm;

    % Weighting: weight by square of gradient magnitude and inverse 
    % distance to gradient intensity centroid.
    sdI2 = sum(dImag2(:));
    xcentroid = sum(sum(dImag2.*xm))/sdI2;
    ycentroid = sum(sum(dImag2.*ym))/sdI2;
    w  = dImag2./sqrt((xm-xcentroid).*(xm-xcentroid)+(ym-ycentroid).*(ym-ycentroid));  

    % least-squares minimization to determine the translated coordinate
    % system origin (xc, yc) such that lines y = mx+b have
    % the minimal total distance^2 to the origin:
    % See function lsradialcenterfit (below)
    [xc yc] = lsradialcenterfit(m, b, w);



    %
    % Return output relative to upper left coordinate
    xc = xc + (Nx+1)/2.0;
    yc = yc + (Ny+1)/2.0;

    % A rough measure of the particle width.
    % Not at all connected to center determination, but may be useful for tracking applications; 
    % could eliminate for (very slightly) greater speed
    Isub = I - min(I(:));
    [px,py] = meshgrid(1:Nx,1:Ny);
    xoffset = px - xc;
    yoffset = py - yc;
    r2 = xoffset.*xoffset + yoffset.*yoffset;
    sigma = sqrt(sum(sum(Isub.*r2))/sum(Isub(:)))/2;  % second moment is 2*Gaussian width

    %

    function [xc yc] = lsradialcenterfit(m, b, w)
        % least squares solution to determine the radial symmetry center

        % inputs m, b, w are defined on a grid
        % w are the weights for each point
        wm2p1 = w./(m.*m+1);
        sw  = sum(sum(wm2p1));
        smmw = sum(sum(m.*m.*wm2p1));
        smw  = sum(sum(m.*wm2p1));
        smbw = sum(sum(m.*b.*wm2p1));
        sbw  = sum(sum(b.*wm2p1));
        det = smw*smw - smmw*sw;
        xc = (smbw*sw - smw*sbw)/det;    % relative to image center
        yc = (smbw*smw - smmw*sbw)/det; % relative to image center
    end

end