% demand: at least 1000 molecules per measurement; if 10 measurements, then at least 10000/500 = 20 frames --> we will gnerate 50
gendata('_data_','snr1-10_sigma1.4',50,[512 512],500,[1.4 1.4],[1 10]); % only one sigma bin 1.4
gendata('_data_','snr1-100_sigma1.3-1.5',50,[512 512],500,[1.3 1.5],[1 100]); % only one sigma bin [1.3,(1.3+((2.2-0.3)/10))]
%gendata('_data_','snr10-20_sigma0.3-2.2',50,[512 512],500,[0.3 2.2],[10 20]); % only one snr bin [10,(10+((100-1)/10))]