\select@language {czech}
\select@language {UKenglish}
\select@language {UKenglish}
\select@language {UKenglish}
\contentsline {subsection}{Citation of this thesis}{viii}{section*.1}
\contentsline {chapter}{Introduction}{1}{chapter*.5}
\contentsline {section}{Motivation and objectives}{1}{section*.6}
\contentsline {section}{Problem statements}{1}{section*.7}
\contentsline {section}{State of the Art ??? what is that?}{1}{section*.8}
\contentsline {chapter}{\chapternumberline {1}Microscopy / Biological background / Background}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}aaaa}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}abb}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}abb}{3}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}bbbbb}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}bccc}{3}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}cccc}{3}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}cddd}{4}{subsection.1.3.1}
\contentsline {chapter}{\chapternumberline {2}Image Processing}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Noise / Denoising / Noise Processing}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Molecule localization}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Crowded field problem}{5}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Estimating the molecule parameters}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}Rendering}{5}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Experimental results}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Noise / Denoising / Noise Processing}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Proposed experiments}{7}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Results}{7}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Molecule localization}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Proposed experiments}{7}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Results}{7}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Estimating the molecule parameters}{8}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Proposed experiments}{8}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Results}{8}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Rendering}{8}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Proposed experiments}{8}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Results}{8}{subsection.3.4.2}
\contentsline {chapter}{\chapternumberline {4}Implementation and testing}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}ImageJ Plug-In}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Testing}{9}{section.4.2}
\contentsline {chapter}{Conclusion}{11}{chapter*.9}
\contentsline {section}{Future work}{11}{section*.10}
\contentsline {appendix}{\chapternumberline {A}Content of CD}{13}{appendix.A}
