function crowded_data()

    addpath('../../experiments/4.crowded_field/');
    
    gendata('.','crowded_data_10',3,[32 32],10,[1.3 1.3],[100 100]);
    gendata('.','crowded_data_20',3,[32 32],20,[1.3 1.3],[100 100]);
    gendata('.','crowded_data_40',3,[32 32],40,[1.3 1.3],[100 100]);
    
    im1 = imread('crowded_data_10.0001.tiff');
    im2 = imread('crowded_data_20.0001.tiff');
    im3 = imread('crowded_data_40.0001.tiff');
    
    subplot(1,3,1),imagesc(im1),axis off,colormap gray;
    subplot(1,3,2),imagesc(im2),axis off,colormap gray;
    subplot(1,3,3),imagesc(im3),axis off,colormap gray;

end