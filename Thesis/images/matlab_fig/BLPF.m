function BLPF(sigma,gamma)
    
    % original noisy image
    image = imread('rice.png');
    image = imnoise(image,'gaussian');
    
    subplot(1,5,1), imshow(image)
    
    fftI = fft2(double(image));
    subplot(1,5,2), imshow(abs(fftshift(fftI)),[24 100000]), colormap gray
    
    % filter
    mask = gen_filter(sigma,size(fftI),gamma);
    subplot(1,5,3), imshow(mask), colormap gray
    
    fftI = fftshift(fftI);
    blpfI_Mag = abs(fftI).*mask;
    subplot(1,5,4), imshow(blpfI_Mag,[24 100000]), colormap gray
    blpfI = ifftshift(blpfI_Mag .* exp(i*angle(fftI)));
    
    % filtered image
    image = uint8(real(ifft2(blpfI)));
    subplot(1,5,5), imshow(image), colormap gray

end

function mask = gen_filter(sigma,siz,gamma)
    mask = zeros(siz);
    u0 = siz(1)/2;
    v0 = siz(2)/2;
    for u = 1:siz(1)
        for v = 1:siz(2)
            mask(u,v) = 1/(1+(sqrt((u0-u).^2+(v0-v).^2)/sigma).^(2*gamma));
        end
    end
end