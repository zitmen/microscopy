function density_vs_gaussian()

    factor = 200;
    
    % density
    G = gaussian(factor*[5 5],factor*1-0.5,factor*4-0.5,factor/10*1.3,1) + ...
        gaussian(factor*[5 5],factor*2-0.5,factor*1-0.5,factor/10*1.3,1) + ...
        gaussian(factor*[5 5],factor*4-0.5,factor*3-0.5,factor/10*1.3,1);
    figure(1),imagesc(G),colormap gray,axis off;
    
    % gaussian
    G = gaussian(factor*[5 5],factor*1-0.5,factor*4-0.5,factor/10*1.3,1) + ...
        gaussian(factor*[5 5],factor*2-0.5,factor*1-0.5,factor/10*1.3,1) + ...
        gaussian(factor*[5 5],factor*4-0.5,factor*3-0.5,factor/10*1.3,2);
    figure(2),imagesc(G),colormap gray,axis off;
    
end

function h = gaussian(size,x0,y0,sigma,I)

    [xi,yi] = meshgrid(1:size(1),1:size(2));
    h = I/(2*pi*sigma*sigma)*exp(-((((xi-x0).^2)./(2*sigma*sigma)) + (((yi-y0).^2)./(2*sigma*sigma))));

end