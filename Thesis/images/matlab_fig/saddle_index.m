function saddle_index()
    hold on
    for i=[20 30]
        y = normpdf(1:50, i, i/5);
        plot(1:50,y)
    end
    hold off
    xlabel('X'),ylabel('Y')
end