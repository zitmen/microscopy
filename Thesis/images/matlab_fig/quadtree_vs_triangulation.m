function quadtree_vs_triangulation()

    I = datagen();
    quadtree_rendering(I);
    triangulation_rendering(I);

end
%%
function I = datagen()

    scale = 32;
    I = uint8(zeros(scale*16));   % output image
    C1 = scale*[8 8];      % center of the circle
    C2 = scale*[8 8];
    R1 = scale*[0 2];      % range of radii
    R2 = scale*[4.5 5];
    A1 = [0 4]*pi/2; % [rad] range of allowed angles
    A2 = [0 4]*pi/2;

    nPoints = scale/16*30;

    urand = @(nPoints,limits)(limits(1) + rand(nPoints,1)*diff(limits));
    randomCircle = @(n,r,a)(pol2cart(urand(n,a),urand(n,r)));

    [P1x,P1y] = randomCircle(nPoints,R1,A1);
    P1x = round(P1x + C1(1));
    P1y = round(P1y + C1(2));

    [P2x,P2y] = randomCircle(nPoints,R2,A2);
    P2x = round(P2x + C2(1));
    P2y = round(P2y + C2(2));

    I(sub2ind(size(I),P1x,P1y)) = 1;
    I(sub2ind(size(I),P2x,P2y)) = 1;

end
%%
function k = qt_fun(thr,IMGs)

    k = sum(sum(IMGs,1),2);
    k = (k(1,1,:) >= thr);
    
end

function quadtree_rendering(I)

%{
    HELP:
    ----
    S = qtdecomp(I, fun) uses the function fun to determine whether to split a block.
        qtdecomp calls fun with all the current blocks of size m-by-m stacked into an m-by-m-by-k array,
        where k is the number of m-by-m blocks. fun returns a logical k-element vector,
        whose values are 1 if the corresponding block should be split, and 0 otherwise.
%}

    S = qtdecomp(I,@(x)qt_fun(4,x));
    blocks = repmat(uint8(0),size(S));

    for dim = [512 256 128 64 32 16 8 4 2 1];    
      numblocks = length(find(S==dim));    
      if (numblocks > 0)        
        values = repmat(uint8(1),[dim dim numblocks]);
        values(2:dim,2:dim,:) = 0;
        blocks = qtsetblk(blocks,S,dim,values);
      end
    end

    blocks(end,1:end) = 1;
    blocks(1:end,end) = 1;

    [X,Y] = find(I);
    figure;
    subplot(1,2,1),imagesc(1-blocks),colormap gray,hold on,plot(Y,X,'xr'),hold off,axis([1 512 1 512]),axis off;
    
    QT = minmax_norm(render_quadtree(zeros(size(I)), size(I), Y, X));
    QT(QT>0.2)=0.1;
    subplot(1,2,2),imagesc(QT),colormap gray,axis off;

end

%%
function O = minmax_norm(I)

    mi = min(I(:));
    ma = max(I(:));
    O = (I - mi) ./ (ma - mi);

end

%%
function triangulation_rendering(I)

    [X,Y] = find(I);
    figure;
    subplot(1,2,1),triplot(DelaunayTri(X,Y)),hold on,plot(X,Y,'xr'),hold off,axis([1 512 1 512]),axis off;
    
    DT = minmax_norm(render_delaunay(zeros(size(I)), size(I), Y, X));
    DT(DT>0.2)=0.1;
    subplot(1,2,2),imagesc(DT),colormap gray,axis off;

end
%%
function IM = render_quadtree(IM, imsiz, xpos, ypos)
    siz = max(imsiz);
    lim = [0, siz]+0.5;
    im = histqtree(single(ypos), single(xpos), lim, lim, 1, ceil(log(max(imsiz))/log(2)));
    IM = IM + im(1:imsiz(1),1:imsiz(2));
end

%%
function IM = render_delaunay(IM, imsiz, xpos, ypos)
    im = delaunay2(single([ypos,xpos]), [1 1 imsiz], imsiz, 'area');
    IM = IM + im(1:imsiz(1),1:imsiz(2));
end