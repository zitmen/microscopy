function DoG_kernel()

    sigma1 = 1;
    sigma2 = 2;
    G1 = gaussian(-10:0.1:+10,0,0,0,sigma1);
    G2 = gaussian(-10:0.1:+10,0,0,0,sigma2);
    
    plot(G1,'b'),hold on,plot(G2,'g'),plot(G1-G2,'r'),hold off;
    legend({'Gaussian G1 with \sigma = 1','Gaussian G2 with \sigma = 2','DoG = G1 - G2'});
    axis([0 200 -0.02 0.16]);

end

function h = gaussian(xi,yi,x0,y0,sigma)

    h = 1/(2*pi*sigma*sigma) .* exp(-((((xi-x0).^2)./(2*sigma*sigma)) + (((yi-y0).^2)./(2*sigma*sigma))));

end