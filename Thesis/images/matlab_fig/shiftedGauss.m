function shiftedGauss()

    XY = [-5:0.5:+5];
    Z0 = zeros(21);

    G = fspecial('gaussian',[21 21],4);
    subplot(1,3,1),surf(XY,XY,G),hold on,surf(XY,XY,Z0),hold off;
    
    G_lowered = G - mean(mean(G));
    subplot(1,3,2),surf(XY,XY,G_lowered),hold on,surf(XY,XY,Z0),hold off;
    
    G_lowered_truncated = G_lowered;
    G_lowered_truncated(G_lowered(:) < 0) = 0;
    subplot(1,3,3),surf(XY,XY,G_lowered_truncated),hold on,surf(XY,XY,Z0),hold off;

end