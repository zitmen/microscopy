function curvature()

    siz = [150 150];
    x0 = 75.5;
    y0 = 75.5;
    sigma1 = 8;
    sigma2 = 10;
    sigma3 = 12;
    sigma4 = 14;
    
    %{
    subplot(1,4,1),hold on,...
        imagesc(gaussian(siz,x0,y0,sigma1)),colormap gray,...
        circle(x0,y0,2*sigma1),...  % with the width=2*sigma, the Gaussian contains 95% of its intensity [http://en.wikipedia.org/wiki/File:Standard_deviation_diagram.svg]
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1) 1 siz(2)]),hold off;
    
    subplot(1,4,2),hold on,...
        imagesc(gaussian(siz,x0,y0,sigma2)),colormap gray,...
        circle(x0,y0,2*sigma2),...  % with the width=2*sigma, the Gaussian contains 95% of its intensity [http://en.wikipedia.org/wiki/File:Standard_deviation_diagram.svg]
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1) 1 siz(2)]),hold off;
    
    subplot(1,4,3),hold on,...
        imagesc(gaussian(siz,x0,y0,sigma3)),colormap gray,...
        circle(x0,y0,2*sigma3),...  % with the width=2*sigma, the Gaussian contains 95% of its intensity [http://en.wikipedia.org/wiki/File:Standard_deviation_diagram.svg]
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1) 1 siz(2)]),hold off;
    
    subplot(1,4,4),hold on,...
        imagesc(gaussian(siz,x0,y0,sigma4)),colormap gray,...
        circle(x0,y0,2*sigma4),...  % with the width=2*sigma, the Gaussian contains 95% of its intensity [http://en.wikipedia.org/wiki/File:Standard_deviation_diagram.svg]
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1) 1 siz(2)]),hold off;
    %}
    
    G1 = gaussian(siz,x0,y0,sigma1);
    G2 = gaussian(siz,x0,y0,sigma2);
    G3 = gaussian(siz,x0,y0,sigma3);
    G4 = gaussian(siz,x0,y0,sigma4);
    
    G = horzcat(vertcat(G1,G2),vertcat(G3,G4));
    figure(1),hold on,...
        imagesc(G),colormap gray,...
        circle(       x0,       y0,2*sigma1),...
        circle(siz(1)+x0,       y0,2*sigma2),...
        circle(       x0,siz(2)+y0,2*sigma3),...
        circle(siz(1)+x0,siz(2)+y0,2*sigma4),...
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 2*siz(1) 1 2*siz(2)]),hold off;
    
    
    figure(2),hold on,...  % this plot is here only because I need hsv colorbar!
        imagesc([double(1/(2*sigma1/10)) double(1/(2*sigma2/10)); double(1/(2*sigma3/10)) double(1/(2*sigma4/10))]),colormap hsv,...
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 2 1 2]),hold off;

end

function h = gaussian(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = 1/(2*pi*sigma*sigma)*exp(-((((xi-x0)^2)/(2*sigma*sigma)) + (((yi-y0)^2)/(2*sigma*sigma))));
        end
    end

end

function circle(x0,y0,r)
    %0.01 is the angle step, bigger values will draw the circle faster but
    %you might notice imperfections (not very smooth)
    ang=0:0.01:2*pi; 
    xp=r*cos(ang);
    yp=r*sin(ang);
    plot(x0+xp,y0+yp,'LineWidth',2,'Color',hsv2rgb([double(1/(r/10)),1.0,1.0]));
end