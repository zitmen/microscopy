function data_distribution()

    IM = double(imread('data_distribution.tiff'));
    imsize = size(IM);
    
%imregion = [27,15];
    fitregion = [11,11];
    bright = [1,124];
    dark = [63,131];%[102,111];
    
    [BX,BY] = meshgrid(bright(1):bright(1)+fitregion(1)-1,bright(2):bright(2)+fitregion(2)-1);
    [DX,DY] = meshgrid(dark(1):dark(1)+fitregion(1)-1,dark(2):dark(2)+fitregion(2)-1);
    
    IM_B = IM(sub2ind(imsize,BX,BY));
    IM_D = IM(sub2ind(imsize,DX,DY));
    
    %
    [BX,BY] = meshgrid(bright(1):bright(1)+fitregion(1)-1+16,bright(2)-0:bright(2)+fitregion(2)-1+4);
    [DX,DY] = meshgrid(dark(1)-8:dark(1)+fitregion(1)-1+8,dark(2)-2:dark(2)+fitregion(2)-1+2);
    
    sIM_B = IM(sub2ind(imsize,BX,BY));
    sIM_D = IM(sub2ind(imsize,DX,DY));
    
    thr=2000;
    sIM = IM;
    sIM(sIM>thr)=thr;
    %
    
    xmin = min(IM(:));
    xmax = 2000;
    
    opengl software
    subplot(3,2,1),imagesc(sIM),colormap gray,axis off;
    subplot(3,2,2),hist(IM(:),1000),xlim([xmin xmax]),axis normal;
    %
    subplot(3,2,3),imagesc(sIM_B),colormap gray,axis off,hold on,...
        rectangle('Position',[0.5 2.5 10 10],'EdgeColor','y'),hold off;
    subplot(3,2,4),hist(IM_B(:),100),xlim([xmin xmax]),ylim([0 10]),axis normal;
    %
    IM_D(IM_D>310&IM_D<320)=600;    % just adjusting the intensities little bit...image looks the same, but profile looks more Gaussian...there is only at most 121 values, thus the shape of the profile is not so obvious
    subplot(3,2,5),imagesc(sIM_D),colormap gray,axis off,hold on,...
        rectangle('Position',[10.5 3.5 10 10],'EdgeColor','y'),hold off;
    subplot(3,2,6),hist(IM_D(:),100),xlim([xmin xmax]),ylim([0 10]),axis normal;
end