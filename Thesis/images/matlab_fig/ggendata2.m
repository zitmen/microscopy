function ggendata2()

    im = genDots([32,32],1,[2 3],[0.3 0.4]);
    imagesc(im),axis off,colormap gray;

end

%% Dots generator
function [im,params] = genDots(imsize,numdots,sigma2_range,I_range)

    % create regular grid for positioning dots 
    [xpos,ypos] = meshgrid(16:16:imsize(2)-16,16:16:imsize(1)-16);
    xpos = xpos(:);
    ypos = ypos(:);
    npts = numel(xpos);

    % select random subset of dots
    idx = randperm(npts)';
    ndots = numdots;
    idx = sort(idx(1:ndots));
    params = zeros(ndots,4,'single'); 

    % generate dots with random parameters [x0, y0, sigma, amplitude]
    r = 4*sqrt(rand(ndots,1));
    fi = 2*pi*rand(ndots,1);
    params(:,1) = ypos(idx) + r.*sin(fi);   % y0
    params(:,2) = xpos(idx) + r.*cos(fi);   % x0
    params(:,3) = sigma2_range(1) + (diff(sigma2_range).*rand(ndots,1));  % sigma - uniformly distributed
    params(:,4) = I_range(1) + (diff(I_range).*rand(ndots,1));  % peak intensity - uniformly distributed

    % create rendering coordinates
    radius = 20;
    [x,y] = meshgrid(-radius:radius,-radius:radius);
    idx = x.^2 + y.^2 < radius^2;
    x = x(idx); y = y(idx);

    % create image of dots
    im = zeros(imsize,'single');
    for I = 1:ndots

      % rendering parameters
      u = floor(params(I,1));
      v = floor(params(I,2));
      du = params(I,1) - floor(params(I,1));
      dv = params(I,2) - floor(params(I,2));
      sigma = params(I,3);
      sigmax = params(I,3);
      sigmay = params(I,3);
      N = params(I,4);

      % take coordinates only inside the image
      idx = ~(v+x < 1 | v+x > imsize(2) | u+y < 1 | u+y > imsize(1));  

      % render  
      % note: Gaussian needs to be normalized here!! otherwise it would be peak signal-to-noise ratio (PSNR)
      %z = N / (2*pi*sigma^2) * exp(-0.5*((x(idx)-dv).^2 + (y(idx)-du).^2)/sigma^2);
	  % note2: problem with low values of sigma! we have to integrate the intensities same as the CCD chip does!!
	  z = N/4.*((erf((x(idx)-dv+0.5)./(sqrt(2)*sigmax))-erf((x(idx)-dv-0.5)./(sqrt(2)*sigmax))).*(erf((y(idx)-du+0.5)./(sqrt(2)*sigmay))-erf((y(idx)-du-0.5)./(sqrt(2)*sigmay))));
      idx = sub2ind(imsize,u+y(idx),v+x(idx));  
      im(idx) = im(idx) + z;
      
    end

end

%% MinMax Normalization to [0,1] range
function O = minmax_norm_01(I)

    mmin = min(min(I));
    mmax = max(max(I));
    
    O = (I - mmin) ./ (mmax - mmin);

end