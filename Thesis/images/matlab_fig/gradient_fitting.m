% note: vector graphics formats are so `great` that they analycaly recompute
% the subsampled images (6x6) and the Gaussian is smooth again...!
% Because of that, I made the `pixelize` function, so all the images are
% oversampled so they are displayed correctly in PDF.
function gradient_fitting()

    x0 = 3.2;
    y0 = 3.1;
    sigma0 = 1;
    siz = [6 6];
    subsmpl = 32;   % subsampling factor

    
    % Smooth Gaussian
    G = gaussian(siz.*subsmpl,0.5+x0*subsmpl,0.5+y0*subsmpl,sigma0*subsmpl);
    subplot(1,3,1),hold on,...
        imagesc(G),colormap gray,...
        plot(0.5+y0*subsmpl,0.5+x0*subsmpl,'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1)*subsmpl 1 siz(1)*subsmpl]),hold off;
    
    % Pixelated Gaussian
    G = gaussian(siz,0.5+x0,0.5+y0,sigma0);
    subplot(1,3,2),hold on,...
        imagesc(pixelize(G,subsmpl)),colormap gray,...
        plot(0.5+y0*subsmpl,0.5+x0*subsmpl,'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1)*subsmpl 1 siz(1)*subsmpl]),hold off;
    
    % Gradient field in pixelated Gaussian
    G = gaussian(siz,0.5+x0,0.5+y0,sigma0);
    [GX,GY] = gradient(G);
    subplot(1,3,3),hold on,...
        imagesc(pixelize(G,subsmpl)),colormap gray,...
        quiver(([1:siz(1)]-0.5).*subsmpl,([1:siz(2)]-0.5).*subsmpl,GX,GY,'y','LineWidth',1),...  % arrows in the direction of the gradient
        plot(0.5+y0*subsmpl,0.5+x0*subsmpl,'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        set(gca,'xtick',[],'ytick',[]),...
        axis([1 siz(1)*subsmpl 1 siz(1)*subsmpl]),hold off;
    
end

function h = gaussian(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = 1/(2*pi*sigma*sigma)*exp(-((((xi-x0)^2)/(2*sigma*sigma)) + (((yi-y0)^2)/(2*sigma*sigma))));
        end
    end

end

function I = pixelize(img,subsmpl)

    I = zeros(size(img).*subsmpl);
    for x=0:size(I,1)-1
        for y=0:size(I,2)-1
            I(1+x,1+y) = img(floor(1+x/subsmpl),floor(1+y/subsmpl));
        end
    end
    
end