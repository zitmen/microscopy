%  Curvature.m

%  Description:
%    The purpose of this function is to compute the curvature of a planar contour. 
%  
%    Inputs:  
%       X_Deriv_1,X_Deriv_2:   Arrays containing the first and second derivatives 
%                              of the x-coordinates of the curve
%       Y_Deriv_1,Y_Deriv_2:   Arrays containing the first and second derivatives 
%                              of the y-coordinates of the curve
%       firstIndex,lastIndex:  The first and last index, where the curvature is 
%                              to be computed.

%    Output:  
%       curvature:             An array containing the curvature of the curve.

%    Bugs and Limitations:     If the denominator is zero, the curvature is zero.

function [curvature] = calc_curvature(X_Deriv_1,X_Deriv_2,Y_Deriv_1,Y_Deriv_2,firstIndex,lastIndex)

[numRows,numCols] = size(X_Deriv_1);
dim       = max(numRows,numCols);
dim       = max(dim,lastIndex);
curvature = zeros(dim,1);
for k = firstIndex:lastIndex
   num   = X_Deriv_1(k) * Y_Deriv_2(k) - X_Deriv_2(k) * Y_Deriv_1(k);
   denom = X_Deriv_1(k) * X_Deriv_1(k) + Y_Deriv_1(k) * Y_Deriv_1(k);
   denom = sqrt(denom);
   denom = denom * denom * denom;
   if denom > 0
       curvature(k) = num / denom ;
   end
end

return;