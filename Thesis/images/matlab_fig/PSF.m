function PSF()

    size = [51 51];
    x0 = 26.5;
    y0 = 26.5;
    sigma0 = 5;

    % ruzny PSF profily
    subplot(1,3,1),surf([1:size(1)],[1:size(2)],airy_disk(size,x0,y0,sigma0),'EdgeAlpha',0.3),colormap cool,set(gca,'xtick',[],'ytick',[]);
    subplot(1,3,2),surf([1:size(1)],[1:size(2)],gaussian(size,x0,y0,sigma0),'EdgeAlpha',0.3),colormap cool,set(gca,'xtick',[],'ytick',[]);
    
    z_limit = -75; P = parabola(size,x0,y0,1); P(P<z_limit) = z_limit; P = minmax_norm(P - z_limit);   % limit the z-axis, flip over the xz-plane and normalize to [0,1] range
    subplot(1,3,3),surf([1:size(1)],[1:size(2)],P,'EdgeAlpha',0.3),colormap cool,set(gca,'xtick',[],'ytick',[]);
    %
    %{
    % NEFUNGUJE!
    figure,hold on,...
        plot([0:25],airy_disk([26 1],1,1,sigma0),'r')%,...
        plot([0:100],gaussian([101 1],1,1,sigma0),'g')%,...
        plot([0:100],parabola([101 1],1,1),'b')%,...
        hold off;%,set(gca,'xtick',[],'ytick',[]);
    %}

end

function h = airy_disk(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            z = (pi*distance([xi yi],[x0 y0]))/(2.3*sigma); % (pi*q)/(lambda*(R/d)) --> q=dist,divisor=sigma
            h(xi,yi) = ((2*bessel1(z))/z)^2;    % my own implementation of Bessel function to confirm the equation in the thesis
            %h(xi,yi) = ((2*besselj(1,z))/z)^2;
        end
    end

end

function b = bessel1(z)

    order = 1;
    b = 0;
    for m = 0:10    % m = 0 --> infinity, but thanks to the huge values of factorials in divisor, it can be approximated by small number of iterations (m), because soon all values goes to zero
        b = b + (((-1)^m) / (factorial(m) * ggamma(m + order + 1))) * ((z/2)^(2*m + order));
    end

end

function g = ggamma(x)
    g = factorial(x-1);
end

function h = gaussian(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = exp(-((((xi-x0)^2)/(2*sigma*sigma)) + (((yi-y0)^2)/(2*sigma*sigma))));
        end
    end

end

function h = parabola(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = -(((xi-x0)/sigma)^2 + ((yi-y0)/sigma)^2);
        end
    end

end

function d = distance(P1,P2)

    d = sqrt((P1(1)-P2(1))^2 + (P1(2)-P2(2))^2);

end

function N = minmax_norm(M)

    mmin = min(min(M));
    mmax = max(max(M));
    N = (M - mmin) ./ (mmax - mmin);

end