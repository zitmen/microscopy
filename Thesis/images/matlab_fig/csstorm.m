function csstorm()

    subsmpl = 32;
    
    % demonstrating the b=Ax equation, b-noisy measured signal, A-measurement function, x-original signal
    
    % b
    G = gaussian([11 11],6,5,1.3) + gaussian([11 11],6,7,1.3) + gaussian([11 11],7,6,1.3) + gaussian([11 11],5,6,1.3);
    subplot(1,3,1),imagesc(pixelize(G,subsmpl)),colormap gray,set(gca,'xtick',[],'ytick',[]);
    % A
    G = [];
    for gi=1:11, G = vertcat(G,gaussian([1 11],1,gi,1.3)); end;
    subplot(1,3,2),imagesc(pixelize(G,subsmpl)),colormap gray,set(gca,'xtick',[],'ytick',[]);
    % x
    I = zeros([subsmpl*11 subsmpl*11]);
    I(subsmpl*(5-0.5),subsmpl*(6-0.5)) = 1;
    I(subsmpl*(7-0.5),subsmpl*(6-0.5)) = 1;
    I(subsmpl*(6-0.5),subsmpl*(5-0.5)) = 1;
    I(subsmpl*(6-0.5),subsmpl*(7-0.5)) = 1;
    subplot(1,3,3),hold on,...
        imagesc(I),colormap gray,...
        plot(subsmpl*(5-0.5),subsmpl*(6-0.5),'gx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        plot(subsmpl*(7-0.5),subsmpl*(6-0.5),'gx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        plot(subsmpl*(6-0.5),subsmpl*(5-0.5),'gx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        plot(subsmpl*(6-0.5),subsmpl*(7-0.5),'gx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        set(gca,'xtick',[],'ytick',[]),axis([1 subsmpl*11 1 subsmpl*11]),axis off,hold off;

    % almost same profile as the 4 Gaussians above
    %G = gaussian([11 11],6,5,1.6);
    %figure(99),imagesc(pixelize(G,subsmpl)),colormap gray,set(gca,'xtick',[],'ytick',[]);
end

function h = gaussian(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = exp(-((((xi-x0)^2)/(2*sigma*sigma)) + (((yi-y0)^2)/(2*sigma*sigma))));
        end
    end

end

function I = pixelize(img,subsmpl)

    I = zeros(size(img).*subsmpl);
    for x=0:size(I,1)-1
        for y=0:size(I,2)-1
            I(1+x,1+y) = img(floor(1+x/subsmpl),floor(1+y/subsmpl));
        end
    end
    
end