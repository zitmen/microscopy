function crowded_field()

    subsmpl = 32;
    
    G = gaussian([51 51],35,25,5) + gaussian([51 51],25,35,5);
    subplot(1,3,1),...
        surf([1:51],[1:51],G,'EdgeAlpha',0.4),colormap cool,...
        set(gca,'xtick',[],'ytick',[]);
    
    G = gaussian([11 11],6,5,1.3) + gaussian([11 11],6,7,1.3);
    [GX,GY] = gradient(G);
    subplot(1,3,2),hold on,...
        imagesc(pixelize(G,subsmpl)),colormap gray,...
        quiver(subsmpl*([1:11]-0.5),subsmpl*([1:11]-0.5),GX,GY,'y','LineWidth',1),...  % arrows in the direction of the gradient
        plot(subsmpl*(5-0.5),subsmpl*(6-0.5),'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        plot(subsmpl*(7-0.5),subsmpl*(6-0.5),'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        set(gca,'xtick',[],'ytick',[]),axis([1 subsmpl*11 1 subsmpl*11]),hold off;
    
    %G = gaussian([11 11],6,6,1.3); % single molecule just for a comparison
    G = gaussian([11 11],5.75,5.85,1.3) + gaussian([11 11],6.3,6.4,1.3);
    subplot(1,3,3),hold on,...
        imagesc(pixelize(G,subsmpl)),colormap gray,...
        plot(subsmpl*(5.75-0.5),subsmpl*(5.85-0.5),'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        plot(subsmpl*(6.3-0.5),subsmpl*(6.4-0.5),'rx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
        set(gca,'xtick',[],'ytick',[]),axis([1 subsmpl*11 1 subsmpl*11]),hold off;

end

function h = gaussian(size,x0,y0,sigma)

    h = zeros(size);
    for xi = 1:size(1)
        for yi = 1:size(2)
            h(xi,yi) = exp(-((((xi-x0)^2)/(2*sigma*sigma)) + (((yi-y0)^2)/(2*sigma*sigma))));
        end
    end

end

function I = pixelize(img,subsmpl)

    I = zeros(size(img).*subsmpl);
    for x=0:size(I,1)-1
        for y=0:size(I,2)-1
            I(1+x,1+y) = img(floor(1+x/subsmpl),floor(1+y/subsmpl));
        end
    end
    
end