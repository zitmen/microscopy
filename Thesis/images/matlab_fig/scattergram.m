function scattergram()

    plot((5.3-0.5),(6.4-0.5),'bx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
    hold on,...
    plot((6.8-0.5),(6.3-0.5),'bx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
    plot((6-0.5),(5.3-0.5),'bx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
    plot((7.2-0.5),(7-0.5),'bx','MarkerSize',10,'LineWidth',2),...  % switch axes - plot(y,x) !!!
    set(gca,'xtick',[],'ytick',[]),axis([1 11 1 11]),axis off,hold off;
end